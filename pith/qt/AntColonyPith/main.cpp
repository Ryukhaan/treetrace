//
//  main.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 11/12/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

#include "preprocess.hpp"
//#include "./orient/orienteer.hpp"
#include "./orientation/RidgeOrienteer.hpp"
#include "./normals/bresenham.hpp"
#include "./ui/viewer.hpp"
#include "./aco/aco.hpp"

//#include "ICA.hpp"
//#include <omp.h>

using namespace cv;
using namespace std;

int radius = 300;//12;
int max_radius = 350;//50;
Mat img, gimg;
int thresh = 100;

float medianMat(Mat Input, int nVals);

const String keys =
"{help h usage ?    |       | print this message   }"
"{@input            |       | input image          }"
"{parameters        |       | parameters file (JSON)}"
"{scale s           | 0.4   | resizing factor}"
"{dx x              | 4     | Number of sub-images in x-axis}"
"{dy y              | 4     | Number of sub-images in y-axis}"
"{lambda l          | 0.875 | Fourier spectrum threshold}"
"{lsigma w          | 6.0   | Gaussian filter on detected line}"
"{gsigma u          | 2.0   | Gaussian standard deviation for local orientation}"
"{bsigma v          | 4.0   | Local window for local orientation}"
"{epsilon e         | 2.0   | Early stop criterion}"
"{iter i            | 50    | Maximum number of iterations}"
"{animated          | true  | Show ants moving}"
"{ant n             | 4     | Number of ants by line and column}"
"{block c           | 3     | Number of block clusters around each ant}"
"{omega o           | 8     | Size of each block cluster}"
"{quantizer q       | 4     | Quantizer for pixels in the preprocessed image}"
"{alpha a           | 2.0   | ACO parameter for pheromone matrix}"
"{beta b            | 1.0   | ACO parameter for desirability matrix}"
"{gamma g           | 0.07  | ACO parameter for pheromone evaporation rate}"
"{kappa k           | 0.8   | Threshold pheromone matrix for pith extraction}"
"{x                 | 0   | Threshold pheromone matrix for pith extraction}"
"{y                 | 0   | Threshold pheromone matrix for pith extraction}";



int main(int argc, char** argv)
{
    CommandLineParser parser(argc, argv, keys);
    parser.about("Ant Colony for Pith Estimation v1.1.0");
    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }
    
    // 1. Initialize timer
    double tt_tic, tt_toc;
    tt_tic = cv::getTickCount();
    
    // 2. Read file and image
    String filename = parser.get<String>(0); // Image input
    if ( filename.empty() )
    {
        return -1;
    }
    img = imread(filename, 1);
    if ( img.empty() )
    {
        cout << "Image not found or cannot be load" << endl;
        return -1;
    }
    
    // 3.1 Declare variables for parameters
    double scale;       // Resize input image
    int div_x, div_y;   // Number of sub-images in x-axis and y-axis
    double gradientsigma, blocksigma; // Gaussian standard deviation for local orientation
    
    int num_ants, num_blocs, bloc_size, quantizer;
    
    double alpha, beta, gamma;
    int max_num_iter;
    double kappa, epsilon;
    bool animated;
    
    double lambda, linesigma; // Two parameters in sawings marks removal
    
    // Added must be removed:
    int x, y;
    // 3.2 Parse parameters.json if --parameters has been provided
    bool has_parameters_file = parser.has("parameters");
    if ( has_parameters_file )
    {
        String parameters_path = parser.get<String>("parameters");
        FileStorage parameters(parameters_path, FileStorage::READ);
        gradientsigma   = parameters["Orientation"]["gradientsigma"];
        blocksigma      = parameters["Orientation"]["blocksigma"];
        linesigma       = parameters["MarksRemoval"]["sigma"];
        lambda          = parameters["MarksRemoval"]["lambda"];
        scale           = parameters["Preprocessing"]["delta"];
        div_x           = parameters["Preprocessing"]["div_x"];
        div_y           = parameters["Preprocessing"]["div_y"];
        num_ants        = parameters["ACO"]["K"];
        num_blocs       = parameters["ACO"]["n"];
        bloc_size       = parameters["ACO"]["omega"];
        quantizer       = parameters["ACO"]["m"];
        alpha           = parameters["ACO"]["alpha"];
        beta            = parameters["ACO"]["beta"];
        gamma           = parameters["ACO"]["gamma"];
        max_num_iter    = parameters["ACO"]["N"];
        kappa           = parameters["ACO"]["kappa"];
        epsilon         = parameters["ACO"]["epsilon"];
        
        int tmp         = parameters["animation"];
        animated        = (bool)tmp;
    }
    else
    {
        // Parameters of pre-processing step
        scale           = parser.get<double>("scale");
        div_x           = parser.get<int>("dx");
        div_y           = parser.get<int>("dy");
        
        // Parameters of sawing removal
        linesigma       = parser.get<double>("lsigma");
        lambda          = parser.get<double>("lambda");
        
        // Parameters of local orientation estimation
        gradientsigma   = parser.get<double>("gsigma");
        blocksigma      = parser.get<double>("bsigma");
        
        // Parameters of ACO algorithm
        num_ants        = parser.get<int>("ant");
        num_blocs       = parser.get<int>("block");
        bloc_size       = parser.get<int>("omega");
        quantizer       = parser.get<int>("quantizer");
        alpha           = parser.get<double>("alpha");
        beta            = parser.get<double>("beta");
        gamma           = parser.get<double>("gamma");
        kappa           = parser.get<double>("kappa");
        
        // Parameters of Early Stop Criteria
        epsilon         = parser.get<double>("epsilon");
        max_num_iter    = parser.get<int>("iter");
        animated        = parser.get<bool>("animated");
        
        // Added must be removed
        x = parser.get<int>("x");
        y = parser.get<int>("y");
    }
    cout << x << ":" << y << endl;
    
    if (!parser.check())
    {
        parser.printErrors();
        return -1;
    }
    
    // 5. Algorithm
    tt_tic = cv::getTickCount();

    // Instanciate orientation algorithm, early stop criteria and ACO
    aco::viewer::Viewer* __viewer = new aco::viewer::Viewer(0.5, 0);
    
    aco::orientation::RidgeOrienteer    orienteer(gradientsigma, blocksigma);
    aco::earlystop::IterAndDistanceStop earlystopper(epsilon, max_num_iter);
    aco::AntColonyOptimization* __aco = new aco::AntColonyOptimization(&orienteer, scale,
                                                                     num_ants, num_blocs, bloc_size, quantizer,
                                                                     kappa, gamma, alpha, beta,
                                                                     &earlystopper);
    /*
    __aco->setDivX(div_x);
    __aco->setDivY(div_y);
    
    tt_tic = cv::getTickCount();
    // Estimate first pith position
    __aco->initialize(img);
    // cout << static_cast<int>(num_ants*num_ants) << endl;
    for(;;)
    {
        //tt_tic = cv::getTickCount();
        __aco->update();
        
        if ( animated )
            __viewer->draw(__aco, img);
        
        if ( earlystopper.checkCriteria(__aco) )
            break;
        
        //tt_toc = cv::getTickCount();
        //cout << (tt_toc - tt_tic) / getTickFrequency() << endl;
    }

    //cout << earlystopper.getCurrentIt() << endl;
    //Mat tau = __aco->__pheromones;
    
    Point2d first_estimate = __aco->extract();
    
    tt_toc = cv::getTickCount();
    double first_timer = (tt_toc - tt_tic) / getTickFrequency();
    */
    
    Point2d first_estimate(x, y);
    // To remove before pushing
    //cout << first_timer / earlystopper.getCurrentIt() << endl;
    //return 0;
    
    //return 0;
    // Second iteration, select a sub-image of size 512x512 around the first estimation
    tt_tic = cv::getTickCount();
    int ww = 256;
    int px = first_estimate.x;
    int py = first_estimate.y;
    int ex = max(min(static_cast<int>((px-ww)), img.rows-ww), 0);
    int ey = max(min(static_cast<int>((py-ww)), img.cols-ww), 0);
    int delta = static_cast<int>(2*ww);
    Rect subRect = Rect(ex, ey, delta, delta);
    Mat simg = img(subRect);
    
    // Change parameter
    earlystopper.setEpsilon(0.5);
    earlystopper.reset();
    
    __aco->setQuantizer(2);
    __aco->setDivX(1);
    __aco->setDivY(1);

    // Estimate second pith position
    __aco->initialize(simg);
    for (;;)
    {
        __aco->update();
        
        if ( animated )
            __viewer->draw(__aco, simg);
        
        if ( earlystopper.checkCriteria(__aco) )
            break;
    }
    Point2d second_estimate = __aco->extract();
    
    // Retrieve correct position in the full image coordinate
    second_estimate.x = second_estimate.x + px - ww;
    second_estimate.y = second_estimate.y + py - ww;
    
    tt_toc = cv::getTickCount();
    double second_timer = (tt_toc - tt_tic) / getTickFrequency();
    
    if ( animated )
    {
        vector<Point2d> piths = {first_estimate, second_estimate};
        __viewer->draw(piths, img);
    }
    
    
    // Save estimation as cross on input image
    Mat OutputImage = cv::Mat::zeros(img.rows, img.cols, img.type());
    string output_img_name = filename + "output.jpg";
    img.copyTo(OutputImage);
    drawMarker(OutputImage, first_estimate, Scalar(100,50,255), MARKER_CROSS, 32, 4);
    drawMarker(OutputImage, second_estimate, Scalar(0,255,150), MARKER_CROSS, 32, 4);
    imwrite(output_img_name, OutputImage);
    
    // Save pith position and computation time for both iterations
    ofstream outputFile;
    string output_name = filename + ".csv"; //"results.csv";
    outputFile.open(output_name);
    outputFile << "1st Px" << "," << "1st Py" << ",";
    outputFile << "2nd Px" << "," << "2nd Py" << ",";
    //outputFile << "1st Time" << "," << "2nd Time" << ",";
    outputFile << "Time" << endl;
    outputFile << "randgen" << endl;
    outputFile << first_estimate.x << "," << first_estimate.y << ",";
    outputFile << second_estimate.x << "," << second_estimate.y << ",";
    //outputFile << first_timer << "," << second_timer << "," << __aco->__generator() << endl;
    outputFile << second_timer << "," << __aco->__generator() << endl;
    return 0;
}
