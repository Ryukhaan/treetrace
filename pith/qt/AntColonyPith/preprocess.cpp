//
//  preprocess.cpp
//  HoughPith
//
//  Created by Remi DECELLE on 01/10/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#include "preprocess.hpp"
#include <numeric>

namespace aco
{
    namespace utils
    {
        Mat fft2(const Mat& I, Mat& complexI)
        {
            Mat padded;                            //expand input image to optimal size
            
            /*
            int m = getOptimalDFTSize( I.rows );
            int n = getOptimalDFTSize( I.cols ); // on the border add zero values
            copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_REPLICATE);
            */
            copyMakeBorder(I, padded, 0, 0, 0, 0, BORDER_REPLICATE);
            Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
            merge(planes, 2, complexI);         // Add to the expanded another plane with zeros
            dft(complexI, complexI);            // this way the result may fit in the source matrix
            
            int cx = complexI.cols/2;
            int cy = complexI.rows/2;
            Mat q0(complexI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
            Mat q1(complexI, Rect(cx, 0, cx, cy));  // Top-Right
            Mat q2(complexI, Rect(0, cy, cx, cy));  // Bottom-Left
            Mat q3(complexI, Rect(cx, cy, cx, cy)); // Bottom-Right
            Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
            q0.copyTo(tmp);
            q3.copyTo(q0);
            tmp.copyTo(q3);
            q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
            q2.copyTo(q1);
            tmp.copyTo(q2);
            
            // compute the magnitude and switch to logarithmic scale
            // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
            split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
            magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
            Mat magI = planes[0];
            magI += Scalar::all(1);                    // switch to logarithmic scale
            log(magI, magI);
            // crop the spectrum, if it has an odd number of rows or columns
            //magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));
            
            
            normalize(magI, magI, 0, 1, NORM_MINMAX); // Transform the matrix with float values into a
            return magI;
        }

        Mat sawing_marks_removal(Mat I, double lambda, double sigma_line)
        {
            int H = I.rows;
            int W = I.cols;
            int xc = (W * 0.5 + 1);
            int yc = (H * 0.5 + 1);
            Mat X(H, W, CV_64F);
            Mat Y(H, W, CV_64F);
            for (int j = 0; j<H; j++) {
                double* xrow = X.ptr<double>(j);
                double* yrow = Y.ptr<double>(j);
                for (int i = 0; i<W; i++) {
                    xrow[i] = i - xc;
                    yrow[i] = j - yc;
                }
            }
            
            int kernel;
            
            //kernel = 2*ceil(2*sigma_fft)+1;
            I.convertTo(I, CV_32FC1);
            Mat F, complexI;
            F = fft2(I, complexI);
            
            //GaussianBlur(F, F, Size(kernel, kernel), sigma_fft);
            //xc = F.rows / 2;
            //yc = F.cols / 2;
            Mat hor = 1 - (abs(X) < 1);
            Mat ver = 1 - (abs(Y) < 1);
            hor.convertTo(hor, F.type());
            normalize(hor, hor, 0, 1, NORM_MINMAX);
            ver.convertTo(ver, F.type());
            normalize(ver, ver, 0, 1, NORM_MINMAX);
            multiply(F, ver, F);
            multiply(F, hor, F);
            
            Mat circlex, circley;
            pow(X, 2.0, circlex);
            pow(Y, 2.0, circley);
            Mat low_pass = (circley + circlex) < pow(F.cols/3.0, 2.0);
            low_pass.convertTo(low_pass, F.type());
            normalize(low_pass, low_pass, 0, 1, NORM_MINMAX);
            multiply(F, low_pass, F);
            normalize(I, I, 0, 1, NORM_MINMAX);
            
            double minVal, maxVal;
            Point minLoc, maxLoc;
            vector<Point> locs;
            //double T = 0.875;
            minMaxLoc(F, &minVal, &maxVal, &minLoc, &maxLoc);
            findNonZero(F>=lambda*maxVal, locs);
            Mat XY = Mat(locs.size(), 2, CV_32F);
            for (int i = 0; i<locs.size(); i++) {
                XY.at<float>(i,0)=locs[i].x;
                XY.at<float>(i,1)=locs[i].y;
            }
            
            // PCA
            Mat covar, mean;
            calcCovarMatrix(XY, covar, mean, COVAR_NORMAL | COVAR_ROWS);
            covar = covar / (XY.rows - 1);
            Mat eigenvalues, eigenvectors;
            eigen(covar, eigenvalues, eigenvectors);
            
            // ICA
            /*
             Mat E, D, eigenvectors, S;
             remean(XY, XY);
             whiten(XY, XY, E, D);
             runICA(XY, S, eigenvectors, XY.cols);
             cout<<eigenvectors<<endl;
             cout<<S<<endl;
             */
            
            double certainty = (eigenvalues.at<double>(0,0) - eigenvalues.at<double>(1,0)) / eigenvalues.at<double>(0,0);
            Mat res;
            if (certainty > 0.66)
            {
                double slope   = eigenvectors.at<double>(1,0) / eigenvectors.at<double>(0,0);
                double w       = max(abs(slope), 1.0);
                Mat line1      = (-w < -slope*X+Y) & (-slope*X+Y<=w);
                
                //slope       = eigenvectors.at<double>(1,1) / eigenvectors.at<double>(0,1);
                //w           = max(abs(slope), 1.0);
                //Mat line2   = (-w < -slope*X+Y) & (-slope*X+Y<=w);
                
                //Mat line = line1 | line2;
                Mat line = line1;
                line.convertTo(line, CV_32F);
                
                kernel = 2 * ceil(2 * sigma_line) + 1;
                GaussianBlur(line, line, Size(kernel, kernel), sigma_line);
                normalize(line, line, 0, 1, NORM_MINMAX);
                line = 1 - line;
                
                //imshow("thick line", line);
                
                Mat padded;
                //int m = getOptimalDFTSize(I.rows);
                //int n = getOptimalDFTSize(I.cols);
                //copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT);
                copyMakeBorder(I, padded, 0, 0, 0, 0, BORDER_CONSTANT);
                Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
                //Mat planes[] = { Mat_<float>(I), Mat::zeros(I.size(), CV_32F) };
                split(complexI, planes);
                multiply(planes[0], line, planes[0]);
                //normalize(planes[0], planes[0], 0, 1, NORM_MINMAX);
                multiply(planes[1], line, planes[1]);
                //normalize(planes[1], planes[1], 0, 1, NORM_MINMAX);
                merge(planes, 2, complexI);
                
                // Ifftshift
                int cx = complexI.cols/2;
                int cy = complexI.rows/2;
                Mat tmp;
                Mat c0(complexI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
                Mat c1(complexI, Rect(cx, 0, cx, cy));  // Top-Right
                Mat c2(complexI, Rect(0, cy, cx, cy));  // Bottom-Left
                Mat c3(complexI, Rect(cx, cy, cx, cy)); // Bottom-Right
                c0.copyTo(tmp);
                c3.copyTo(c0);
                tmp.copyTo(c3);
                c1.copyTo(tmp);                         // swap quadrant (Top-Right with Bottom-Left)
                c2.copyTo(c1);
                tmp.copyTo(c2);
                dft(complexI, res, DFT_INVERSE | DFT_REAL_OUTPUT);
                normalize(res, res, 0, 1, NORM_MINMAX);
            }
            else
            {
                I.copyTo(res);
            }
            return res;
        }
    }
}
