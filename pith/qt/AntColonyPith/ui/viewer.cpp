//
//  viewer.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 24/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "viewer.hpp"

#include "../aco/aco.hpp"

namespace aco
{
    namespace viewer
    {
        void Viewer::draw(const AntColonyOptimization *aco, const Mat &BackgroundArray)
        {
            namedWindow("ACO");
            namedWindow("Pheromones Matrix");
            namedWindow("Orientation");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            resize(DisplayArray, DisplayArray, Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows));
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            double ratio = __scale / aco->getScale();
            for (Point ant: aco->__ants) {
                drawMarker(DisplayArray, ratio * ant , Scalar(0,127,255), MARKER_CROSS, 32, 5);
            }
            imshow("ACO", DisplayArray);
            
            Mat PheromonesUI;
            normalize(aco->__pheromones, PheromonesUI, 0, 1, NORM_MINMAX);
            imshow("Pheromones Matrix", PheromonesUI);
            
            Mat OrientationUI;
            normalize(aco->__OrientationArray, OrientationUI, 0, 1, NORM_MINMAX);
            imshow("Orientation", OrientationUI);
            
            waitKey(__delay);
        };
    
        void Viewer::draw(const vector<Point2d> piths, const Mat &BackgroundArray)
        {
            namedWindow("Detected Piths");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            resize(DisplayArray, DisplayArray, Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows));
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            
            for (int i = 0; i < piths.size(); i++)
            {
                Point2d pith = piths.at(i);
                drawMarker(DisplayArray, __scale * pith , Scalar((255 * i /piths.size()),0,0), MARKER_CROSS, 64, 5);
            }
            imshow("Detected Piths", DisplayArray);
            waitKey(0);
        
        }
    }
}
