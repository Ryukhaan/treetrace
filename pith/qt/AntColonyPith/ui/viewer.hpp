//
//  viewer.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 24/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef viewer_hpp
#define viewer_hpp

#include <stdio.h>
#include <vector>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

namespace aco
{
    class AntColonyOptimization;

    namespace viewer
    {
        class Viewer
        {
        public:
            Viewer(): __scale(1.0), __delay(1) {};
            Viewer(double scale, double delay): __scale(scale), __delay(delay) {};
            virtual ~Viewer() {};
            
            virtual void draw(const AntColonyOptimization *aco, const Mat &BackgroundArray);
            virtual void draw(const vector<Point2d> piths, const Mat &BackgroundArray);
        
            void setScale(double scale)
            {
                __scale = scale;
            }
            
            void setDelay(double delay)
            {
                __delay = delay;
            }
            
            double getScale()
            {
                return __scale;
            }
            
            double getDelay()
            {
                return __delay;
            }
            
        private:
            double __scale;
            double __delay;
        };
    }
}

#endif /* viewer_hpp */
