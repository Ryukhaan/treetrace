//
//  earlystop.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "earlystop.hpp"

#include "aco.hpp"

namespace aco
{
    namespace earlystop
    {
        bool DistanceStop::checkCriteria(const AntColonyOptimization *_aco)
        {
            return _aco->__distances.at(0) <= epsilon;
        }
    
        bool IterAndDistanceStop::checkCriteria(const AntColonyOptimization *_aco)
        {
            current_it++;
            if ( current_it >= max_num_it ) return true;

            double meandis = DBL_MAX;
            int bw = 5; // look 5 iterations backward
            if (current_it >= bw)
            {
                meandis = 0;
                for (int k=current_it-bw; k<current_it; k++) {
                    meandis += _aco->__distances.at(k);
                }
                meandis = meandis / 5.0;
            }
            if (meandis <= epsilon) return true;
            return false;
        }
    }
}
