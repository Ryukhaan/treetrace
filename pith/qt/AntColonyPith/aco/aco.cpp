//
//  aco.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "aco.hpp"

using namespace std;
using namespace cv;

namespace aco
{
    AntColonyOptimization::AntColonyOptimization(orientation::Orienteer *O, double s,
                                                 int na, int nb, int bs,int q,
                                                 double k, double er, double a, double b,
                                                 earlystop::EarlyStop *S)
    {
        scale       = s;
        num_ants    = na;
        num_blocs   = nb;
        bloc_size   = bs;
        quantizer   = q;
        kappa       = k;
        evaporation_rate = er;
        alpha       = a;
        beta        = b;
        
        Orienteer       = O;
        //EarlyStopper    = S;
        
        vector<double> distances = {};
        __p = Point2d(-1, -1);
        
        // Reproductibility ?
        __generator.seed(1);
    }

    void AntColonyOptimization::initialize(const cv::Mat &InputArray)
    {
        // Initialize image
        __InputArray = Mat::zeros(InputArray.rows, InputArray.cols, InputArray.type());
        // Preprocessed image
        std::vector<cv::Mat> SubImages = preprocess(InputArray, __InputArray);
        // Compute orientation
        int H = __InputArray.rows;
        int W = __InputArray.cols;
        __OrientationArray = orientation(SubImages, H, W);
        
        // Initialize pheromones matrix
        int height = __InputArray.rows;
        int width  = __InputArray.cols;
        int hq  = floor(height / quantizer);
        int wq  = floor(width / quantizer);
        __pheromones = Mat::zeros(hq, wq, CV_32F);
        
        // Initialize ants positions
        __ants.clear();
        double delta_y = height / num_ants;
        double delta_x = width / num_ants;
        for (int i = 0; i<num_ants; i++) {
            for (int j = 0; j<num_ants; j++) {
                //int k   = i * num_ants + j;
                int ay  = floor(i*delta_y+floor(delta_y/2));
                int ax  = floor(j*delta_x+floor(delta_x/2));
                __ants.push_back(Point(ax,ay));
            }
        }
        //cout << "Fin initialisation" << endl;
        return;
    }

    std::vector<cv::Mat> AntColonyOptimization::preprocess(const cv::Mat &InputArray, cv::Mat &OutputArray)
    {
        
        // Convert into grayscale. Beware that cv::imread create BGR image (not RGB image) !
        cvtColor(InputArray, OutputArray, COLOR_BGR2GRAY);
        
        // Resizing
        resize(OutputArray, OutputArray, Size(scale * InputArray.cols, scale * InputArray.rows));
        // Padding image
        int padx = ceil(OutputArray.cols / (float)div_x) * div_x; // 4x3 subimages
        int pady = ceil(OutputArray.rows / (float)div_y) * div_y; // 4x3 subimages
        copyMakeBorder(OutputArray, OutputArray, 0, pady - OutputArray.rows, 0, padx - OutputArray.cols, BORDER_REPLICATE);
        
        // Retrieve orientation with Jain filter and Sawing Marks removal
        OutputArray.convertTo(OutputArray, CV_32F);
        
        // Remove sawings marks on subimage
        int sW = OutputArray.cols / div_x;
        int sH = OutputArray.rows / div_y;
        std::vector<cv::Mat> img_array;
        for (int i = 0; i<div_y; i++)
        {
            for (int j = 0; j<div_x; j++)
            {
                cv::Mat SubArray;
                OutputArray(cv::Range(i*sH,(i+1)*sH-1), cv::Range(j*sW, (j+1)*sW-1)).copyTo(SubArray);
                SubArray = utils::sawing_marks_removal(SubArray, 0.875, 6.0); //lambda, linesigma); // change lambda and sigma
                SubArray.convertTo(SubArray, CV_32F);
                img_array.push_back(SubArray);
            }
        }
        return img_array;
    };

    cv::Mat AntColonyOptimization::orientation(const std::vector<cv::Mat> InputArrays, int H, int W)
    {
        int sW = W / div_x;
        int sH = H / div_y;
        
        // Compute Orientation for each subimage
        std::vector<cv::Mat> orientation;
        for (int i = 0; i < InputArrays.size(); i++)
        {
            cv::Mat SubArray      = InputArrays.at(i);
            orientation.push_back(Orienteer->estimate(SubArray));
        }
        
        // 3. Recreate a full image with orientation
        cv::Mat full_filter_img     = Mat(H, W, InputArrays[0].type(), DBL_MIN);
        cv::Mat full_orientation    = Mat(H, W, InputArrays[0].type(), DBL_MIN);
        if (div_x != 1 && div_y != 1)
        {
            for (int i = 0; i < InputArrays.size(); i++)
            {
                int dx = (i % div_x) * sW;
                int dy = (i / div_x) * sH;
                cv::Mat SubArray = orientation.at(i);
                SubArray.copyTo(full_orientation(cv::Rect(dx, dy, sW-1, sH-1)));
            }
        }
        else
        {
            orientation.at(0).copyTo(full_orientation);
        }
        return full_orientation;
    };

    void AntColonyOptimization::update()
    {
        int delta = floor(num_blocs * bloc_size * 0.5);
        int height = __InputArray.rows;
        int width  = __InputArray.cols;
        int hq  = floor(height / quantizer);
        int wq  = floor(width / quantizer);
        
        Mat tau = Mat::zeros(hq, wq, CV_32F);
        
    #ifdef _OPENMP
        int nmax = omp_get_max_threads();
        cout << nmax << endl;
    #endif
    #pragma omp parallel num_thread(nmax)
        for (int i = 0; i<__ants.size(); i++)
        {
            Point ant = __ants.at(i);
            vector<Point3d> linepos;
            for (int n = 0; n<num_blocs; n++) {
                for (int m = 0; m<num_blocs; m++) {
                    int dy = ant.y - delta + n*bloc_size;
                    int dx = ant.x - delta + m*bloc_size;
                    Mat sub_orient = __OrientationArray(Rect(dx, dy, bloc_size, bloc_size));
                    //orient(Rect(dx, dy, bloc_size, bloc_size));
                    float theta = static_cast<float>(mean(sub_orient)[0]);
                    int cx = ant.x - delta + m * bloc_size + bloc_size/2;
                    int cy = ant.y - delta + n * bloc_size + bloc_size/2;
                    cx = floor(cx/quantizer);
                    cy = floor(cy/quantizer);
                    linepos.push_back(Point3d(cx, cy, theta));
                }
            }
            Mat A = accumulate_normals(linepos, hq, wq);
            A = A > 0;
            A &= 0x1;
            A.convertTo(A, tau.type());
            
            tau = tau + A;
            
            // Compute Distance Transform (DT)
            Mat mask = Mat::zeros(hq, wq, CV_8U) + Scalar(255);
            Mat dist = Mat::zeros(hq, wq, CV_32F) + Scalar(255);
            int aqx = floor(ant.x / quantizer);
            int aqy = floor(ant.y / quantizer);
            mask.at<uint8_t>(aqy, aqx) = 0;
            distanceTransform(mask, dist, DIST_L2, 3);
            normalize(dist, dist, 0.0, 1.0, NORM_MINMAX);
            pow(dist+Scalar(1), -1.0, dist);
            
            // Compute Pheromones Probabilites (PP)
            Mat Q, proba;
            double s = cv::sum( __pheromones )[0];
            if (s == 0)
                proba = Mat::ones(hq, wq, CV_32F) / (hq * wq);
            else
                proba = __pheromones / s;
            
            // Power DT and PP
            pow(proba, alpha, proba);
            pow(dist, beta, dist);
            multiply(proba, dist, Q);
            
            // Normalize to have probabilites
            proba = Q / (cv::sum( Q )[0]);
            
            // Select a col
            Mat pcol;
            cv::reduce(proba, pcol, 0, cv::REDUCE_SUM);
            std::vector<float> _col(pcol.rows);
            pcol.row(0).copyTo(_col);
            cv::normalize(_col, _col, 0, 1, cv::NORM_MINMAX);
            std::discrete_distribution<int> cdistribution (_col.begin(), _col.end());
            int cnumber = cdistribution(__generator);
            // Select a row
            Mat prow;
            std::vector<float> _row(pcol.rows);
            proba.col(cnumber).copyTo(_row);
            cv::normalize(_row, _row, 0, 1, cv::NORM_MINMAX);
            std::discrete_distribution<int> rdistribution (_row.begin(), _row.end());
            int rnumber = rdistribution(__generator);
            
            int py = rnumber;
            int px = cnumber;
            
            // Update ants position
            py = quantizer * py;
            px = quantizer * px;
            py = max(min(py, height - delta - 1), delta+1);
            px = max(min(px, width  - delta - 1), delta+1);
            
            __ants.at(i).x = px;
            __ants.at(i).y = py;
        }
        
        __pheromones = (1-evaporation_rate) * __pheromones + tau;
        
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(__pheromones, &minVal, &maxVal, &minLoc, &maxLoc);
        Mat binary = __pheromones>=kappa*maxVal;
        Moments m = moments(binary);
        Point2d q(m.m10/m.m00, m.m01/m.m00);
        
        __distances.push_back(sqrt((__p.x-q.x)*(__p.x-q.x) + (__p.y-q.y)*(__p.y-q.y)));
        __p = q;
        
        //if ( EarlyStopper->checkCriteria(this) )
        //    return;
    }

    Point2d AntColonyOptimization::extract()
    {
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(__pheromones, &minVal, &maxVal, &minLoc, &maxLoc);
        Mat binary = __pheromones>=kappa*maxVal;
        Moments m = moments(binary);
        Point2d r(m.m10/m.m00, m.m01/m.m00);
        r.y = quantizer * r.y / scale;
        r.x = quantizer * r.x / scale;
        return r;
    }
};
