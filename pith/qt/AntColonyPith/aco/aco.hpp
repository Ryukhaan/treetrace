//
//  aco.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef aco_hpp
#define aco_hpp

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

#include "earlystop.hpp"
#include "../orientation/orienteer.hpp"
#include "../normals/bresenham.hpp"

namespace aco
{
    //template <typename T, typename S>
    class AntColonyOptimization
    {
    public:
        AntColonyOptimization() {};
        AntColonyOptimization(orientation::Orienteer *Orienteer,
                              double scale,
                              int num_ants, int num_blocs, int bloc_size,int quantizer,
                              double kappa, double evaporation_rate, double alpha, double beta,
                              earlystop::EarlyStop *Stopper);
        virtual ~AntColonyOptimization() {};
        //virtual Point2d compute(const cv::Mat &InputArray);
        
        virtual void update();
        virtual Point2d extract();
        
        std::vector<cv::Mat> preprocess(const cv::Mat &InputArray, cv::Mat &OutputImage);
        cv::Mat orientation(const std::vector<cv::Mat> InputArrays, int H, int W);
        void initialize(const cv::Mat &InputArray);
        
        //Point2d applyACO(const cv::Mat &InputArray, const cv::Mat &OrientationArray);
    
    public:
        void setScale(double s)
        {
            scale = s;
        }
        
        void setNumAnts(int n)
        {
            num_ants = n;
        }
        
        void setNumBlocs(int b)
        {
            num_blocs = b;
        }
        
        void setQuantizer(int q)
        {
            quantizer = q;
        }
        
        void setDivX(int x)
        {
            div_x = x;
        }
        
        void setDivY(int y)
        {
            div_y = y;
        }
        
    public:
        
        double getScale() const
        {
            return scale;
        }
        
        double getNumAnts() const
        {
            return num_ants;
        }
        
    protected:
        double scale;
        int num_ants;
        int num_blocs;
        int bloc_size;
        int quantizer;
        double kappa;
        double evaporation_rate;
        double alpha;
        double beta;
        
        int div_x;
        int div_y;
        
        orientation::Orienteer  *Orienteer;
        //earlystop::EarlyStop    *EarlyStopper;
        
        Point2d __p;
    
    public:
        std::default_random_engine __generator;
    
    public:
        vector<Point> __ants;
        Mat __pheromones;
        vector<double> __distances;
        
        Mat __InputArray;
        Mat __OrientationArray;
    };
}

#endif /* aco_hpp */
