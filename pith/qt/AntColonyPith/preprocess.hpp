//
//  preprocess.hpp
//  HoughPith
//
//  Created by Remi DECELLE on 01/10/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//

#ifndef preprocess_hpp
#define preprocess_hpp

#include <stdio.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

//#include "ICA.hpp"

using namespace cv;
using namespace std;

namespace aco
{
    namespace utils
    {
        Mat fft2(const Mat& I, Mat& complexI);
        Mat sawing_marks_removal(Mat I, double lambda, double sigma_line);
    }
}

#endif /* preprocess_hpp */
