//
//  RidgeOrienteer.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "RidgeOrienteer.hpp"

namespace aco
{
    namespace orientation
    {
        Mat RidgeOrienteer::estimate(const Mat& im)
        {
            cv::Mat grad_x, grad_y;
            int sze = 6 * round(gradientsigma);
            
            if (sze % 2 == 0) {
                sze++;
            }
            
            // Define Gaussian kernel
            cv::Mat gaussKernelX = cv::getGaussianKernel(sze, gradientsigma, constants::DDEPTH);
            cv::Mat gaussKernelY = cv::getGaussianKernel(sze, gradientsigma, constants::DDEPTH);
            cv::Mat gaussKernel = gaussKernelX * gaussKernelY.t();
            
            // Peform Gaussian filtering
            cv::Mat fx, fy;
            cv::Mat kernelx = (cv::Mat_<float>(1, 3) << -0.5, 0, 0.5);
            cv::Mat kernely = (cv::Mat_<float>(3, 1) << -0.5, 0, 0.5);
            cv::filter2D(gaussKernel, fx, -1, kernelx);
            cv::filter2D(gaussKernel, fy, -1, kernely);
            
            utils::gradient(gaussKernel, fx, fy); // Gradient of Gaussian
            
            
            grad_x = cv::Mat::zeros(im.rows, im.cols, constants::DDEPTH);
            grad_y = cv::Mat::zeros(im.rows, im.cols, constants::DDEPTH);
            
            // Pourquoi j'ai besoin d'effectuer une operation sur im pour pouvoir appliquer filter2D ?
            //
            //cv::normalize(im, im, 0, 1, NORM_MINMAX);
            cv::filter2D(im, grad_x, -1, fx); // Gradient of the image in x
            cv::filter2D(im, grad_y, -1, fy); // Gradient of the image in y

            cv::Mat grad_xx, grad_xy, grad_yy;
            cv::multiply(grad_x, grad_x, grad_xx);
            cv::multiply(grad_x, grad_y, grad_xy);
            cv::multiply(grad_y, grad_y, grad_yy);
            
            // Now smooth the covariance data to perform a weighted summation of the data
            int sze2 = 6 * round(blocksigma);
            
            if (sze2 % 2 == 0) {
                sze2++;
            }
            
            cv::Mat gaussKernelX2 = cv::getGaussianKernel(sze2, blocksigma, constants::DDEPTH);
            cv::Mat gaussKernelY2 = cv::getGaussianKernel(sze2, blocksigma, constants::DDEPTH);
            cv::Mat gaussKernel2 = gaussKernelX2 * gaussKernelY2.t();
            
            cv::filter2D(grad_xx, grad_xx, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            cv::filter2D(grad_xy, grad_xy, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            cv::filter2D(grad_yy, grad_yy, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            
            grad_xy *= 2;
            
            // Analytic solution of principal direction
            cv::Mat denom, sin2theta, cos2theta;
            
            cv::Mat G1, G2, G3;
            cv::multiply(grad_xy, grad_xy, G1);
            G2 = grad_xx - grad_yy;
            cv::multiply(G2, G2, G2);
            
            G3 = G1 + G2;
            cv::sqrt(G3, denom);
            
            cv::divide(grad_xy, denom, sin2theta);
            cv::divide(grad_xx - grad_yy, denom, cos2theta);
            
            
            int sze3 = 6 * round(smoothsigma);
            
            if (sze3 % 2 == 0) {
                sze3 += 1;
            }
            
            if (smoothsigma > 0)
            {
                cv::Mat gaussKernelX3 = cv::getGaussianKernel(sze3, smoothsigma, constants::DDEPTH);
                cv::Mat gaussKernelY3 = cv::getGaussianKernel(sze3, smoothsigma, constants::DDEPTH);
                cv::Mat gaussKernel3 = gaussKernelX3 * gaussKernelY3.t();
                
                cv::filter2D(cos2theta, cos2theta, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
                cv::filter2D(sin2theta, sin2theta, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            }
            
            sin2theta.convertTo(sin2theta, constants::DDEPTH);
            cos2theta.convertTo(cos2theta, constants::DDEPTH);
            cv::Mat orientim = cv::Mat::zeros(sin2theta.rows, sin2theta.cols, constants::DDEPTH);
            
            // Pointer access more effective than Mat.at<T>()
            for (int i = 0; i < sin2theta.rows; i++) {
                const float *sin2theta_i = sin2theta.ptr<float>(i);
                const float *cos2theta_i = cos2theta.ptr<float>(i);
                float *orientim_i = orientim.ptr<float>(i);
                for (int j = 0; j < sin2theta.cols; j++) {
                    orientim_i[j] = (std::atan2(sin2theta_i[j], cos2theta_i[j])) / 2;
                }
            }
            return orientim;
        }
    }
}
