//
//  orienteer.hpp
//  
//
//  Created by Remi DECELLE on 13/01/2020.
//

#ifndef orienteer_hpp
#define orienteer_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "basic.hpp"
#include "../preprocess.hpp"

namespace aco
{
    namespace orientation
    {
        class Orienteer
        {
        public:
            Orienteer() {};
            virtual ~Orienteer() {};
            virtual Mat estimate(const Mat &InputArray)
            {
                return Mat::zeros(InputArray.rows, InputArray.cols, InputArray.type());
            }
        };
    }
}
#endif /* orienteer_hpp */
