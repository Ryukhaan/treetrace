//
//  basic.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "basic.hpp"

namespace aco
{
    namespace utils
    {
        void gradient(cv::Mat image, cv::Mat xGradient, cv::Mat yGradient)
        {
            xGradient = cv::Mat::zeros(image.rows, image.cols, constants::DDEPTH);
            yGradient = cv::Mat::zeros(image.rows, image.cols, constants::DDEPTH);
            
            // Pointer access more effective than Mat.at<T>()
            for (int i = 1; i < image.rows - 1; i++) {
                const float *image_i = image.ptr<float>(i);
                float *xGradient_i = xGradient.ptr<float>(i);
                float *yGradient_i = yGradient.ptr<float>(i);
                for (int j = 1; j < image.cols - 1; j++) {
                    float xPixel1 = image_i[j - 1];
                    float xPixel2 = image_i[j + 1];
                    
                    float yPixel1 = image.at<float>(i - 1, j);
                    float yPixel2 = image.at<float>(i + 1, j);
                    
                    float xGrad;
                    float yGrad;
                    
                    if (j == 0) {
                        xPixel1 = image_i[j];
                        xGrad = xPixel2 - xPixel1;
                    } else if (j == image.cols - 1) {
                        xPixel2 = image_i[j];
                        xGrad = xPixel2 - xPixel1;
                    } else {
                        xGrad = 0.5 * (xPixel2 - xPixel1);
                    }
                    
                    if (i == 0) {
                        yPixel1 = image_i[j];
                        yGrad = yPixel2 - yPixel1;
                    } else if (i == image.rows - 1) {
                        yPixel2 = image_i[j];
                        yGrad = yPixel2 - yPixel1;
                    } else {
                        yGrad = 0.5 * (yPixel2 - yPixel1);
                    }
                    
                    xGradient_i[j] = xGrad;
                    yGradient_i[j] = yGrad;
                }
            }
        }
    }
}
