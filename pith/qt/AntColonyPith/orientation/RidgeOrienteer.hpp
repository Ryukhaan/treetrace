//
//  RidgeOrienteer.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef RidgeOrienteer_hpp
#define RidgeOrienteer_hpp

#include <stdio.h>
#include "orienteer.hpp"
#include "basic.hpp"

namespace aco
{
    namespace orientation
    {
        class RidgeOrienteer : public Orienteer
        {
        public:
            RidgeOrienteer(double grad, double block):
                gradientsigma(grad), blocksigma(block), smoothsigma(0.0) {};
            RidgeOrienteer(double grad, double block, double smooth):
                gradientsigma(grad), blocksigma(block), smoothsigma(smooth) {};
            virtual ~RidgeOrienteer() {};
            virtual Mat estimate(const Mat &InputArray);
            
        protected:
            double gradientsigma;
            double blocksigma;
            double smoothsigma;
        };
    }
}
#endif /* RidgeOrienteer_hpp */
