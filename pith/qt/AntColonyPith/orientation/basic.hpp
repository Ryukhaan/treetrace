//
//  basic.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef basic_hpp
#define basic_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace aco
{
    namespace constants
    {
        static const int DDEPTH = CV_32FC1;
    }

    namespace utils
    {
        void gradient(cv::Mat image, cv::Mat xGradient, cv::Mat yGradient);
    }
}

#endif /* basic_hpp */
