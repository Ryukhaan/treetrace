//
//  bresenham.cpp
//  
//
//  Created by Remi DECELLE on 10/07/2020.
//

#include "bresenham.hpp"


Mat accumulate_normals(const vector<Point3d> orient, const int h, const int w)
{
    // point 3D:
    // x = x position
    // y = y position
    // z = orientation at (x,y)
    Mat A = Mat::zeros(h, w, CV_32F);
    for (int k=0; k<orient.size(); k++) {
        int i = orient.at(k).x;
        int j = orient.at(k).y;
        //double a = static_cast<double>(tan(orient.at<float>(j,i)));
        double a = tan(orient.at(k).z);
        auto f = [](const int x, const float a, const int cx, const int cy) { return a*(x-cx) + cy;};
        auto g = [](const int y, const float a, const int cx, const int cy) { return (y-cy)/a + cx;};
        Mat x(4, 1, CV_64F, {-1,-1,-1,-1});
        Mat y(4, 1, CV_64F, {-1,-1,-1,-1});
        
        if (abs(a) > 0) {
            x.at<double>(0,0) = g(0, a, i, j);
            y.at<double>(0,0) = 0;
            x.at<double>(2,0) = g(h-1, a, i, j);
            y.at<double>(2,0) = h-1;
        }
        if (a < 1e18) {
            x.at<double>(1,0) = 0;
            y.at<double>(1,0) = f(0, a, i, j);
            x.at<double>(3,0) = w-1;
            y.at<double>(3,0) = f(w-1, a, i, j);
        }
        Point starter, ender;
        Mat P = (x >= 0) & (x < w) & (y >= 0) & (y < h);
        //P &= 0x1;
        vector<int> p;
        for (int m=0; m<P.rows; m++)
            //if (P.at<bool>(m,0)) p.push_back(m);
            if (P.at<uint8_t>(m,0)) p.push_back(m);
                
        
        if (p.size() < 2) continue;
        
        starter = Point(x.at<double>(p[0],0), y.at<double>(p[0],0));
        ender   = Point(x.at<double>(p[1],0), y.at<double>(p[1],0));
        
        LineIterator it(A, starter, ender);
        for (int k = 0; k<it.count; k++, ++it)
            A.at<float>(it.pos())++;
    }
    return A;
}
