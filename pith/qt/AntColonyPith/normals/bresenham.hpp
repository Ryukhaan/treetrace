//
//  bresenham.hpp
//  
//
//  Created by Remi DECELLE on 10/07/2020.
//

#ifndef bresenham_hpp
#define bresenham_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

Mat accumulate_normals(const vector<Point3d> orient, const int h, const int w);

#endif /* bresenham_hpp */
