import os
import sys
import time

import numpy as np
import math
import cv2 as cv

import skimage.io as io

import matplotlib.image as img
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.animation import FuncAnimation

import accumulator
 
# Parameters
default_parameters = {
	'scale': 0.4,
	'num_ants': 4,
	'cluster_size': 3,
	'bloc_size': 8,
	'gamma': 0.07,
	'quantizer': 5,
	'alpha': 2.0,
	'beta': 1.0,
	'max_num_iter': 50,
	'early_stop_thresh': 2,
	'early_stop_count': 5,
	'retrieve_point': 0.8
}

class ACO(object):
	'''docstring for ACO'''
	def __init__(self,
				image 	 = None,
				orientim = None,
				arg 	 = default_parameters):
		super(ACO, self).__init__()
		self.image			= image
		self.orientim 		= orientim
		self.parameters 	= arg
		if self.image != None:
			self.__initialize()


	def reset(self):
		self.__initialize()

	def __initialize(self):
		self.total_num_ants 		= int(self.parameters['num_ants']**2)
		self.distances 				= np.zeros((self.parameters['max_num_iter'], 1))
		self.old_px 				= 0
		self.old_py 				= 0

		W, H 				= self.image.shape
		self.image_height 	= H
		self.image_width 	= W
		self.delta 			= math.ceil(self.parameters['cluster_size'] * self.parameters['bloc_size'] * 0.5)
		self.__initialize_ants()
		self.__initialize_tau()

	def __initialize_ants(self):
		hg = self.image_height / self.parameters['num_ants'] 
		wg = self.image_width / self.parameters['num_ants']

		self.ants = np.zeros(self.total_num_ants, dtype=[
							('position',	float, 	2),
							('size', 		float, 	1),
							('color', 		float, 	4)])
		self.ants['size']  = 60 * np.ones((self.total_num_ants))
		viridis = cm.get_cmap('plasma', self.total_num_ants)
		self.ants['color'] = viridis.colors
		for i in range(self.parameters['num_ants']):
			for j in range(self.parameters['num_ants']):
				k = i * self.parameters['num_ants'] + j
				self.ants['position'][k, 0] = i * hg + math.ceil(hg*0.5)
				self.ants['position'][k, 1] = j * wg + math.ceil(wg*0.5)

	def __initialize_tau(self):
		self.tau_height 	= math.ceil(self.image_width / self.parameters['quantizer'])
		self.tau_width 		= math.ceil(self.image_height / self.parameters['quantizer'])
		self.tau 			= np.random.uniform(0.0, 1.0, size=(self.tau_height, self.tau_width))

	#def __initialize_ui(self):
	#	global fig
	#	global axis
	#	fig.canvas.mpl_connect('button_press_event', self.on_click)
	#	sca = axis.scatter(self.ants['position'][:,0], self.ants['position'][:,1],
	#		s = self.ants['size'], marker='1',
	#		edgecolors=self.ants['color'], facecolors=self.ants['color'])

	def run(self):
		# Process each iteration
		for it in range(self.parameters['max_num_iter']):

			# Move ants and check early stopping
			if self.__step(it):
				break

		# Retrieve pith estimation (in image dimension)
		return self.__retrieve(back_to_image = True)

	def step(self, frame_number):
		if frame_number < self.parameters['max_num_iter']:
			self.__step(frame_number)

	def __step(self, it):
		# Initialize Rho to zeros
		self.rho = np.zeros((self.tau_height, self.tau_width))

		# Move each ant
		for ant in range(self.total_num_ants):
			self.__move(ant)

		# Update pheromones matrix and others stuffs
		self.__update(it, self.rho)

		# Check if early stop is needed
		return self.__check_early_stop(it)

	def __move(self, kth):
		ax 	= self.ants['position'][kth, 0]
		ay  = self.ants['position'][kth, 1]

		ant_orient = np.zeros((self.tau_height, self.tau_width))
		ant_centre = np.zeros((self.tau_height, self.tau_width))

		# For each block, retrieve block's centre and block's orientation
		for ix in range(self.parameters['cluster_size']):
			for iy in range(self.parameters['cluster_size']):
				cy 		= math.floor(ay-self.delta+iy*self.parameters['bloc_size'])
				cx 		= math.floor(ax-self.delta+ix*self.parameters['bloc_size'])
				theta 	= np.median( \
							self.orientim[cy:cy+self.parameters['bloc_size'], \
							 		 cx:cx+self.parameters['bloc_size']])
				cx = ax - self.delta + ix * self.parameters['bloc_size'] - self.parameters['bloc_size'] * 0.5
				cy = ay - self.delta + iy * self.parameters['bloc_size'] - self.parameters['bloc_size'] * 0.5
				cx = math.ceil(cx / self.parameters['quantizer'])
				cy = math.ceil(cy / self.parameters['quantizer'])
				ant_centre[cy, cx] = 1
				ant_orient[cy, cx] = theta

		# Deposit pheromone in rho
		A = self.__deposit(ant_centre, ant_orient)
		self.rho = self.rho + A

		# Compute desirability and probabilistic matrix
		desirability 	= self.__desirability(ax, ay)
		probabilistic 	= self.__probabilistic(desirability)

		# Random choice
		new_position 	= self.__random_selection(probabilistic) 

		# Update ant's position
		self.__update_ant(kth, new_position)


	def __desirability(self, ax, ay):
		bw = np.ones((self.tau_height, self.tau_width), dtype=np.uint8)
		bw[math.ceil(ay / self.parameters['quantizer']), math.ceil(ax / self.parameters['quantizer'])] = 0
		dist = cv.distanceTransform(bw, cv.DIST_L2, 3)
		return 1. / (dist + 1.)

	def __probabilistic(self, desirability):
		tau = np.power(self.tau, 		self.parameters['alpha'])
		eta = np.power(desirability, 	self.parameters['beta'])
		P 	= np.multiply(tau, eta)
		return P / np.sum(P)

	def __random_selection(self, probabilistic):
		tau_size = self.tau_height * self.tau_width
		pp = np.random.choice(tau_size, 1, p=np.reshape(probabilistic, (tau_size)))
		px = int(pp[0] % self.tau_width)
		py = math.floor(pp[0] / self.tau_width)
		return (px, py)

	def __deposit(self, centre, orient):
		return accumulator.deposit(centre, orient)


	def __update_ant(self, kth, new_position):
		px = new_position[0] * self.parameters['quantizer']
		py = new_position[1] * self.parameters['quantizer']
		self.ants['position'][kth, 0] = max(min(px, self.image_width - self.delta), self.delta)
		self.ants['position'][kth, 1] = max(min(py, self.image_height - self.delta), self.delta)	

	def __update(self, it, rho):
		# Update pheromones matrix
		self.tau = (1. - self.parameters['gamma']) * self.tau + rho

		# Update distance between old pith estimation and current pith estimation
		px, py 				= self.__retrieve()
		dx 					= (self.old_px - px)**2
		dy 					= (self.old_py - py)**2
		self.distances[it] 	= dx + dy
		self.old_px 		= px
		self.old_py 		= py

	def __check_early_stop(self, it):
		# Retrieve the last distances
		current_distances = self.distances[it:it+self.parameters['early_stop_count'], :]

		# Check if iteration_number is above an integer (early_stop_count)
		cond_it 	= it > self.parameters['early_stop_count']

		# Check if mean distances is below a threshold (early_stop_thresh)
		cond_acc	= np.mean(current_distances) < self.parameters['early_stop_thresh']

		return (cond_it and cond_acc)


	def __retrieve(self, back_to_image = False):
		# Retrieve the pith according to pheromones matrix
		
		# Get indices with pheromone's value above a threshold (according to maximum value)
		M 		= np.amax(self.tau)
		indexes = np.where(self.tau >= M*self.parameters['retrieve_point'])
		coord 	= list(zip(indexes[0], indexes[1]))

		# Compute barycentre
		px = np.mean(indexes[0])
		py = np.mean(indexes[1])

		# Convert back to image dimension
		if back_to_image:
			px 		= self.parameters['quantizer'] * px / self.parameters['scale']
			py 		= self.parameters['quantizer'] * py / self.parameters['scale']

		return px, py

	def estimate(self):
		return self.__retrieve(back_to_image = True)



