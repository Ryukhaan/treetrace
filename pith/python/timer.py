import os
import sys
import time


class Timer(object):
	'''docstring for Timer'''

	default_msg = 'It took {time} seconds to complete {step}'

	def __init__(self):
		super(Timer, self).__init__()
		self.start  	= 0.0
		self.end 		= 0.0
		self.runtime 	= 0.0
		self.on_paused  = True

	def tic(self):
		self.start 		= time.time()
		self.on_paused  = False

	def toc(self):
		self.end 		= time.time()
		self.runtime	= self.end - self.start
		self.on_paused 	= True

	def pause(self):
		if self.on_paused:
			self.tic()
			self.on_paused = False
		else:
			self.toc()
			self.runtime = self.runtime + (self.end - self.start)


	def print(self, msg=default_msg, step='this step'):
		print(msg.format(time=self.runtime, step=step))
		