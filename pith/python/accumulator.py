import numpy as np
import cv2 as cv
from skimage.draw import line

def deposit(bimg, oimg):
	h, w 	= bimg.shape
	A 		= np.zeros(bimg.shape)
	idxs 	= cv.findNonZero(bimg)

	#cv.imshow('bimg', cv.normalize(bimg, None, 0, 255, cv.NORM_MINMAX, cv.CV_8UC1))
	#cv.waitKey(0)

	rows 		= idxs[:,:,1]
	cols 		= idxs[:,:,0]
	for i in range(len(cols)):
		row = rows[i]
		col = cols[i]
		theta = oimg[row, col]
		a = np.tan(theta)
		X = np.zeros((4,1), dtype=np.uint16)
		Y = np.zeros((4,1), dtype=np.uint16)
		if abs(a) > 0:
			X[0] = a * (0 - col) + row
			Y[0] = 0
			X[2] = a * (h - 1 - col) + row
			Y[2] = h - 1
		if a < 1e18:
			X[1] = 0
			Y[1] = (0 - row) / a + col
			X[3] = w - 1
			Y[3] = (w - 1 - row) / a + col

		#X = np.array(X, dtype=np.uint16)
		#Y = np.array(Y, dtype=np.uint16)

		PX = (np.logical_and(X >= 0, X < w))
		PY = (np.logical_and(Y >= 0, Y < h))
		X = X[np.logical_and(PX,PY)]
		Y = Y[np.logical_and(PX,PY)]

		if len(X) < 2 or len(Y) < 2:
			continue

		rr, cc = line(X[0], Y[0], X[1], Y[1])
		tmp = np.zeros(bimg.shape)
		tmp[cc, rr] = 1
		A = A + tmp
	return A