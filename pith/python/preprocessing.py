import numpy as np
import cv2 as cv


def sawing_marks_removal(img, sigma):
	if len(img.shape) == 3:
		I = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
	else:
		I = img

	[h, w] 	= I.shape
	xc 		= np.ceil(w / 2 + 1)
	yc 		= np.ceil(h / 2 + 1)
	x 		= np.linspace(0, w, w)
	y 		= np.linspace(0, h, h)
	xx, yy 	= np.meshgrid(x, y)

	dft 		= cv.dft(img, flags = cv.DFT_COMPLEX_OUTPUT)
	dft_shift 	= np.fft.fftshift(dft)
	F 			= np.log(cv.magnitude(dft_shift[:,:,0],dft_shift[:,:,1])+1)

	low_pass_filter = (np.power((xx-xc),2) + np.power((yy-yc),2)) > np.power((h/3),2)
	F 				= np.multiply(F, low_pass_filter)

	ind 	= np.unravel_index(np.argmax(F, axis=None), F.shape)
	F[ind] 	= 0

	T 		= 0.8
	M 		= np.amax(np.amax(F, axis=2), axis=2)
	XY 		= cv.findNonZero(F >= T*M)
	cov 	= np.cov(XY, rowvar=False)
	_, w 	= np.linalg.eig(cov)

	slope1 	= w[1,0] / w[0,0]
	slope2 	= w[1,1] / w[0,1]
	w 		= max(1, abs(slope1))
	line1 	= np.logical_and(-w < -slope1 * xx + yy, -slope1 * xx + yy <= w)
	w 		= max(1, abs(slope2))
	line2	= np.logical_and(-w < -slope2 * xx + yy, -slope2 * xx + yy <= w)

	line 	= np.logical_or(line1, line2)
	line 	= cv.GaussianBlur(line, (2*ceil(2*sigma)+1,2*ceil(2*sigma)+1), sigma)
	line 	= cv.normalize(line, None, 0.0, 1.0, cv.NORM_MINMAX, cv.CV_32F)
	line 	= 1 - line

	dft_shift 	= np.multiply(dft_shift, line)

	idft_shift 	= np.fft.ifft2(np.fft.ifftshift(dft_shift))
	J = idft_shift.real
	J = cv.normalize(J, None, 0.0, 1.0, cv.NORM_MINMAX, cv.CV_32F)
	cv.imshow('Sawing', J)
	return J