import os
import sys

import numpy as np
import math
import cv2 as cv

import skimage.io as io

import matplotlib.image as img
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.animation import FuncAnimation

import ACO
import timer
from ridgeorient import ridge_orient
from preprocessing import sawing_marks_removal

default_ui_parameters = {
	'animation': True,
	'image': None,
	'speed': 100
}
pause = False

if __name__ == '__main__':
	#===============#
	#	Initialize 	#
	#===============#
	chrono 	= timer.Timer()
	colony 	= ACO.ACO()

	# UI update
	def update(frame_number):
		global colony
		if frame_number < ACO.default_parameters['max_num_iter']:
			colony.step(frame_number)
			sca.set_offsets(np.array([colony.ants['position'][:,1], colony.ants['position'][:,0]]).T)
			cv.imshow('Pheronomes', cv.normalize(colony.tau, None, 0, 255, cv.NORM_MINMAX, cv.CV_8UC1))
			cv.moveWindow('Pheronomes', 800, 10)
			cv.waitKey(1)

	#===================#
	#	Read Image 		#
	#===================#
	chrono.tic()
	if len(sys.argv) > 1:
		filepath = sys.argv[1]
	else:
		filepath = os.path.join(os.getcwd(), 'real.jpg')
	img 	= io.imread(filepath, plugin='matplotlib')
	img 	= np.array(img)
	uiimg 	= img
	try:
		[W, H, C] = img.shape
	except:
		[W, H] = img.shape
		C = 1
	scale 		= ACO.default_parameters['scale']
	uiimg 		= cv.resize(uiimg, 	(int(scale * H), int(scale * W)), cv.INTER_CUBIC)

	chrono.toc()
	chrono.print(step='Reading')

	# UI figure and axis
	ants_fig = plt.figure(1)
	ants_fig.canvas.manager
	ants_ax 	= ants_fig.add_axes([0, 0, 1, 1], frameon=False)
	if C==1:
		ants_ax.imshow(uiimg, cmap='gray')
	else:
		ants_ax.imshow(uiimg)

	#===================#
	#	Preprocessing	#
	#===================#
	chrono.tic()
	# Convert into grayscale
	if C == 3:
		img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
	if C == 4:
		img = cv.cvtColor(img[:,:,:-1], cv.COLOR_BGR2GRAY)
	# Downsampling
	gray 		= cv.resize(img, 	(int(scale * H), int(scale * W)), cv.INTER_CUBIC)
	gray 		= cv.normalize(gray, None, 0, 255, cv.NORM_MINMAX, cv.CV_32FC1)
	# Compute Tree Ring Local Orientations
	orientim 	= ridge_orient(gray, 2.0, 4.0)

	chrono.toc()
	chrono.print(step='Preprocessing')

	#===================#
	#	ACO algorithm 	#
	#===================#
	chrono.tic()
	colony.image    = gray
	colony.orientim = orientim
	colony.reset()

	# Handling UI Figure
	def on_click(event):
		global pause
		pause = not pause
		if pause:
			animation.event_source.stop()
		else:
			animation.event_source.start()
	ants_fig.canvas.mpl_connect('button_press_event', on_click)
	sca = ants_ax.scatter(colony.ants['position'][:,0], colony.ants['position'][:,1],
			s = colony.ants['size'], marker='1',
			edgecolors=colony.ants['color'], facecolors=colony.ants['color'])
	animation = FuncAnimation(ants_fig, update, interval=1, blit=False)
	plt.show()

	# Retrieve pith estimation
	# And moves on the second run
	px, py = colony.estimate()
	ACO.default_parameters['scale'] = 0.8
	scale =  ACO.default_parameters['scale']
	ww 	 = 512
	sy   = math.ceil((py-ww)*scale)
	ey   = math.ceil((py+ww)*scale)
	sx   = math.ceil((px-ww)*scale)
	ex   = math.ceil((px+ww)*scale)
	gray = cv.resize(img, 	(int(scale * H), int(scale * W)), cv.INTER_CUBIC)
	gray = cv.normalize(gray, None, 0, 255, cv.NORM_MINMAX, cv.CV_8UC1)
	gray = gray[sy:ey, sx:ex]
	orientim 		= ridge_orient(gray, 2.0, 4.0)
	colony.image 	= gray
	colony.orientim = orientim
	colony.reset()


	ants_fig = plt.figure(1)
	ants_ax  = ants_fig.add_axes([0, 0, 1, 1], frameon=False)
	uiimg 	 = cv.resize(uiimg, 	(int(scale * H), int(scale * W)), cv.INTER_CUBIC)
	uiimg 	 = uiimg[sy:ey, sx:ex, :]
	if C==1:
		ants_ax.imshow(uiimg, cmap='gray')
	else:
		ants_ax.imshow(uiimg)
	ants_fig.canvas.mpl_connect('button_press_event', on_click)
	sca = ants_ax.scatter(colony.ants['position'][:,0], colony.ants['position'][:,1],
			s = colony.ants['size'], marker='1',
			edgecolors=colony.ants['color'], facecolors=colony.ants['color'])
	animation = FuncAnimation(ants_fig, update, interval=1, blit=False)
	plt.show()

	ppx, ppy 	 = colony.estimate()
	
	chrono.toc()
	chrono.print(step='ACO')
	print('{} - {}'.format(ppx+px-ww, ppy+py-ww))
