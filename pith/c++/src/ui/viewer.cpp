//
//  viewer.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 24/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "viewer.hpp"

#include "../aco/aco.hpp"

namespace aco
{
    namespace viewer
    {
        void Viewer::draw(const AntColonyOptimization *aco, const Mat &BackgroundArray)
        {
            //namedWindow("ACO");
            //namedWindow("Pheromones Matrix");
            //namedWindow("Orientation");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            Size display_size = Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows);
            resize(DisplayArray, DisplayArray, display_size);
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            double ratio = __scale / aco->getScale();
            for (Point ant: aco->__ants) {
                drawMarker(DisplayArray, ratio * ant , Scalar(0,127,255), MARKER_CROSS, 32, 4);
                putText(DisplayArray, "Ant", ratio * ant - Point(32,32), FONT_HERSHEY_DUPLEX, 2.0, Scalar(0,127,255));
            }
            //imshow("ACO", DisplayArray);
            
            Mat PheromonesUI;
            normalize(aco->__pheromones, PheromonesUI, 0, 1, NORM_MINMAX);
            cvtColor(PheromonesUI, PheromonesUI, COLOR_GRAY2BGR);
            PheromonesUI.convertTo(PheromonesUI, DisplayArray.type());
            resize(PheromonesUI, PheromonesUI, display_size, INTER_NEAREST);
            //imshow("Pheromones Matrix", PheromonesUI);
            
            Mat DstArray(Size(2*DisplayArray.cols, DisplayArray.rows), DisplayArray.type(), Scalar::all(0));
            Mat RoiArray = DstArray(Rect(0,0,DisplayArray.cols,DisplayArray.rows));
            DisplayArray.copyTo(RoiArray);
            int delta_x = (DisplayArray.cols - PheromonesUI.cols) / 2;
            int delta_y = (DisplayArray.rows - PheromonesUI.rows) / 2;
            RoiArray = DstArray(Rect(DisplayArray.cols+delta_x, 0+delta_y, PheromonesUI.cols, PheromonesUI.rows));
            PheromonesUI.copyTo(RoiArray);
            
            imshow("ACO - Left: Image and Ants, Right: Pheromones Map", DstArray);
            /*
            Mat OrientationUI;
            normalize(aco->__OrientationArray, OrientationUI, 0, 1, NORM_MINMAX);
            imshow("Orientation", OrientationUI);
            */
            
            waitKey(__delay);
        };
    
        void Viewer::draw(const vector<Point2d> piths, const Mat &BackgroundArray)
        {
            namedWindow("Detected Piths");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            resize(DisplayArray, DisplayArray, Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows));
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            
            vector<string> texts = {"First Estimate", "Second Estimate"};
            vector<Point2d> deltas = {Point2d(48,32), Point2d(48,-64)};
            for (int i = 0; i < piths.size(); i++)
            {
                Point2d pith = piths.at(i);
                Scalar color = Scalar((255 * i /piths.size()),0,0);
                drawMarker(DisplayArray, __scale * pith , color, MARKER_CROSS, 64, 4);
                putText(DisplayArray, texts.at(i), __scale * pith - deltas.at(i), FONT_HERSHEY_DUPLEX, 1.0, color);
            }
            imshow("Detected Piths", DisplayArray);
            waitKey(0);
        
        }

        void Viewer::save(const vector<Point2d> piths, const Mat &BackgroundArray, const string filename)
        {
            namedWindow("Detected Piths");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            resize(DisplayArray, DisplayArray, Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows));
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            
            vector<string> texts = {"Estimation 1", "Estimation 2"};
            vector<Point2d> deltas = {Point2d(48,32), Point2d(48,-64)};
            for (int i = 0; i < piths.size(); i++)
            {
                Point2d pith = piths.at(i);
                Scalar color = Scalar((255 * i /piths.size()),0,0);
                drawMarker(DisplayArray, __scale * pith , color, MARKER_CROSS, 64, 4);
                putText(DisplayArray, texts.at(i), __scale * pith - deltas.at(i), FONT_HERSHEY_DUPLEX, 1.0, color);
            }
            cv::imwrite(filename, 255*DisplayArray);
        }

        void Viewer::save(const AntColonyOptimization *aco, const Mat &BackgroundArray, const string filename, const int antsize)
        {
            //namedWindow("ACO");
            //namedWindow("Pheromones Matrix");
            //namedWindow("Orientation");
            
            Mat DisplayArray;
            BackgroundArray.copyTo(DisplayArray);
            Size display_size = Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows);
            resize(DisplayArray, DisplayArray, display_size);
            DisplayArray.convertTo(DisplayArray, CV_32F);
            normalize(DisplayArray, DisplayArray, 0, 1, cv::NORM_MINMAX);
            double ratio = __scale / aco->getScale();
            for (Point ant: aco->__ants) {
                drawMarker(DisplayArray, ratio * ant , Scalar(0,127,255), MARKER_CROSS, antsize, static_cast<int>(sqrt(antsize)));
                //putText(DisplayArray, "Fourmi", ratio * ant - Point(32,32),  FONT_HERSHEY_SIMPLEX, 2.0, Scalar(0,127,255), 8);
            }
            //imshow("ACO", DisplayArray);
            
            Mat PheromonesUI;
            normalize(aco->__pheromones, PheromonesUI, 0, 1, NORM_MINMAX);
            cvtColor(PheromonesUI, PheromonesUI, COLOR_GRAY2BGR);
            PheromonesUI.convertTo(PheromonesUI, DisplayArray.type());
            resize(PheromonesUI, PheromonesUI, display_size, INTER_NEAREST);
            //imshow("Pheromones Matrix", PheromonesUI);
            
            Mat DstArray(Size(2*DisplayArray.cols, DisplayArray.rows), DisplayArray.type(), Scalar::all(0));
            Mat RoiArray = DstArray(Rect(0,0,DisplayArray.cols,DisplayArray.rows));
            DisplayArray.copyTo(RoiArray);
            int delta_x = (DisplayArray.cols - PheromonesUI.cols) / 2;
            int delta_y = (DisplayArray.rows - PheromonesUI.rows) / 2;
            RoiArray = DstArray(Rect(DisplayArray.cols+delta_x, 0+delta_y, PheromonesUI.cols, PheromonesUI.rows));
            PheromonesUI.copyTo(RoiArray);
            
            //imshow("ACO - Left: Image and Ants, Right: Pheromones Map", DstArray);
            cv::imwrite(filename, 255*DstArray);
            /*
            Mat OrientationUI;
            normalize(aco->__OrientationArray, OrientationUI, 0, 1, NORM_MINMAX);
            imshow("Orientation", OrientationUI);
            */
            
            //waitKey(__delay);
        };
    }
}
