//
//  viewer.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 24/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef viewer_hpp
#define viewer_hpp

#include <stdio.h>
#include <vector>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

namespace aco
{
    class AntColonyOptimization;

    namespace viewer
    {
        class Viewer
        {
        public:
            Viewer(): __scale(1.0), __delay(1) {};
            Viewer(double scale, double delay): __scale(scale), __delay(delay) {};
            virtual ~Viewer() {};
            
            virtual void draw(const AntColonyOptimization *aco, const Mat &BackgroundArray);
            virtual void draw(const vector<Point2d> piths, const Mat &BackgroundArray);
            virtual void save(const vector<Point2d> piths, const Mat &BackgroundArray, const string filename);
            virtual void save(const AntColonyOptimization *aco, const Mat &BackgroundArray, const string filename, const int antsize=64);
        
            void setScale(double scale)
            {
                __scale = scale;
            }
            
            void setDelay(double delay)
            {
                __delay = delay;
            }
            
            double getScale()
            {
                return __scale;
            }
            
            double getDelay()
            {
                return __delay;
            }
            
        private:
            double __scale;
            double __delay;
        };
    }
}

#endif /* viewer_hpp */
