//
//  preprocess.hpp
//  HoughPith
//
//  Created by Remi DECELLE on 01/10/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef preprocess_hpp
#define preprocess_hpp

#include <stdio.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

//#include "ICA.hpp"

using namespace cv;
using namespace std;

namespace aco
{
    namespace utils
    {
        Mat fft2(const Mat& I, Mat& complexI);
        Mat sawing_marks_removal(const Mat &I, double lambda, double sigma_line);
        Mat get_intermediary_SMR(const Mat &I, Mat &fft, Mat &ffft, double lambda, double sigma_line);
    }
}

#endif /* preprocess_hpp */
