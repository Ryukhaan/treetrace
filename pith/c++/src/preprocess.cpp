//
//  preprocess.cpp
//  HoughPith
//
//  Created by Remi DECELLE on 01/10/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "preprocess.hpp"
#include <numeric>
#include <chrono>

#include "./normals/bresenham.hpp"

namespace aco
{
    namespace utils
    {
        /*
         * This function implements FFT2
         * Line 1 in Algorithm 1
         */
        Mat fft2(const Mat& I, Mat& complexI)
        {
            Mat padded;                         //expand input image to optimal size
            copyMakeBorder(I, padded, 0, 0, 0, 0, BORDER_REPLICATE);
            Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
            merge(planes, 2, complexI);         // Add to the expanded another plane with zeros
            dft(complexI, complexI);            // this way the result may fit in the source matrix
            
            int cx = complexI.cols/2;
            int cy = complexI.rows/2;
            Mat q0(complexI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
            Mat q1(complexI, Rect(cx, 0, cx, cy));  // Top-Right
            Mat q2(complexI, Rect(0, cy, cx, cy));  // Bottom-Left
            Mat q3(complexI, Rect(cx, cy, cx, cy)); // Bottom-Right
            Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
            q0.copyTo(tmp);
            q3.copyTo(q0);
            tmp.copyTo(q3);
            q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
            q2.copyTo(q1);
            tmp.copyTo(q2);
            
            // compute the magnitude and switch to logarithmic scale
            // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
            split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
            magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
            Mat magI = planes[0];
            magI += Scalar::all(1);                    // switch to logarithmic scale
            log(magI, magI);
            // crop the spectrum, if it has an odd number of rows or columns
            
            normalize(magI, magI, 0, 1, NORM_MINMAX); // Transform the matrix with float values into a
            return magI;
        }

        /*
         * ===============
         *   Algorithm 1
         * ===============
         * This function implements Algorithm 1
         */
        Mat sawing_marks_removal(const Mat &I, double lambda, double sigma_line)
        {
            // Create meshgrid
            int H = I.rows;
            int W = I.cols;
            int xc = (W * 0.5 + 1);
            int yc = (H * 0.5 + 1);
            Mat X(H, W, CV_64F);
            Mat Y(H, W, CV_64F);
            for (int j = 0; j<H; j++) {
                double* xrow = X.ptr<double>(j);
                double* yrow = Y.ptr<double>(j);
                for (int i = 0; i<W; i++) {
                    xrow[i] = i - xc;
                    yrow[i] = j - yc;
                }
            }
            int kernel;
            
            // Line 1 and 2 in Alg. 1
            // Apply FFT2
            Mat G, complexF;
            I.convertTo(G, CV_32FC1);
            G = fft2(I, complexF);
            
            // Line 3 in Alg. 1
            // Remove horizontal and vertical line passing through the center
            Mat hor = 1 - (abs(X) < 1);
            Mat ver = 1 - (abs(Y) < 1);
            hor.convertTo(hor, G.type());
            normalize(hor, hor, 0, 1, NORM_MINMAX);
            ver.convertTo(ver, G.type());
            normalize(ver, ver, 0, 1, NORM_MINMAX);
            multiply(G, ver, G);
            multiply(G, hor, G);
            
            // Line 4 in Alg. 1
            // Low-pass filter
            Mat circlex, circley;
            pow(X, 2.0, circlex);
            pow(Y, 2.0, circley);
            double freq = max(G.cols, G.rows);
            Mat low_pass = (circley + circlex) < pow(freq/3.0, 2.0);
            low_pass.convertTo(low_pass, G.type());
            normalize(low_pass, low_pass, 0, 1, NORM_MINMAX);
            multiply(G, low_pass, G);
            
            // Line 5 in Alg.1
            // Find the maximum value in Fourier spectrum
            double minVal, maxVal;
            Point minLoc, maxLoc;
            vector<Point> locs;
            minMaxLoc(G, &minVal, &maxVal, &minLoc, &maxLoc);
            
            // Line 6 in Alg.1
            // Threshold FFT
            findNonZero(G>=lambda*maxVal, locs);
            
            // Line 7 in Alg.1
            // Retrieve locations of the remained points
            Mat P = Mat(static_cast<int>(locs.size()), 2, CV_32F);
            for (int i = 0; i<locs.size(); i++) {
                P.at<float>(i,0)=locs[i].x;
                P.at<float>(i,1)=locs[i].y;
            }
            
            // Line 8-9 in Alg.1
            // Principal Component Analysis
            PCA pca_analysis(P, Mat(), PCA::DATA_AS_ROW);
            
            //Store the eigenvalues and eigenvectors
            vector<Point2d> eigenvectors(2);
            vector<double> eigenvalues(2);
            for (int i = 0; i < 2; i++)
            {
                eigenvectors[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                          pca_analysis.eigenvectors.at<double>(i, 1));
                eigenvalues[i] = pca_analysis.eigenvalues.at<double>(i);
            }
            
            // Line 10 in Alg.1
            // Compute certainty (denoted as zeta)
            double zeta = (eigenvalues[0] - eigenvalues[1]) / eigenvalues[0];
            
            // Line 11 in Alg.1
            // If zeta is above 0.66
            // which means the main component is dominant over the second
            // then remove this direction in Fourier domain
            Mat J;
            if (zeta > 0.66)
            {
                // Line 12 in Alg.1
                // Create line in the main direction
                double angle = atan2(eigenvectors[0].y, eigenvectors[0].x);
                vector<Point3d> s = {Point3d(xc, yc, angle)};
                Mat line = accumulate_normals(s, G.rows, G.cols);

                // Line 13 in Alg.1
                // Smooth the line with a gaussian filter
                // of sigma equals to sigma_line 
                // and a kernel of size 2*2*sigma+1
                kernel = 2 * ceil(2 * sigma_line) + 1;
                GaussianBlur(line, line, Size(kernel, kernel), sigma_line);
                normalize(line, line, 0, 1, NORM_MINMAX);
                line = 1 - line;
                
                // Line 14 in Alg.1
                // Pixel-wise multiplication between FFT and the previous line
                Mat planes[] = { Mat_<float>(I), Mat::zeros(I.size(), CV_32F) };
                split(complexF, planes);
                multiply(planes[0], line, planes[0]);
                multiply(planes[1], line, planes[1]);
                merge(planes, 2, complexF);

                // Line 15 in Alg.1
                // Do not forget to shift before applying ifft
                int cx = complexF.cols/2;
                int cy = complexF.rows/2;
                Mat tmp;
                Mat c0(complexF, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
                Mat c1(complexF, Rect(cx, 0, cx, cy));  // Top-Right
                Mat c2(complexF, Rect(0, cy, cx, cy));  // Bottom-Left
                Mat c3(complexF, Rect(cx, cy, cx, cy)); // Bottom-Right
                c0.copyTo(tmp);
                c3.copyTo(c0);
                tmp.copyTo(c3);
                c1.copyTo(tmp);                         // swap quadrant (Top-Right with Bottom-Left)
                c2.copyTo(c1);
                tmp.copyTo(c2);
                // Apply ifft
                dft(complexF, J, DFT_INVERSE | DFT_REAL_OUTPUT);
                normalize(J, J, 0, 1, NORM_MINMAX);
            }
            else
            {
                // Line 15 in Alg.1
                return I;
            }
            // Line 15 in Alg.1
            return J;
        }
        /*
         * ==== END ====
         *  Algorithm 1
         * ==== END ====
         */
    
        /*
         * This function allow to have intermediary images for the preprocessing
         */
        Mat get_intermediary_SMR(const Mat &I, Mat &FFTArray, Mat &FFFTArray, double lambda, double sigma_line)
        {
            // Create meshgrid
            int H = I.rows;
            int W = I.cols;
            int xc = (W * 0.5 + 1);
            int yc = (H * 0.5 + 1);
            Mat X(H, W, CV_64F);
            Mat Y(H, W, CV_64F);
            for (int j = 0; j<H; j++) {
                double* xrow = X.ptr<double>(j);
                double* yrow = Y.ptr<double>(j);
                for (int i = 0; i<W; i++) {
                    xrow[i] = i - xc;
                    yrow[i] = j - yc;
                }
            }
            int kernel;
            
            // FFT2
            Mat J;
            I.convertTo(J, CV_32FC1);
            Mat F, complexI;
            F = fft2(J, complexI);
            F.copyTo(FFTArray);
            F.copyTo(FFFTArray);

            // First filtering
            // Remove horizontal and vertical line passing through the center
            Mat hor = 1 - (abs(X) < 1);
            Mat ver = 1 - (abs(Y) < 1);
            hor.convertTo(hor, F.type());
            normalize(hor, hor, 0, 1, NORM_MINMAX);
            ver.convertTo(ver, F.type());
            normalize(ver, ver, 0, 1, NORM_MINMAX);
            multiply(F, ver, F);
            multiply(F, hor, F);

            // Low-pass filter
            Mat circlex, circley;
            pow(X, 2.0, circlex);
            pow(Y, 2.0, circley);
            double freq = max(F.cols, F.rows);
            Mat low_pass = (circley + circlex) < pow(freq/3.0, 2.0);
            low_pass.convertTo(low_pass, F.type());
            normalize(low_pass, low_pass, 0, 1, NORM_MINMAX);
            multiply(F, low_pass, F);
            
            // Find the maximum value in Fourier spectrum
            double minVal, maxVal;
            Point minLoc, maxLoc;
            vector<Point> locs;
            minMaxLoc(F, &minVal, &maxVal, &minLoc, &maxLoc);
            
            // Threshold FFT
            findNonZero(F>=lambda*maxVal, locs);
            
            // Retrieve locations of the remained points
            Mat XY = Mat(locs.size(), 2, CV_64F);
            for (int i = 0; i<locs.size(); i++) 
            {
                XY.at<double>(i,0)=locs[i].x;
                XY.at<double>(i,1)=locs[i].y;
            }
            
            // Principal Component Analysis (with covariance matrix)
            PCA pca_analysis(XY, Mat(), PCA::DATA_AS_ROW);
            //Store the eigenvalues and eigenvectors
            vector<Point2d> eigen_vecs(2);
            vector<double> eigen_val(2);
            for (int i = 0; i < 2; i++)
            {
                eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                            pca_analysis.eigenvectors.at<double>(i, 1));
                eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
            }
            // Compute certainty (denoted as zeta)
            double zeta = (eigen_val[0] - eigen_val[1]) / eigen_val[0];
            
            // If zeta is above 0.66
            // which means the main component is dominant over the second
            // then remove this direction in Fourier domain
            Mat output;
            I.copyTo(output);
            if (zeta > 0.66)
            {
                // Create line in the main direction
                double angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x);
                vector<Point3d> s = {Point3d(xc, yc, angle)};
                Mat line = accumulate_normals(s, F.rows, F.cols);

                // Smooth the line with a gaussian filter
                kernel = 2 * ceil(2 * sigma_line) + 1;
                GaussianBlur(line, line, Size(kernel, kernel), sigma_line);
                normalize(line, line, 0, 1, NORM_MINMAX);
                line = 1 - line;

                // Pixel-wise multiplication between FFT and the previous line
                Mat padded;
                copyMakeBorder(J, padded, 0, 0, 0, 0, BORDER_CONSTANT);
                Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
                split(complexI, planes);
                multiply(planes[0], line, planes[0]);
                multiply(planes[1], line, planes[1]);
                merge(planes, 2, complexI);

                // Keep FFT after line removing
                magnitude(planes[0], planes[1], planes[0]);
                Mat magI = planes[0];
                magI += Scalar::all(1);
                log(magI, magI);
                normalize(magI, magI, 0, 1, NORM_MINMAX);
                magI.copyTo(FFFTArray);

                // Do not forget to shift before applying ifft
                int cx = complexI.cols/2;
                int cy = complexI.rows/2;
                Mat tmp;
                Mat c0(complexI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
                Mat c1(complexI, Rect(cx, 0, cx, cy));  // Top-Right
                Mat c2(complexI, Rect(0, cy, cx, cy));  // Bottom-Left
                Mat c3(complexI, Rect(cx, cy, cx, cy)); // Bottom-Right
                c0.copyTo(tmp);
                c3.copyTo(c0);
                tmp.copyTo(c3);
                c1.copyTo(tmp);                         // swap quadrant (Top-Right with Bottom-Left)
                c2.copyTo(c1);
                tmp.copyTo(c2);
                
                // Apply ifft
                dft(complexI, output, DFT_INVERSE | DFT_REAL_OUTPUT);
                normalize(output, output, 0, 1, NORM_MINMAX);
            }
            return output;
        }
    }
}
