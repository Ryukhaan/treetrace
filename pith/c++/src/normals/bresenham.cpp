//
//  bresenham.cpp
//  
//
//  Created by Remi DECELLE on 10/07/2020.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "bresenham.hpp"
#include <iostream>
#include <assert.h>

/*
 * ===============
 *   Algorithm 6
 * ===============
 * This function implements Algorithm 5
 * which correspond to the line 6 in Alg. 4
 */
Mat accumulate_normals(const vector<Point3d> lines, const int h, const int w)
{
    // Line 1 in Alg.5
    Mat A = Mat::zeros(h, w, CV_32F);
    // Line 2 in Alg.5
    for (auto line : lines)
    {
        // Line 3 in Alg.5
        int i = line.x;
        int j = line.y;
        // Line 4-5 in Alg.5
        double a = tan(line.z);
        // Line 6 in Alg.5
        double b = j - a * i;

        // Line 7 in Alg.5
        vector<Point> P;
        Point V=Point(-1,-1), W=Point(-1,-1), H=Point(-1,-1), G=Point(-1,-1);
        
        // Line 8 to 13 in Alg.5
        // Compute intersection with y=0 and y=h-1
        // But only if a is not close to 0
        if (abs(a) > 1e-12)
        {
            V = Point(-b/a, 0);
            W = Point((h-1-b)/a, h-1);
        }
        // Compute intersection with x=0 and x=w-1
        // But only if a is not close to PI/2 (or -PI/2)
        if (abs(a) < 1e12)
        {
            H = Point(0, b);
            G = Point(w-1, a*(w-1)+b);
        }
         
        // Line 14 in Alg.5
        if (0 <= V.x && V.x < w) P.push_back(V);
        if (0 <= W.x && W.x < w) P.push_back(W);
        if (0 <= H.y && H.y < h) P.push_back(H);
        if (0 <= G.y && G.y < h) P.push_back(G);
        assert(P.size()>=2);
        
        // Line 15 in Alg.5
        LineIterator S(A, P.at(0), P.at(1));
        
        // Line 16-17 in Alg.5
        for (int k = 0; k<S.count; k++, ++S)
            A.at<float>(S.pos())++;
    }
    // Line 18 in Alg.5
    return A;
}
/*
 * ==== END ====
 *  Algorithm 5
 * ==== END ====
 */
