//
//  bresenham.hpp
//  
//
//  Created by Remi DECELLE on 10/07/2020.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef bresenham_hpp
#define bresenham_hpp

#include <stdio.h>
#include <vector>
#include <set>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

Mat accumulate_normals(const vector<Point3d> orient, const int h, const int w);

#endif /* bresenham_hpp */
