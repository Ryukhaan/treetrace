//
//  main.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 11/12/2019.
//  Copyright © 2019 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
  
   
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <stdexcept>

#include "preprocess.hpp"
#include "./orientation/RidgeOrienteer.hpp"
#include "./normals/bresenham.hpp"
#include "./ui/viewer.hpp"
#include "./aco/aco.hpp"
#include "./aco/writer.hpp"

using namespace cv;
using namespace std;

const String keys =
"{help h usage ?    |       | print this message   }"
"{@input            |       | input image          }"
"{parameters        |       | parameters file (JSON)}"
"{delta d           | 0.4   | resizing factor}"
"{dx x              | 4     | Number of sub-images in x-axis}"
"{dy y              | 4     | Number of sub-images in y-axis}"
"{lambda l          | 0.875 | Fourier spectrum threshold}"
"{sigma s           | 6.0   | Gaussian standard deviation for a smooth detected line}"
"{sigmag sg         | 2.0   | Gaussian standard deviation for gradient computation }"
"{sigmab sb         | 4.0   | Gaussian standard deviation for local orientation estimate}"
"{sigmas ss         | 0.0   | Gaussian standard deviation for local orientation smoothing}"
"{epsilon e         | 2.0   | Early stop criterion}"
"{iter N            | 50    | Maximum number of iterations}"
"{animated          | true  | Show ants moving}"
"{ant K             | 4     | Number of ants by line and column (equals to K - not K squared)}"
"{block n           | 3     | Number of block clusters around each ant}"
"{omega o           | 8     | Size of each block cluster}"
"{quantizer m       | 4     | Quantizer for pixels in the preprocessed image}"
"{alpha a           | 2.0   | ACO parameter for pheromone matrix}"
"{beta b            | 1.0   | ACO parameter for desirability matrix}"
"{gamma g           | 0.07  | ACO parameter for pheromone evaporation rate}"
"{kappa k           | 0.8   | Threshold pheromone matrix for pith extraction}"
"{intermediary      | 0     | Output intermediary images (fft, filtered, orientation, ..)}"
"{seed              | 1     | Seed value for reproducibility}";


Mat read_file(const String filename)
{
    Mat img;
    if ( filename.empty() )
    {
        throw invalid_argument("A file must be provided.");
    }
    img = imread(filename, 1);
    if ( img.empty() )
    {
        throw invalid_argument("Image not found or cannot be loaded.");
    }
    return img;
}

void check_dimensions(const int H, const int W, const double scale, const int K, const int num_blocs, const int bloc_size)
{
    int height  = H * scale;
    int width   = W * scale;
    int delta_y = height / K;
    int delta_x = width / K;
    
    if ( delta_y < num_blocs * bloc_size || delta_x < num_blocs * bloc_size )
        throw invalid_argument("The ant's neighborhood is too large to the image (try to increase Scale, or decrease omega/block).");
}

void check_orientation_values(const double sigma_g, const double sigma_b, const double sigma_s)
{
    if ( sigma_g <= 0 )
        throw invalid_argument("Sigma_g must be strictly positive.");
    if ( sigma_b <= 0 )
        throw invalid_argument("Sigma_b must be strictly positive.");
    if ( sigma_s < 0 )
        throw invalid_argument("Sigma_s must be positive.");
}

void check_mark_removal(const double sigma, const double lambda)
{
    if (sigma <= 0)
        throw invalid_argument("Sigma (marks removal) must be strictly positive.");
    if (lambda < 0 || lambda > 1)
        throw invalid_argument("Lambda (marks removal) must be between 0 and 1 (included).");
}

void check_aco(const int m, const double alpha, const double beta, const double gamma, const double kappa)
{
    if (m <= 0)
        throw invalid_argument("m must be strictly positive.");
    if (alpha < 0)
        throw invalid_argument("alpha must be positive.");
    if (beta < 0)
        throw  invalid_argument("beta must be positive.");
    if (gamma < 0)
        throw invalid_argument("gamma must be positive.");
    if (kappa < 0 || kappa > 1)
        throw invalid_argument("kappa must be between 0 and 1.");
}

int main(int argc, char** argv)
{
    // Initialize parser
    CommandLineParser parser(argc, argv, keys);
    parser.about("Ant Colony for Pith Estimation v1.1.0");
    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }
    
    // Read filename and the corresponding image
    Mat img;
    string filename = parser.get<String>(0); // Image input
    try
    {
        img = read_file(filename);
    }
    catch ( invalid_argument& ia)
    {
        cerr << ia.what() << endl;
        return -1;
    }
    string basename = filename.substr(0,filename.find_last_of('.'));
    
    // Declare variables for parameters
    double delta;       // Resize input image
    int div_x, div_y;   // Number of sub-images in x-axis and y-axis
    double sigma_g, sigma_b, sigma_s; // Gaussian standard deviation for local orientation
    
    int K, n, omega, m; // K = Number of ants, n number of blocks, omega size of a block, m is the quantizer
    
    double alpha, beta, gamma;
    int N;
    double kappa, epsilon;
    double lambda, sigma; // Two parameters in sawings marks removal
    
    // Misc variables
    bool animated, intermediary;
    int seed = 1;
    
    // Parse parameters.json if --parameters has been provided
    bool has_parameters_file = parser.has("parameters");
    if ( has_parameters_file )
    {
        String parameters_path = parser.get<String>("parameters");
        FileStorage parameters(parameters_path, FileStorage::READ);
        sigma_g         = parameters["Orientation"]["sigma_g"];
        sigma_b         = parameters["Orientation"]["sigma_b"];
        sigma_s         = parameters["Orientation"]["sigma_s"];
        
        sigma           = parameters["MarksRemoval"]["sigma"];
        lambda          = parameters["MarksRemoval"]["lambda"];
        
        delta           = parameters["Preprocessing"]["delta"];
        div_x           = parameters["Preprocessing"]["div_x"];
        div_y           = parameters["Preprocessing"]["div_y"];
        
        K               = parameters["ACO"]["K"];
        n               = parameters["ACO"]["n"];
        omega           = parameters["ACO"]["omega"];
        m               = parameters["ACO"]["m"];
        alpha           = parameters["ACO"]["alpha"];
        beta            = parameters["ACO"]["beta"];
        gamma           = parameters["ACO"]["gamma"];
        N               = parameters["ACO"]["N"];
        kappa           = parameters["ACO"]["kappa"];
        epsilon         = parameters["ACO"]["epsilon"];
        
        int tmp         = parameters["animation"];
        animated        = (bool)tmp;
        tmp             = parameters["intermediary"];
        intermediary    = (bool)tmp;
        seed            = parameters["seed"];
    }
    else
    {
        // Parameters of pre-processing step
        delta   = parser.get<double>("delta");
        div_x   = parser.get<int>("dx");
        div_y   = parser.get<int>("dy");
        
        // Parameters of sawing removal
        sigma   = parser.get<double>("sigma");
        lambda  = parser.get<double>("lambda");
        
        // Parameters of local orientation estimation
        sigma_g = parser.get<double>("sigmag");
        sigma_b = parser.get<double>("sigmab");
        sigma_s = parser.get<double>("sigmas");
        
        // Parameters of ACO algorithm
        K       = parser.get<int>("ant");
        n       = parser.get<int>("block");
        omega   = parser.get<int>("omega");
        m       = parser.get<int>("quantizer");
        alpha   = parser.get<double>("alpha");
        beta    = parser.get<double>("beta");
        gamma   = parser.get<double>("gamma");
        kappa   = parser.get<double>("kappa");
        
        // Parameters of Early Stop Criteria
        epsilon = parser.get<double>("epsilon");
        N       = parser.get<int>("iter");
        
        // Misc parameters
        animated        = parser.get<bool>("animated");
        intermediary    = parser.get<bool>("intermediary");
        seed            = parser.get<int>("seed");
    }

    if (!parser.check())
    {
        parser.printErrors();
        return -1;
    }
    
    // Check that parameters are compatible
    try
    {
        // First run
        check_dimensions(img.rows, img.cols, delta, K, n, omega);
        // Second run
        check_dimensions(512, 512, delta, K, n, omega);
        // Orientation computation values
        check_orientation_values(sigma_g, sigma_b, sigma_s);
        // Mark removal
        check_mark_removal(sigma, lambda);
        // ACO
        check_aco(m, alpha, beta, gamma, kappa);
        if (N < 0)
            throw invalid_argument("N (number of iterations) must be strictly positive.");
    }
    catch (invalid_argument &ia)
    {
        cerr << ia.what() << endl;
        return -1;
    }
    

    // First run (see Section 5.2)
    // Instanciate orientation algorithm, early stop criteria and ACO
    aco::viewer::Viewer* __viewer   = new aco::viewer::Viewer(0.5, 0);
    aco::io::Writer* __writer       = new aco::io::Writer();
    aco::orientation::RidgeOrienteer    orienteer(sigma_g, sigma_b, sigma_s);
    aco::earlystop::IterAndDistanceStop earlystopper(epsilon, N);
    aco::AntColonyOptimization* __aco = new aco::AntColonyOptimization(&orienteer, delta,
                                                                     K, n, omega, m,
                                                                     kappa, gamma, alpha, beta,
                                                                     &earlystopper, seed);
    
    __aco->setSigma(sigma);
    __aco->setLambda(lambda);
    __aco->setDivX(div_x);
    __aco->setDivY(div_y);
    
    if ( intermediary )
    {
        __writer->setBasename(basename);
        __writer->setFilename("_output.jpg");
        __writer->saveIntermediariesImages(img, __aco);
    }

    // Basename for showing pairs during iterations
    __writer->setBasename(basename + "_coarse_");

    // Initialize timer
    double tt_tic, tt_toc;
    tt_tic = cv::getTickCount();
    
    // Estimate first pith position
    // This is the first run in our experiments (see Section 5.2)
    // Line 1 to 10 in Alg.3
    __aco->initialize(img);
    // Line 11 in Alg.3
    int jcount = 0;
    for(;;)
    {
        // UI
        if ( animated )
        {
            //__viewer->draw(__aco, img);
            __viewer->save(__aco, img, "/Users/remidecelle/Documents/aco-"+to_string(jcount)+".jpg");
            jcount++;
        }
        
        // Save iterations
        if ( intermediary )
            if ( earlystopper.showingIterations() )
                __writer->saveIteration(img, __aco, earlystopper.getCurrentIt());

        // Line 13 to 18 in Alg.3
        __aco->update();
        
        // Line 18 to 22 in Alg.3
        if ( earlystopper.checkCriteria(__aco) )
            break;
    }
    if ( intermediary )
        __writer->saveIteration(img, __aco, earlystopper.getCurrentIt());

    // Line 23 in Alg.3
    Point2d first_estimate = __aco->extract();
    
    
    // Second run, select a sub-image of size 512x512 around the first estimation
    // See Section 5.2 (in our experiments)
    
    __writer->setBasename(basename + "_fine_");

    
    int ww = 256;
    int px = first_estimate.x;
    int py = first_estimate.y;
    int ex = max(min(static_cast<int>((px-ww)), img.rows-ww), 0);
    int ey = max(min(static_cast<int>((py-ww)), img.cols-ww), 0);
    int dd = static_cast<int>(2*ww);
    Rect subRect = Rect(ex, ey, dd, dd);
    Mat simg = img(subRect);
    
    // Change parameter
    
    earlystopper.setEpsilon(0.5);
    earlystopper.reset();
    
    orienteer.set_sigma_g(3.0);
    orienteer.set_sigma_b(3.0);
    orienteer.set_sigma_s(1.0);
    __aco->setQuantizer(2);
    __aco->setDivX(1);
    __aco->setDivY(1);

    // Estimate second pith position
    __aco->initialize(simg);
    for (;;)
    {
        // UI
        if ( animated )
        {
            __viewer->save(__aco, simg, "/Users/remidecelle/Documents/aco-"+to_string(jcount)+".jpg", 16);
            //__viewer->draw(__aco, simg);
            ++jcount;
        }

        // Save iterations
        if ( intermediary )
            if ( earlystopper.showingIterations() )
                __writer->saveIteration(simg, __aco, earlystopper.getCurrentIt());
        
        __aco->update();
        
        if ( earlystopper.checkCriteria(__aco) )
            break;
    }
    if ( intermediary )
        __writer->saveIteration(simg, __aco, earlystopper.getCurrentIt());

    Point2d second_estimate = __aco->extract();
    
    // Retrieve correct position in the full image coordinate
    second_estimate.x = second_estimate.x + px - ww;
    second_estimate.y = second_estimate.y + py - ww;
    
    tt_toc = cv::getTickCount();
    double second_timer = (tt_toc - tt_tic) / getTickFrequency();
    
    vector<Point2d> piths = {first_estimate, second_estimate};
    if ( animated )
    {
        //__viewer->draw(piths, img);
        __viewer->save(piths, img, "/Users/remidecelle/Documents/aco-"+to_string(jcount)+".jpg");
    }
    
    //__writer->setBasename(basename);
    //__writer->setFilename("_output.jpg");
    //__writer->savePiths(img, piths);

    __writer->setFilename(".csv");
    __writer->saveResults(piths, second_timer, __aco);
    
    delete __viewer;
    delete __writer;
    delete __aco;
    return 0;
}
