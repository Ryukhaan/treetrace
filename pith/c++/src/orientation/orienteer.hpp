//
//  orienteer.hpp
//  
//
//  Created by Remi DECELLE on 13/01/2020.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef orienteer_hpp
#define orienteer_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include "basic.hpp"
#include "../preprocess.hpp"

namespace aco
{
    namespace orientation
    {
        class Orienteer
        {
        public:
            Orienteer() {};
            virtual ~Orienteer() {};
            virtual Mat estimate(const Mat &InputArray)
            {
                return Mat::zeros(InputArray.rows, InputArray.cols, InputArray.type());
            }
        };
    }
}
#endif /* orienteer_hpp */
