//
//  RidgeOrienteer.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "RidgeOrienteer.hpp"

namespace aco
{
    namespace orientation
    {
        /*
         * ===============
         *   Algorithm 2
         * ===============
         * This function implements Algorithm 2
         * Compute the local orientation of an image I
         */
        Mat RidgeOrienteer::estimate(const Mat& I)
        {
            cv::Mat D_x, D_y;
            int sze = 6 * round(sigma_g) + 1;
            // Define Gaussian kernel
            cv::Mat gaussKernelX = cv::getGaussianKernel(sze, sigma_g, DDEPTH);
            cv::Mat F = gaussKernelX * gaussKernelX.t();
            
            // Line 1 in Alg.2
            // Perform Gaussian filtering on gradient
            cv::Mat fx, fy;
            cv::Sobel(F, fx, DDEPTH, 1, 0);
            cv::Sobel(F, fy, DDEPTH, 0, 1);

            // Line 2 in Alg.2
            D_x = cv::Mat::zeros(I.rows, I.cols, DDEPTH);
            D_y = cv::Mat::zeros(I.rows, I.cols, DDEPTH);
            cv::filter2D(I, D_x, -1, fx); // Gradient of the image in x
            cv::filter2D(I, D_y, -1, fy); // Gradient of the image in y

            // Line 3 in Alg.2
            cv::Mat X, Y, Z;
            cv::multiply(D_x, D_x, X);
            cv::multiply(D_y, D_y, Y);
            cv::multiply(D_x, D_y, Z);
            
            // Now smooth the covariance data to perform a weighted summation of the data
            int sze2 = 6 * round(sigma_b) + 1;
            cv::Mat gaussKernelX2 = cv::getGaussianKernel(sze2, sigma_b, DDEPTH);
            cv::Mat G = gaussKernelX2 * gaussKernelX2.t();
            
            cv::filter2D(X, X, -1, G, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            cv::filter2D(Z, Z, -1, G, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            cv::filter2D(Y, Y, -1, G, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            Z *= 2;
            
            // Line 4-5 in Alg.2
            // Analytic solution of principal direction
            cv::Mat denom, theta_x, theta_y;
            cv::Mat G1, G2, G3;
            // Compute denominator only once since it will be used twice right after
            cv::multiply(Z, Z, G1);
            G2 = X - Y;
            cv::multiply(G2, G2, G2);
            G3 = G1 + G2;
            cv::sqrt(G3, denom);
            // Compute theta_x and theta_y
            cv::divide(Z, denom, theta_x);
            cv::divide(X - Y, denom, theta_y);
            
            // Line 6-7 in Alg.2
            // Last smoothing (with a gaussian filter again)
            // This one is not mandatory
            if (sigma_s > 0)
            {
                int sze3 = 6 * round(sigma_s) + 1;
                cv::Mat gaussKernelX3 = cv::getGaussianKernel(sze3, sigma_s, DDEPTH);
                cv::Mat gaussKernel3 = gaussKernelX3 * gaussKernelX3.t();
                cv::filter2D(theta_y, theta_y, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
                cv::filter2D(theta_x, theta_x, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
            }
            
            // Line 8 in Alg.2
            theta_x.convertTo(theta_x, DDEPTH);
            theta_y.convertTo(theta_y, DDEPTH);
            cv::Mat I_theta = cv::Mat::zeros(theta_x.rows, theta_x.cols, DDEPTH);
            // Pointer access more effective than Mat.at<T>()
            for (int i = 0; i < theta_x.rows; i++) {
                const float *theta_xi = theta_x.ptr<float>(i);
                const float *theta_yi = theta_y.ptr<float>(i);
                float *I_thetai = I_theta.ptr<float>(i);
                for (int j = 0; j < theta_x.cols; j++) {
                    I_thetai[j] = (std::atan2(theta_xi[j], theta_yi[j])) / 2;
                }
            }
            
            return I_theta;
        }
        /*
         * ==== END ====
         *  Algorithm 2
         * ==== END ====
         */
    }
}
