//
//  RidgeOrienteer.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef RidgeOrienteer_hpp
#define RidgeOrienteer_hpp

#include <stdio.h>
#include "orienteer.hpp"
//#include "basic.hpp"

namespace aco
{
    namespace orientation
    {
        static const int DDEPTH = CV_32FC1;
    
        class RidgeOrienteer : public Orienteer
        {
        public:
            RidgeOrienteer(double grad, double block):
                sigma_g(grad), sigma_b(block), sigma_s(0.0) {};
            RidgeOrienteer(double grad, double block, double smooth):
                sigma_g(grad), sigma_b(block), sigma_s(smooth) {};
            virtual ~RidgeOrienteer() {};
            virtual Mat estimate(const Mat &InputArray);

            void set_sigma_g(double sigma)
            {
                sigma_g = sigma;
            };
            void set_sigma_b(double sigma)
            {
                sigma_b = sigma;
            };
            void set_sigma_s(double sigma)
            {
                sigma_s = sigma;
            };
            
        protected:
            double sigma_g;
            double sigma_b;
            double sigma_s;
        };
    }
}
#endif /* RidgeOrienteer_hpp */
