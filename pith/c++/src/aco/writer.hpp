//
//  writer.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 21/05/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef writer_hpp
#define writer_hpp

#include <stdio.h>
#include <vector>
#include <cfloat>
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

using namespace std;
using namespace cv;

namespace aco
{
	class AntColonyOptimization;

	namespace io {

		static const string DEFAULT_FILENAME = "output.jpg";
		static const string DEFAULT_BASENAME = "~/";
		static const string CSV_FORMAT = "csv";
		static const string JPG_FORMAT = "jpg";
		static const string PNG_FORMAT = "png";

		class Writer
		{
		public:
			Writer()
			{
				__filename = DEFAULT_FILENAME;
				__basename = DEFAULT_BASENAME;
				__format   = JPG_FORMAT;
			};

			Writer(string str)
			{
				__filename = str;
				__basename = DEFAULT_BASENAME;
				__format   = JPG_FORMAT;
			};

			~Writer() {};

			void saveIntermediariesImages(const Mat &image, const AntColonyOptimization* aco);
			void savePiths(Mat image, vector<Point2d> piths);
			void saveResults(vector<Point2d> piths, double time, const AntColonyOptimization* aco);
			void saveIteration(const Mat& image, const AntColonyOptimization* aco, int it);

			void setFilename(string str)
			{
				__filename = str;
			}

			void setBasename(string str)
			{
				__basename = str;
			}
			void setFormat(string str)
			{
				__format = str;
			}

			string getFormat()
			{
				return __format;
			}
		
		private:
			string __filename;
			string __basename;
			string __format;
		};

	}
}

#endif /* writer_hpp */