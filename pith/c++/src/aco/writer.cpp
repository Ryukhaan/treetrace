//
//  writer.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 21/05/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//  
 
#include "writer.hpp"
#include "aco.hpp"

namespace aco
{
    namespace io
    {
		void Writer::saveIntermediariesImages(const Mat &image, const AntColonyOptimization* aco)
		{
			double scale = aco->getScale();
			vector<Mat> images = aco->getIntermediaryImages(image);

			string resizedname = __basename + "_resized.jpg";
			string fftname 	= __basename + "_fft.jpg";
			string ffftname = __basename + "_filterd_fft.jpg";
			string filtered = __basename + "_filtered_image.jpg";
			string angname  = __basename + "_orientation.jpg";

			Mat ResizedArray;
			// Convert into grayscale. Beware that cv::imread create BGR image (not RGB image) !
			cvtColor(image, ResizedArray, COLOR_BGR2GRAY);
			// Resizing
			resize(ResizedArray, ResizedArray, Size(aco->getDelta() * image.cols, aco->getDelta() * image.rows), INTER_AREA);
			Mat FFTArray 			= images.at(0);
			Mat FilteredFFTArray 	= images.at(1);
			Mat FilteredArray 		= images.at(2);
			Mat AngularArray 		= images.at(3);

			normalize(FFTArray, FFTArray, 0, 255, cv::NORM_MINMAX);
			normalize(FilteredFFTArray, FilteredFFTArray, 0, 255, cv::NORM_MINMAX);
			normalize(FilteredArray, FilteredArray, 0, 255, cv::NORM_MINMAX);
			normalize(AngularArray, AngularArray, 0, 255, cv::NORM_MINMAX);

			imwrite(resizedname, ResizedArray);
			imwrite(fftname, FFTArray);
			imwrite(ffftname, FilteredFFTArray);
			imwrite(filtered, FilteredArray);
			imwrite(angname, AngularArray);
			return;
		}

		void Writer::savePiths(Mat image, vector<Point2d> piths)
		{
			Mat OutputImage = cv::Mat::zeros(image.rows, image.cols, image.type());
			string output_img_name = __basename + __filename;
			image.copyTo(OutputImage);
			Point2d first_estimate 	= piths.at(0);
			Point2d second_estimate = piths.at(1);
			drawMarker(OutputImage, first_estimate, Scalar(100,50,255), MARKER_CROSS, 32, 4);
			drawMarker(OutputImage, second_estimate, Scalar(0,255,150), MARKER_CROSS, 32, 4);
			imwrite(output_img_name, OutputImage);
			return;
		}

		void Writer::saveResults(vector<Point2d> piths, double time, const AntColonyOptimization* aco)
		{
			Point2d first_estimate 	= piths.at(0);
			Point2d second_estimate = piths.at(1);
			// Save pith position and computation time for both iterations
			ofstream outputFile;
			string output_name = __basename + __filename; //"results.csv";
			outputFile.open(output_name);
			outputFile << "1st Px" << "," << "1st Py" << ",";
			outputFile << "2nd Px" << "," << "2nd Py" << ",";
			outputFile << "Time" << "," << "randgen" << endl;
			outputFile << first_estimate.x << "," << first_estimate.y << ",";
			outputFile << second_estimate.x << "," << second_estimate.y << ",";
			outputFile << time << "," << aco->__generator << endl;
			return;
		}

		void Writer::saveIteration(const Mat& image, const AntColonyOptimization* aco, int it)
		{
			double __scale = 0.5;
			string iteration_name = __basename + "_at_step_" + to_string(it) + ".jpg";

            Mat DisplayArray;
            image.copyTo(DisplayArray);
            Size display_size = Size(__scale * DisplayArray.cols, __scale * DisplayArray.rows);
            resize(DisplayArray, DisplayArray, display_size);
            DisplayArray.convertTo(DisplayArray, CV_32F);
            double ratio = __scale / aco->getScale();
            for (Point ant: aco->__ants) {
                drawMarker(DisplayArray, ratio * ant , Scalar(0,100,255), MARKER_CROSS, 32, 4);
                putText(DisplayArray, "Ant", ratio * ant - Point(24,24), FONT_HERSHEY_DUPLEX, 1.0, Scalar(0,127,255));
            }
            
            Mat PheromonesUI;
            normalize(aco->__pheromones, PheromonesUI, 0, 255, NORM_MINMAX);
            cvtColor(PheromonesUI, PheromonesUI, COLOR_GRAY2BGR);
            PheromonesUI.convertTo(PheromonesUI, DisplayArray.type());
            resize(PheromonesUI, PheromonesUI, display_size, INTER_NEAREST);

            Mat DstArray(Size(2*DisplayArray.cols, DisplayArray.rows), DisplayArray.type(), Scalar::all(0));
            Mat RoiArray = DstArray(Rect(0,0,DisplayArray.cols,DisplayArray.rows));
            DisplayArray.copyTo(RoiArray);
            int delta_x = (DisplayArray.cols - PheromonesUI.cols) / 2;
            int delta_y = (DisplayArray.rows - PheromonesUI.rows) / 2;
            RoiArray = DstArray(Rect(DisplayArray.cols+delta_x, 0+delta_y, PheromonesUI.cols, PheromonesUI.rows));
            PheromonesUI.copyTo(RoiArray);

			imwrite(iteration_name, DstArray);
		}

    }
}