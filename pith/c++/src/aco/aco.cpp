//
//  aco.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "aco.hpp"

using namespace std;
using namespace cv;

/*
 * This function implements the median given a 2D matrix
 * See line 3 in Alg.4
 * 
 * The algorithm uses histogram to calculate the median
 * 
 */
float median(const Mat input )
{
    double m    = (input.rows*input.cols) / 2;
    int bin     = 0;
    float med   = -4.0;

    int histSize  = 256;
    float range[] = { -CV_PI, CV_PI };
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    Mat hist;
    // Compute histogram of 2d matrix
    calcHist( &input, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
    // Find the median value inside the histogram
    for ( int i = 0; i < histSize && med < -CV_PI; ++i )
    {
        bin += cvRound( hist.at< float >( i ) );
        if ( bin > m && med < -CV_PI )
            med = i * (2.0 * CV_PI / histSize) - CV_PI;
    }
    return med;
}

namespace aco
{
    AntColonyOptimization::AntColonyOptimization(orientation::Orienteer *O, double s,
                                                 int na, int nb, int bs,int q,
                                                 double k, double er, double a, double b,
                                                 earlystop::EarlyStop *S, int seed)
    {
        delta   = s;
        K       = na;
        n       = nb;
        omega   = bs;
        m       = q;
        kappa   = k;
        gamma   = er;
        alpha   = a;
        beta    = b;
        
        Orienteer   = O;

        vector<double> distances = {};
        __p = Point2d(-1, -1);
        
        // Reproductibility 
        __generator.seed(seed);
    }

    /*
     * This function implements line 1 to 10 in Algorithm 3
     */
    void AntColonyOptimization::initialize(const cv::Mat &InputArray)
    {
        // Initialize image
        __InputArray = Mat::zeros(InputArray.rows, InputArray.cols, InputArray.type());
        // Line 1 in Alg.3
        // Preprocessed image
        std::vector<cv::Mat> SubImages = preprocess(InputArray, __InputArray);
        // Line 2 in Alg.3
        // Compute orientation
        int height = __InputArray.rows;
        int width  = __InputArray.cols;
        __OrientationArray = orientation(SubImages, height, width);
        
        // Line 3-4 in Alg.3
        // Initialize ants positions
        double delta_y = height / K;
        double delta_x = width / K;
        
        // Line 5-9 in Alg.3
        __ants.clear();
        for (int i = 0; i<K; i++) {
            for (int j = 0; j<K; j++) {
                int a  = floor(j*delta_x+floor(delta_x/2));
                int b  = floor(i*delta_y+floor(delta_y/2));
                __ants.push_back(Point(a,b));
            }
        }
        
        // Line 10 in Alg.3
        // Initialize pheromones matrix
        int hq  = floor(height / m);
        int wq  = floor(width / m);
        __pheromones = Mat::zeros(hq, wq, CV_32F);
        RNG rng = RNG();
        rng.fill(__pheromones, RNG::NORMAL, 0, 1);
        
        return;
    }

    /*
     * This function implements our expriments (see Section 5.2)
     * We apply Algorithm 1 (sawing marks removal) on sub-images
     * The purpose of this is to better detect sawing marks in order to better remove them.
     */
    std::vector<cv::Mat> AntColonyOptimization::preprocess(const cv::Mat &InputArray, cv::Mat &OutputArray)
    {
        
        // Convert into grayscale. Beware that cv::imread create BGR image (not RGB image) !
        cvtColor(InputArray, OutputArray, COLOR_BGR2GRAY);
        
        // Resizing
        resize(OutputArray, OutputArray, Size(delta * InputArray.cols, delta * InputArray.rows), INTER_AREA);
        // Padding image
        int padx = ceil(OutputArray.cols / (float)div_x) * div_x;
        int pady = ceil(OutputArray.rows / (float)div_y) * div_y;
        copyMakeBorder(OutputArray, OutputArray, 0, pady - OutputArray.rows, 0, padx - OutputArray.cols, BORDER_REPLICATE);
        
        // Retrieve orientation with Jain filter and Sawing Marks removal
        OutputArray.convertTo(OutputArray, CV_32F);
        
        // Remove sawings marks on sub-image
        int sW = OutputArray.cols / div_x;
        int sH = OutputArray.rows / div_y;
        std::vector<cv::Mat> img_array;
        for (int i = 0; i<div_y; i++)
        {
            for (int j = 0; j<div_x; j++)
            {
                cv::Mat SubArray;
                OutputArray(cv::Range(i*sH,(i+1)*sH-1), cv::Range(j*sW, (j+1)*sW-1)).copyTo(SubArray);
                // Apply Algorithm 1
                SubArray = utils::sawing_marks_removal(SubArray, lambda, sigma);
                //SubArray.convertTo(SubArray, CV_32F);
                img_array.push_back(SubArray);
            }
        }
        return img_array;
    };

    /*
     * This function implements our expriments (see Section 5.2)
     * We apply Algorithm 2 (local orientation) on sub-images
     */
    cv::Mat AntColonyOptimization::orientation(const std::vector<cv::Mat> InputArrays, int H, int W)
    {
        int sW = W / div_x;
        int sH = H / div_y;
        
        // Compute Orientation for each subimage
        std::vector<cv::Mat> orientation;
        for (int i = 0; i < InputArrays.size(); i++)
        {
            cv::Mat SubArray      = InputArrays.at(i);
            orientation.push_back(Orienteer->estimate(SubArray));
        }
        
        // 3. Recreate a full image with orientation
        cv::Mat full_orientation    = Mat(H, W, InputArrays[0].type(), DBL_MIN);
        if (div_x != 1 && div_y != 1)
        {
            for (int i = 0; i < InputArrays.size(); i++)
            {
                int dx = (i % div_x) * sW;
                int dy = (i / div_x) * sH;
                cv::Mat SubArray = orientation.at(i);
                SubArray.copyTo(full_orientation(cv::Rect(dx, dy, sW-1, sH-1)));
            }
        }
        else
        {
            orientation.at(0).copyTo(full_orientation);
        }
        return full_orientation;
    };

    /*
     * This function implements line 13 to 18 in Algorithm 3
     * This function also implements Algorithm 4 since line 15 is the Alg.4
     */
    void AntColonyOptimization::update()
    {
        int delta = floor(n * omega * 0.5);
        int height = __InputArray.rows;
        int width  = __InputArray.cols;
        int hq  = floor(height / m);
        int wq  = floor(width / m);
        Mat sum_rho = Mat::zeros(hq, wq, CV_32F);
        
    #ifdef _OPENMP
        int nmax = omp_get_max_threads();
        cout << nmax << endl;
    #endif
    #pragma omp parallel num_thread(nmax)
        // Line 13 in Alg.3
        for (int i = 0; i<__ants.size(); i++)
        {
            // Line 14 in Alg.3
            Point ant = __ants.at(i);
            
            // Line 15 in Alg.3
            // This line corresponds to Alg.4
            /*
             * =============
             *  Algorithm 4
             * =============
             */
            // Line 1 in Alg.4
            vector<Point3d> L;
            
            // Line 2 in Alg.4
            for (int j = 0; j<n; j++) {
                for (int k = 0; k<n; k++) {
                    
                    // Line 3 in Alg.4
                    int dy = ant.y - delta + j*omega;
                    int dx = ant.x - delta + k*omega;
                    Mat h = __OrientationArray(Rect(dx, dy, omega, omega));
                    float theta = static_cast<float>(median(h));
                    
                    // Line 4 in Alg.4
                    int u = ant.x - delta + k * omega + omega/2;
                    int v = ant.y - delta + j * omega + omega/2;
                    u = floor(u/m);
                    v = floor(v/m);
                    L.push_back(Point3d(u, v, theta));
                }
            }
            
            // Line 5 in Alg.4
            Mat rho_k = accumulate_normals(L, hq, wq);
            rho_k.convertTo(rho_k, sum_rho.type());
            
            // Line 6-7 in Alg.4
            // Compute the Desirability Map - i.e. Distance Transform (DT)
            Mat mask = Mat::zeros(hq, wq, CV_8U) + Scalar(255);
            Mat eta = Mat::zeros(hq, wq, CV_32F) + Scalar(255);
            int aqx = floor(ant.x / m);
            int aqy = floor(ant.y / m);
            mask.at<uint8_t>(aqy, aqx) = 0;
            distanceTransform(mask, eta, DIST_L2, 3);
            normalize(eta, eta, 0.0, 1.0, NORM_MINMAX);
            pow(eta+Scalar(1), -1.0, eta);
            cv::Mat ttmp;
            cv::normalize(eta, ttmp, 0, 255, cv::NORM_MINMAX);
            cv::imwrite("/Users/remidecelle/Documents/eta.png", ttmp);
            // Line 8-9 in Alg.4
            // Compute Pheromones Probabilites (PP)
            cv::Mat tau, puv;
            __pheromones.convertTo(tau, CV_32F);
            eta.convertTo(eta, CV_32F);
            pow(tau, alpha, tau);
            pow(eta, beta, eta);
            multiply(eta, tau, puv);
            puv = puv / (cv::sum( puv )[0]);
            
            cv::normalize(puv, ttmp, 0, 255, cv::NORM_MINMAX);
            cv::imwrite("/Users/remidecelle/Documents/puv.png", ttmp);
            // Line 10 in Alg.4
            // Select a col
            Mat pcol;
            cv::reduce(puv, pcol, 0, cv::REDUCE_SUM);
            std::vector<float> _col(pcol.rows);
            pcol.row(0).copyTo(_col);
            std::discrete_distribution<int> cdistribution (_col.begin(), _col.end());
            
            // Line 11 in Alg.4
            int px = cdistribution(__generator);
            
            // Line 12 in Alg.4
            // Select a row
            std::vector<float> _row(pcol.rows);
            puv.col(px).copyTo(_row);
            normalize(_row, _row, 0, 1, NORM_MINMAX);
            std::discrete_distribution<int> rdistribution (_row.begin(), _row.end());
            
            // Line 13 in Alg.4
            int py = rdistribution(__generator);
            
            // Line 14 in Alg.4
            // Update ants position
            py = m * py;
            px = m * px;
            py = max(min(py, height - delta - 1), delta+1);
            px = max(min(px, width  - delta - 1), delta+1);
            __ants.at(i).x = px;
            __ants.at(i).y = py;
            
            /*
             * ==== END ====
             *  Algorithm 4
             * ==== END ====
             */
            
            // Line 16 in Alg.3 ( sum on k of rho_k^{(t)} )
            sum_rho = sum_rho + rho_k;
        }
        
        // Line 16 in Alg.3
        __pheromones = (1-gamma) * __pheromones + sum_rho;
        
        cv::Mat ttmp;
        cv::normalize(__pheromones, ttmp, 0, 255, cv::NORM_MINMAX);
        cv::imwrite("/Users/remidecelle/Documents/pheromones.png", ttmp);
        // Line 17 in Alg.3
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(__pheromones, &minVal, &maxVal, &minLoc, &maxLoc);
        Mat binary = __pheromones>=kappa*maxVal;
        Moments m = moments(binary);
        Point2d q(m.m10/m.m00, m.m01/m.m00);
        
        // Line 18 in Alg.3
        __distances.push_back(sqrt((__p.x-q.x)*(__p.x-q.x) + (__p.y-q.y)*(__p.y-q.y)));
        __p = q;
    }

    /*
     * ===============
     *   Algorithm 6
     * ===============
     * This function implements Algorithm 6
     */
    Point2d AntColonyOptimization::extract()
    {
        // Line 1 in Alg. 5
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(__pheromones, &minVal, &maxVal, &minLoc, &maxLoc);
        
        // Line 2 in Alg. 5
        Mat binary = __pheromones>=kappa*maxVal;
        
        // Line 3 in Alg. 5
        Moments M = moments(binary);
        
        // Line 4 in Alg. 5
        Point2d r(M.m10/M.m00, M.m01/M.m00);
        // Do not forget to get back to image coordinates
        r.y = m * r.y / delta;
        r.x = m * r.x / delta;
        
        // Line 5 in Alg. 5
        return r;
    }
    /*
     * ==== END ====
     *  Algorithm 6
     * ==== END ====
     */

    /*
     * This function allow to get all intermediaries images
     */
    vector<Mat> AntColonyOptimization::getIntermediaryImages(const cv::Mat &InputArray) const
    {
        vector<Mat> res;
        Mat OutputArray;
        // Convert into grayscale. Beware that cv::imread create BGR image (not RGB image) !
        cvtColor(InputArray, OutputArray, COLOR_BGR2GRAY);
        
        // Resizing
        resize(OutputArray, OutputArray, Size(delta * InputArray.cols, delta * InputArray.rows));
        
        // Padding image
        int padx = ceil(OutputArray.cols / (float)div_x) * div_x; // 4x4 subimages
        int pady = ceil(OutputArray.rows / (float)div_y) * div_y; // 4x4 subimages
        copyMakeBorder(OutputArray, OutputArray, 0, pady - OutputArray.rows, 0, padx - OutputArray.cols, BORDER_REPLICATE);
        
        // Retrieve orientation with Jain filter and Sawing Marks removal
        OutputArray.convertTo(OutputArray, CV_32F);
        Mat FilteredArray       = Mat(OutputArray.rows, OutputArray.cols, OutputArray.type(), DBL_MIN);
        Mat FFTArray            = Mat(OutputArray.rows, OutputArray.cols, OutputArray.type(), DBL_MIN);
        Mat FilteredFFTArray    = Mat(OutputArray.rows, OutputArray.cols, OutputArray.type(), DBL_MIN);
        Mat AngularArray        = Mat(OutputArray.rows, OutputArray.cols, OutputArray.type(), DBL_MIN);

        // Remove sawings marks on sub-image
        int sW = OutputArray.cols / div_x;
        int sH = OutputArray.rows / div_y;
        for (int i = 0; i<div_y; i++)
        {
            for (int j = 0; j<div_x; j++)
            {
                int dy = i * sH;
                int dx = j * sW;
                cv::Mat SubArray        = cv::Mat(sH, sW, CV_32F, DBL_MIN);
                cv::Mat SubFFTArray     = cv::Mat(sH, sW, CV_32F, DBL_MIN);
                cv::Mat SubFFFTArray    = cv::Mat(sH, sW, CV_32F, DBL_MIN);
                OutputArray(cv::Range(i*sH,(i+1)*sH-1), cv::Range(j*sW, (j+1)*sW-1)).copyTo(SubArray);
                // Apply FFT filtering
                SubArray = utils::get_intermediary_SMR(SubArray, SubFFTArray, SubFFFTArray, lambda, sigma);
                
                SubArray.convertTo(SubArray, CV_32F);
                cv::Mat SubAngularArray = Orienteer->estimate(SubArray);

                normalize(SubArray, SubArray, 0, 1, NORM_MINMAX);
                normalize(SubFFTArray, SubFFTArray, 0, 1, NORM_MINMAX);
                normalize(SubFFFTArray, SubFFFTArray, 0, 1, NORM_MINMAX);
                cv::Rect subrect = cv::Rect(dx, dy, sW-1, sH-1);

                SubAngularArray.copyTo(AngularArray(subrect));
                SubArray.copyTo(FilteredArray(subrect));
                SubFFTArray.copyTo(FFTArray(subrect));
                SubFFFTArray.copyTo(FilteredFFTArray(subrect));
            }
        }
        res.push_back(FFTArray);
        res.push_back(FilteredFFTArray);
        res.push_back(FilteredArray);
        res.push_back(AngularArray);

        return res;
    }
};
