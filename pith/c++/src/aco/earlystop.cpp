//
//  earlystop.cpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#include "earlystop.hpp"

#include "aco.hpp"

namespace aco
{
    namespace earlystop
    {
        bool DistanceStop::checkCriteria(const AntColonyOptimization *_aco)
        {
            return _aco->__distances.at(0) <= epsilon;
        }
    
        /*
         * This function implements line 10 to 13 in Algorithm 3
         */
        bool IterAndDistanceStop::checkCriteria(const AntColonyOptimization *_aco)
        {
            current_it++;
            if ( current_it >= max_num_it ) return true;

            double meandis = DBL_MAX;
            int bw = 5; // look 5 iterations backward
            if (current_it >= bw)
            {
                meandis = 0;
                for (int k=current_it-bw; k<current_it; k++) {
                    meandis += _aco->__distances.at(k);
                }
                meandis = meandis / 5.0;
            }
            if (meandis <= epsilon) return true;
            return false;
        }
    }
}
