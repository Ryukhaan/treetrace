//
//  earlystop.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef earlystop_hpp
#define earlystop_hpp

#include <stdio.h>
#include <vector>
#include <cfloat>
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

namespace aco
{
    class AntColonyOptimization;

    namespace earlystop
    {
        static const int    DEFAULT_MAX_NUM_ITER    = 50;
        static const double DEFAULT_EPSILON         = 1.0;
    
        class EarlyStop
        {
        public:
            EarlyStop() {};
            virtual ~EarlyStop() {};
            virtual bool checkCriteria(const AntColonyOptimization *aco)
            {
                return true;
            };
        };
    
        class MaxNumItStop : virtual public EarlyStop
        {
        public:
            MaxNumItStop(): max_num_it(DEFAULT_MAX_NUM_ITER), current_it(0) {};
            MaxNumItStop(int n): max_num_it(n), current_it(0) {};
            virtual ~MaxNumItStop() {};
            
            virtual bool checkCriteria(const AntColonyOptimization *aco)
            {
                current_it ++;
                return current_it >= max_num_it;
            }
            
            virtual void operator++()
            {
                current_it++;
            }
            
            virtual void reset()
            {
                current_it = 0;
            }
            
        public:
            void setMaxNumIt(int n)
            {
                max_num_it = n;
            };
            
            void setCurrentIt(int n)
            {
                current_it = 0;
            };
            
            int getMaxNumIt()
            {
                return max_num_it;
            };
            
            int getCurrentIt()
            {
                return current_it;
            };

            bool showingIterations()
            {
                return current_it == 0 || current_it == max_num_it/2;
            }
            
        protected:
            int current_it;
            int max_num_it;
        };
    
        class DistanceStop : virtual public EarlyStop
        {
        public:
            DistanceStop(): epsilon(DEFAULT_EPSILON) {};
            DistanceStop(double e): epsilon(e) {};
            virtual ~DistanceStop() {};
            
            virtual bool checkCriteria(const AntColonyOptimization *aco);
        
        public:
            void setEpsilon(double e)
            {
                epsilon = e;
            };
            
            double getEpsilon()
            {
                return epsilon;
            };

            
        protected:
            double epsilon;
        };

        class IterAndDistanceStop : public DistanceStop, MaxNumItStop
        {
        public:
            IterAndDistanceStop()
            {
                epsilon     = DEFAULT_EPSILON;
                max_num_it  = DEFAULT_MAX_NUM_ITER;
                current_it  = 0;
            };
            IterAndDistanceStop(double e, int it)
            {
                epsilon     = e;
                max_num_it  = it;
                current_it  = 0;
            };
            virtual ~IterAndDistanceStop() {};
            
            void reset()
            {
                current_it = 0;
            }
            
            int getCurrentIt()
            {
                return current_it;
            }

            bool showingIterations()
            {
                return current_it == 0 || current_it == max_num_it/2;
            }
            
            bool checkCriteria(const AntColonyOptimization *aco);
            
        };
    }
}
#endif /* earlystop_hpp */
