//
//  aco.hpp
//  AntColonyPith
//
//  Created by Remi DECELLE on 23/07/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
#ifndef aco_hpp
#define aco_hpp

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

#include "earlystop.hpp"
#include "../orientation/orienteer.hpp"
#include "../normals/bresenham.hpp"

namespace aco
{
    //template <typename T, typename S>
    class AntColonyOptimization
    {
    public:
        AntColonyOptimization() {};
        AntColonyOptimization(orientation::Orienteer *Orienteer,
                              double scale,
                              int num_ants, int num_blocs, int bloc_size, int quantizer,
                              double kappa, double evaporation_rate, double alpha, double beta,
                              earlystop::EarlyStop *Stopper, int seed = 1);
        virtual ~AntColonyOptimization() {};
        //virtual Point2d compute(const cv::Mat &InputArray);
        
        virtual void update();
        virtual Point2d extract();
        
        std::vector<cv::Mat> preprocess(const cv::Mat &InputArray, cv::Mat &OutputImage);
        cv::Mat orientation(const std::vector<cv::Mat> InputArrays, int H, int W);
        void initialize(const cv::Mat &InputArray);
        
        //Point2d applyACO(const cv::Mat &InputArray, const cv::Mat &OrientationArray);
        std::vector<cv::Mat> getIntermediaryImages(const cv::Mat &InputArray) const;
        
    public:
        void setScale(double s)
        {
            delta = s;
        }
        
        void setNumAnts(int n)
        {
            K = n;
        }
        
        void setNumBlocs(int b)
        {
            n = b;
        }
        
        void setQuantizer(int q)
        {
            m = q;
        }
        
        void setDivX(int x)
        {
            div_x = x;
        }
        
        void setDivY(int y)
        {
            div_y = y;
        }
        
        void setLambda(double l)
        {
            lambda = l;
        }
        
        void setSigma(double s)
        {
            sigma = s;
        }
        
    public:
        
        double getScale() const
        {
            return delta;
        }
        
        double getNumAnts() const
        {
            return K;
        }

        double getDelta() const
        {
            return delta;
        }
        
    protected:
        double delta;
        int K;
        int n;
        int omega;
        int m;
        double kappa;
        double gamma;
        double alpha;
        double beta;
        
        // Line removal
        double sigma;
        double lambda;
        
        int div_x;
        int div_y;
        
        orientation::Orienteer  *Orienteer;
        
        Point2d __p;
    
    public:
        std::default_random_engine __generator;
    
    public:
        vector<Point> __ants;
        Mat __pheromones;
        vector<double> __distances;
        
        Mat __InputArray;
        Mat __OrientationArray;
    };
}

#endif /* aco_hpp */
