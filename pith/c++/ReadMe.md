# Ant Colony Optimization for Estimating Pith Position on Images of Tree Log Ends

Version 1.0
Last update: 08/03/2021
Authors: 
-	Rémi Decelle, remi.decelle@loria.fr
-	Phuc Ngo, hoai-diem-phuc.ngo@loria.fr
-	Isabelle Debled-Rennesson, isabelle.debled-rennesson@loria.fr
-	Frédéric Mothe, frederic.mothe@inrae.fr
-	Fleur Longuetaud, fleur.longuetaud@inrae.fr

## Get started

#### 1. Folders
All files are in src/ folder.
There are 5 subfolders:
    1.  **aco** including the early-stop criteria algorithm (earlystop.cpp) corresponding to the line 11 to 13 in Alg.3 and the main code of Alg.3, Alg.4 and Alg.6 (aco.cpp);
    2.  **normals** including the algorithm for pheromones deposit (Alg.5);
    3.  **orientation** including local orientation computation (Alg.2);
    4.  **ui** including codes for animation and future interface. 
    
Algorithm 1 is implemented in the file **preprocessing.cpp**
Algorithm 2 is implemented in the file **RidgeOrienteer.cpp**
Algorithm 3 is implemented in the files **earlystop.cpp**, **aco.cpp** and **main.cpp**
Algorithm 4 is implemented in the file **aco.cpp** (function *update*)
Algorithm 5 is implemented in the file **bresenham.cpp**
Algorithm 6 is implemented in the file **aco.cpp** (function *extract*)

### 2.    Dependencies

Required: 

-  `Opencv 4.+`

#### 2.    Compilation Instructions

The following steps allow to compile the code:
``` 
mkdir build/
cd build/
cmake ..
make
```

#### 3.    Examples of usage

Here some examples of usage:
```
./AntColonyPith ../samples/harvest.jpeg
./AntColonyPith --input ../samples/logyard.jpg --ant=10 --animated=false
```

To have information about arguments:
```
./AntColonyPith --help
```
or
```
./AntColonyPith -h
```
