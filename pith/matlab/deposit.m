% Decelle R�mi
%
% Hough-like accumulation
% Drawing lines (bresenham) for some pixels according to each pixel's
% orientation and position
%
% March 2019

% Parameters
% ----------
% G : Logical matrix indicating which pixel to accumulate and their
% position
% O : Orientation matrix
%
% Returns
% -------
% A : Accumulator

function [A] = deposit(G, O)

[h, w]          = size(G);
A               = zeros(size(G));
% Get position of pixels to accumulate
[erows, ecols]  = find(G);
n               = length(erows);

% For each pixel to accumualate
for i=1:n
    % Get its position
    row = erows(i);
    col = ecols(i);
    % Get its orientation
    theta = O(row, col);
    % Check if theta is not to small
    if abs(theta - 0.001) < 0
        continue
    end
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line to draw : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    % Retrieve (X,Y) where line is at image's borders
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersection with y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersection with x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersection with y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    % Intersection  with x = w
    if a < 1e16
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get (X,Y) at image borders
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    if length(X) < 2 ||length(Y) < 2
        continue
    end
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    
    % Set of points from bresenham algorithm
    [lx, ly] = bresenham(sx, sy, ex, ey);
    % Convert into indices
    idxs     = sub2ind(size(G), ly, lx);
    % Increment by one amount of pheromone
    A(idxs)  = A(idxs) + 1;
end
end