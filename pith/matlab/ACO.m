% Decelle R�mi 
% Phd Student at INRAE & LORIA
% The University of Lorraine, France
%
% First version: January 2020

function [px, py] = ACO(image, orientim, varargin)
%% Parse inputs
p = inputParser;

% Resize
defaultScale        = 0.4;

% Ant pheromone deposit parameters
defaultNAnts        = 4; 
defaultClusterSize  = 3; 
defaultBlocSize     = 8;

% Update
defaultGamma        = 0.07;
defaultQuantizer    = 5;
defaultAlpha        = 2.0;
defaultBeta         = 1.0;

% Misc
defaultAnimation    = false;
defaultMaxNumIter   = 50;
defaultThresh       = 2;
defaultIThresh      = 5;
defaultUIImage      = image;

defaultSpeed        = 0.01;

addParameter(p,'scale',defaultScale,@isnumeric);
addParameter(p,'ants',defaultNAnts,@isnumeric);
addParameter(p,'block',defaultBlocSize,@isnumeric);
addParameter(p,'cluster',defaultClusterSize,@isnumeric);

addParameter(p,'alpha',defaultAlpha,@isnumeric);
addParameter(p,'beta',defaultBeta,@isnumeric);
addParameter(p,'gamma',defaultGamma,@isnumeric);
addParameter(p,'quantizer',defaultQuantizer,@isnumeric);

addParameter(p,'iter',defaultMaxNumIter,@isnumeric);
addParameter(p,'thresh',defaultThresh,@isnumeric);
addParameter(p,'ithresh', defaultIThresh, @isnumeric);
addParameter(p,'animation',defaultAnimation,@islogical);
addParameter(p,'uiimage', defaultUIImage, @isnumeric);
addParameter(p,'speed', defaultSpeed, @isnumeric);

parse(p,varargin{:});
scale       = p.Results.scale;

alpha       = p.Results.alpha;
beta        = p.Results.beta;
gamma       = p.Results.gamma;
quantizer   = p.Results.quantizer;

bloc_size   = p.Results.block;
num_blocs   = p.Results.cluster;
num_ants    = p.Results.ants;

animation   = p.Results.animation;
num_max_it  = p.Results.iter;
thresh      = p.Results.thresh;
ithresh     = p.Results.ithresh;
uiimage     = p.Results.uiimage;
speed       = p.Results.speed;


%% Initialize
old_px          = 0;
old_py          = 0;
dis             = zeros(1,num_max_it);

I               = image;
[H, W]          = size(I);
ants_position   = zeros(num_ants*num_ants, 3);
% Half size of an ant neighborhood
delta           = ceil(num_blocs * bloc_size / 2);
% Size of pheromone matrix
Hq              = ceil(H/quantizer);
Wq              = ceil(W/quantizer);
% Space between each ant
hg              = H / num_ants;
wg              = W / num_ants;
% Initialize ants positions like a uniform grid
for i=1:num_ants
    for j=1:num_ants
        k = (i-1) * num_ants + j;
        ants_position(k,1) = ceil((i-1)*hg + ceil(hg/2));
        ants_position(k,2) = ceil((j-1)*wg + ceil(wg/2));
        ants_position(k,3) = 1;
    end
end
% Initialize pheromones matrix (could be randomized or set to a default value)
%pheromones = 30 * ones(Hq, Wq);
%pheromones = randi(20, Hq, Wq);
pheromones = rand(Hq, Wq);

%% UI
if animation
    [gm, ants, pith] = uiants('', '', '', ants_position, pheromones, uiimage, quantizer, 'init');
end

%% Get starting !
for it=1:num_max_it
    
    % UI
    if animation
        [gm, ants, pith] = uiants(gm, ants, pith, ants_position, pheromones, uiimage, quantizer);
    end
    
    % Rho matrix
    rho = zeros(Hq, Wq);
    % Move each ant
    for i=1:size(ants_position,1)   
        ay = ants_position(i,1);
        ax = ants_position(i,2);
        anto = zeros(Hq, Wq);
        antg = zeros(Hq, Wq);
        for iy=0:num_blocs-1
            for ix=0:num_blocs-1
                % Retrieve local orientation of a bloc
%                 mR = reliability( ...
%                     uint16(ay-delta+iy*bloc_size):uint16(ay-delta+(iy+1)*bloc_size-1), ...
%                     uint16(ax-delta+ix*bloc_size):uint16(ax-delta+(ix+1)*bloc_size-1));
                mO = orientim( ...
                    uint16(ay-delta+iy*bloc_size):uint16(ay-delta+(iy+1)*bloc_size-1), ...
                    uint16(ax-delta+ix*bloc_size):uint16(ax-delta+(ix+1)*bloc_size-1));
                %[~, idx] = max(mR(:));
                % Take the median value
                %theta = mO(idx);
                theta = median(mO(:));
                
                % Compute the coordinate of the block centre
                cx = ax - delta + ix * bloc_size + bloc_size/2;
                cy = ay - delta + iy * bloc_size + bloc_size/2;
                cx = ceil(cx/quantizer);
                cy = ceil(cy/quantizer);
                anto(cy,cx) = theta;
                antg(cy,cx) = 1;
            end
        end
        % Draw lines for each block according to their orientation
        A = deposit(antg, anto);

        % Accumulate pheromones
        rho = rho + A;
        
        % Compute desirability
        desirability = zeros(Hq, Wq);
        desirability(ceil(ay / quantizer), ceil(ax / quantizer)) = 1;
        desirability = bwdist(desirability);
        desirability = 1 ./ (desirability+1);
        
        % Compute probability to moves to a pixel
        P = (pheromones.^alpha) .* (desirability.^beta);
        %P = pheromones .* desirability;
        P = P ./ sum(P, 'all');
        [px, py] = pinky(linspace(1, Wq, Wq), linspace(1, Hq, Hq), P);
        
        % Update ant position
        py = quantizer * py;
        px = quantizer * px;
        py = max(min(py, H - delta), delta+1);
        px = max(min(px, W - delta), delta+1);
        
        ants_position(i,1) = py;
        ants_position(i,2) = px;
        
    end
    
    % Update pheromone matrix
    pheromones = (1-gamma) * pheromones + rho;
    
    M           = max(pheromones(:));
    [rows,cols] = find(pheromones>=0.8*M);
    py          = mean(rows(:));
    px          = mean(cols(:));
    di          = (old_px-px)^2+(old_py-py)^2;
    old_px      = px;
    old_py      = py;
    dis(it)   = di;
    
    if (it > ithresh && mean(dis(it-ithresh:it))< thresh)
        break;
    end

end
% Take the barycenter as the pith
M           = max(pheromones(:));
[rows,cols] = find(pheromones>=0.8*M);
py          = mean(rows(:));
px          = mean(cols(:));
px          = quantizer * px / scale;
py          = quantizer * py / scale;
end