% Decelle R�mi 
% Phd Student at INRAE & LORIA
% The University of Lorraine, France
%
% First version: January 2020

function J = filtering(img, sigma, band, opt)
if ~exist('opt','var') || isempty(opt)
    debug = 0;
else
    debug = 1;
end

if length(size(img)) == 3
    I = rgb2gray(img);
else
    I = img;
end

% Get middle and define sigma for gaussian
[h, w] = size(I);
xc = ceil(w / 2 + 1);
yc = ceil(h / 2 + 1);

% Create circle to occult boundaries
[X, Y] = meshgrid(1:w, 1:h);

% Compute FFT
Fc = fftshift(fft2(I));
Fc = log(abs(Fc)+1);
Fs = Fc;

% Remove horizontal and vertical value
hor = 1 - ((abs(X-xc)-1) < 1);
ver = 1 - ((abs(Y-yc)-1) < 1);
Fc = Fc .* im2double(hor);
Fc = Fc .* im2double(ver);

% Band-pass filter
myCircle = ((X-xc).^2 + (Y-yc).^2 > (h/3).^2) | ((X-xc).^2 + (Y-yc).^2 <= (h/band).^2);
Fc = Fc .* im2double(1-myCircle);

% Get only 0.875 of the maximum value
T = 0.875;
M = max(Fc(:));
[rows, cols] = find(Fc>=T*M);

% PCA
XY      = [cols, rows];
C       = cov(XY);
[W,D]   = eig(C);
if D(1) > D(4)
    certainty = (D(1) - D(4)) / D(1);
    slope = W(2,1) / W(1,1);
else
    certainty = (D(4) - D(1)) / D(4);
    slope = W(2,2) / W(1,2);
end

% Compute line in the same direction as the retrieved eigenvector
[X, Y] = meshgrid(-floor(w/2):ceil(w/2)-1, -floor(h/2):ceil(h/2)-1);
a = slope;
k = 1;
%b = 0;
w = max(abs(a), 1);
lineFilter = (-k*w < -a * X + Y) & (-a * X + Y <= k*w);

% Check if certainty if higher than 0.5
if certainty > 0.66
    lineFilter = 1 - lineFilter;
    lineFilter = imgaussfilt(im2double(lineFilter), sigma);
    lineFilter = (lineFilter - min(lineFilter(:))) / (max(lineFilter(:)) - min(lineFilter(:)));
else
    lineFilter = ones(size(X));
end

% Remove line
Ft = fftshift(fft2(I));
res = Ft .* lineFilter;
J = real(ifft2(ifftshift(res)));
J = normaliser(J);

% Display each step
if debug
    f1 = figure(1); f1.Name = 'Img originale';
    f2 = figure(2); f2.Name = 'FFT originale';
    f3 = figure(3); f3.Name = 'Passe-bande';
    f4 = figure(4); f4.Name = 'Seuille';
    f5 = figure(5); f5.Name = 'Droites';
    f6 = figure(6); f6.Name = 'Resultat';
    
    figure(f1), imagesc(img), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f2), imagesc(Fs), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f3), imagesc(Fc), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f4), imagesc(Fc>=T*M), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f5), imagesc(lineFilter),  colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f6), imagesc(J), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    pause();
end
end

function Y = normaliser(X)
M0 = 0;
V0 = 1;
M = mean(X(:));
V = std(X(:));
f1 = X > M;
Y1 = M0 + sqrt(V0 .* (X-M).^2 / V);
Y2 = M0 - sqrt(V0 .* (X-M).^2 / V);
Y = Y1 .* f1 + Y2 .* (1-f1);
end



