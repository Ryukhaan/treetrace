% Decelle R�mi 
% Phd Student at INRAE & LORIA
% The University of Lorraine, France
%
% First version: January 2020
% Last version: March 2020

function [gm, ants, pith] = uiants(gm, ants, pith, ants_position, pheromones, uiimage, quantizer, init, speed)

% Check if it initialize or not
if ~exist('init','var') || isempty(init)
    initialize = 0;
else
    initialize = 1;
end

if ~exist('speed','var') || isempty(speed)
    speed = 0.01;
end

[Hq, Wq] = size(pheromones);

if initialize
    figure(1), clf;
    figure(2), clf;
    
    % Intersect image
    figure(1);
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    if ~exist('gm','var') || isempty(gm)
        gm = imagesc(log(pheromones+1));
    end
    colormap gray;
    axis image;
    
    % Display ants and remove axis
    figure(2),
    imagesc(uiimage);
    colormap gray, axis image, hold on;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    yplot = ants_position(:,1);
    xplot = ants_position(:,2);
    if ~exist('ants', 'var') || isempty(ants)
        ants  = scatter(xplot, yplot, 64, [1 1 0.3], 'x', 'LineWidth', 2);
    end
    
    % Draw pith estimation
    M = max(pheromones(:));
    [rows,cols] = find(pheromones>=0.8*M);
    pith_y = mean(rows(:));
    pith_x = mean(cols(:));
    if ~exist('pith', 'var') || isempty(pith)
        pith  = scatter(quantizer * pith_x, quantizer * pith_y, 64, 'ro', 'filled');
    end
    drawnow;
else
    % Update ants position
    set(gm, 'CData', log(pheromones+1));
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    % Display pheromones on uiimage
    %         tmp         = imresize(memorizer, [H, W], 'nearest');
    %         r           = uiimage(:,:,1);
    %         r(tmp>=1)   = 0;
    %         g           = uiimage(:,:,2);
    %         g(tmp>=1)   = 255;
    %         b           = uiimage(:,:,3);
    %         b(tmp>=1)   = 0;
    %         set(lines, 'CData', cat(3,r,g,b), 'AlphaData', 1);
    %         drawnow;
    
    yplot = ants_position(:,1);
    xplot = ants_position(:,2);
    set(ants, 'XData', xplot, 'YData', yplot);
    
    % Update pith potision
    M = max(pheromones(:));
    [rows,cols] = find(pheromones>=0.8*M);
    pith_y = mean(rows(:));
    pith_x = mean(cols(:));
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    set(pith, 'XData', quantizer * pith_x, 'YData', quantizer * pith_y);
    hold off;
    
    % Display cluster of block for the first ant
    %         ay = ants_position(1,1);
    %         ax = ants_position(1,2);
    %         xx = ax-delta;
    %         j = 1;
    %         for ii=0:num_blocs
    %             yy = ay-delta+ii*bloc_size;
    %             set(pl(j), 'XData', [xx,xx+num_blocs*bloc_size], 'YData', [yy,yy]);
    %             j = j + 1;
    %         end
    %         yy = ay-delta;
    %         for ii=0:num_blocs
    %             xx = ax-delta+ii*bloc_size;
    %             set(pl(j), 'XData', [xx,xx], 'YData', [yy,yy+num_blocs*bloc_size]);
    %             j = j + 1;
    %         end
    
    drawnow;
    pause(speed);
end
end