import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import cv2 as cv
import numpy as np

import time
import argparse

import SternBrocot
import estimater
import DSL

def parsing():
	desc   = "Compute for one pixel the longest discrete line"
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument('--x', 
		type=int, default=200, help='X-coordinate')
	parser.add_argument('--y', 
		type=int, default=230, help='Y-coordinate')
	#parser.add_argument('--folder',
	#	type=str, default='./out/', help='Path folder for Dilatations/Erosions')
	parser.add_argument('--image',
		type=str, default='circle_noise.png', help='Image name')
	args = parser.parse_args()
	return args

def createXYZ(args):
	xs = []
	ys = []
	zs = []
	colors_erodes = []
	colors_dilates = []
	for i in range(0, len(args.dilates)):
	    for j in range(0, len(args.dilates[i])):
	        a, b = args.fracs[i][j]
	        xs.append(2*(i+1)+1)
	        ys.append(b)
	        zs.append(a)
	        v    = args.erodes[i][j][args.y,args.x]
	        colors_erodes.append(v)
	        v    = args.dilates[i][j][args.y,args.x]
	        colors_dilates.append(v)
	return xs, ys, zs, colors_erodes, colors_dilates

def scatter_plot(I, args):
	n = len(args.dilates)
	dn = int(np.ceil(np.sqrt(n)))
	fig, ax = plt.subplots(dn, dn, figsize=(7,7))
	ax = ax.flatten()
	cm_hot = cm.get_cmap('hot', 256)
	newcolors = cm_hot(np.linspace(0, 0.7, 256))
	newcolors = newcolors
	newcmp = ListedColormap(newcolors)

	kk = 2*(n+1)+1
	sum_tree = np.zeros((kk, kk))
	count_tr = np.ones((kk, kk))
	for i in range(0, n):
		ys = []
		xs = []
		cs = []
		for j in range(0, len(args.dilates[i])):
			a, b = args.fracs[i][j]
			xs.append(b)
			ys.append(a)
			cs.append(args.erodes[i][j][args.y, args.x])
			sum_tree[b, a] += args.erodes[i][j][args.y, args.x]
			count_tr[b, a] += 1
		im = ax[i].scatter(xs, ys, s=8, c=cs, alpha=1, cmap=newcmp)
		ax[i].set_facecolor((0.5, 0.6, 0.8))
		ax[i].set_title("L={}, Pavage={},.., {}".format(2*(i+1)+1, -i, i))
	fig.subplots_adjust(wspace=0.5, hspace=0.5)
	cbar_ax = fig.add_axes([0.95, 0.15, 0.01, 0.75])
	fig.colorbar(im, cax=cbar_ax)
	#fig.canvas.manager.window.showMaximized()
	sum_tree = np.divide(sum_tree, count_tr)
	xs = []
	ys = []
	cs = []
	for q in range(0, len(args.dilates[n-1])):
		a, b = args.fracs[-1][q]
		xs.append(b)
		ys.append(a)
		cs.append(sum_tree[b,a])
	ax[i].scatter(xs, ys, s=8, c=cs, alpha=1, cmap=newcmp)
	ax[i-1].cla()
	ax[i-1].imshow(sum_tree)
	i += 1
	ind = np.unravel_index(np.argmax(sum_tree, axis=None), sum_tree.shape)
	print(ind)
	#max_dilates = np.zeros((n, 1))
	#max_erodes  = np.zeros((n, 1))
	#min_dilates = np.zeros((n, 1))
	#min_erodes  = np.zeros((n, 1))
	#arg_dilates = np.zeros((n, 1))
	#arg_erodes  = np.zeros((n, 1))
	#for length in range(0, n):
	#	d = np.array(np.array(args.dilates[length][:])[:, args.y, args.x])
	#	e = np.array(np.array(args.erodes[length][:])[:, args.y, args.x])
	#	max_dilates[length] = np.amax(d)
	#	max_erodes[length]  = np.amax(e)
	#	min_dilates[length] = np.amin(d)
	#	min_erodes[length]  = np.amin(e)
	#	arg_dilates[length] = np.argmax(d)
	#	arg_erodes[length]  = np.argmax(e)
	#arg2dilate = np.argmax(min_dilates, axis=0)
	#arg2erode  = np.argmax(max_erodes, axis=0)
	#max_dilate = np.amax(max_dilates, axis=0)
	#max_erode  = np.amax(max_erodes, axis=0)
	#min_dilate = np.amin(min_dilates, axis=0)
	#min_erode  = np.amin(min_erodes, axis=0)
	#Gdir_dil  = max_dilate - min_dilate
	#Gdir_ero  = max_erode - min_erode
	#Gdir = np.maximum(Gdir_dil, Gdir_ero)
	#if Gdir == Gdir_ero:
	#    p = arg2erode[0]
	#    q = arg_erodes[p]    
	#else:
	#    p = arg2dilate[0]
	#    q = arg_dilates[p]
	
	bb = ind[0] - kk if ind[0] > n+1 else ind[0]
	aa = ind[1] - kk if ind[1] > n+1 else ind[1]

	#aa, bb = args.fracs[-1][ind]
	ww = np.maximum(abs(aa), abs(bb))
	line, _, _ = DSL.construct(aa, bb, 0, ww, 11)
	subI = np.array(I[args.y-n:args.y+n, args.x-n:args.x+n], dtype=np.int32)
	for p in range(0, line.shape[0]):
		subI[-line[p,1]+n, line[p,0]+n] = 257
	viridis = cm.get_cmap('gray', 256)
	newcolors = viridis(np.linspace(0, 1, 256))
	newcolors[-1, :] = np.array([1., 0., 0., 1.])
	newcmp = ListedColormap(newcolors)
	ax[i].imshow(subI, cmap=newcmp)
	ax[i].scatter(n, n, c="#00ff00", marker='+')
	ax[i].grid(which='minor', axis='both', linestyle='-', color='black', linewidth=1)
	print("{}/{} - {}".format(aa,bb,11))
	plt.show()


if __name__ == '__main__':
	args = parsing()
	args.dilates = np.load('./out/dilates.npy', allow_pickle=True)
	args.erodes  = np.load('./out/erodes.npy', allow_pickle=True)
	args.fracs   = np.load('./out/fracs.npy', allow_pickle=True)
	xs, ys, zs, cd, ce = createXYZ(args)
	I = cv.imread("./images/{}".format(args.image))
	I = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
	I = cv.normalize(I, None, 0, 255, cv.NORM_MINMAX)
	scatter_plot(I, args)


