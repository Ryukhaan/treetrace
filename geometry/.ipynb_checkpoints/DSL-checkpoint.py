import numpy as np
from enum import Enum

class Octant(Enum):
    NONE    = 0
    FIRST   = 1
    SECOND  = 2
    THIRD   = 3
    FOURTH  = 4
    FIFTH   = 5
    SIXTH   = 6
    SEVENTH = 7
    EIGHTH  = 8

def check_octant(a,b):
    if   ( 0 <= a and a < b):
        return Octant.FIRST
    elif ( 0 <= b and b <= a):
        return Octant.SECOND
    elif ( a < 0 and 0 < b and b < np.abs(a)):
        return Octant.THIRD
    elif ( a < 0 and 0 <= b and np.abs(a) <= b):
        return Octant.FOURTH
    elif ( a <= 0 and b < 0 and a > b):
        return Octant.FIFTH
    elif ( a < 0 and a <= b):
        return Octant.SIXTH
    elif ( b <= 0 and 0 > a and a < np.abs(b)):
        return Octant.SEVENTH
    elif ( b < 0 and 0 > a and np.abs(b) <= a):
        return Octant.EIGHTH
    else:
        return Octant.NONE
    
def construct_line(a, b, mu, omega, l):
    pixel_idx_list = np.zeros((1,2), np.int32)
    dx1   = 0
    dy1   = 0
    err1  = 0
    dx2   = 0
    dy2   = 0
    err2  = 0
    
    for i in range(1, l+1):
        dx1   = dx1 + 1
        err1 = err1 + a
        if err1 >= (mu + omega):
            err1 = err1 - b
            dy1   = dy1 + 1
        pixel_idx_list = np.append(pixel_idx_list, [[dx1, dy1]], axis=0)
        
        dx2   = dx2  - 1
        err2  = err2 - a
        if (err2 < mu):
            err2 = err2 + b
            dy2   = dy2 - 1
        pixel_idx_list = np.append(pixel_idx_list, [[dx2, dy2]], axis=0)
    
    return pixel_idx_list, err1, err2

def construct(a, b, mu, omega, l):
    octant = check_octant(a, b)
    if octant == Octant.FIRST:
        pixel_idx_list, err1, err2 = construct_line(a, b, mu, omega, l)
    elif octant == Octant.SECOND:
        pixel_idx_list, err1, err2 = construct_line(b, a, mu, omega, l)
        # (x,y) -> (y,x)
        pixel_idx_list             = np.fliplr(pixel_idx_list)
    elif octant == Octant.THIRD:
        pixel_idx_list, err1, err2 = construct_line(b, -a, mu, omega, l)
        # (x,y) -> (y,-x)
        pixel_idx_list             = np.fliplr(pixel_idx_list)
        pixel_idx_list[:, 1]       = -pixel_idx_list[:, 1]
    elif octant == Octant.FOURTH:
        pixel_idx_list, err1, err2 = construct_line(-a, b, mu, omega, l)
        # (x,y) -> (x, -y)
        pixel_idx_list[:, 1]       = -pixel_idx_list[:, 1]
    elif octant == Octant.FIFTH:
        pixel_idx_list, err1, err2 = construct_line(-a, -b, mu, omega, l)
        # (x,y) -> (-x, -y)
        pixel_idx_list = -pixel_idx_list
    elif octant == Octant.SIXTH:
        pixel_idx_list, err1, err2 = construct_line(-b, -a, mu, omega, l)
        # (x,y) -> (-y, -x)
        pixel_idx_list = -pixel_idx_list
        pixel_idx_list = np.fliplr(pixel_idx_list)
    elif octant == Octant.SEVENTH:
        pixel_idx_list, err1, err2 = construct_line(-b, a, mu, omega, l)
        # (x,y) -> (-y, x)
        pixel_idx_list[:,1] = -pixel_idx_list[:,1]
        pixel_idx_list = np.fliplr(pixel_idx_list)
    else:
        pixel_idx_list, err1, err2 = construct_line(-a, b, mu, omega, l)
        # (x,y) -> (-x,y)
        pixel_idx_list[:, 0] = -pixel_idx_list[:, 0]
    return pixel_idx_list, err1, err2


def extend(pixel_idx_list, a, b, mu, omega, err1, err2):
    octant = check_octant(a, b)
    if octant == Octant.FIRST:
        pixel_idx_list, de1, de2  = extend_line(pixel_idx_list,
                                                a, b, mu, omega, err1, err2)
    elif octant == Octant.SECOND:
        tmp_list = np.fliplr(pixel_idx_list)
        pixel_idx_list, de1, de2 = extend_line(tmp_list,
                                               b, a, mu, omega, err1, err2)
        pixel_idx_list = np.fliplr(pixel_idx_list)
    elif octant == Octant.THIRD:
        tmp_list       = pixel_idx_list
        tmp_list[:, 1] = -tmp_list[:, 1]
        tmp_list       = np.fliplr(tmp_list)
        pixel_idx_list, de1, de2 = extend_line(tmp_list,
                                               b, -a, mu, omega, err1, err2)
        pixel_idx_list             = np.fliplr(pixel_idx_list)
        pixel_idx_list[:, 1]       = -pixel_idx_list[:, 1]
    elif octant == Octant.FOURTH:
        tmp_list = pixel_idx_list
        tmp_list[:, 1] = -pixel_idx_list[:, 1]
        pixel_idx_list, de1, de2 = extend_line(tmp_list,
                                               -a, b, mu, omega, err1, err2)
        pixel_idx_list[:, 1] = -pixel_idx_list[:, 1]
    elif octant == Octant.FIFTH:
        # (x,y) -> (-x, -y)
        tmp_list = -pixel_idx_list
        pixel_idx_list, de1, de2 = extend_line(tmp_list,
                                               -a, b, mu, omega, err1, err2)
        pixel_idx_list = -pixel_idx_list
    elif octant == Octant.SIXTH:
        # (x,y) -> (-y, -x)
        tmp_list = -pixel_idx_list
        tmp_list = np.fliplr(tmp_list)
        pixel_idx_list, de1, de2 = extend_line(tmp_list,
                                               -a, b, mu, omega, err1, err2)
        pixel_idx_list = np.fliplr(-pixel_idx_list)
    elif octant == Octant.SEVENTH:
        # (x,y) -> (-y, x)
        tmp_list = np.fliplr(pixel_idx_list)
        tmp_list[:, 0] = -tmp_list[:, 0]
        pixel_idx_list, de1, de2 = extend_line(pixel_idx_list,
                                     a, b, mu, omega, err1, err2)
        pixel_idx_list = np.fliplr(pixel_idx_list)
        pixel_idx_list[:, 0] = -pixel_idx_list[:, 0]
    else:
        # (x,y) -> (-x,y)
        tmp_list = pixel_idx_list
        tmp_list[:, 0] = -tmp_list[:, 0]
        pixel_idx_list, de1, de2 = extend_line(pixel_idx_list,
                                     a, b, mu, omega, err1, err2)
        pixel_idx_list[:, 0] = -pixel_idx_list[:, 0]
    return pixel_idx_list, de1, de2

    
def extend_line(pixel_idx_list, a, b, mu, omega, err1, err2):
    m, _    = pixel_idx_list.shape
    new_idx = []
    dx = pixel_idx_list[-2, 0]
    dy = pixel_idx_list[-2, 1]
    dx     = dx + 1
    err1   = err1 + a
    if err1 >= (mu + omega):
        err1 = err1 - b
        dy   = dy + 1
    new_idx.append([dx,dy])
    
    dx = pixel_idx_list[-1, 0]
    dy = pixel_idx_list[-1, 1]
    dx   = dx  - 1
    err2 = err2 - a
    if (err2 < mu):
        err2 = err2 + b
        dy   = dy - 1
    new_idx.append([dx,dy])
    
    return np.array(new_idx), err1, err2


def thicken_line(pixel_idx_list, a, b, k):
    thick_line = pixel_idx_list
    octant     = check_octant(a, b)
    sign       = np.sign(k)
    for i in range(0, np.abs(k)):
        if octant == Octant.FIRST:
            thick_line += sign * [0, -1]
        elif octant == Octant.SECOND:
            thick_line += sign * [1, 0]
        elif octant == Octant.THIRD:
            thick_line += sign * [-1, 0]
        elif octant == Octant.FOURTH:
            thick_line += sign * [0, -1]
        thick_line = np.concatenate((pixel_idx_list, thick_line), axis=0)
    return thick_line