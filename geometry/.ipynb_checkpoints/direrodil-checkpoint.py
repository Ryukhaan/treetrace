import matplotlib.pyplot as plt
import cv2 as cv
import SternBrocot
import estimater
import numpy as np
import time
I = cv.imread('./images/treerings.png')
alpha = 0.5
I = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
I = cv.resize(I, (int(alpha*I.shape[1]), int(alpha*I.shape[0])))
I = cv.normalize(I, None, 0, 255, cv.NORM_MINMAX)

dilates, erodes, fracs = estimater.compute_for_graylevel(I, max_depth=10, max_width=1)
print("Finish -> Start G")
L = int(len(dilates))
H, W = dilates[0][0].shape
max_dilates = np.zeros((L, H, W))
max_erodes  = np.zeros((L, H, W))
min_dilates = np.zeros((L, H, W))
min_erodes  = np.zeros((L, H, W))

arg_dilates = np.zeros((L, H, W))
arg_erodes  = np.zeros((L, H, W))
for length in range(0, L):
    print(length)
    max_dilates[length, :, :] = np.amax(np.array(dilates[length][:]), axis=0)
    max_erodes[length, :, :]  = np.amax(np.array(erodes[length][:]), axis=0)
    
    min_dilates[length, :, :] = np.amin(np.array(dilates[length][:]), axis=0)
    min_erodes[length, :, :]  = np.amin(np.array(erodes[length][:]), axis=0)
    
    arg_dilates[length, :, :] = np.argmax(np.array(dilates[length][:]), axis=0)
    arg_erodes[length, :, :]  = np.argmax(np.array(erodes[length][:]), axis=0)
    
print("Length done -> ARGMAX")  
#arg2dilate = np.argmax(min_dilates, axis=0)
#arg2erode  = np.argmax(max_erodes, axis=0)

max_dilate = np.amax(max_dilates, axis=0)
max_erode  = np.amax(max_erodes, axis=0)
min_dilate = np.amin(min_dilates, axis=0)
min_erode  = np.amin(min_erodes, axis=0)

Gdir_dil  = max_dilate - min_dilate
Gdir_ero  = max_erode - min_erode
Gdir = np.maximum(Gdir_dil, Gdir_ero)

#orientation = np.zeros((H,W))
#for y in range(0, H):
#    for x in range(0, W):
#        if Gdir[y,x] == Gdir_dil[y,x]:
#            m = arg2dilate[y,x]
#            n = arg_dilates[m][y,x]    
#        else:
#            m = arg2erode[y,x]
#            n = arg_erodes[m][y,x]
#       #m = arg2erode[y,x]
#        #n = arg_erodes[m][y,x] 
#        a, b = fracs[m][int(n)]
#        orientation[y, x] = np.arctan(a/(b+1e-16))

#cv.imwrite('./out/rings_dir.png', cv.normalize(orientation, None, 0, 255, cv.NORM_MINMAX))
cv.imwrite('./out/rings_gdir.png', cv.normalize(Gdir, None, 0, 255, cv.NORM_MINMAX))
cv.imwrite('./out/rings_gero.png', cv.normalize(Gdir_dil, None, 0, 255, cv.NORM_MINMAX))
cv.imwrite('./out/rings_gdil.png', cv.normalize(Gdir_ero, None, 0, 255, cv.NORM_MINMAX))
#fig, ax = plt.subplots(2, 2, figsize=(7,7))
#ax = ax.flatten()
#ax[0].imshow(I, 'gray')
#ax[1].imshow(arg2open, 'gray')
#ax[2].imshow(arg2clos, 'gray')
#ax[3].imshow(orientation, 'gray')
#
#plt.show()