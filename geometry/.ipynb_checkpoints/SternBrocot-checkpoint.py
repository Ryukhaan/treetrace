import numpy as np
import functools 

from enum import Enum

class Sign(Enum):
    NONE = 0
    UP   = 1
    DOWN = -1

class Node():
    def __init__(self, a = 0, b = 0, width=0, cut=False):
        self.a      = a
        self.b      = b
        self.is_cut = cut
        self.T      = None
        self.err1   = 0
        self.err2   = 0
        self.width  = 0
        self.sign   = Sign.NONE
        
    def get_frac(self):
        return self.a, self.b
        
    def __str__(self):
        cut = "Cut" if self.is_cut else "Uncut"
        return "({}/{}, {})".format(self.a, self.b, cut)
            
class SternBrocot():
    def __init__(self, nodes=None, ddepth = 0):
        self.initialize(nodes)
        self.ddepth = ddepth
        self.k_max  = 0
        self.k_min  = 0
    
    def initialize(self, nodes):
        if nodes is None:
            self.nodes = [Node(a=0,b=1),
                     Node(a=1,b=1),
                     Node(a=1,b=0),
                     Node(a=-1,b=1)]
        else:
            self.node = nodes
        
    def increment(self):
        self.ddepth += 1
        n = len(self.nodes)
        new_nodes = []
        
        # For each node
        for i in range(0, n):
            curr_node = self.nodes[i]
            prev_node = self.nodes[(i-1)%n]
            next_node = self.nodes[(i+1)%n]
            a1, b1    = curr_node.get_frac()
            a2, b2    = next_node.get_frac()
            
            if not (curr_node.is_cut and next_node.is_cut and prev_node.is_cut):
                new_nodes.append(curr_node)
                
            new_a = np.abs(a1) + np.abs(a2)
            new_b = b1 + b2
            if (new_b > (self.ddepth+1)) or (new_a > (self.ddepth+1)):
                continue
                
            new_res = self.nodes[i].is_cut and self.nodes[(i+1)%n].is_cut
            if np.sign(a2) == 0:
                new_a    = np.sign(a1) * new_a
            else:
                new_a    = np.sign(a2) * new_a
            new_node = Node(a = new_a, b = new_b, cut = new_res) 
            new_nodes.append(new_node)
            
        self.nodes = new_nodes
    
    def candidates(self):
        res = len(self.nodes)
        for i in range(0, len(self.nodes)):
            if (self.nodes[i].is_cut):
                res -= 1
        return res
            
    def __str__(self):
        return "[ {} ]".format(functools.reduce(lambda head,tail : str(head) + ", " + str(tail), self.nodes))
        