import cv2 as cv
import matplotlib.pyplot as plt

import SternBrocot
import estimater
import DSL 

import numpy as np
import math
import argparse
import sys

# Debug
import sys
import numpy
import networkx as nx

def parsing():
	desc   = "Compute for one pixel the longest discrete line"
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument('image', 
		type=str, help='Image path')
	parser.add_argument('--px', 
		type=int, default=-1, help='X-coordinate')
	parser.add_argument('--py', 
		type=int, default=-1, help='Y-coordinate')
	parser.add_argument('--max-length', 
		type=int, default=math.inf, help='Maximum of length')
	parser.add_argument('--max-frac', 
		type=int, default=math.inf, help='Maximum of orientation i.e. (1/max-length)')
	args = parser.parse_args()
	return args

def compute_for_xy(args):
	i, j = args.py, args.px
	I  = cv.imread(args.image)
	n = len(I.shape)
	if n == 3:
		I  = cv.cvtColor(I, cv.COLOR_RGB2GRAY)
	I  = cv.normalize(I, None, 0, 1, cv.NORM_MINMAX)
	#nodes 	= [SternBrocot.Node(a=0,b=1), SternBrocot.Node(a=1,b=1), SternBrocot.Node(a=1,b=0)]
	_tree 	= SternBrocot.SternBrocot(use_signed_orientation=False)
	_dtree 	= SternBrocot.GraphSB(tree=_tree)
	#out_tree = SternBrocot.compute_for_xy_deco(I, i, j, _dtree,
	#	max_depth=args.max_length,
	#	max_orientation=args.max_frac)
	
	out_tree = estimater.compute_for_xy(I, i, j, 
		init_tree=_dtree,
		max_depth=args.max_length,
		max_orientation=args.max_frac)
	#exit()
	print(out_tree)
	print("Width")
	print(estimater.widthen(I, i, j, out_tree))
	print("Ortho")
	print(estimater.estimate_width(I, i, j, out_tree))
	angle = out_tree.angular()
	print("theta")
	print(angle) 
	print(out_tree.length())

def compute_for_all(args):
	args = parsing()

	# Reading image
	I  = cv.imread(args.image)
	if len(I.shape)==3:
		I  = cv.cvtColor(I, cv.COLOR_RGB2GRAY)
	I = cv.normalize(I, None, 0, 1, cv.NORM_MINMAX)
	#A = np.zeros(I.shape)
	#L = np.ones(I.shape, dtype=np.uint8)
	#Dmin = np.ones(I.shape, dtype=np.uint8)
	#Dmax = np.ones(I.shape, dtype=np.uint8)
	#DTmin = np.ones(I.shape, dtype=np.uint8)
	#DTmax = np.ones(I.shape, dtype=np.uint8)
	for i in range(0, I.shape[0]):
		for j in range(0, I.shape[1]):
			if I[i,j] != 1:
				Dmin[i,j] = 0
				Dmax[i,j] = 0
				DTmin[i,j] = 0
				DTmax[i,j] = 0
				L[i,j] = 0
				continue;
			T = estimater.compute_for_xy(I, i, j, 
				init_tree=SternBrocot.SternBrocot(use_signed_orientation=False),
				max_depth=args.max_length,
				max_orientation=args.max_frac)
			#kmax, kmin = estimater.widthen(I, i, j, T)
			#Dmin[i,j] = kmin
			#Dmax[i,j] = kmax
			#kmax, kmin = estimater.estimate_width(I, i, j ,T)
			#DTmin[i,j] = kmin if kmin > 0 else DTmin[i,j]
			#DTmax[i,j] = kmax if kmax > 0 else DTmax[i,j]
			#A[i, j] = T.angular()
			#L[i, j] = T.length()
		#print(i)
	
    #np.savetxt('out/orientation.out', A)
	#width_map =  np.array(Dmin + Dmax, dtype=np.int32)
	##A = cv.normalize(A, None, 0, 255, cv.NORM_MINMAX)
	#A = A.astype(np.float32) * 255. / 180.
	#cv.imwrite('out/DTmin.png', DTmin)
	#cv.imwrite('out/DTmax.png', DTmax)
	#cv.imwrite('out/Dmin.png', Dmin)
	#cv.imwrite('out/Dmax.png', Dmax)
	#cv.imwrite('out/distance_map.png', cv.normalize(DTmin+DTmax, None, 0, 255, cv.NORM_MINMAX))
	#cv.imwrite('out/width_map.png', cv.normalize(width_map, None, 0, 255, cv.NORM_MINMAX))
	#cv.imwrite('out/orientation_map.png', A)
	#cv.imwrite('out/length_map.png', L)

if __name__ == '__main__':
	args = parsing()
	if args.px == -1 or args.py == -1:
		compute_for_all(args)
	else:
		compute_for_xy(args)