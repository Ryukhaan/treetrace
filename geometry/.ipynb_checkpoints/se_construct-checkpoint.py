import cv2 as cv
import matplotlib.pyplot as plt

import SternBrocot
import estimater
import DSL 

import numpy as np
import math
import argparse
import sys

# Debug
import sys
import numpy

my_node = [SternBrocot.Node(a=0,b=1), SternBrocot.Node(a=1, b=1)]
tree = SternBrocot.SternBrocot(nodes=my_node)
fig, ax = plt.subplots(figsize=(2,2))
for i in range(0, 4):
	tree.increment()
	ddepth = tree.ddepth
	for node in tree.nodes:
		a, b  = node.get_frac()
		# Compute omega and mu
		omega = np.maximum(np.abs(a), np.abs(b))
		mu    = 0 #np.ceil(omega//2)
		# Construct the line
		line, _, _ = DSL.construct(a, b, mu, omega, ddepth)
		se = np.zeros((2*ddepth+1, 2*ddepth+1), dtype=np.uint8)
		for j in range(0, line.shape[0]):
			se[-line[j,1]+ddepth, line[j,0]+ddepth] = 255
		ax.cla()
		ax.imshow(se, 'gray')
		ax.grid(which='minor', axis='both', linestyle='-', color='gray', linewidth=1)
		# Major ticks
		ax.set_xticks(np.arange(0, se.shape[1], 1))
		ax.set_yticks(np.arange(0, se.shape[0], 1))
		# Labels for major ticks
		ax.set_xticklabels(se.shape[1] * [" "])
		ax.set_yticklabels(se.shape[0] * [" "])
		# Minor ticks
		ax.set_xticks(np.arange(-.5, se.shape[1], 1), minor=True)
		ax.set_yticks(np.arange(-.5, se.shape[0], 1), minor=True)
		#plt.pause(0.01)
		extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
		fig.savefig('./out/se/{}_{}_{}.png'.format(ddepth,a,b), bbox_inches=extent)
