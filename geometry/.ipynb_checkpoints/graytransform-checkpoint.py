import matplotlib.pyplot as plt
import cv2 as cv
import SternBrocot
import estimater
import numpy as np
import time
import DSL
import copy

def gray_transform(I, n, x, y):
    H, W = I.shape
    k = (n-1)//2
    dilates = np.zeros((n,n))
    erodes = np.zeros((n,n))
    for a in range(n):
        for b in range(k+1):
            aa = a - k
            bb = b - k
            if aa == 0 and bb == 0: continue
            mu    = 0
            
            G = np.gcd(np.abs(aa),np.abs(bb))
            aa, bb = aa//G, bb//G
            omega = np.maximum(np.abs(aa), np.abs(bb))
            if ( G == 1):
                if omega % 2 == 1:
                    ddepth = omega + 2
                else:
                    ddepth = omega + 1
            else:
                if omega % 2 == 1:
                    ddepth = omega + 2 + (2*(G-1))
                else:
                    ddepth = omega + 1 + (2*(G-1))
                    
            center = (ddepth-1)//2
            # Construct the line
            #T, _, _ = DSL.construct(aa//G, bb//G, mu, omega, square)
            X = np.linspace(0, ddepth-1, ddepth)
            Y = np.linspace(0, ddepth-1, ddepth)
            xx , yy = np.meshgrid(X-center, Y-center)
            kernel = np.array((0 <= (aa * xx - bb * yy)) & ((aa * xx - bb * yy) < omega), dtype=np.uint8)
            dil = cv.dilate(I, kernel, 1)
            ero = cv.erode(I, kernel, 1)
            
            dilates[a,b] = dil[y,x]
            erodes[a,b] = ero[y,x]
    
    T = np.flipud(np.fliplr(dilates[:, 0:k+1]))
    dilates[:, k::] = T
    dilates[k,k] = I[y,x]
    dilates[:,:] = dilates - I[y,x]
    T = np.fliplr(np.flipud(erodes[:, 0:k+1]))
    erodes[:, k::] = T
    erodes[k,k] = I[y,x]
    erodes[:,:] = erodes - I[y,x]
    return dilates, erodes


plt.rcParams['text.usetex'] = True
plt.rcParams["mathtext.default"] = 'it'

I = cv.imread('./dgmm/new_vessel.png')
J = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
#I = cv.cvtColor(I, cv.COLOR_BGR2RGB)
#I = cv.resize(I, (int(alpha*I.shape[1]), int(alpha*I.shape[0])))
J = cv.normalize(J, None, 0, 255, cv.NORM_MINMAX)

#x, y, n = 447, 125, 61 # Vessel Noise
#x, y, n = 306, 527, 61 # Finger
x, y, n = 245, 597, 61 # Vessel New
#x, y, n = 282, 422, 61 # Finger
k = (n-1)//2
dilates, erodes = gray_transform(J, n, x, y)

fig = plt.figure(figsize=(7,7))
ax1 = fig.add_subplot(132)
ax1.set_xlabel("a")
ax1.set_ylabel("b")
ax1.set_title(r'$\mathcal{E}(I)(x,y)$')
ax1.set_xticks([0,k,n-1])
ax1.set_yticks([0,k,n-1])
ax1.set_xticklabels([str(-k),str(0),str(k)])
ax1.set_yticklabels([str(-k),str(0),str(k)])
ax1.imshow(erodes, cmap='gray')

ax2 = fig.add_subplot(133)
ax2.set_xticks([0,k,n-1])
ax2.set_yticks([0,k,n-1])
ax2.set_xticklabels([str(-k),str(0),str(k)])
ax2.set_yticklabels([str(-k),str(0),str(k)])
ax2.set_xlabel("a")
ax2.set_ylabel("b")
ax2.set_title(r'$\mathcal{D}(I)(x,y)$')
ax2.imshow(dilates, cmap='gray')


K = np.dstack((J,J,J))
K[y,x,:] = [255,0,0]
ax3 = fig.add_subplot(131)
ax3.imshow(K[y-n:y+n, x-n:x+n])
ax3.axis('off')
ax3.set_title("Image")

print(np.amax(erodes) - np.amin(erodes))
print(np.amax(dilates) - np.amin(dilates))
plt.savefig('/Users/remidecelle/Documents/transform-gray-2.png', dpi=300, bbox_inches='tight')
#plt.show()
