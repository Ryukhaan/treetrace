import numpy as np
import copy
import math

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import cv2 as cv

import DSL
import SternBrocot

    
def compute_for_xy(BW, y, x, tree=None, max_depth=math.inf, max_orientation=math.inf):
    tree = SternBrocot.SternBrocot() if tree is None else tree
    
    old_tree = copy.deepcopy(tree)
    value = BW[y, x]

    # UI
    I = BW.copy()
    fig, ax = plt.subplots(figsize=(7,7))
    ax.imshow(I)
    while tree.candidates() > 1 and tree.ddepth < max_depth:
        
        # Save old tree
        old_tree = copy.deepcopy(tree)
        
        # Increment fraction
        if ((tree.ddepth+1) < max_orientation):
            tree.increment()
        else:
            tree.ddepth += 1
            
        ddepth = tree.ddepth
        square = 2*ddepth+1
            
        # For each node (orientation a/b)
        for idx, node in enumerate(tree.nodes):
            if (node.is_cut):
                continue
                
            # Retrieve coefficient a, b from node
            a, b  = node.get_frac()

            # Compute omega and mu
            omega = np.maximum(np.abs(a), np.abs(b))
            mu    = 0 #np.ceil(omega//2)
            
            # Construct the line
            node.T, node.err1, node.err2 = DSL.fast_construct(a, b, mu, omega, ddepth)
            
            se = node.T

            # Proceed to erode / dilatation
            for i in range(0, se.shape[0]):
                # Check if line of length square can be construct
                if ( y - se[i,1] < 0 
                   or y - se[i,1] >= BW.shape[1]
                   or x + se[i,0] < 0
                   or x + se[i,0] >= BW.shape[0]):
                    tree.nodes[idx].is_cut = True
                    break;
                if BW[y-se[i,1], x+se[i,0]] == value: 
                    I[y-se[i,1], x+se[i,0]] = ddepth + 1 
                new_value = BW[y-se[i,1], x+se[i,0]]
                cond_cut = new_value != value
                tree.nodes[idx].is_cut = cond_cut
                if cond_cut:
                    break

        # UI
        nx = x / I.shape[1]
        ny = 1. - y / I.shape[0]
        ax.cla()
        viridis = cm.get_cmap('Spectral', ddepth+2)
        newcolors = viridis(np.linspace(0, 1, ddepth+2))
        newcolors[0, :] = np.array([0/256, 0/256, 0/256, 1])
        newcolors[1, :] = np.array([256/256, 256/256, 256/256, 1])
        newcmp = ListedColormap(newcolors)
        # Display Colormap and Image
        cc = ax.imshow(I, cmap=newcmp)
        # Display Xlim and Ylim
        xmin = np.maximum(x-ddepth-1, 0)
        xmax = np.minimum(x+ddepth+1, I.shape[1]-1)
        ymin = np.maximum(y-ddepth-1, 0)
        ymax = np.minimum(y+ddepth+1, I.shape[0]-1)
        #ax.set_xlim(xmin, xmax)
        #ax.set_ylim(ymin, xmax)
        axins = ax.inset_axes([.5-float(nx>0.5)/2, .5-float(ny>0.5)/2, .5, .5])
        axins.imshow(I, cmap=newcmp, 
            origin='lower', interpolation="nearest")
        # sub region of the original image
        #x1, x2, y1, y2 = -1.5, -0.9, -2.5, -1.9
        axins.set_xlim(xmin, xmax)
        axins.set_ylim(ymax, ymin)
        axins.set_xticklabels('')
        axins.set_yticklabels('')
        ax.indicate_inset_zoom(axins)
        lines = False
        plt.pause(0.01)

    # Display Grid
    #ax.grid(which='minor', axis='both', linestyle='-', color='black', linewidth=1)
    ## Major ticks
    #ax.set_xticks(np.arange(xmin, xmax, 1))
    #ax.set_yticks(np.arange(ymin, ymax, 1))
    ## Labels for major ticks
    #ax.set_xticklabels((xmax-xmin) * [" "])
    #ax.set_yticklabels((ymax-ymin) * [" "])
    ## Minor ticks
    #ax.set_xticks(np.arange(xmin-.5, xmax, 1), minor=True)
    #ax.set_yticks(np.arange(ymin-.5, ymax, 1), minor=True)
    #fig.colorbar(cc)
    #plt.pause(0.01)
    plt.show()
    if (tree.candidates() == 0):
        tree = copy.deepcopy(old_tree)

    return tree

def estimate_width(BW, y, x, tree):
    ddepth = tree.ddepth
    square = 2*ddepth+1
    value = BW[y, x]

    #tmp = BW.copy()

    k_min = []
    k_max = []
    for idx, node in enumerate(tree.nodes):
        if (node.is_cut):
            continue
        
        a, b  = node.get_frac()
        omega = np.maximum(np.abs(a), np.abs(b))
        mu    = 0
        
        #T, err1, err2 = DSL.construct(-b, a, mu, omega, 1)
        kk = 1

        tk_max = 0
        tk_min = 0
        border1_not_found = True
        border2_not_found = True
        while border1_not_found or border2_not_found:
            T, _, _ = DSL.fast_construct(-b, a, mu, omega, kk)
            if border1_not_found:
                new_value = BW[y-T[-1,1], x+T[-1,0]]
                #tmp[y-T[-1,1], x+T[-1,0]] = 2
                border1_not_found = new_value == value
                if border1_not_found:
                    tk_max += 1
            if border2_not_found:
                new_value = BW[y-T[-2,1], x+T[-2,0]]
                #tmp[y-T[-2,1], x+T[-2,0]] = 3
                border2_not_found = new_value == value
                if border2_not_found:
                    tk_min += 1
            kk = kk + 1
            #T, _, _ = DSL.extend(T, -b, a, mu, omega, err1, err2)
        
        #T2, _, _, = DSL.construct(-b, a, mu, omega, 8)
        #for i in range(0, T2.shape[0]):
        #    tmp[y-T2[i,1], x+T2[i,0]] = 4
        #T2, _, _, = DSL.construct(a, b, mu, omega, 8)
        #for i in range(0, T2.shape[0]):
        #    tmp[y-T2[i,1], x+T2[i,0]] = 5
        #plt.imshow(tmp, 'jet')
        #plt.colorbar()
        #plt.show()

        k_max.append(tk_max)
        k_max.append(tk_min)
        

    resmax = np.amax(k_max) if len(k_max) > 0 else 0
    resmin = np.amin(k_max) if len(k_max) > 0 else 0
    return resmax, resmin

def display_lines(BW, y, x, tree):
    ddepth = tree.ddepth
    square = 2*ddepth+1 

    viridis = cm.get_cmap('viridis', 2*len(tree.nodes)+2)
    newcolors = viridis(np.linspace(0, 1, 256))
    newcolors[0, :] = np.array([0/256, 0/256, 0/256, 1])
    newcolors[1, :] = np.array([256/256, 256/256, 256/256, 1])
    newcmp = ListedColormap(newcolors)
    
    kmax, kmin = estimate_width(BW, y, x, tree)
    print(kmax, kmin)
    delta = np.maximum(ddepth, kmax)
    I = np.pad(BW, ((delta,delta), (delta,delta)))
    ny = y+delta
    nx = x+delta
    for idx, node in enumerate(tree.nodes):
        if (node.is_cut):
            continue
            
        a, b    = node.get_frac()
        omega   = np.maximum(np.abs(a), np.abs(b))
        mu      = 0 #np.ceil(omega//2)
        T, _, _ = DSL.construct(a, b, mu, omega, ddepth)

        for i in range(0, T.shape[0]):
           I[ny-T[i,1], nx+T[i,0]] = 2 * idx
        
        T, _, _ = DSL.construct(-b, a, mu, omega, delta)
        for i in range(0, T.shape[0]):
           I[ny-T[i,1], nx+T[i,0]] = 2 * idx - 1 #* I[ny-T[i,1], nx+T[i,0]]
        
        #print("Fraction : {} / {} - Length : {}".format(a,b,square))
        fig, ax = plt.subplots(figsize=(7,7))

        I[y+delta, x+delta] = 4
        ax.imshow(I, cmap=newcmp)
        #ax.grid(which='minor', axis='both', linestyle='-', color='black', linewidth=1)

        ## Major ticks
        #ax.set_xticks(np.arange(0, I.shape[1], 1))
        #ax.set_yticks(np.arange(0, I.shape[0], 1))

        ## Labels for major ticks
        #ax.set_xticklabels(I.shape[1] * [" "])
        #ax.set_yticklabels(I.shape[0] * [" "])

        ## Minor ticks
        #ax.set_xticks(np.arange(-.5, I.shape[1], 1), minor=True)
        #ax.set_yticks(np.arange(-.5, I.shape[0], 1), minor=True)
        plt.show()


def compute_for_graylevel(I, 
    tree=SternBrocot.SternBrocot(), 
    max_depth=math.inf, 
    max_orientation=math.inf):
    
    openings = []
    closings = []
    coefficients = []
    while tree.ddepth < max_depth:
        coefficient = []
        opening = []
        closing = []
        if ((tree.ddepth+1) < max_orientation):
            tree.increment()
        else:
            tree.ddepth += 1
        
        ddepth = tree.ddepth
        square = 2*ddepth+1
        for idx, node in enumerate(tree.nodes):
            if (node.is_cut):
                continue
            # Retrieve coefficient a, b from node
            a, b  = node.get_frac()
            # Compute omega and mu
            omega = np.maximum(np.abs(a), np.abs(b))
            mu    = 0 #np.ceil(omega//2)
            # Construct the line
            node.T, node.err1, node.err2 = DSL.construct(a, b, mu, omega, ddepth)
            
            kernel = np.zeros((square, square), dtype=np.uint8)
            # Proceed to erode / dilatation
            for i in range(0, node.T.shape[0]):
                kernel[-node.T[i,1]+ddepth, node.T[i,0]+ddepth] = 1

            coefficient.append((a,b))
            opening.append(cv.morphologyEx(I, cv.MORPH_OPEN, kernel))
            closing.append(cv.morphologyEx(I, cv.MORPH_CLOSE, kernel))

        coefficients.append(coefficient)
        openings.append(opening)
        closings.append(closing)

    return openings, closings, coefficients


def orientation_gray(openings, closings, coefficients):
    tmp = openings[0][0]
    maxplus = np.zeros(tmp.shape)
    minplus = 1e6 * np.ones(tmp.shape)
    maxminus = np.zeros(tmp.shape)
    minminus = 1e6 * np.ones(tmp.shape)

    n = len(openings)
    for length in range(0,n-1):
        pass
    #for y in range(0, tmp.shape[0]):
    #    for x in range(0, tmp.shape[1]):
    #
    #        max_val = maxplus[y,x]
    #        min_val = minplus[y,x]
    #        for length in range(0, n):
    #            opening_i = openings[length]
    #            m = len(opening_i)
    #            for orientation in range(0, m):
    #                value = opening_i[orientation][y,x]
    #                if  max_val < value:
    #                    maxplus[y,x] = value
    #                if min_val > value:
    #                    minplus[y,x] = value
    #
    #        max_val = maxminus[y,x]
    #        min_val = minminus[y,x]
    #        for length in range(0, n):
    #            closing_i = closings[length]
    #            m = len(closing_i)
    #            for orientation in range(0, m):
    #                value = closing_i[orientation][y,x]
    #                if  max_val < value:
    #                    maxminus[y,x] = value
    #                if min_val > value:
    #                    minminus[y,x] = value 
    Gdirplus = maxplus - minplus
    Gdirminus = maxminus - minminus
    G = np.maximum(Gdirplus, Gdirminus)
    return G                   


                



