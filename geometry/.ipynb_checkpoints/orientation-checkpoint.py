import matplotlib.pyplot as plt
import cv2 as cv
import SternBrocot
import estimater
import numpy as np
import time
I = cv.imread('./images/treerings.png')
alpha = 0.4
I = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
I = cv.resize(I, (int(alpha*I.shape[1]), int(alpha*I.shape[0])))
I = cv.normalize(I, None, 0, 255, cv.NORM_MINMAX)

openings, closings, fracs = estimater.compute_for_graylevel(I, max_depth=27, max_orientation=8)

n = len(openings)
max_openings = np.zeros((n, openings[0][0].shape[0], openings[0][0].shape[1]))
max_closings = np.zeros((n, openings[0][0].shape[0], openings[0][0].shape[1]))
min_openings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
min_closings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))

arg_openings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
arg_closings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
for length in range(0,n):
    max_openings[length, :, :] = np.amax(np.array(openings[length][:]), axis=0)
    max_closings[length, :, :] = np.amax(np.array(closings[length][:]), axis=0)
    
    min_openings[length, :, :] = np.amin(np.array(openings[length][:]), axis=0)
    min_closings[length, :, :] = np.amin(np.array(closings[length][:]), axis=0)
    
    arg_openings[length, :, :] = np.argmax(np.array(openings[length][:]), axis=0)
    arg_closings[length, :, :] = np.argmin(np.array(closings[length][:]), axis=0)
  

arg2open = np.argmin(min_openings, axis=0)
arg2clos = np.argmax(max_closings, axis=0)

max_opening = np.amax(max_openings, axis=0)
max_closing = np.amax(max_closings, axis=0)
min_opening = np.amin(min_openings, axis=0)
min_closing = np.amin(min_closings, axis=0)

Gdir_open  = max_opening - min_opening
Gdir_clos = max_closing - min_closing
Gdir = np.maximum(Gdir_open, Gdir_clos)

orientation = np.zeros(max_opening.shape)
for y in range(0, arg2open.shape[0]):
    for x in range(0, arg2open.shape[1]):
        if Gdir[y,x] == Gdir_open[y,x]:
            m = arg2open[y,x]
            n = arg_openings[m][y,x]    
        else:
            m = arg2clos[y,x]
            n = arg_closings[m][y,x]
        a, b = fracs[m][int(n)]
        orientation[y, x] = np.arctan(a/(b+1e-12))

fig, ax = plt.subplots(2, 2, figsize=(7,7))
ax = ax.flatten()
ax[0].imshow(I, 'gray')
ax[1].imshow(arg2open, 'gray')
ax[2].imshow(arg2clos, 'gray')
ax[3].imshow(orientation, 'gray')

plt.show()