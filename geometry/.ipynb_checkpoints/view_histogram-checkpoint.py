import numpy as np
import matplotlib.pyplot as plt 
import json
import skimage.io as skio
import skimage.draw as skdraw
from skimage import exposure
import copy
import sys
from skimage.feature import hog

import subprocess
import argparse


def get_parser():
	parser = argparse.ArgumentParser(
	  description="Tree Rings Segmentation"
	  )

	#-- Which iamge --#
	parser.add_argument("--image",
	  help="Path to input image")

	#-- Main parameters (shape, name, filters) --#
	parser.add_argument("--length",
	  help="Maximum DSS Length",
	  type=str,
	  default=13)
	parser.add_argument("--nbins",
	  help="Number of bins in histogram",
	  type=str,
	  default=10)

	return parser.parse_args()


if __name__ == '__main__':

	args = get_parser()
	program = './DSSFilter'
	subprocess.call([program] + [args.image, "--length="+args.length, "--nbins="+args.nbins])
	# f = open("./histograms.json", 'r')
	# data = json.load(f)
	# img = skio.imread(args.image) 
	# img = np.uint8(img > 127)

	# fig = plt.figure()
	# img_ax = fig.add_subplot(121)
	# his_ax = fig.add_subplot(122, projection='polar')
	# #coh_ax = fig.add_subplot(133)
	# img_ax.imshow(img>0)
	# bins = np.array(data["angle"])

	# def orientation(data, h=0, w=0):
	# 	res = np.zeros((h,w))
	# 	n = len(data["angle"])
	# 	for row in data["data"]:
	# 		for col in data["data"][row]:
	# 			if (img[int(row), int(col)] == 0): continue
	# 			res[int(row)][int(col)] = data["angle"][np.argmax(data["data"][row][col])]
	# 	return res

	# def normalize(data, h=0, w=0):
	# 	res = np.zeros((h,w))
	# 	for row in data:
	# 		for col in data[row]:
	# 			if (img[int(row), int(col)] == 0): continue
	# 			nelt = np.sum(data[row][col])
	# 			if nelt == 0:
	# 				coh = 0
	# 			else:
	# 				coh = (np.amax(data[row][col]) - np.amin(data[row][col])) / (np.sum(data[row][col]))
	# 			res[int(row)][int(col)] = coh
	# 	return res

	# coherence = normalize(data["data"], img.shape[0], img.shape[1])
	# th_map = orientation(data, img.shape[0], img.shape[1])

	# xoff = np.cos(th_map)
	# yoff = np.sin(th_map)
	# quiveropts = dict(color='red', headlength=0, pivot='middle', scale=1, 
 #    				linewidth=1., units='xy', width=.05, headwidth=1)
	# u = xoff * (img>0)
	# v = yoff * (img>0)
	# img_ax.quiver(u,v,headaxislength=0, **quiveropts)

	# def onclick(event):
	# 	x = int(event.xdata+.4)
	# 	y = int(event.ydata+.4)
	# 	img_ax.clear()
	# 	img_ax.imshow(img>0)
	# 	img_ax.scatter(x, y, c='blue',marker='+')
	# 	img_ax.quiver(u,v, headaxislength=0, **quiveropts)
	# 	his_ax.clear()
	# 	new_data = data["data"][str(y)][str(x)]

	# 	dins = np.concatenate((bins, bins+np.pi, [bins[0]+2*np.pi]), axis=0)
	# 	radii = np.concatenate((new_data, new_data, [new_data[0]]), axis=0)
	# 	widths = np.diff(dins)
	# 	his_ax.bar(dins[:-1], radii[:-1], width=widths, zorder=1, align='edge',edgecolor='C0', fill=False, linewidth=1)
	# 	his_ax.set_yticklabels([])
	# 	his_ax.grid(alpha=0.1)  
	# 	his_ax.set_title(coherence[y,x])
	# 	plt.draw()

	# cid = fig.canvas.mpl_connect('button_press_event', onclick)
	# plt.show()

