import numpy as np
import functools 

from enum import Enum

class Sign(Enum):
    NONE = 0
    UP   = 1
    DOWN = -1

class Node():
    def __init__(self, a = 0, b = 0, cut=False):
        self.a      = a
        self.b      = b
        self.is_cut = cut
        self.T      = None
        self.mu     = 0
        
    def get_frac(self):
        return self.a, self.b
        
    def __str__(self):
        cut = "Cut" if self.is_cut else "Uncut"
        return "({}/{}, {})".format(self.a, self.b, cut)
            
class SternBrocot():

    signed_nodes = [Node(a=0,b=1), Node(a=1,b=1), 
                    Node(a=1,b=0), Node(a=-1,b=1),
                    Node(a=0,b=-1),Node(a=-1,b=-1),
                    Node(a=-1,b=0),Node(a=1, b=-1)]

    unsigned_nodes = [Node(a=0,b=1), Node(a=1,b=1),
                    Node(a=1,b=0), Node(a=-1,b=1)]

    def __init__(self, 
                nodes=None, 
                ddepth=0,
                use_signed_orientation=True):
        self.initialize(nodes, use_signed_orientation)
        self.ddepth = ddepth
    
    def initialize(self, nodes, use_signed_orientation):
        if nodes is None:
            if use_signed_orientation:
                self.nodes = self.signed_nodes
            else:
                self.nodes = self.unsigned_nodes
        else:
            self.nodes = nodes
        
    def increment(self):
        self.ddepth += 1
        n = len(self.nodes)
        new_nodes = []
        # For each node
        for i in range(0, n):
            # Get next nodes (and the previous one)
            curr_node = self.nodes[i]
            prev_node = self.nodes[(i-1)%n]
            next_node = self.nodes[(i+1)%n]
            a1, b1    = curr_node.get_frac()
            a2, b2    = next_node.get_frac()
            if not (curr_node.is_cut and next_node.is_cut and prev_node.is_cut):
                new_nodes.append(curr_node)
            # Compute nex numerator and denominator
            new_a = np.abs(a1) + np.abs(a2)
            new_b = np.abs(b1) + np.abs(b2)
            if (new_b > (self.ddepth+1)) or (new_a > (self.ddepth+1)):
                continue 
            new_res = self.nodes[i].is_cut and self.nodes[(i+1)%n].is_cut
            new_a *= np.sign(a1) if np.sign(a2) == 0 else np.sign(a2)
            new_b *= np.sign(b2) if np.sign(b1) == 0 else np.sign(b1)
            new_node = Node(a = new_a, b = new_b, cut = new_res) 
            new_nodes.append(new_node)
            
        self.nodes = new_nodes
    
    def candidates(self):
        return len(list(filter(lambda x: not x.is_cut, self.nodes)))
            
    def __str__(self):
        return "[ {} ]".format(functools.reduce(lambda head,tail : str(head) + ", " + str(tail), self.nodes))
        