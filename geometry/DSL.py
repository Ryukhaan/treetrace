import numpy as np
from enum import Enum

class Octant(Enum):
    NONE    = 0
    FIRST   = 1
    SECOND  = 2
    THIRD   = 3
    FOURTH  = 4
    FIFTH   = 5
    SIXTH   = 6
    SEVENTH = 7
    EIGHTH  = 8

def check_octant(a,b):
    if (b == 0):
        if (a == 0):
            return Octant.NONE
        elif (a > 0):
            return Octant.THIRD
        else: # a < 0
            return Octant.SEVENTH
    elif (b > 0):
        if (a == 0):
            return Octant.FIRST
        elif (a > 0):
            if (b > a):
                return Octant.FIRST
            else:
                return Octant.SECOND
        else: # a < 0
            if (b >= -a):
                return Octant.FOURTH
            else:
                return Octant.THIRD
    else: # b < 0
        if (a == 0):
            return Octant.FIFTH
        elif (a > 0):
            if (-b >= a):
                return Octant.EIGHTH
            else:
                return Octant.SEVENTH
        else: # a < 0
            if (-b > -a):
                return Octant.FIFTH
            else:
                return Octant.SIXTH


    
def construct_line(a, b, mu, omega, dss_length):
    line  = np.zeros((1,2), np.int32)
    dx1   = 0
    dy1   = 0
    err1  = 0
    dx2   = 0
    dy2   = 0
    err2  = 0
    
    for i in range(1, dss_length+1):
        dx1   = dx1 + 1
        err1 = err1 + a
        if err1 >= (mu + omega):
            err1 = err1 - b
            dy1   = dy1 + 1
        line = np.append(line, [[dx1, dy1]], axis=0)
        
        dx2   = dx2  - 1
        err2  = err2 - a
        if (err2 < mu):
            err2 = err2 + b
            dy2   = dy2 - 1
        line = np.append(line, [[dx2, dy2]], axis=0)
    
    return line, err1, err2

def construct(a, b, mu, omega, dss_length):
    octant = check_octant(a, b)
    if octant == Octant.FIRST:
        line, err1, err2 = construct_line(a, b, mu, omega, dss_length)
    elif octant == Octant.SECOND:
        line, err1, err2 = construct_line(b, a, mu, omega, dss_length)
        # (x,y) -> (y, x)
        line             = np.fliplr(line)
    elif octant == Octant.THIRD:
        line, err1, err2 = construct_line(b, -a, mu, omega, dss_length)
        # (x,y) -> (y, -x)
        line             = np.fliplr(line)
        line[:, 1]       = -line[:, 1]
    elif octant == Octant.FOURTH:
        line, err1, err2 = construct_line(-a, b, mu, omega, dss_length)
        # (x,y) -> (x, -y)
        line[:, 1]       = -line[:, 1]
    elif octant == Octant.FIFTH:
        line, err1, err2 = construct_line(-a, -b, mu, omega, dss_length)
        # (x,y) -> (-x, -y)
        line = -line
    elif octant == Octant.SIXTH:
        line, err1, err2 = construct_line(-b, -a, mu, omega, dss_length)
        # (x,y) -> (-y, -x)
        line = -line
        line = np.fliplr(line)
    elif octant == Octant.SEVENTH:
        line, err1, err2 = construct_line(-b, a, mu, omega, dss_length)
        # (x,y) -> (-y, x)
        line = np.fliplr(line)
        line[:,0] = -line[:,0]
    else:
        line, err1, err2 = construct_line(a, -b, mu, omega, dss_length)
        # (x,y) -> (-x,y)
        line[:, 0] = -line[:, 0]
    return line, err1, err2


def fast_construct(a, b, mu, omega, l):
    def FCL(a, b, my, omega, i):
        line = np.empty((2,2), dtype=np.int32)
        x = i
        n = a * x - mu
        y = n // b
        #remainder1 = n % b
        line[0,:] = [x,y]

        x = -i
        n = a * x - mu
        y = n // b
        #remainder2 = n % b
        line[1,:] = [x,y]
        #return line, remainder1, remainder2
        return line, 0, 0

    octant = check_octant(a, b)
    if octant == Octant.FIRST:
        line, err1, err2 = FCL(a, b, mu, omega, l)
    elif octant == Octant.SECOND:
        line, err1, err2 = FCL(b, a, mu, omega, l)
        # (x,y) -> (y, x)
        line             = np.fliplr(line)
    elif octant == Octant.THIRD:
        line, err1, err2 = FCL(b, -a, mu, omega, l)
        # (x,y) -> (y, -x)
        line             = np.fliplr(line)
        line[:, 1]       = -line[:, 1]
    elif octant == Octant.FOURTH:
        line, err1, err2 = FCL(-a, b, mu, omega, l)
        # (x,y) -> (x, -y)
        line[:, 1]       = -line[:, 1]
    elif octant == Octant.FIFTH:
        line, err1, err2 = FCL(-a, -b, mu, omega, l)
        # (x,y) -> (-x, -y)
        line = -line
    elif octant == Octant.SIXTH:
        line, err1, err2 = FCL(-b, -a, mu, omega, l)
        # (x,y) -> (-y, -x)
        line = -line
        line = np.fliplr(line)
    elif octant == Octant.SEVENTH:
        line, err1, err2 = FCL(-b, a, mu, omega, l)
        # (x,y) -> (-y, x)
        line = np.fliplr(line)
        line[:,0] = -line[:,0]
    else:
        line, err1, err2 = FCL(a, -b, mu, omega, l)
        # (x,y) -> (-x,y)
        line[:, 0] = -line[:, 0]
    return line, err1, err2


def extend(pixel_idx_list, a, b, mu, omega, err1, err2):
    octant = check_octant(a, b)
    new_list = pixel_idx_list
    if octant == Octant.FIRST:
        return extend_line(new_list, a, b, mu, omega, err1, err2)
    elif octant == Octant.SECOND:
        # (x,y) -> (y,x)
        new_list         = np.fliplr(new_list)
        new_list, e1, e2 = extend_line(new_list, b, a, mu, omega, err1, err2)
        new_list         = np.fliplr(new_list)
    elif octant == Octant.THIRD:
        # (x,y) -> (y, -x)
        new_list         = np.fliplr(new_list)
        new_list[:, 0]   = -new_list[:, 0]
        new_list, e1, e2 = extend_line(new_list, b, -a, mu, omega, err1, err2)
        new_list         = np.fliplr(new_list)
        new_list[:, 1]   = -new_list[:, 1]
    elif octant == Octant.FOURTH:
        # (x,y) -> (x,-y)
        new_list[:, 1]   = -new_list[:, 1]
        new_list, e1, e2 = extend_line(new_list, -a, b, mu, omega, err1, err2)
        new_list[:, 1]   = -new_list[:, 1]
    elif octant == Octant.FIFTH:
        # (x,y) -> (-x, -y)
        new_list = -new_list
        new_list, e1, e2 = extend_line(new_list, -a, -b, mu, omega, err1, err2)
        new_list = -new_list
    elif octant == Octant.SIXTH:
        # (x,y) -> (-y, -x)
        new_list = -np.fliplr(new_list)
        new_list, e1, e2 = extend_line(new_list, -b, -a, mu, omega, err1, err2)
        new_list = -np.fliplr(new_list)
    elif octant == Octant.SEVENTH:
        # (x,y) -> (-y, x)
        new_list = np.fliplr(new_list)
        new_list[:, 1] = -new_list[:, 1]
        new_list, e1, e2 = extend_line(new_list, -b, a, mu, omega, err1, err2)
        new_list = np.fliplr(new_list)
        new_list[:, 0] = -new_list[:, 0]
    else:
        # (x,y) -> (-x,y)
        new_list[:, 0] = -new_list[:, 0]
        new_list, e1, e2 = extend_line(new_list, a, -b, mu, omega, err1, err2)
        new_list[:, 0] = -new_list[:, 0]
    return new_list, e1, e2

    
def extend_line(line, a, b, mu, omega, err1, err2):
    m, _    = line.shape
    new_idx = []
    dx      = line[-2, 0]
    dy      = line[-2, 1]
    dx      = dx + 1
    err1    = err1 + a
    if err1 >= (mu + omega):
        err1 = err1 - b
        dy   = dy + 1
    new_idx.append([dx,dy])
    
    dx = line[-1, 0]
    dy = line[-1, 1]
    dx   = dx  - 1
    err2 = err2 - a
    if (err2 < mu):
        err2 = err2 + b
        dy   = dy - 1
    new_idx.append([dx,dy])
    
    return np.array(new_idx), err1, err2


def thicken_line(line, a, b, k):
    thick_line = line.copy()
    octant     = check_octant(a, b)
    sign       = np.sign(k)
    for i in range(0, np.abs(k)):
        if octant == Octant.FIRST or octant == Octant.FIFTH:
            thick_line += sign * np.array([0, -1], dtype=np.int32)
        elif octant == Octant.SECOND or octant == Octant.SIXTH:
            thick_line += sign * np.array([1, 0], dtype=np.int32)
        elif octant == Octant.THIRD or octant == Octant.SEVENTH:
            thick_line += sign * np.array([-1, 0], dtype=np.int32)
        elif octant == Octant.FOURTH or octant == Octant.EIGHTH:
            thick_line += sign * np.array([0, -1], dtype=np.int32)
        else:
            pass
        #thick_line = np.concatenate((pixel_idx_list, thick_line), axis=0)
    return thick_line