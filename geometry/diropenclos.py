import matplotlib.pyplot as plt
import cv2 as cv
import SternBrocot
import estimater
import numpy as np
import time

I = cv.imread('./images/circle_noise.png')
I = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
I = cv.normalize(I, None, 0, 255, cv.NORM_MINMAX)

openings = np.load('./out/openings.npy', allow_pickle=True)
closings = np.load('./out/closings.npy', allow_pickle=True)
fracs    = np.load('./out/fracs_oc.npy', allow_pickle=True)

H, W = I.shape
n    = len(openings)
kk   = 2*(n+1)+1
orientation = np.zeros((H,W))
for x in range(0,H):
    for y in range(0,W):
        #print(x,y)
        #start = time.time()
        s_tree_open = np.zeros((kk, kk))
        s_tree_clos = np.zeros((kk, kk))
        c_tree_open = np.ones((kk, kk))
        c_tree_clos = np.ones((kk, kk))
        for i in range(0, n):
            for j in range(0, len(openings[i])):
                a, b = fracs[i][j]
                s_tree_open[b, a] += openings[i][j][y, x]
                s_tree_clos[b, a] += closings[i][j][y, x]
                c_tree_open[b, a] += 1
                c_tree_clos[b, a] += 1

        s_tree_open = np.divide(s_tree_open, c_tree_open)
        s_tree_clos = np.divide(s_tree_clos, c_tree_clos)
        Gdir_open = np.amax(s_tree_open, axis=None) - np.amin(s_tree_open, axis=None)
        Gdir_clos = np.amax(s_tree_clos, axis=None) - np.amin(s_tree_clos, axis=None)
        if Gdir_open > Gdir_clos:
            ind = np.unravel_index(np.argmax(s_tree_open, axis=None), s_tree_open.shape)
        else:
            ind = np.unravel_index(np.argmax(s_tree_clos, axis=None), s_tree_clos.shape)
        bb = ind[0] - kk if ind[0] > n+1 else ind[0]
        aa = ind[1] - kk if ind[1] > n+1 else ind[1]
        orientation[y, x] = np.arctan(aa/(bb+1e-16))
        #print(time.time() - start)

cv.imwrite('./out/orientation_oc.png', cv.normalize(orientation, None, 0, 255, cv.NORM_MINMAX))
#cv.imwrite('./out/orientation_dil.png', cv.normalize(orientation_dil, None, 0, 255, cv.NORM_MINMAX))

# L = int(len(dilates))
# H, W = dilates[0][0].shape
# max_dilates = np.zeros((L, H, W))
# max_erodes  = np.zeros((L, H, W))
# min_dilates = np.zeros((L, H, W))
# min_erodes  = np.zeros((L, H, W))

# arg_dilates = np.zeros((L, H, W))
# arg_erodes  = np.zeros((L, H, W))
# for length in range(0, L):
#     max_dilates[length, :, :] = np.amax(np.array(dilates[length][:]), axis=0)
#     max_erodes[length, :, :]  = np.amax(np.array(erodes[length][:]), axis=0)
    
#     min_dilates[length, :, :] = np.amin(np.array(dilates[length][:]), axis=0)
#     min_erodes[length, :, :]  = np.amin(np.array(erodes[length][:]), axis=0)
    
#     arg_dilates[length, :, :] = np.argmax(np.array(dilates[length][:]), axis=0)
#     arg_erodes[length, :, :]  = np.argmax(np.array(erodes[length][:]), axis=0)
  

# arg2dilate = np.argmax(min_dilates, axis=0)
# arg2erode  = np.argmax(max_erodes, axis=0)

# max_dilate = np.amax(max_dilates, axis=0)
# max_erode  = np.amax(max_erodes, axis=0)
# min_dilate = np.amin(min_dilates, axis=0)
# min_erode  = np.amin(min_erodes, axis=0)

# Gdir_dil  = max_dilate - min_dilate
# Gdir_ero  = max_erode - min_erode
# Gdir = np.maximum(Gdir_dil, Gdir_ero)

# orientation = np.zeros((H,W))
# for y in range(0, H):
#     for x in range(0, W):
#         if Gdir[y,x] == Gdir_dil[y,x]:
#             m = arg2dilate[y,x]
#             n = arg_dilates[m][y,x]    
#         else:
#             m = arg2erode[y,x]
#             n = arg_erodes[m][y,x]
#         #m = arg2erode[y,x]
#         #n = arg_erodes[m][y,x] 
#         a, b = fracs[m][int(n)]
#         orientation[y, x] = np.arctan(a/(b+1e-16))

# cv.imwrite('./out/smooth1.png', cv.normalize(max_erode, None, 0, 255, cv.NORM_MINMAX))
# cv.imwrite('./out/smooth2.png', cv.normalize(max_dilate, None, 0, 255, cv.NORM_MINMAX))
# #fig, ax = plt.subplots(3, 3, figsize=(7,7))
# #ax = ax.flatten()
# #ax[0].imshow(I, 'gray')
# #ax[1].imshow(Gdir, 'gray')
# #ax[2].imshow(max_erode, 'gray')
# #ax[3].imshow(min_erode, 'gray')
# #ax[4].imshow(max_dilate, 'gray')
# #ax[5].imshow(min_dilate, 'gray')
# #ax[6].imshow(arg2dilate, 'gray')
# #ax[7].imshow(arg2erode, 'gray')
# #ax[8].imshow(orientation, 'gray')

# plt.show()