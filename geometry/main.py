import cv2 as cv
import matplotlib.pyplot as plt

import SternBrocot
import estimater
import DSL 

import numpy as np
import math
import argparse
import sys

# Debug
import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)

def parsing():
	desc   = "Compute for one pixel the longest discrete line"
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument('image', 
		type=str, help='Image path')
	parser.add_argument('--px', 
		type=int, default=-1, help='X-coordinate')
	parser.add_argument('--py', 
		type=int, default=-1, help='Y-coordinate')
	parser.add_argument('--max-length', 
		type=int, default=math.inf, help='Maximum of length')
	parser.add_argument('--max-frac', 
		type=int, default=math.inf, help='Maximum of orientation i.e. (1/max-length)')
	args = parser.parse_args()
	return args

def compute_for_xy(args):
	i, j = args.py, args.px
	I  = cv.imread(args.image)
	n = len(I.shape)
	if n == 3:
		I  = cv.cvtColor(I, cv.COLOR_RGB2GRAY)
	I  = cv.normalize(I, None, 0, 1, cv.NORM_MINMAX)
	tree = estimater.compute_for_xy(I, i, j, 
		init_tree=SternBrocot.SternBrocot(),
		max_depth=args.max_length,
		max_orientation=args.max_frac)

	estimater.widthen(I, i, j, tree)
	print(tree)
	#element.display_lines(I, i, j, tree)
	#print(element.estimate_width(I, i, j, tree))
	#angle = 0.
	#n = len(tree.nodes)
	#kk = 0
	#for k in range(0, n):
	#	if tree.nodes[k].is_cut:
	#		continue
	#	a, b    = tree.nodes[k].get_frac()
	#	angle  += np.arctan(a / (b+1e-12)) * 180 / np.pi
	#	kk += 1
	#angle   = angle / float(kk)
	#print(angle) 

def compute_for_all(args):
	args = parsing()

	# Reading image
	I  = cv.imread(args.image)
	if len(I.shape)==3:
		I  = cv.cvtColor(I, cv.COLOR_RGB2GRAY)
	I = cv.normalize(I, None, 0, 1, cv.NORM_MINMAX)
	A = np.zeros(I.shape)
	L = np.zeros(I.shape, dtype=np.uint8)
	Dmin = np.zeros(I.shape, dtype=np.uint8)
	Dmax = np.zeros(I.shape, dtype=np.uint8)

	for i in range(1, I.shape[0]-1):
		for j in range(1, I.shape[1]-1):
			if I[i,j] != 1:
				continue;
			T = estimater.compute_for_xy(I, i, j, 
				init_tree=SternBrocot.SternBrocot(),
				max_depth=args.max_length,
				max_orientation=args.max_frac)
			#exit()
			kmax, kmin = estimater.widthen(I, i, j, T)
			#print(kmax, kmin)
			Dmin[i,j] = kmin
			Dmax[i,j] = kmax
			angle = 0.
			n = 1
			for k in range(0, len(T.nodes)):
				if T.nodes[k].is_cut:
					continue
				a, b    = T.nodes[k].get_frac()
				angle  += np.arctan2(a, b+1e-12) * 180 / np.pi
				n += 1
			angle   = angle / float(n) 
			A[i, j] = angle
			L[i, j] = T.ddepth
		print(i)
	#np.savetxt('out/orientation.out'.format(args.image), A)
	width_map =  np.array(Dmin + Dmax, dtype=np.int32)
	A = cv.normalize(A, None, 0, 255, cv.NORM_MINMAX)
	cv.imwrite('out/Dmin.png', Dmin)
	cv.imwrite('out/Dmax.png', Dmax)
	cv.imwrite('out/width_map.png', width_map)
	cv.imwrite('out/orientation_map.png', A)
	cv.imwrite('out/length_map.png', L)

if __name__ == '__main__':
	args = parsing()
	if args.px == -1 or args.py == -1:
		compute_for_all(args)
	else:
		compute_for_xy(args)