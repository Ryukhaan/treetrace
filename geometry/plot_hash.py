import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

def hash1(x, n):
	return sum([ord(x[i]) * np.power(128, i) for i in range(0,len(x))]) % n

def hash2(x, n):
	return int(''.join([str(ord(char)) for char in x])) % n

def hash3(x, n):
	return sum([ord(c) for c in x]) % n 

def hash4(x, n):
	return int(''.join([format(ord(c), "b") for c in x])) % n 


n = int(sys.argv[1])
p = int(sys.argv[2])
with open("/usr/share/dict/words") as file:
	occ = [0] * n
	for word in file.readlines():
		if p == 1: v = hash1(word.rstrip(), n)
		if p == 2: v = hash2(word.rstrip(), n)
		if p == 3: v = hash3(word.rstrip(), n)
		if p == 4: v = hash4(word.rstrip(), n)
		occ[v] += 1

plt.scatter(np.linspace(0,n-1,n), occ, s=5, marker='+')
plt.ylabel("Nombre de collisions")
plt.xlabel("H(x)")
#plt.show()
plt.savefig('hash_collision_{}_n{}.png'.format(p, n))


