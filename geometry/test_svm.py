import numpy as np # linear algebra
import json
import os
from matplotlib import pyplot as plt
from skimage import color
from skimage.feature import hog
from sklearn import svm
from sklearn.metrics import classification_report,accuracy_score
import subprocess

import pickle
from joblib import dump, load

dataset = "/Users/remidecelle/Documents/Treetrace/mnist_png/"
program = './DSSFilter'
categories = ['0','1','2','3','4','5','6','7','8','9']

x_test = []
y_test = []
clf = load('svm.joblib')

for label in os.listdir(dataset+"training/"):
	if label == ".DS_Store": continue
	for idx, file in enumerate(os.listdir(dataset+"training/" + label + "/")):
		if idx % 1000 != 0: continue
		if file == ".DS_Store": continue
		filename = dataset+"training/" + label + "/" + file
		subprocess.call([program] + [filename])
		f = open("./histograms.json", 'r')
		data = json.load(f)
		f.close()
		x_test.append(np.array([np.array(x, dtype=np.int32) for y in data["data"] for x in data["data"][y]]))
		y_test.append(int(label))

y_pred = clf.predict(x_test)
print("Accuracy: "+str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))

