import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import cv2 as cv
import numpy as np

import time
import argparse

import SternBrocot
import estimater
import DSL

def parsing():
	desc   = "Compute for one pixel the longest discrete line"
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument('--x', 
		type=int, default=200, help='X-coordinate')
	parser.add_argument('--y', 
		type=int, default=230, help='Y-coordinate')
	#parser.add_argument('--folder',
	#	type=str, default='./out/', help='Path folder for Dilatations/Erosions')
	parser.add_argument('--image',
		type=str, default='circle_noise.png', help='Image name')
	args = parser.parse_args()
	return args

def createXYZ(args):
	xs = []
	ys = []
	zs = []
	colors_erodes = []
	colors_dilates = []
	for i in range(0, len(args.dilates)):
	    for j in range(0, len(args.dilates[i])):
	        a, b = args.fracs[i][j]
	        xs.append(2*(i+1)+1)
	        ys.append(b)
	        zs.append(a)
	        v    = args.erodes[i][j][args.y,args.x]
	        colors_erodes.append(v)
	        v    = args.dilates[i][j][args.y,args.x]
	        colors_dilates.append(v)
	return xs, ys, zs, colors_erodes, colors_dilates

def scatter_plot(I, args):
	n = len(args.dilates)
	dn = int(np.ceil(np.sqrt(n)))
	fig, ax = plt.subplots(dn, dn, figsize=(7,7))
	ax = ax.flatten()
	for i in range(0, n):
		ys = []
		xs = []
		cs = []
		for j in range(0, len(args.dilates[i])):
			a, b = args.fracs[i][j]
			xs.append(b)
			ys.append(a)
			cs.append(args.erodes[i][j][args.y, args.x])
		ax[i].scatter(xs, ys, s=8, c=cs, alpha=1, cmap='viridis')
		ax[i].set_title("Length {}".format(2*(i+1)+1))

	fig.subplots_adjust(wspace=0.5, hspace=0.5)
	#fig.canvas.manager.window.showMaximized()
	max_dilates = np.zeros((n, 1))
	max_erodes  = np.zeros((n, 1))
	min_dilates = np.zeros((n, 1))
	min_erodes  = np.zeros((n, 1))
	arg_dilates = np.zeros((n, 1))
	arg_erodes  = np.zeros((n, 1))
	for length in range(0, n):
		d = np.array(np.array(args.dilates[length][:])[:, args.y, args.x])
		e = np.array(np.array(args.erodes[length][:])[:, args.y, args.x])
		max_dilates[length] = np.amax(d)
		max_erodes[length]  = np.amax(e)
		min_dilates[length] = np.amin(d)
		min_erodes[length]  = np.amin(e)
		arg_dilates[length] = np.argmax(d)
		arg_erodes[length]  = np.argmax(e)
	
	arg2dilate = np.argmax(min_dilates, axis=0)
	arg2erode  = np.argmax(max_erodes, axis=0)
	max_dilate = np.amax(max_dilates, axis=0)
	max_erode  = np.amax(max_erodes, axis=0)
	min_dilate = np.amin(min_dilates, axis=0)
	min_erode  = np.amin(min_erodes, axis=0)
	Gdir_dil  = max_dilate - min_dilate
	Gdir_ero  = max_erode - min_erode
	Gdir = np.maximum(Gdir_dil, Gdir_ero)
	if Gdir == Gdir_ero:
	    p = arg2erode[0]
	    q = arg_erodes[p]    
	else:
	    p = arg2dilate[0]
	    q = arg_dilates[p]
	aa, bb = args.fracs[p][int(q)]
	ww = np.maximum(abs(aa), abs(bb))
	line, _, _ = DSL.construct(aa, bb, 0, ww, p)

	i += 1
	subI = np.array(I[args.y-n:args.y+n, args.x-n:args.x+n], dtype=np.int32)
	for p in range(0, line.shape[0]):
		subI[-line[p,1]+n, line[p,0]+n] = 257

	viridis = cm.get_cmap('gray', 256)
	newcolors = viridis(np.linspace(0, 1, 256))
	newcolors[-1, :] = np.array([1., 0., 0., 1.])
	newcmp = ListedColormap(newcolors)
	ax[i].imshow(subI, cmap=newcmp)
	ax[i].scatter(n, n, c="#00ff00", marker='+')

	ax[i].grid(which='minor', axis='both', linestyle='-', color='black', linewidth=1)
	## Major ticks
	#ax[i].set_xticks(np.arange(0, subI.shape[1], 1))
	#ax[i].set_yticks(np.arange(0, subI.shape[0], 1))
	## Labels for major ticks
	#ax[i].set_xticklabels(subI.shape[1] * [" "])
	#ax[i].set_yticklabels(subI.shape[0] * [" "])
	## Minor ticks
	#ax[i].set_xticks(np.arange(-.5, subI.shape[1], 1), minor=True)
	#ax[i].set_yticks(np.arange(-.5, subI.shape[0], 1), minor=True)
	#ax[i].set_xlim(args.x-n, args.x+n)
	##ax[i].set_ylim(args.y+n, args.y-n)
	plt.show()


def plot(I, x, y, xs, ys, zs, c_erodes,c_dilates):
	#fig = plt.figure(figsize=(7,7))
	#ax1 = fig.add_subplot(121)
	#_ = ax1.imshow(I, 'gray')
	#_ = ax1.scatter(x, y, c="#ff0000", marker='+')
	#ax1.set_xlim(x-31,x+31)
	#ax1.set_ylim(y+31,y-31)
	#ax2 = fig.add_subplot(122, projection='3d')
	#ax2.scatter(xs, ys, zs, c=c_erodes, alpha=1)
	#ax2.view_init(0,0)
	#ax2.dist = 7
	#plt.show()
	
	fig, ax = plt.subplots(3, 1, figsize=(7,7))
	ax = ax.flatten()
	_ = ax[0].imshow(I, 'gray')
	_ = ax[0].scatter(x, y, c="#ff0000", marker='+')
	pcopen = ax[1].scatter(xs, ys, s=8, c=colors_erodes, cmap='viridis')
	xtick = np.linspace(3, 2*len(erodes)+1, len(erodes), dtype=np.uint8)
	_ = ax[1].set_xticks(xtick)
	pcclos = ax[2].scatter(xs, ys, s=8, c=colors_dilates, cmap='viridis')
	_ = ax[2].set_xticks(xtick)
	_ = fig.colorbar(pcopen, ax=ax[1])
	_ = fig.colorbar(pcclos, ax=ax[2])
	plt.show()

if __name__ == '__main__':
	args = parsing()
	args.dilates = np.load('./out/dilates.npy', allow_pickle=True)
	args.erodes  = np.load('./out/erodes.npy', allow_pickle=True)
	args.fracs   = np.load('./out/fracs.npy', allow_pickle=True)
	xs, ys, zs, cd, ce = createXYZ(args)
	I = cv.imread("./images/{}".format(args.image))
	I = cv.cvtColor(I, cv.COLOR_BGR2GRAY)
	I = cv.normalize(I, None, 0, 255, cv.NORM_MINMAX)
	#plot(I, args.x, args.y, xs, ys, zs, cd, ce)
	scatter_plot(I, args)


