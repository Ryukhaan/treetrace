import numpy as np
import copy
import math
import time

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import cv2 as cv

import DSL
import SternBrocot

debug = False   

def compute_for_xy(BW, y, x, init_tree=None, max_depth=math.inf, max_orientation=math.inf):
    global debug
    tree = SternBrocot.SternBrocot() if init_tree is None else copy.deepcopy(init_tree)
    
    old_tree = copy.copy(tree)
    value = BW[y, x]
    # UI
    if debug:
        I = BW.copy()
        fig, ax = plt.subplots(figsize=(7,7))
        ax.imshow(I)

    while tree.candidates() > 1 and tree.ddepth < max_depth:
        # Save old tree
        old_tree = copy.copy(tree)

        # Increment fraction
        if ((tree.ddepth) < max_orientation):
            tree.increment()
        else:
            tree.ddepth += 1

        ddepth = tree.ddepth
        square = 2*ddepth+1
            
        # For each node (orientation a/b)
        for idx, node in enumerate(tree.nodes):
            if (node.is_cut):
                continue
                
            # Retrieve coefficient a, b from node
            a, b  = node.get_frac()

            # Compute omega and mu
            omega = np.maximum(np.abs(a), np.abs(b))

            if ddepth < 5:
                for mu in range(0, -omega, -1):
                    # Construct the line
                    node.T, _, _ = DSL.fast_construct(a, b, mu, omega, ddepth)
                    se = node.T
                    # Proceed to erode / dilatation
                    mu_cond = True
                    for i in range(0, se.shape[0]):
                        # Check if line of length square can be construct
                        if ( y - se[i,1] < 0 
                           or y - se[i,1] >= BW.shape[1]
                           or x + se[i,0] < 0
                           or x + se[i,0] >= BW.shape[0]):
                            tree.nodes[idx].is_cut = True
                            break;
                        # UI
                        if debug and BW[y-se[i,1], x+se[i,0]] == value: 
                            I[y-se[i,1], x+se[i,0]] = ddepth+2

                        new_value = BW[y-se[i,1], x+se[i,0]]
                        cond_cut = new_value != value
                        mu_cond = mu_cond and not cond_cut
                        if cond_cut:
                            break

                    if mu_cond:
                        node.mu = mu
                        break

                if not mu_cond:
                    tree.nodes[idx].is_cut = cond_cut
            else:
                mu    = node.mu #np.ceil(omega//2)
                
                # Construct the line
                node.T, _, _ = DSL.fast_construct(a, b, mu, omega, ddepth)
                
                se = node.T

                # Proceed to erode / dilatation
                for i in range(0, se.shape[0]):
                    # Check if line of length square can be construct
                    if ( y - se[i,1] < 0 
                       or y - se[i,1] >= BW.shape[1]
                       or x + se[i,0] < 0
                       or x + se[i,0] >= BW.shape[0]):
                        tree.nodes[idx].is_cut = True
                        break;

                    if debug and BW[y-se[i,1], x+se[i,0]] == value: 
                        I[y-se[i,1], x+se[i,0]] = ddepth + 2
                        #I[y-se[i,1], x+se[i,0]] += 1
                        
                    new_value = BW[y-se[i,1], x+se[i,0]]
                    cond_cut = new_value != value
                    tree.nodes[idx].is_cut = cond_cut
                    if cond_cut:
                        break


        # # UI
        if debug:
            I[y, x] = 2
            nx = x / I.shape[1]
            ny = 1. - y / I.shape[0]
            ax.cla()
            viridis = cm.get_cmap('autumn', ddepth+3)
            newcolors = viridis(np.linspace(0, 1, ddepth+3))
            newcolors = np.flipud(newcolors**0.5)
            newcolors[0, :] = np.array([0., 0., 0., 1])
            newcolors[1, :] = np.array([1., 1., 1., 1])
            newcmp = ListedColormap(newcolors)
            # Display Colormap and Image
            cc = ax.imshow(I, cmap=newcmp)
            # Display Xlim and Ylim
            xmin = np.maximum(x-ddepth-1, 0)
            xmax = np.minimum(x+ddepth+1, I.shape[1]-1)
            ymin = np.maximum(y-ddepth-1, 0)
            ymax = np.minimum(y+ddepth+1, I.shape[0]-1)
            axins = ax.inset_axes([.5-float(nx>0.5)/2, .5-float(ny>0.5)/2, .5, .5])
            axins.imshow(I, cmap=newcmp, 
                origin='lower', interpolation="nearest")
            # sub region of the original image
            axins.set_xlim(xmin, xmax)
            axins.set_ylim(ymax, ymin)
            axins.set_xticklabels('')
            axins.set_yticklabels('')
            ax.indicate_inset_zoom(axins)
            lines = False
            plt.pause(0.01)

    if debug:
        ax.cla()
        plt.pause(1)
        plt.close()

    if (tree.candidates() == 0):
        return old_tree
    return tree

def estimate_width(BW, y, x, tree):
    ddepth = tree.ddepth
    square = 2*ddepth+1
    value = BW[y, x]
    #tmp = BW.copy()
    k_min = []
    k_max = []
    for idx, node in enumerate(tree.nodes):
        if (node.is_cut):
            continue  
        a, b  = node.get_frac()
        omega = np.maximum(np.abs(a), np.abs(b))
        mu    = node.mu

        kk = 1
        tk_max = 0
        tk_min = 0
        border1_not_found = True
        border2_not_found = True
        while border1_not_found or border2_not_found:
            T, _, _ = DSL.fast_construct(-b, a, mu, omega, kk)
            if border1_not_found:
                new_value = BW[y-T[-1,1], x+T[-1,0]]
                #tmp[y-T[-1,1], x+T[-1,0]] = 2
                border1_not_found = new_value == value
                if border1_not_found:
                    tk_max += 1
                else:
                    pmax = 0. if tk_max == 0 else np.sqrt(T[-1,1]**2 + T[-1,0]**2)
            if border2_not_found:
                new_value = BW[y-T[-2,1], x+T[-2,0]]
                #tmp[y-T[-2,1], x+T[-2,0]] = 3
                border2_not_found = new_value == value
                if border2_not_found:
                    tk_min += 1
                else:
                    pmin = 0. if tk_min == 0 else np.sqrt(T[-2,1]**2 + T[-2,0]**2)
            kk = kk + 1

        #k_max.append(pmax)
        #k_max.append(pmin)
        k_max.append(tk_max)
        k_max.append(tk_min)
        

    resmax = np.amax(k_max) if len(k_max) > 0 else 0
    resmin = np.amin(k_max) if len(k_max) > 0 else 0
    return resmax, resmin

def widthen(BW, y, x, tree):
    ddepth = tree.ddepth
    square = 2*ddepth+1
    value = BW[y, x]
    width_max = [0]
    width_min = [0]

    #if debug:
    #    fig, ax = plt.subplots(figsize=(7,7))

    for idx, node in enumerate(tree.nodes):
        if (node.is_cut):
            continue
        a, b  = node.get_frac()
        mu    = node.mu
        omega = np.maximum(np.abs(a), np.abs(b))

        #if debug:
        #    I = np.array(BW, dtype=np.int32)
        #    ax.imshow(I)

        T, _, _ = DSL.construct(a, b, mu, omega, ddepth)
        T = np.array(T, dtype=np.int32)
        k_max = 0
        k_min = 0
        T_max = T
        T_min = T
        is_widthable_max = True
        is_widthable_min = True
        while is_widthable_max or is_widthable_min:
            #if debug:
            #    if k_min < ddepth:
            #        for i in range(0, T_min.shape[0]):
            #            I[y-T_min[i,1],x+T_min[i,0]] = k_min+2
            #    if k_max < half_ddepth:
            #        for i in range(0, T_max.shape[0]):
            #            I[y-T_max[i,1],x+T_max[i,0]] = k_max+2

            if is_widthable_max:
                T_max = DSL.thicken_line(T_max, a, b, 1)
                k_max += 1
                for i in range(0, T_max.shape[0]):
                    cond = BW[y-T_max[i,1],x+T_max[i,0]] != value
                    if cond:
                        k_max -= 1
                        is_widthable_max = False
                        break
                #if k_max >= ddepth:
                #    is_widthable_max = False
            
            if is_widthable_min:
                T_min = DSL.thicken_line(T_min, a, b, -1)
                k_min += 1
                for i in range(0, T_min.shape[0]):
                    cond = BW[y-T_min[i,1],x+T_min[i,0]] != value
                    if cond:
                        k_min -= 1
                        is_widthable_min = False
                        break
                #if is_widthable_min >= ddepth:
                #    is_widthable_min = False

        #if debug:
        #    viridis = cm.get_cmap('Spectral', k_max+k_min+3)
        #    newcolors = viridis(np.linspace(0, 1, k_max+k_min+3))
        #    newcolors[0, :] = np.array([0., 0., 0., 1])
        #    newcolors[1, :] = np.array([1., 1., 1., 1])
        #    newcmp = ListedColormap(newcolors)
        #    ax.imshow(I, cmap=newcmp)
        #    plt.pause(0.1)
        
        width_max.append(k_max)
        width_min.append(k_min)

    #plt.show()
    return np.amax(width_max), np.amax(width_min)



def display_lines(BW, y, x, tree):
    ddepth = tree.ddepth
    square = 2*ddepth+1 

    viridis = cm.get_cmap('viridis', 2*len(tree.nodes)+2)
    newcolors = viridis(np.linspace(0, 1, 256))
    newcolors[0, :] = np.array([0/256, 0/256, 0/256, 1])
    newcolors[1, :] = np.array([256/256, 256/256, 256/256, 1])
    newcmp = ListedColormap(newcolors)
    
    kmax, kmin = estimate_width(BW, y, x, tree)
    print(kmax, kmin)
    delta = np.maximum(ddepth, int(kmax))
    I = np.pad(BW, ((delta,delta), (delta,delta)))
    ny = y+delta
    nx = x+delta
    for idx, node in enumerate(tree.nodes):
        if (node.is_cut):
            continue
            
        a, b    = node.get_frac()
        print("{}/{}".format(a,b))
        omega   = np.maximum(np.abs(a), np.abs(b))
        mu      = 0 #np.ceil(omega//2)
        T, _, _ = DSL.construct(a, b, mu, omega, ddepth-1)

        for i in range(0, T.shape[0]):
           I[ny-T[i,1], nx+T[i,0]] = 2 * idx
        
        T, _, _ = DSL.construct(-b, a, mu, omega, delta)
        for i in range(0, T.shape[0]):
           I[ny-T[i,1], nx+T[i,0]] = 2 * idx - 1 #* I[ny-T[i,1], nx+T[i,0]]
        
        #print("Fraction : {} / {} - Length : {}".format(a,b,square))
        fig, ax = plt.subplots(figsize=(7,7))

        I[y+delta, x+delta] = 4
        ax.imshow(I, cmap=newcmp)
        #ax.grid(which='minor', axis='both', linestyle='-', color='black', linewidth=1)

        ## Major ticks
        #ax.set_xticks(np.arange(0, I.shape[1], 1))
        #ax.set_yticks(np.arange(0, I.shape[0], 1))

        ## Labels for major ticks
        #ax.set_xticklabels(I.shape[1] * [" "])
        #ax.set_yticklabels(I.shape[0] * [" "])

        ## Minor ticks
        #ax.set_xticks(np.arange(-.5, I.shape[1], 1), minor=True)
        #ax.set_yticks(np.arange(-.5, I.shape[0], 1), minor=True)
        plt.show()


def compute_for_graylevel(I, 
                        tree=SternBrocot.SternBrocot(use_signed_orientation=True), 
                        max_depth=math.inf, 
                        max_orientation=math.inf,
                        max_width=1):
    openings = []
    closings = []
    coefficients = []

    if debug:
        fig, ax = plt.subplots(figsize=(7,7))
        #ax.imshow(I)

    while tree.ddepth < max_depth:
        print(tree.ddepth);
        #print(tree)
        coefficient = []
        opening = []
        closing = []
        if ((tree.ddepth+1) < max_orientation):
            tree.increment()
        else:
            tree.ddepth += 1
        ddepth = tree.ddepth
        square = 2*ddepth+1
        for idx, node in enumerate(tree.nodes):
            if (node.is_cut):
                continue
            # Retrieve coefficient a, b from node
            a, b  = node.get_frac()
            # Compute omega and mu
            omega = np.maximum(np.abs(a), np.abs(b))
            mu    = 0 #np.ceil(omega//2)
            # Construct the line
            node.T, node.err1, node.err2 = DSL.construct(a, b, mu, omega, ddepth)
            
            # Create mask for erosion and dilatation
            kernel = np.zeros((square, square), dtype=np.uint8)
            for i in range(0, node.T.shape[0]):
                kernel[-node.T[i,1]+ddepth, node.T[i,0]+ddepth] = 1

            if debug:
                ax.cla()
                ax.imshow(kernel)
                plt.pause(0.01)

            coefficient.append((a,b))
            # Proceed morphology openings / closings
            #res_open = cv.morphologyEx(I, cv.MORPH_OPEN, kernel)
            #res_clos = cv.morphologyEx(I, cv.MORPH_CLOSE, kernel)
            #res_open = cv.dilate(I, kernel, 1)
            res_open = cv.erode(255-I, kernel, 1)
            res_clos = cv.erode(I, kernel, 1)

            res_open = res_open.astype(np.float32)
            res_clos = res_clos.astype(np.float32)
            octant = DSL.check_octant(a,b)
            if octant == DSL.Octant.FIRST \
                or octant == DSL.Octant.EIGHTH \
                or octant == DSL.Octant.FOURTH \
                or octant == DSL.Octant.FIFTH:
                A = np.array([[1], [1], [0]], dtype=np.uint8)
            else :
                A = np.array([1,1,0], dtype=np.uint8)

            As = np.flipud(A)
            # With openings / closings
            #Ap = kernel.copy()
            #Am = kernel.copy()
            #ww = max_width if ddepth >= max_width else ddepth
            #Ap = np.pad(Ap, (ww-1,ww-1), mode='constant', constant_values=((0,0),(0,0)))
            #Am = np.pad(Am, (ww-1,ww-1), mode='constant', constant_values=((0,0),(0,0)))
            #for width in range(0, ww-1):
            #    Ap = cv.dilate(Ap, A)
            #    res_open += cv.morphologyEx(I, cv.MORPH_OPEN, Ap)
            #    res_clos += cv.morphologyEx(I, cv.MORPH_CLOSE, Ap)
            #    Am = cv.dilate(Am, As)
            #    res_open += cv.morphologyEx(I, cv.MORPH_OPEN, Ap)
            #    res_clos += cv.morphologyEx(I, cv.MORPH_CLOSE, Ap)
            
            add_open_p = res_open.copy()
            add_open_m = res_open.copy()
            add_clos_p = res_clos.copy()
            add_clos_m = res_clos.copy()
            ww = max_width if ddepth >= max_width else ddepth
            for width in range(0, ww-1):
                #add_open_p = cv.dilate(add_open_p, A, 1)
                #add_open_m = cv.dilate(add_open_m, As, 1)
                add_open_p = cv.erode(add_open_p, A, 1)
                add_open_m = cv.erode(add_open_m, As, 1)
                res_open += add_open_p
                res_open += add_open_m
                add_clos_p = cv.erode(add_clos_p, A, 1)
                add_clos_m = cv.erode(add_clos_m, As, 1)
                res_clos += add_clos_p
                res_clos += add_clos_m

            res_open = res_open #/ (2*ww)
            res_clos = res_clos #/ (2*ww)
            opening.append(res_open)
            closing.append(res_clos)

        coefficients.append(coefficient)
        openings.append(opening)
        closings.append(closing)

    return openings, closings, coefficients


def orientation_gray(openings, closings, coefficients):
    n = len(openings)
    max_openings = np.zeros((n, openings[0][0].shape[0], openings[0][0].shape[1]))
    max_closings = np.zeros((n, openings[0][0].shape[0], openings[0][0].shape[1]))
    min_openings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
    min_closings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))

    arg_openings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
    arg_closings = np.zeros((n, closings[0][0].shape[0], openings[0][0].shape[1]))
    for length in range(0,n):
        max_openings[length, :, :] = np.amax(np.array(openings[length][:]), axis=0)
        max_closings[length, :, :] = np.amax(np.array(closings[length][:]), axis=0)
        
        min_openings[length, :, :] = np.amin(np.array(openings[length][:]), axis=0)
        min_closings[length, :, :] = np.amin(np.array(closings[length][:]), axis=0)
        
        arg_openings[length, :, :] = np.argmax(np.array(openings[length][:]), axis=0)
        arg_closings[length, :, :] = np.argmin(np.array(closings[length][:]), axis=0)
      

    arg2open = np.argmin(max_openings, axis=0)
    arg2clos = np.argmax(min_closings, axis=0)

    max_opening = np.amax(max_openings, axis=0)
    max_closing = np.amax(max_closings, axis=0)
    min_opening = np.amin(min_openings, axis=0)
    min_closing = np.amin(min_closings, axis=0)

    Gdir_open  = max_opening - min_opening
    Gdir_clos = max_closing - min_closing
    Gdir = np.maximum(Gdir_open, Gdir_clos)

    orientation = np.zeros(max_opening.shape)
    for y in range(0, arg2open.shape[0]):
        for x in range(0, arg2open.shape[1]):
            if Gdir[y,x] == Gdir_open[y,x]:
                m = arg2open[y,x]
                n = arg_openings[m][y,x]    
            else:
                m = arg2clos[y,x]
                n = arg_closings[m][y,x]
            a, b = coefficients[m][int(n)]
            orientation[y, x] = np.arctan(a/(b+1e-12))

    plt.imshow(orientation, 'gray')
    plt.colorbar()
    plt.show()
    return orientation, Gdir, max_opening, min_closing     


def gray_transform(I, n, x, y):

    if debug:
        fig, ax = plt.subplots(figsize=(7,7))

    k = (n-1)//2
    dilates = np.zeros((n,n))
    erodes = np.zeros((n,n))
    for a in range(n):
        for b in range(n):
            aa = a - k
            bb = b - k
            if aa == 0 and bb == 0: continue
            omega = np.maximum(np.abs(aa), np.abs(bb))
            mu    = 0
            
            print(omega, nn)
            ddepth = (omega+1)//2 * np.gcd(np.abs(aa),np.abs(bb))
            assert (ddepth % 2 == 1)
            square = (ddepth-1)//2
            
            # Construct the line
            T, _, _ = DSL.construct(a, b, mu, omega, square-1)
            kernel = np.zeros((ddepth, ddepth), dtype=np.uint8)
            print(T.shape, kernel.shape)
            for i in range(0, T.shape[0]):
                kernel[-T[i,1]+ddepth, T[i,0]+ddepth] = 1
            
            dilates[a,b] = cv.erode(255-I, kernel, 1)
            erodes[a,b] = cv.erode(I, kernel, 1)
    
    return dilates, erodes