import os

from trainer import Trainer
from config import get_config
from constants import Flags
import dataloader
import utils

def main(config):
  if config.mode == Flags.EVAL_MODE:
    return Trainer.eval(config)
  train_path, val_path, trainsize, valsize = dataloader.loading(config)
  trainer = Trainer(config, train_path, val_path, trainsize, valsize)
  if config.mode == Flags.TRAINING_MODE:
    trainer.train()
  if config.mode == Flags.PREDICT_MODE:
    val_data = '{}/data/{}/val/images/'.format(os.getcwd(), config.set)
    #val_data = config.set
    trainer.test(val_data, valpath=True)
  if config.mode == Flags.ANALYZE_MODE:
    trainer.analyze()
  if config.mode == Flags.SHOW_MODE:
    trainer.show_overlay()




if __name__ == "__main__":
  config = get_config()
  main(config)

