#!/bin/bash

if [[ $1 == 1 ]]; then
	python3 main.py --model=simple --name=SawSimple1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=simple --name=SawSimple8 --set="sawmill_8" --mode=train --verbose
fi

if [[ $1 == 2 ]]; then
	python3 main.py --model=unet --name=SawUnet1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=unet --name=SawUnet8 --set="sawmill_8" --mode=train --verbose
fi

if [[ $1 == 3 ]]; then
	python3 main.py --model=attunet --name=SawAtt1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=attunet --name=SawAtt8 --set="sawmill_8" --mode=train --verbose
fi

if [[ $1 == 4 ]]; then
	python3 main.py --model=cbam --name=SawCbam1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=cbam --name=SawCbam8 --set="sawmill_8" --mode=train --verbose
fi

if [[ $1 == 5 ]]; then
	python3 main.py --model=mam --name=SawMam1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=mam --name=SawMam8 --set="sawmill_8" --mode=train --verbose
fi

if [[ $1 == 6 ]]; then
	python3 main.py --model=triplet --name=TripletUnet1 --set="sawmill_1" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet2 --set="sawmill_2" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet3 --set="sawmill_3" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet4 --set="sawmill_4" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet5 --set="sawmill_5" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet6 --set="sawmill_6" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet7 --set="sawmill_7" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet8 --set="sawmill_8" --mode=train --verbose
fi
