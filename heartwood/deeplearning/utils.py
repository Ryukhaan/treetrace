import numpy as np
import os
import cv2

import IPython.display as display
from IPython.display import clear_output
import PIL
import PIL.Image
import tensorflow as tf
import matplotlib.pyplot as plt

#from skimage.color import rgb2lab
#from skimage.segmentation import slic
import skimage.io as io
from skimage import img_as_ubyte
import pathlib
from glob import glob

import metrics

def save_image(save_path,
                npyfile,
                file_name="predicted"):
        #if len(npyfile.shape) == 3:
        #  img = npyfile[:,:,0]
        #else:
        #name, ext = file_name.split(".")
        #filename = name + ".png"
        img = npyfile
        io.imsave(os.path.join(save_path, file_name),img_as_ubyte(img))

def parse_image(img_path):
    """Load an image and its mask.
    Return a dictionary.

    Parameters
    ----------
    img_path : str
        Image (not the mask) location.

    Returns
    -------
    dict
        Dictionary mapping an image and its mask.
    """
    # Load image (jpeg foramt)
    image = tf.io.read_file(img_path)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)

    # Convert to hsv color space
    #image = rgb_to_lab(image) #tf.image.rgb_to_hsv(image)

    # For one Image path:
    # .../traindir/images/logyard_A.jpg
    # Its corresponding annotation path is:
    # .../traindir/mask/logyard_A.png
    mask_path = tf.strings.regex_replace(img_path, "images", "mask")
    mask_path = tf.strings.regex_replace(mask_path, "jpeg", "png")

    mask = tf.io.read_file(mask_path)
    # The masks contain an image with one channel
    mask = tf.image.decode_png(mask, channels=1)
    mask = tf.where(mask >= 1, 
    	np.dtype('uint8').type(255), #(1)
    	np.dtype('uint8').type(0))
    # Note that we have to convert the new value (0)
    # With the same dtype than the tensor itself
    return (image, mask)

def normalize(image: tf.Tensor) -> tf.Tensor:
	return cv2.normalize(image, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

def resize_and_rescale(image, mask, imgshape):
	"""
	Apply resize of shape (height, width, channels)
	and rescale between [0,1]

	Notes
	-----
	Mask is resized using nearest interpolation 
	while input image is resized using bilinear interpolation.

	Parameters
	----------
	image 	: Tensor
		Input image
	mask 	: Tensor
		Output mask
	imgshape: tuple
		Wanted shape (height, width, channels)

	Returns
	-------
	tuple
		Output image and its mask of shape imgshape.
	"""
	image = tf.image.resize(image, imgshape)
	mask  = tf.image.resize(mask,  imgshape, method='bicubic', antialias=True)
	#mask  = tf.image.resize(mask,  imgshape, method='nearest')
	image = tf.cast(image, tf.float32) / 255.0
	#image = tf.image.per_image_standardization(image)
	#mask  = tf.cast(mask, tf.float32) / 255.0
	mask  = tf.cast(tf.where(mask > 1,
				np.dtype('uint8').type(1),
				np.dtype('uint8').type(0)), tf.float32)
	mask  = tf.cast(mask, tf.float32)
	return image, mask

@tf.function
def data_augmentation(data, seed=42):
	"""
	Apply some transformations to an input tuple
	containing a train image and its mask.

	Notes
	-----
	Rotation / Translation has also to be applied on the mask

	Parameters
	----------
	image_label : tuple
	    A tuple containing an image and its mask.
	image_shape : tuple
		A tuple containing (height, width, channels) for resizing.
	seed 		: int
		Seed number	

	Returns
	-------
	tuple
	    A modified image and its mask.
	"""
	image, mask = data

	#-- Flip Up / Down --#
	if tf.random.uniform((), seed=seed) > 0.5:
		image = tf.image.flip_up_down(image)
		mask  = tf.image.flip_up_down(mask)
	#-- Flip Left / Right --#
	if tf.random.uniform((), seed=seed) > 0.5:
		image = tf.image.flip_left_right(image)
		mask  = tf.image.flip_left_right(mask)
	#-- Rot90 --#
	if tf.random.uniform((), seed=seed) > 0.5:
		image = tf.image.rot90(image)
		mask  = tf.image.rot90(mask)
	#new_seed = (seed,2)
	#image = tf.image.stateless_random_brightness(
    #  image, max_delta=0.5, seed=new_seed)
	#image = tf.image.stateless_random_contrast(
    #  image, lower=0.1, upper=0.9, seed=new_seed)

	return image, mask

@tf.function
def prepare(ds, seed, batch_size, image_shape, augment=False):
	def f(x,y):
		return resize_and_rescale(x, y, image_shape)
	def g(x,y):
		data = x,y
		return data_augmentation(data, seed=seed)
	h = g if augment else f
	#return (
	#		ds
	#		.shuffle(32)
	#		.map(h, num_parallel_calls=tf.data.AUTOTUNE)
	#		.batch(batch_size)
	#		.prefetch(tf.data.AUTOTUNE)
	#		)
	ds = ds.shuffle(buffer_size=1024)
	ds = ds.map(h, num_parallel_calls=tf.data.AUTOTUNE)
	ds = ds.batch(batch_size)
	#return ds.prefetch(buffer_size=tf.data.AUTOTUNE)
	return ds


# def parse_vector(img_path):
# 	"""Load an image and its mask.
# 	Return a dictionary.

# 	Parameters
# 	----------
# 	img_path : str
# 	    Image (not the mask) location.

# 	Returns
# 	-------
# 	dict
# 	    Dictionary mapping an image and its mask.
# 	"""
# 	# Load image (jpeg foramt)
# 	#image = tf.io.read_file(img_path)
# 	#image = tf.image.decode_jpeg(image, channels=3)
# 	#image = tf.image.convert_image_dtype(image, tf.float32)
# 	# Convert to hsv color space
# 	#image = tf.image.rgb_to_hsv(image)
# 	image = imread(img_path)
# 	image = rgb2lab(image)

# 	# For one Image path:
# 	# .../traindir/images/logyard_A.jpg
# 	# Its corresponding annotation path is:
# 	# .../traindir/mask/logyard_A.png
# 	mask_path = tf.strings.regex_replace(img_path, "images", "mask")
# 	mask_path = tf.strings.regex_replace(mask_path, "jpeg", "png")

# 	mask = tf.io.read_file(mask_path)
# 	mask = tf.image.decode_png(mask, channels=1)
# 	mask = tf.where(mask >= 1, 
# 		np.dtype('uint8').type(255), #(1)
# 		np.dtype('uint8').type(0))

# 	superpixel_labels = slic(image, n_segments=512, compactness=70, start_label=1)
# 	nb_superpixels = np.max(superpixel_labels) + 1
# 	height = image.shape[0]
# 	width  = image.shape[1]
# 	# pour calculer la position du barycentre dans la largeur de l'image
# 	x_idx = np.repeat(range(width), height)
# 	x_idx = np.reshape(x_idx, [width, height])
# 	x_idx = np.transpose(x_idx)
# 	# pour calculer la position du barycentre dans la hauteur de l'image
# 	y_idx = np.repeat(range(height), width)
# 	y_idx = np.reshape(y_idx, [height, width])
# 	# extraire les caractéristiques de chaque superpixel
# 	l = image[:,:,0]
# 	a = image[:,:,1]
# 	b = image[:,:,2]
# 	feature_superpixels = [] 
# 	y = []
# 	for label in range(nb_superpixels): 
# 		# pixels appartenant au superpixels
# 		idx = superpixel_labels == label 
# 		elt = len(idx)  
# 		sp = []
# 		sp.append(np.mean(a[idx]))
# 		sp.append(np.mean(b[idx]))
# 		sp.append(np.mean(l[idx]))
# 		sp.append(np.mean(x_idx[idx]) / (width - 1))
# 		sp.append(np.mean(y_idx[idx]) / (height - 1)) 		
# 		# constitution du vecteur descripteur
# 		feature_superpixels.append(sp)
# 		# constitution du vecteur de sortie
# 		if np.sum(mask[idx]) > float(len(idx) * 0.5):
# 			y.append([1,0])
# 		else:
# 			y.append([0,1])

# 	return (feature_superpixels, y)

def show_prediction(network, img_path):
	image = tf.io.read_file(img_path)
	image = tf.image.decode_jpeg(image, channels=3)
	image = tf.image.convert_image_dtype(image, tf.float32)

	mask_path = tf.strings.regex_replace(img_path, "images", "mask")
	mask_path = tf.strings.regex_replace(mask_path, "jpeg", "png")
	mask = tf.io.read_file(mask_path)
	mask = tf.image.decode_png(mask, channels=1)
	mask = tf.where(mask >= 1, 
		np.dtype('uint8').type(255),
		np.dtype('uint8').type(0))

	image, mask = resize_and_rescale(image, mask, [256,256])
	inference = network.predict(tf.expand_dims(image, 0))
	pred_mask = inference[0,:,:,:]
	#tf.keras.preprocessing.image.save_img(
	#		''.join([os.getcwd(), 'file.png']), 
	#				pred_mask, scale=False)
	pred_img = tf.keras.preprocessing.image.array_to_img(pred_mask)
	pred_img = cv2.normalize(np.array(pred_img), None, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F)
	#pred_img = tf.keras.backend.round(tf.keras.backend.clip(pred_img, 0, 1))
	fig, ax = plt.subplots(nrows=1, ncols=3)
	ax = ax.flatten()
	ax[0].imshow(tf.keras.preprocessing.image.array_to_img(image))
	ax[1].imshow(tf.keras.preprocessing.image.array_to_img(mask))
	im = ax[2].imshow(pred_img)
	
	fig.colorbar(im, ax=ax[2])
	plt.show()
	plt.close()

def eval(args):
	directory = args.logdir
	directory = directory + "/predict/" + str(args.set) + "/" + str(args.model) + "/"
	files = sorted(os.listdir(directory))
	all_mcc = []
	line = []
	M_tot = 0,0,0,0
	for i in range(0,8):
		dataset = os.getcwd() + "/data/{}_{}/val/mask/".format(args.set, i+1)
		n = 0
		mcc = 0.0
		M_val = 0,0,0,0
		for file in sorted(os.listdir(dataset)):
			if file == '.DS_Store': continue
			filename = directory + file
			filename = tf.strings.regex_replace(filename, "png", "jpeg")
			image = tf.io.read_file(filename)
			image = tf.image.decode_jpeg(image, channels=1)

			mask = tf.io.read_file(dataset + file)
			mask = tf.image.decode_png(mask, channels=1)
			mask = tf.where(mask >= 1, 
				np.dtype('uint8').type(1),
				np.dtype('uint8').type(0))

			mask = tf.image.resize(mask, (304,304), method='nearest')
			y_pred = tf.where(image >= 127, 
	    		np.dtype('uint8').type(1), #(1)
	    		np.dtype('uint8').type(0))
			M_i = metrics.get_confusion_matrix(mask, y_pred)
			M_val = tuple(map(sum, zip(M_val, M_i)))
			#print(file + " MCC = " + str(c_mmc))
			c_mmc = metrics.numpy_mcc(mask, y_pred)
			# z_coeff = np.arctanh(c_mmc)
			# all_mcc.append(z_coeff)
			n += 1
			mcc += c_mmc

		M_tot = tuple(map(sum, zip(M_val, M_tot)))
		line.append("{:.3f}".format(metrics.mcc_from_matrix(M_val)))
		print(format(mcc / n))
	line.append("{:.3f}".format(metrics.mcc_from_matrix(M_tot)))
	#line.append("{:.3f}".format(np.tanh(np.std(all_mcc))))
	print(" & ".join(line))
	#print("{:.3f}".format(np.mean(all_mcc)))
	#print("{:.3f}".format(np.std(all_mcc)))

	# i = 0
	# mcc = 0.0
	# for file in files:
	# 	if file == ".DS_Store": continue
	# 	filename = directory + file
	# 	image = tf.io.read_file(filename)
	# 	image = tf.image.decode_jpeg(image, channels=1)

	# 	mask_path = os.getcwd() + "/data/logyard/mask/"
	# 	mask_name = tf.strings.regex_replace(file, "jpeg", "png")
	# 	mask = tf.io.read_file(mask_path + mask_name)
	# 	mask = tf.image.decode_png(mask, channels=1)
	# 	mask = tf.where(mask >= 1, 
	# 		np.dtype('uint8').type(1),
	# 		np.dtype('uint8').type(0))

	# 	mask = tf.image.resize(mask, (304,304), method='nearest')
	# 	#y_pred = tf.image.resize(image, (mask.shape[0], mask.shape[1]), 
	# 	#	method='bicubic', antialias=True)
	# 	y_pred = tf.where(image >= 127, 
 #    		np.dtype('uint8').type(1), #(1)
 #    		np.dtype('uint8').type(0))
	# 	c_mmc = metrics.numpy_mcc(mask, y_pred)
	# 	#print(file + " MCC = " + str(c_mmc))
	# 	i = i + 1
	# 	mcc += c_mmc
	# print(mcc / i)


