import numpy as np 
import os
import tensorflow as tf 
from enum import Enum

import utils
import layers

class NetworkType(Enum):
	_unet_t 		= "unet"
	_gating_t 		= "attunet"
	_cbam_t 		= "cbam"
	_simple_t 		= "simple"
	_triplet_t 		= "triplet"
	_mam_t 			= "mam"

	def __str__(self):
		return self.value


class SimpleUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs  = tf.keras.layers.Input(self.input_size) # 304x304x3

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs,
									num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1) # 152x152x8
		
		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1,
									num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2) # 76x76x16

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2,
									num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3) # 38x38x32

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3,
									num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4) # 19x19x32

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4,
									num_filters=16*self.out_features)

		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.block_4, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1,
									num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.block_3, self.up_2])
		self.up_2 	 = layers.depth_conv2d_block(inputs=self.merge_2,
									num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.block_2, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3,
									num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.block_1, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4,
									num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)


class Unet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs  = tf.keras.layers.Input(self.input_size) # 256x256x3

		self.block_1 = layers.conv2d_block(inputs=self.inputs,
									num_filters=self.out_features,
									recurrent=2)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1) # 128x128x8
		
		self.block_2 = layers.conv2d_block(inputs=self.max_1,
									num_filters=2*self.out_features,
									recurrent=2)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2) # 64x64x16

		self.block_3 = layers.conv2d_block(inputs=self.max_2,
									num_filters=4*self.out_features,
									recurrent=2)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3) # 32x32x32

		self.block_4 = layers.conv2d_block(inputs=self.max_3,
									num_filters=8*self.out_features,
									recurrent=2)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4) # 32x32x32

		self.block_5 = layers.conv2d_block(inputs=self.max_4,
									num_filters=16*self.out_features,
									recurrent=2)

		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.block_4, self.up_1])
		self.up_1 	 = layers.conv2d_block(inputs=self.merge_1,
									num_filters=4*self.out_features,
									recurrent=2)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.block_3, self.up_2])
		self.up_2 	 = layers.conv2d_block(inputs=self.merge_2,
									num_filters=2*self.out_features,
									recurrent=2)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.block_2, self.up_3])	
		self.up_3 	 = layers.conv2d_block(inputs=self.merge_3,
									num_filters=self.out_features,
									recurrent=2)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.block_1, self.up_4])	
		self.up_4 	 = layers.conv2d_block(inputs=self.merge_4,
									num_filters=self.out_features,
									recurrent=2)

		self.outputs = layers.ShakeoutConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)

class AttUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs 	= tf.keras.layers.Input(self.input_size)

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs, 
										num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1)

		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1, 
										num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2, 
										num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3, 
										num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4, 
										num_filters=16*self.out_features)

		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = layers.attention_gating_block(self.block_5, 
													self.block_4, 
													8*self.out_features)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1, 
										num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = layers.attention_gating_block(self.up_1, 
													self.block_3, 
													4*self.out_features)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = layers.depth_conv2d_block(inputs=self.merge_2, 
										num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = layers.attention_gating_block(self.up_2, 
													self.block_2, 
													2*self.out_features)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3, 
										num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = layers.attention_gating_block(self.up_3, 
													self.block_1, 
													self.out_features)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4, 
										num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)


class CbamUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs 	= tf.keras.layers.Input(self.input_size)

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs, 
										   num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1)

		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1, 
										   num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2, 
										   num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3, 
										   num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4, 
										   num_filters=16*self.out_features)

		
		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = layers.cbam_attention_block(self.block_4)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1, 
										   num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = layers.cbam_attention_block(self.block_3)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = layers.depth_conv2d_block(inputs=self.merge_2, 
										   num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = layers.cbam_attention_block(self.block_2)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3, 
										   num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = layers.cbam_attention_block(self.block_1)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4, 
										   num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)


class CbamUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs 	= tf.keras.layers.Input(self.input_size)

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs, 
										   num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1)

		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1, 
										   num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2, 
										   num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3, 
										   num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4, 
										   num_filters=16*self.out_features)

		
		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = layers.cbam_attention_block(self.block_4)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1, 
										   num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = layers.cbam_attention_block(self.block_3)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = layers.depth_conv2d_block(inputs=self.merge_2, 
										   num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = layers.cbam_attention_block(self.block_2)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3, 
										   num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = layers.cbam_attention_block(self.block_1)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4, 
										   num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)

class MAMUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs 	= tf.keras.layers.Input(self.input_size)

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs, 
										   num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1)

		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1, 
										   num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2, 
										   num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3, 
										   num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4, 
										   num_filters=16*self.out_features)

		
		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = layers.morpho_attention_block(self.block_4, name='alpha_1')
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1, 
										   num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = layers.morpho_attention_block(self.block_3, name='alpha_2')
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = layers.depth_conv2d_block(inputs=self.merge_2, 
										   num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = layers.morpho_attention_block(self.block_2, name='alpha_3')
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3, 
										   num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = layers.morpho_attention_block(self.block_1, name='alpha_4')
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4, 
										   num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)

class TripletUnet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs  = tf.keras.layers.Input(self.input_size)

		self.block_1 = layers.depth_conv2d_block(inputs=self.inputs, 
										   num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1)

		self.block_2 = layers.depth_conv2d_block(inputs=self.max_1, 
										   num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)

		self.block_3 = layers.depth_conv2d_block(inputs=self.max_2, 
										   num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)

		self.block_4 = layers.depth_conv2d_block(inputs=self.max_3, 
										   num_filters=8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)

		self.block_5 = layers.depth_conv2d_block(inputs=self.max_4, 
										   num_filters=16*self.out_features)

		
		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = layers.triplet_attention(self.block_4, name='alpha_1')
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = layers.depth_conv2d_block(inputs=self.merge_1, 
										   num_filters=8*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = layers.triplet_attention(self.block_3, name='alpha_2')
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = layers.depth_conv2d_block(inputs=self.merge_2, 
										   num_filters=4*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = layers.triplet_attention(self.block_2, name='alpha_3')
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = layers.depth_conv2d_block(inputs=self.merge_3, 
										   num_filters=2*self.out_features)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = layers.triplet_attention(self.block_1, name='alpha_4')
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])	
		self.up_4 	 = layers.depth_conv2d_block(inputs=self.merge_4, 
										   num_filters=self.out_features)

		self.outputs = layers.ShakeoutDepthConv2D(filters=1, kernel_size=(1,1), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)
