import utils
import tensorflow as tf
from glob import glob

def create_generator(batch_size,
					train_path,
					image_folder,
					mask_folder,
					aug_dict,
					target_size,
					image_color_mode = "rgb",
					mask_color_mode  = "grayscale",
					image_save_prefix= "image",
					mask_save_prefix = "mask",
					flag_multi_class = False,
					num_class = 2,
					save_to_dir = None,
					seed = 1):
    '''
        can generate image and mask at the same time
        use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
        if you want to visualize the results of generator, set save_to_dir = "your path"
    '''
    image_datagen   = tf.keras.preprocessing.image.ImageDataGenerator(**aug_dict)
    mask_datagen    = tf.keras.preprocessing.image.ImageDataGenerator(**aug_dict)
    #valid_datagen   = ImageDataGenerator(**aug_dict)

    image_generator = image_datagen.flow_from_directory(
                                                    train_path,
                                                    classes = [image_folder],
                                                    class_mode = 'categorical',
                                                    target_size = target_size,
                                                    color_mode=image_color_mode,
                                                    batch_size = batch_size,
                                                    save_to_dir = save_to_dir,
                                                    save_prefix  = image_save_prefix,
                                                    seed = seed)
    mask_generator = mask_datagen.flow_from_directory(
                                                  train_path,
                                                  classes = [mask_folder],
                                                  class_mode = 'categorical',
                                                  target_size = target_size,
                                                  color_mode = mask_color_mode,
                                                  batch_size = batch_size,
                                                  save_to_dir = save_to_dir,
                                                  save_prefix  = mask_save_prefix,
                                                  seed = seed)
    while True:
        x = image_generator.next()
        y = mask_generator.next()
        yield (x[0], y[0])

def loading(args):
	train_path = '{}/{}/train/'.format(args.datadir, args.set)
	train_dataset = glob(train_path + "images/*.{}".format(args.ext))
	TRAINSET_SIZE = len(train_dataset)

	val_path = '{}/{}/val/'.format(args.datadir, args.set)
	val_dataset = glob(val_path + "images/*.{}".format(args.ext))
	VALSET_SIZE = len(val_dataset)

	print(train_path, val_path)
	return train_path, val_path, TRAINSET_SIZE, VALSET_SIZE
# def loading(args):
# 	SEED        = args.seed
# 	EPOCHS      = args.epochs
# 	BUFFER_SIZE = 40
# 	AUTOTUNE    = tf.data.experimental.AUTOTUNE
# 	BATCH_SIZE  = args.batch_size

# 	data_dir = args.datadir
# 	if args.set != 'logyard':
# 		exit()

# 	#-- Training Dataset --#
# 	train_data = '/{}/train/images/'.format(args.set)
# 	train_dataset = tf.data.Dataset.list_files(data_dir + train_data + "*.jpeg", seed=SEED)
# 	train_dataset = train_dataset.map(utils.parse_image)
# 	TRAINSET_SIZE = len(glob(data_dir + train_data + "*.jpeg"))

# 	#-- Validation Dataset --#
# 	val_data = '/{}/val/images/'.format(args.set)
# 	val_dataset = tf.data.Dataset.list_files(data_dir + val_data + "*.jpeg", seed=SEED)
# 	val_dataset = val_dataset.map(utils.parse_image)
# 	VALSET_SIZE = len(glob(data_dir + val_data + "*.jpeg"))

# 	# -- Train Dataset --#
# 	dataset = {'train': train_dataset, 'val': val_dataset}
# 	imgshape = list(eval(args.shape))[0:2]

# 	#-- Create a generator --#
# 	#rng = tf.random.Generator.from_seed(args.seed, alg='philox')
# 	dataset['train'] = utils.prepare(dataset['train'], 
# 	                        image_shape=imgshape,
# 	                        batch_size=BATCH_SIZE,
# 	                        seed=SEED,
# 	                        augment=False)
# 	dataset['val'] 	 = utils.prepare(dataset['val'], 
# 	                        image_shape=imgshape,
# 	                        batch_size=BATCH_SIZE,
# 	                        seed=SEED,
# 	                        augment=False)
# 	return dataset, TRAINSET_SIZE, VALSET_SIZE