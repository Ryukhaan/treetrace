import argparse

from constants import *
from networks import NetworkType

parser = argparse.ArgumentParser(
  description="Heartwood Segmentation Training/Testing"
  )

#-- Which network / mode --#
parser.add_argument("--model",
  type=NetworkType,
  choices=list(NetworkType),
  help="Kind of network")
parser.add_argument("--mode",
  type=Flags,
  choices=list(Flags),
  help="Training mode or prediction mode")

#-- Main parameters (shape, name, filters) --#
parser.add_argument("--name",
  help="Model name (without hdf5 extension)",
  type=str,
  default=None)
parser.add_argument("--it",
  help="Model number of epoch",
  type=str,
  default=1)
parser.add_argument("--num-filt",
  help="Number of filter in the first Conv2d layer",
  type=int,
  default=DefaultValues['num_filters'])
parser.add_argument("--shape",
  help="Input shape: (height, width, channels)",
  type=str,
  default=DefaultValues['input_shape'])

#-- Hyperparameters --#
parser.add_argument("--lr",
  help="Learning rate",
  type=float,
  default=DefaultValues['learning_rate'])
parser.add_argument("--batch-size",
  help="Batch Size",
  type=int,
  default=DefaultValues['batch_size'])
parser.add_argument("--epochs",
  help="Number of epochs",
  type=int,
  default=DefaultValues['epochs'])

#-- Directories --#
parser.add_argument("--logdir",
  help="Directory for logs",
  type=str,
  default=DefaultDirectories['logdir'])
parser.add_argument("--datadir",
  help="Directory for datas with train/ and val/ subdirectories",
  type=str,
  default=DefaultDirectories['datadir'])
parser.add_argument("--set",
  help="Set to train / test (logyard or sawmill)",
  type=str,
  default=DefaultValues['subset'])
parser.add_argument("--ext",
  help="Images extension (jpeg, png, ...)",
  type=str,
  default="jpeg")

#-- Misc. --#
parser.add_argument('--seed',
  help="Seed for reproducibility",
  type=int,
  default=DefaultValues['seed'])
parser.add_argument('--verbose',
  help="Verbose mode",
  action='store_true')

def get_config():
  return parser.parse_args()