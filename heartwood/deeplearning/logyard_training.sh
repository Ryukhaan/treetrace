#!/bin/bash

if [[ $1 == 1 ]]; then
	python3 main.py --model=simple --name=Sunet1 --set="logyard_1" --mode=train --verbose 
	python3 main.py --model=simple --name=Sunet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=simple --name=Sunet8 --set="logyard_8" --mode=train --verbose
fi

if [[ $1 == 2 ]]; then
	python3 main.py --model=unet --name=Unet1 --set="logyard_1" --mode=train --verbose
	python3 main.py --model=unet --name=Unet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=unet --name=Unet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=unet --name=Unet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=unet --name=Unet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=unet --name=Unet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=unet --name=Unet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=unet --name=Unet8 --set="logyard_8" --mode=train --verbose
fi

if [[ $1 == 3 ]]; then
	python3 main.py --model=attunet --name=AttUnet1 --set="logyard_1" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=attunet --name=AttUnet8 --set="logyard_8" --mode=train --verbose
fi

if [[ $1 == 4 ]]; then
	python3 main.py --model=cbam --name=CbamUnet1 --set="logyard_1" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=cbam --name=CbamUnet8 --set="logyard_8" --mode=train --verbose
fi

if [[ $1 == 5 ]]; then
	python3 main.py --model=mam --name=MAM2Unet1 --set="logyard_1" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=mam --name=MAM2Unet8 --set="logyard_8" --mode=train --verbose
fi

if [[ $1 == 6 ]]; then
	python3 main.py --model=triplet --name=TripletUnet1 --set="logyard_1" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet2 --set="logyard_2" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet3 --set="logyard_3" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet4 --set="logyard_4" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet5 --set="logyard_5" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet6 --set="logyard_6" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet7 --set="logyard_7" --mode=train --verbose
	python3 main.py --model=triplet --name=TripletUnet8 --set="logyard_8" --mode=train --verbose
fi
