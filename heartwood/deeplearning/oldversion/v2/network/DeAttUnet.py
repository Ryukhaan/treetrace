import numpy as np 
import os
import tensorflow as tf 

import utils
from model 	import augmented
from layers import shakeout

l2 			= tf.keras.regularizers.L2(0.01)
weight_init = tf.initializers.glorot_uniform()
drate 		= 0.2

def _ConvBlock(inputs, out_features, ksize=3):
	s = shakeout.ShakeoutConv2D(
				filters = out_features, kernel_size = ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(inputs)
	b = tf.keras.layers.BatchNormalization()(s)
	l = tf.keras.layers.LeakyReLU()(b)
	return l

def _UpConvBlock(inputs, out_features, ksize=3):
	s = shakeout.ShakeoutConv2D(
				filters = out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(inputs)
	b = tf.keras.layers.BatchNormalization()(s)
	l = tf.keras.layers.LeakyReLU()(b)
	return l

def _Attention(G, X, out_features):
	W_g = shakeout.ShakeoutConv2D(filters=out_features, 
		kernel_size=1,
		padding='same')(G)
	W_g = tf.keras.layers.BatchNormalization()(W_g)

	W_x = shakeout.ShakeoutConv2D(filters=out_features, 
			kernel_size=1,
			padding='same')(X)
	W_x = tf.keras.layers.BatchNormalization()(W_x)

	return tf.keras.layers.Activation('sigmoid')(tf.keras.layers.Add()([W_g,W_x]))

# def _DeconvBlock(out_features):
# 	return tf.keras.Sequential(
# 		[
# 			shakeout.ShakeoutConv2D(
# 				filters = out_features, kernel_size=(1,1), 
# 				padding = 'same', 
# 				kernel_initializer = weight_init,
# 				kernel_regularizer = l2),
# 			tf.keras.layers.BatchNormalization(),
# 			tf.keras.layers.LeakyReLU(),
# 			shakeout.ShakeoutConv2D(
# 				filters = 3, kernel_size=(1,1), 
# 				padding = 'same', 
# 				kernel_initializer = weight_init,
# 				kernel_regularizer = l2),
# 			tf.keras.layers.BatchNormalization(),
# 			tf.keras.layers.LeakyReLU()
# 		])

class DeAttUnet():
	"""
	"""
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs 	= tf.keras.layers.Input(self.input_size)

		# deconv = _DeconvBlock(16)(inputs)

		self.block_1 = _ConvBlock(self.inputs, self.out_features)
		self.max_1 	= tf.keras.layers.MaxPooling2D()(self.block_1)
		self.block_2 = _ConvBlock(self.max_1, 2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2)
		self.block_3 = _ConvBlock(self.max_2, 4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3)
		self.block_4 = _ConvBlock(self.max_3, 8*self.out_features)
		self.max_4   = tf.keras.layers.MaxPooling2D()(self.block_4)
		self.block_5 = _ConvBlock(self.max_4, 16*self.out_features)

		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_5)
		self.att_1 	 = _Attention(self.up_1, self.block_4, 8*self.out_features)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.att_1, self.up_1])
		self.up_1 	 = _UpConvBlock(self.merge_1, 8*self.out_features)
		#block_6 = _ConvBlock(8*self.out_features)(up_1)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.att_2 	 = _Attention(self.up_2, self.block_3, 4*self.out_features)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.att_2, self.up_2])
		self.up_2    = _UpConvBlock(self.merge_2, 4*self.out_features)
		#block_7 = _ConvBlock(4*self.out_features)(up_2)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.att_3 	 = _Attention(self.up_3, self.block_2, 2*self.out_features)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.att_3, self.up_3])	
		self.up_3 	 = _UpConvBlock(self.merge_3, 2*self.out_features)
		#block_8 = _ConvBlock(2*self.out_features)(up_3)

		self.up_4 	 = tf.keras.layers.UpSampling2D()(self.up_3)
		self.att_4 	 = _Attention(self.up_4, self.block_1, self.out_features)
		self.merge_4 = tf.keras.layers.Concatenate(axis=3)([self.att_4, self.up_4])
		self.up_4 	 = _UpConvBlock(self.merge_4, self.out_features)
		#block_8 = _ConvBlock(self.out_features)(up_4)

		self.outputs = shakeout.ShakeoutConv2D(filters=1, kernel_size=(3,3), 
						padding = 'same', 
						kernel_initializer = weight_init,
						kernel_regularizer = l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_4)

		return tf.keras.Model(self.inputs, self.outputs)