from enum import Enum

class NetworkType(Enum):
	_UNET_t 		= "unet"
	_ATTUNET_t 		= "attunet"
	_R2UNET_t 		= "r2unet"
	_R2ATTUNET_t	= "r2attunet"
	_CBAMUNET_t 	= "cbam"
	_CLASSIC_t 		= "classic"
	_TESTNET_t 		= "testnet"

	def __str__(self):
		return self.value
