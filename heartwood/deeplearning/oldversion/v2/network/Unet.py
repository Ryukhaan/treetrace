import numpy as np 
import os
import tensorflow as tf 

import utils
from model 	import augmented
from layers import layers

class Unet():
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		self.inputs  = tf.keras.layers.Input(self.input_size) # 256x256x3

		self.block_1 = layers.conv2d_block(inputs=self.inputs,
									num_filters=self.out_features)
		self.max_1 	 = tf.keras.layers.MaxPooling2D()(self.block_1) # 128x128x8
		
		self.block_2 = layers.conv2d_block(inputs=self.max_1,
									num_filters=2*self.out_features)
		self.max_2   = tf.keras.layers.MaxPooling2D()(self.block_2) # 64x64x16

		self.block_3 = layers.conv2d_block(inputs=self.max_2,
									num_filters=4*self.out_features)
		self.max_3   = tf.keras.layers.MaxPooling2D()(self.block_3) # 32x32x32

		self.block_4 = layers.conv2d_block(inputs=self.max_3,
									num_filters=8*self.out_features)

		self.up_1 	 = tf.keras.layers.UpSampling2D()(self.block_4)
		self.merge_1 = tf.keras.layers.Concatenate(axis=3)([self.block_3, self.up_1])
		self.up_1 	 = layers.conv2d_block(inputs=self.merge_1,
									num_filters=4*self.out_features)

		self.up_2 	 = tf.keras.layers.UpSampling2D()(self.up_1)
		self.merge_2 = tf.keras.layers.Concatenate(axis=3)([self.block_2, self.up_2])
		self.up_2 	 = layers.conv2d_block(inputs=self.merge_2,
									num_filters=2*self.out_features)

		self.up_3 	 = tf.keras.layers.UpSampling2D()(self.up_2)
		self.merge_3 = tf.keras.layers.Concatenate(axis=3)([self.block_1, self.up_3])	
		self.up_3 	 = layers.conv2d_block(inputs=self.merge_3,
									num_filters=self.out_features)

		self.outputs = layers.ShakeoutConv2D(filters=1, kernel_size=(3,3), 
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(self.up_3)

		return tf.keras.Model(self.inputs, self.outputs)




