import numpy as np 
import os
import tensorflow as tf 

import utils
from model 	import augmented
from layers import shakeout

l2 			= tf.keras.regularizers.L2(0.01)
weight_init = tf.initializers.glorot_uniform()
drate 		= 0.2
#ksize 		= 3

def _ConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[	
			shakeout.ShakeoutConv2D(
				filters = out_features, kernel_size = ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU()
		])

def _UpConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[
			shakeout.ShakeoutConv2D(
				filters=out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU()
		])

def _DeconvBlock(out_features):
	return tf.keras.Sequential(
		[
			shakeout.ShakeoutConv2D(
				filters = out_features, kernel_size=1,
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2,
				activation='sigmoid'),
		])

class TestNet():
	"""
	"""
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		inputs 	= tf.keras.layers.Input(self.input_size)

		deconv = _DeconvBlock(16)(inputs)
		deconv = _DeconvBlock(16)(inputs)
		outputs = tf.keras.Sequential(
			[
				tf.keras.layers.Conv2D(1, (3,3), 
							padding = 'same', 
							kernel_initializer = weight_init,
							kernel_regularizer = l2,
							activation=tf.keras.activations.hard_sigmoid)
			])(deconv)

		return tf.keras.Model(inputs, outputs)