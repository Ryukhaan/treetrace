import numpy as np 
import os
import tensorflow as tf 

import utils
from model 	import augmented
from layers import shakeout

l2 			= tf.keras.regularizers.L2(0.01)
weight_init = tf.initializers.glorot_uniform()
drate 		= 0.2

def _Attention(G, X, out_features):
	W_g = tf.keras.Sequential([
		tf.keras.layers.Conv2D(out_features, 
			kernel_size=1,
			padding='same'),
		tf.keras.layers.BatchNormalization()
		])(G)

	W_x = tf.keras.Sequential([
		tf.keras.layers.Conv2D(out_features, 
			kernel_size=1,
			padding='same'),
		tf.keras.layers.BatchNormalization()
		])(X)
	relu = tf.keras.layers.ReLU()(tf.keras.layers.Add()([W_g,W_x]))
	psi  = tf.keras.Sequential([
		tf.keras.layers.Conv2D(1, 
			kernel_size=1,
			padding='same',
			activation='sigmoid'),
		tf.keras.layers.BatchNormalization(1),
		])(relu)
	return tf.keras.layers.Multiply()([X, psi])

def _RecurrentBlock(X, out_features, ksize=3, t=2):
	conv = tf.keras.Sequential(
		[
			shakeout.ShakeoutConv2D(
				filters=out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU()
		])
	y = conv(X)
	for i in range(t):
		y = conv(tf.keras.layers.Add()([y, X]))
	return y

def _RRCNNBlock(X, out_features, t=2):
	x1 = tf.keras.layers.Conv2D(
		filters=out_features, kernel_size=1,
		padding='same',
		kernel_initializer = weight_init,
		kernel_regularizer = l2)(X)
	b1 = _RecurrentBlock(x1, out_features, t=t)
	b2 = _RecurrentBlock(b1, out_features, t=t)
	return tf.keras.layers.Add()([b2,x1])


def _ConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[	
			#-- First Convolution Block --#
			tf.keras.layers.Conv2D(
				filters = out_features, kernel_size = ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU(),
			tf.keras.layers.SpatialDropout2D(drate),
			#-- Second Convolution Block --#
			tf.keras.layers.Conv2D(
				filters=out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU(),
			tf.keras.layers.SpatialDropout2D(drate)
		])

def _UpConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[
			tf.keras.layers.Conv2D(
				filters=out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU(),
			tf.keras.layers.SpatialDropout2D(drate)
		])

class AttR2Unet():
	"""
	"""
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		inputs 	= tf.keras.layers.Input(self.input_size)

		block_1 = _RRCNNBlock(inputs, self.out_features)
		max_1 	 = tf.keras.layers.MaxPooling2D()(block_1)
		block_2 = _RRCNNBlock(max_1, 2*self.out_features)
		max_2   = tf.keras.layers.MaxPooling2D()(block_2)
		block_3 = _RRCNNBlock(max_2, 4*self.out_features)
		max_3   = tf.keras.layers.MaxPooling2D()(block_3)
		block_4 = _RRCNNBlock(max_3, 8*self.out_features)
		max_4   = tf.keras.layers.MaxPooling2D()(block_4)
		block_5 = _RRCNNBlock(max_4, 16*self.out_features)

		up 	 	= tf.keras.layers.UpSampling2D()(block_5)
		att 	= _Attention(up, block_4, self.out_features)
		merge   = tf.keras.layers.Concatenate(axis=3)([att, up])
		#up_1 	= _UpConvBlock(8*self.out_features)(merge)
		block_6 = _RRCNNBlock(merge, 8*self.out_features)

		up 	 	= tf.keras.layers.UpSampling2D()(block_6)
		att 	= _Attention(up, block_3, self.out_features)
		merge   = tf.keras.layers.Concatenate(axis=3)([att, up])
		#up_2    = _UpConvBlock(4*self.out_features)(merge)
		block_7 = _RRCNNBlock(merge, 4*self.out_features)

		up 	 	= tf.keras.layers.UpSampling2D()(block_7)
		att 	= _Attention(up, block_2, self.out_features)
		merge   = tf.keras.layers.Concatenate(axis=3)([att, up])	
		#up_3 	= _UpConvBlock(2*self.out_features)(merge)
		block_8 = _RRCNNBlock(merge, 2*self.out_features)

		up 	 	= tf.keras.layers.UpSampling2D()(block_8)
		att 	= _Attention(up, block_1, self.out_features)
		merge   = tf.keras.layers.Concatenate(axis=3)([att, up])
		#up_4 	= _UpConvBlock(self.out_features)(merge)
		block_8 = _RRCNNBlock(merge, self.out_features)

		outputs = tf.keras.layers.Conv2D(1, (3,3), 
							padding = 'same', 
							kernel_initializer = weight_init,
							kernel_regularizer = l2,
							activation=tf.keras.activations.hard_sigmoid)(block_8)

		return tf.keras.Model(inputs, outputs)