import numpy as np 
import os
import tensorflow as tf 

import utils
from model 	import augmented
from layers import shakeout

l2 			= tf.keras.regularizers.L2(0.01)
weight_init = tf.initializers.glorot_uniform()
drate 		= 0.2
#ksize 		= 3

def _ConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[	
			#-- First Convolution Block --#
			shakeout.ShakeoutConv2D(
				filters = out_features, kernel_size = ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU(),
			#tf.keras.layers.Dropout(drate),
			#-- Second Convolution Block --#
			# tf.keras.layers.SkakeoutConv2D(
			# 	filters=out_features, kernel_size=ksize, 
			# 	padding = 'same', 
			# 	kernel_initializer = weight_init,
			# 	kernel_regularizer = l2),
			# tf.keras.layers.BatchNormalization(),
			# tf.keras.layers.LeakyReLU(),
			#tf.keras.layers.Dropout(drate)
		])

def _UpConvBlock(out_features, ksize=3):
	return tf.keras.Sequential(
		[
			shakeout.ShakeoutConv2D(
				filters=out_features, kernel_size=ksize, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2),
			tf.keras.layers.BatchNormalization(),
			tf.keras.layers.LeakyReLU(),
			#tf.keras.layers.Dropout(drate)
		])

class SimpleUnetShake():
	"""
	"""
	def __init__(self, args=None, pretrained_weights=None):
		self.input_size  	= eval(args.shape) 
		self.out_features 	= args.num_filt
		self.args 	 		= args

	def model(self):
		inputs 	= tf.keras.layers.Input(self.input_size)

		deconv = shakeout.ShakeoutConv2D(
				filters = 16, kernel_size=(1,1), 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(inputs)
		deconv = tf.keras.layers.BatchNormalization()(deconv)
		deconv = tf.keras.layers.LeakyReLU()(deconv)

		block_1 = _ConvBlock(self.out_features)(deconv)
		max_1 	= tf.keras.layers.MaxPooling2D()(block_1)
		block_2 = _ConvBlock(2*self.out_features)(max_1)
		max_2   = tf.keras.layers.MaxPooling2D()(block_2)
		block_3 = _ConvBlock(4*self.out_features)(max_2)
		max_3   = tf.keras.layers.MaxPooling2D()(block_3)
		block_4 = _ConvBlock(8*self.out_features)(max_3)
		max_4   = tf.keras.layers.MaxPooling2D()(block_4)
		block_5 = _ConvBlock(16*self.out_features)(max_4)

		up 	 	= tf.keras.layers.UpSampling2D()(block_5)
		merge   = tf.keras.layers.Concatenate(axis=3)([block_4, up])
		up_1 	= _UpConvBlock(8*self.out_features)(merge)
		#block_6 = _ConvBlock(8*self.out_features)(up_1)

		up 	 	= tf.keras.layers.UpSampling2D()(up_1)
		merge   = tf.keras.layers.Concatenate(axis=3)([block_3, up])
		up_2    = _UpConvBlock(4*self.out_features)(merge)
		#block_7 = _ConvBlock(4*self.out_features)(up_2)

		up 	 	= tf.keras.layers.UpSampling2D()(up_2)
		merge   = tf.keras.layers.Concatenate(axis=3)([block_2, up])	
		up_3 	= _UpConvBlock(2*self.out_features)(merge)
		#block_8 = _ConvBlock(2*self.out_features)(up_3)

		up 	 	= tf.keras.layers.UpSampling2D()(up_3)
		merge   = tf.keras.layers.Concatenate(axis=3)([block_1, up])
		up_4 	= _UpConvBlock(self.out_features)(merge)
		#block_8 = _ConvBlock(self.out_features)(up_4)

		outputs = tf.keras.Sequential(
			[
				tf.keras.layers.Conv2D(1, (3,3), 
							padding = 'same', 
							kernel_initializer = weight_init,
							kernel_regularizer = l2,
							activation='sigmoid')
			])(up_4)

		return tf.keras.Model(inputs, outputs)

