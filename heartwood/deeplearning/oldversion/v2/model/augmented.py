import tensorflow as tf 
import utils

class _Model(tf.keras.Model):

	# @tf.function
	# def test_step(self, data):
	# 	x, y = data #utils.data_augmentation(data)
	# 	y_pred = self(x, training=False)
	# 	self.compiled_metrics.update_state(y, y_pred)

	@tf.function
	def train_step(self, data):
		x, y = data

		#-- Include data augmentaiton on the flight --#
		#x, y = utils.data_augmentation(data)

		with tf.GradientTape() as tape:
			y_pred = self(x, training=True)  # Forward pass
			# Compute the loss value
			# (the loss function is configured in `compile()`)
			loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

		# Compute gradients
		trainable_vars = self.trainable_variables
		gradients = tape.gradient(loss, trainable_vars)
		# Update weights
		self.optimizer.apply_gradients(zip(gradients, trainable_vars))
		# Update metrics (includes the metric that tracks the loss)
		self.compiled_metrics.update_state(y, y_pred)
		# Return a dict mapping metric names to current value
		return {m.name: m.result() for m in self.metrics}