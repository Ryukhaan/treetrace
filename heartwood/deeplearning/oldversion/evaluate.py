import numpy as np 
import os
import csv
import sys
import skimage.io as io
import skimage.transform as transform
import cv2
#import skimage.transform as trans

#import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as img
import matplotlib.pyplot as plt
import scipy.misc
from PIL import Image

from sklearn.model_selection import train_test_split

from tensorflow.keras.models import *
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras import backend as keras

import model
import data

# Folders Train, Valid, Test and Save
if len(sys.argv) != 3:
	print("Required 2 parameters ({} found)".format(len(sys.argv)))
	exit(2)


# Load network weights
net 	= model.unet()
name 	= "".join([model.save_dir, '/', sys.argv[1]])
try:
	net.load_weights(name)
except IOError:
	print("Not weights found ({} not exists)".format(name))
	exit(2)


valid_dir	= sys.argv[2]
nsteps 		= 100
if os.path.isdir(valid_dir):
	input_dir=os.path.join(valid_dir, 'input')
	nsteps = len([x for x in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, x))])

input_folder  = 'input'
label_folder  = 'output'

data_gen_args = dict(rescale = 1.0 / 255,
                     rotation_range=0.2,
                     width_shift_range=0.05,
                     height_shift_range=0.05,
                     shear_range=0.05,
                     zoom_range=0.05,
                     horizontal_flip=True,
                     fill_mode='nearest')


# Create network
__validator     = data.trainset_generator(1, valid_dir, input_folder, label_folder,
                                          data_gen_args, save_to_dir=None)


print(net.evaluate_generator(__validator, steps=nsteps, verbose=1))




