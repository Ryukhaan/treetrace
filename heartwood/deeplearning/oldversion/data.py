from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import os
import glob
import skimage.io as io
import skimage.transform as trans
from skimage import img_as_ubyte
import itertools
#import image

import cv2

import model

def trainset_generator(batch_size,
                       train_path,
                       image_folder,
                       mask_folder,
                       aug_dict,
                       image_color_mode = "rgb",
                       mask_color_mode  = "grayscale",
                       image_save_prefix= "image",
                       mask_save_prefix = "mask",
                       flag_multi_class = False,
                       num_class = 2,
                       save_to_dir = None,
                       target_size = (model.height, model.width),
                       seed = 1):
    '''
        can generate image and mask at the same time
        use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
        if you want to visualize the results of generator, set save_to_dir = "your path"
        '''
    image_datagen   = ImageDataGenerator(**aug_dict)
    mask_datagen    = ImageDataGenerator(**aug_dict)
    valid_datagen   = ImageDataGenerator(**aug_dict)

    image_generator = image_datagen.flow_from_directory(
                                                    train_path,
                                                    classes = [image_folder],
                                                    class_mode = 'categorical',
                                                    target_size = target_size,
                                                    color_mode=image_color_mode,
                                                    batch_size = batch_size,
                                                    save_to_dir = save_to_dir,
                                                    save_prefix  = image_save_prefix,
                                                    seed = seed)
    mask_generator = mask_datagen.flow_from_directory(
                                                  train_path,
                                                  classes = [mask_folder],
                                                  class_mode = 'categorical',
                                                  target_size = target_size,
                                                  color_mode = mask_color_mode,
                                                  batch_size = batch_size,
                                                  save_to_dir = save_to_dir,
                                                  save_prefix  = mask_save_prefix,
                                                  seed = seed)
    while True:
        x = image_generator.next()
        y = mask_generator.next()
        yield (x[0], y[0])

# def predict_generator(image_folder,
#                       num_image=30,
#                       image_color_mode = "rgb",
#                       mask_color_mode  = "grayscale",
#                       image_save_prefix= "image",
#                       mask_save_prefix = "mask",
#                       flag_multi_class = False,
#                       num_class = 2,
#                       save_to_dir = None,
#                       target_size = (model.height, model.width),
#                       seed = 1):
#     '''
#         can generate image and mask at the same time
#         use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
#         if you want to visualize the results of generator, set save_to_dir = "your path"
#         '''
#     imgs = []
#     for i in os.listdir(image_folder):
#       if i == '.DS_Store': continue
#       imgs.append(os.path.join(image_folder, i))
#       print(os.path.join(image_folder, i))

#     print(len(imgs))

#     i=0
#     while i < len(imgs):  
#       img = io.imread(imgs[i])
#       #img   = cv2.imread(imgs[i])
#       i   = i + 1
#       #print(i)
#       img = img / 255
#       #img = trans.resize(img,target_size)
#       img = cv2.resize(img, target_size)
#       #img = np.reshape(img,img.shape+(1,)) if (not flag_multi_class) else img
#       img = np.reshape(img,(1,)+img.shape)
#       yield img
    
# def gen_train_npy(image_path,mask_path,flag_multi_class = False,num_class = 2,image_prefix = "image",mask_prefix = "mask",image_as_gray = True,mask_as_gray = True):
#     image_name_arr = glob.glob(os.path.join(image_path,"%s*.png"%image_prefix))
#     image_arr = []
#     mask_arr = []
#     for index,item in enumerate(image_name_arr):
#         img = io.imread(item,as_gray = image_as_gray)
#         img = np.reshape(img,img.shape + (1,)) if image_as_gray else img
#         mask = io.imread(item.replace(image_path,mask_path).replace(image_prefix,mask_prefix),as_gray = mask_as_gray)
#         mask = np.reshape(mask,mask.shape + (1,)) if mask_as_gray else mask
#         #img,mask = adjustData(img,mask,flag_multi_class,num_class)
#         image_arr.append(img)
#         mask_arr.append(mask)
#     image_arr = np.array(image_arr)
#     mask_arr = np.array(mask_arr)
#     return image_arr,mask_arr

def save_image(save_path,
                npyfile,
                file_name="predicted"):
        #if len(npyfile.shape) == 3:
        #  img = npyfile[:,:,0]
        #else:
        img = npyfile
        io.imsave(os.path.join(save_path, file_name),img_as_ubyte(img))

