import numpy as np 
import os
import skimage.io as io
import skimage.transform as trans
import numpy as np

import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import *
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras import backend as keras

import metrics as m
import os

#=======================#
# 	Global Parameters 	#
# 	----------------- 	#
#=======================#

# Image size
height		= 256
width   	= 256
channels 	= 3
numFilt 	= 16

# Hyperparameters
learning_rate		= 0.1 #unused
batch_size			= 4
epochs				= 40
steps_per_epoch 	= 16
validation_steps	= 8


name = "heartwood_3.hdf5"

train_folder	= "train_256x256"
valid_folder	= "valid_256x256"
test_folder		= "test_256x256"


train_dir_input 	= os.path.join(os.getcwd(), 'dataset', train_folder, 'input')
valid_dir_input 	= os.path.join(os.getcwd(), 'dataset', valid_folder, 'input')
test_dir_input 		= os.path.join(os.getcwd(), 'dataset', test_folder, 'input')

train_dir_output 	= os.path.join(os.getcwd(), 'dataset', train_folder, 'output')
valid_dir_output	= os.path.join(os.getcwd(), 'dataset', valid_folder, 'output')
test_dir_output 	= os.path.join(os.getcwd(), 'dataset', test_folder, 'output')

save_dir 			= os.path.join(os.getcwd(), 'model', 'save')
logs_dir 			= os.path.join(os.getcwd(), 'model', 'logs', 'scalars')
fullname 			= "".join([save_dir, '/', name])

#numDatas = 49

#===========#
# 	Model 	#
# 	----- 	#
#===========#
def balanced_bce(inputs, label, name='cross_entropy_loss'):
    """
    Ref.: Holistically-Nested Edge Detection (CVPR 15)'
    Implements Equation [2] in https://arxiv.org/pdf/1504.06375.pdf
    """
    y = tf.cast(label, tf.float32)

    negatives 	= tf.math.reduce_sum(1.-y)
    positives   = tf.math.reduce_sum(y)
    # Equation (2) (refer to the article)
    beta = positives / (negatives + positives)
    # Equation (2) divide by 1 - beta
    pos_weight = beta / (1. - beta)

    cost = tf.nn.weighted_cross_entropy_with_logits(
        logits=inputs, labels=label, pos_weight=pos_weight)
    # Multiply by 1 - beta 
    cost = tf.math.reduce_mean(cost * (1. - beta))

    # Check if image has no segmented pixels return 0 else return complete error function
    return tf.where(tf.equal(positives, 0.0), 0.0, cost, name=name)

def unet(pretrained_weights = None,input_size = (height, width, channels)):
	inputs 	= Input(input_size)
	
	# conv1 	= Conv2D(numFilt, 11, data_format='channels_last', activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
	# conv1 	= Conv2D(numFilt, 11, data_format='channels_last', activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
	# drop1 	= Dropout(0.5)(conv1)
	# pool1 	= MaxPooling2D(pool_size=(2, 2))(drop1)
	
	# conv2 	= Conv2D(2*numFilt, 9, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
	# conv2 	= Conv2D(2*numFilt, 9, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
	# drop2 	= Dropout(0.5)(conv2)
	# pool2 	= MaxPooling2D(pool_size=(2, 2))(drop2)
	
	# conv3 	= Conv2D(4*numFilt, 7, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
	# conv3 	= Conv2D(4*numFilt, 7, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
	# drop3 	= Dropout(0.5)(conv3)
	# pool3 	= MaxPooling2D(pool_size=(2, 2))(drop3)
	
	# conv4 	= Conv2D(8*numFilt, 5, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
	# conv4 	= Conv2D(8*numFilt, 5, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
	# drop4 	= Dropout(0.5)(conv4)
	# pool4 	= MaxPooling2D(pool_size=(2, 2))(drop4)

	# conv5 	= Conv2D(16*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
	# conv5 	= Conv2D(16*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
	# drop5 	= Dropout(0.5)(conv5)

	# up6 	= Conv2D(8*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
	# merge6	= concatenate([drop4,up6], axis = 3)
	# conv6	= Conv2D(8*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
	# conv6	= Conv2D(8*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

	# up7		= Conv2D(4*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
	# merge7	= concatenate([conv3,up7], axis = 3)
	# conv7	= Conv2D(4*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
	# conv7	= Conv2D(4*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

	# up8		= Conv2D(2*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
	# merge8	= concatenate([conv2,up8], axis = 3)
	# conv8	= Conv2D(2*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
	# conv8	= Conv2D(2*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

	# up9 	= Conv2D(numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
	# merge9 	= concatenate([conv1,up9], axis = 3)
	# conv9 	= Conv2D(numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
	# conv9 	= Conv2D(numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
	# conv9 	= Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
	# conv10 	= Conv2D(1, 1, activation = 'sigmoid')(conv9)

	conv1 	= Conv2D(numFilt, 11, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
	drop1 	= Dropout(0.2)(conv1)
	conv1 	= Conv2D(numFilt, 11, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop1)
	#drop1 	= Dropout(0.2)(conv1)
	pool1 	= MaxPooling2D(pool_size=(2, 2))(drop1)
	
	conv2 	= Conv2D(2*numFilt, 9, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
	drop2 	= Dropout(0.2)(conv2)
	conv2 	= Conv2D(2*numFilt, 9, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop2)
	#drop2 	= Dropout(0.2)(conv2)
	pool2 	= MaxPooling2D(pool_size=(2, 2))(drop2)
	
	conv3 	= Conv2D(4*numFilt, 7, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
	drop3 	= Dropout(0.2)(conv3)
	conv3 	= Conv2D(4*numFilt, 7, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop3)
	#drop3 	= Dropout(0.2)(conv3)
	pool3 	= MaxPooling2D(pool_size=(2, 2))(drop3)
	
	conv4 	= Conv2D(8*numFilt, 5, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
	drop4 	= Dropout(0.2)(conv4)
	conv4 	= Conv2D(8*numFilt, 5, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop4)
	#drop4 	= Dropout(0.2)(conv4)
	pool4 	= MaxPooling2D(pool_size=(2, 2))(drop4)

	conv5 	= Conv2D(16*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
	drop5 	= Dropout(0.2)(conv5)
	conv5 	= Conv2D(16*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop5)

	up6 	= Conv2D(8*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
	merge6	= concatenate([conv4,up6], axis = 3)
	conv6	= Conv2D(8*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
	drop6	= Dropout(0.2)(conv6)
	conv6	= Conv2D(8*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop6)

	up7		= Conv2D(4*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop6))
	merge7	= concatenate([conv3,up7], axis = 3)
	conv7	= Conv2D(4*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
	drop7	= Dropout(0.2)(conv7)
	conv7	= Conv2D(4*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop7)

	up8		= Conv2D(2*numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop7))
	merge8	= concatenate([conv2,up8], axis = 3)
	conv8	= Conv2D(2*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
	drop8	= Dropout(0.2)(conv8)
	conv8	= Conv2D(2*numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop8)

	up9 	= Conv2D(numFilt, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop8))
	merge9 	= concatenate([conv1,up9], axis = 3)
	conv9 	= Conv2D(numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
	drop9	= Dropout(0.2)(conv9)
	conv9 	= Conv2D(numFilt, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(drop9)
	#conv9 	= Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
	conv10 	= Conv2D(1, 3, padding='same', activation = 'sigmoid')(drop9)

	model 	= Model(inputs = inputs, outputs = conv10)

	model.compile(optimizer = Adam(lr = 1e-4), loss = 'binary_crossentropy', metrics = [m.MCC, m.IoU])

	return model
