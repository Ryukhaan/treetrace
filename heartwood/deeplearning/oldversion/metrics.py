import numpy as np 
import os
import skimage.io as io
import skimage.transform as trans
import numpy as np

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import *
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras import backend as keras
K = keras

import tensorflow as tf 
import os

def MCC(y_true, y_pred):
	y_pred_pos = keras.round(keras.clip(y_pred, 0, 1))
	y_pred_neg = 1 - y_pred_pos

	y_pos = keras.round(keras.clip(y_true, 0, 1))
	y_neg = 1 - y_pos

	tp = keras.sum(y_pos * y_pred_pos)
	tn = keras.sum(y_neg * y_pred_neg)

	fp = keras.sum(y_neg * y_pred_pos)
	fn = keras.sum(y_pos * y_pred_neg)

	numerator = (tp * tn - fp * fn)
	denominator = keras.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

	return numerator / (denominator + keras.epsilon())

def precision(y_true, y_pred):
	'''Calculates the precision, a metric for multi-label classification of
	how many selected items are relevant.
	'''
	true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
	predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
	precision = true_positives / (predicted_positives + K.epsilon())
	return precision


def recall(y_true, y_pred):
	'''Calculates the recall, a metric for multi-label classification of
	how many relevant items are selected.
	'''
	true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
	possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
	recall = true_positives / (possible_positives + K.epsilon())
	return recall

def fscore(y_true, y_pred):
	'''Calculates the F score, the weighted harmonic mean of precision and recall.
	This is useful for multi-label classification, where input samples can be
	classified as sets of labels. By only using accuracy (precision) a model
	would achieve a perfect score by simply assigning every class to every
	input. In order to avoid this, a metric should penalize incorrect class
	assignments as well (recall). The F-beta score (ranged from 0.0 to 1.0)
	computes this, as a weighted mean of the proportion of correct class
	assignments vs. the proportion of incorrect class assignments.
	With beta = 1, this is equivalent to a F-measure. With beta < 1, assigning
	correct classes becomes more important, and with beta > 1 the metric is
	instead weighted towards penalizing incorrect class assignments.
	'''
	#if beta < 0:
	#    raise ValueError('The lowest choosable beta is zero (only precision).')
	    
	# If there are no true positives, fix the F score at 0 like sklearn.
	if K.sum(K.round(K.clip(y_true, 0, 1))) == 0:
	    return 0

	p = precision(y_true, y_pred)
	r = recall(y_true, y_pred)
	#bb = beta ** 2
	bb = 1.0
	fbeta_score = (1 + bb) * (p * r) / (bb * p + r + K.epsilon())
	return fbeta_score

def accuracy(y_true, y_pred):
	'''Calculates the Matthews correlation coefficient measure for quality
	of binary classification problems.
	'''
	y_pred_pos = K.round(K.clip(y_pred, 0, 1))
	y_pred_neg = 1 - y_pred_pos

	y_pos = K.round(K.clip(y_true, 0, 1))
	y_neg = 1 - y_pos

	tp = K.sum(y_pos * y_pred_pos)
	tn = K.sum(y_neg * y_pred_neg)

	fp = K.sum(y_neg * y_pred_pos)
	fn = K.sum(y_pos * y_pred_neg)

	numerator = (tp + tn)
	denominator = (tp + fp + fn + tn)

	return numerator / denominator


def IoU(y_true, y_pred):
	'''Calculates the Matthews correlation coefficient measure for quality
	of binary classification problems.
	'''
	y_pred_pos = K.round(K.clip(y_pred, 0, 1))
	y_pred_neg = 1 - y_pred_pos

	y_pos = K.round(K.clip(y_true, 0, 1))
	y_neg = 1 - y_pos

	tp = K.sum(y_pos * y_pred_pos)
	tn = K.sum(y_neg * y_pred_neg)

	fp = K.sum(y_neg * y_pred_pos)
	fn = K.sum(y_pos * y_pred_neg)

	numerator = (tp)
	denominator = (tp + fp + fn + K.epsilon())

	return numerator / denominator

def nice2(y_true, y_pred):
	'''
	Calculates the Matthews correlation coefficient measure for quality
	of binary classification problems.
	'''
	y_pred_pos = K.round(K.clip(y_pred, 0, 1))
	y_pred_neg = 1 - y_pred_pos

	y_pos = K.round(K.clip(y_true, 0, 1))
	y_neg = 1 - y_pos

	tp = K.sum(y_pos * y_pred_pos)
	tn = K.sum(y_neg * y_pred_neg)

	fp = K.sum(y_neg * y_pred_pos)
	fn = K.sum(y_pos * y_pred_neg)

	return 0.5 * ((fn / (fn + tp + K.epsilon())) + (fp / (fp + tn + K.epsilon())))


def ametrics(y_true, y_pred):
	y_pred_pos = keras.round(keras.clip(y_pred, 0, 1))
	y_pred_neg = 1 - y_pred_pos

	y_pos = keras.round(keras.clip(y_true, 0, 1))
	y_neg = 1 - y_pos

	tp = keras.sum(y_pos * y_pred_pos)
	tn = keras.sum(y_neg * y_pred_neg)

	fp = keras.sum(y_neg * y_pred_pos)
	fn = keras.sum(y_pos * y_pred_neg)

	numerator = (tp * tn - fp * fn)
	denominator = keras.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

	mcc = numerator / (denominator + keras.epsilon())
	nic = 0.5 * ((fn / (fn + tp + K.epsilon())) + (fp / (fp + tn + K.epsilon())))
	iou = tp / (tp + fp + fn + K.epsilon())
	acc = (tp + tn) / (tp + tn + fp + fn)
	pre = tp / (tp + fp)
	rec = tp / (tp + fn)
	fsc = 2*tp / (2*tp + fn + fp)
	return pre, rec, acc, fsc, nic, iou, mcc