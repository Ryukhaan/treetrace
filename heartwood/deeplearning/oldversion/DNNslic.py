import numpy as np
import os 
import tensorflow as tf
from glob import glob

from SLICNet import SLICNet
import utils
from metrics.matthews_correlation import matthews_correlation 

from sklearn.model_selection import train_test_split

data_dir    = os.path.join(os.getcwd(), 'data')
AUTOTUNE    = tf.data.experimental.AUTOTUNE
SETNAME = 'slic'
#-- Training Dataset --#
train_data = '/{}/train/'.format(SETNAME)
train_dataset = glob(data_dir + train_data + "*.txt") #tf.data.Dataset.list_files(data_dir + train_data + "*.jpeg")
TRAINSET_SIZE = len(train_dataset)

X, Y = [], []
for file in train_dataset:
    with open(file, 'r') as text:
        for line in text.readlines():
            values = np.array(line.strip('\n').split(','))
            values = values.astype(np.float32)
            out = values[-1]
            X.append(values[:-1])
            if out == 1.:
                Y.append(0.)
            else:
                Y.append(1.)

X = np.array(X)
Y = np.array(Y)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.20, random_state=42)

val_set = (X_test, Y_test)
TRAINSET_SIZE = X_train.shape[0]
VALSET_SIZE   = X_test.shape[0]

print(X_train)
#-- Validation Dataset --#
#val_data = '/{}/val/'.format(SETNAME)
#val_dataset = glob(data_dir + val_data + "*.txt") #tf.data.Dataset.list_files(data_dir + val_data + "*.jpeg")
#VALSET_SIZE = len(val_dataset)

# -- Train Dataset --#
#dataset = {'train': train_dataset, 'val': val_dataset}

# Apprentissage du model 
# Get model
network = SLICNet(5)
model   = network.model()
# Instantiate an optimizer to train the model.
model.compile(optimizer = tf.keras.optimizers.Adam(
    learning_rate = 1e-8), 
    loss    = tf.keras.losses.BinaryCrossentropy(),#balanced_bce, 
    metrics = ['accuracy', 'mse'])

model.summary()

NUM_EPOCHS  = 100
MODEL_NAME  = "slicnet"
LOG_DIR     = os.path.join(os.getcwd(), 'logs')
BATCH_SIZE  = 32
STEPS_PER_EPOCH   = TRAINSET_SIZE // BATCH_SIZE
VALIDATION_STEPS  = VALSET_SIZE // BATCH_SIZE
SCALAR_DIR = "{}/scalars/{}/".format(LOG_DIR, MODEL_NAME)
CHECKPOINT_NAME = '{}/{}/model.hdf5'.format(LOG_DIR, MODEL_NAME)
CHECKPOINT_DIR = os.path.dirname(CHECKPOINT_NAME)

tensorboard_callback  = tf.keras.callbacks.TensorBoard(
    log_dir=SCALAR_DIR,
    write_images=True)
model_checkpoint      = tf.keras.callbacks.ModelCheckpoint(
    CHECKPOINT_NAME,
    monitor='val_loss',
    verbose=True,
    save_best_only=True)
                    
model_history = model.fit(x=X_train, y=Y_train,
    batch_size=BATCH_SIZE,
    epochs=NUM_EPOCHS,
    steps_per_epoch=STEPS_PER_EPOCH,
    validation_steps=VALIDATION_STEPS,
    validation_data=val_set,
    callbacks=[model_checkpoint, tensorboard_callback])