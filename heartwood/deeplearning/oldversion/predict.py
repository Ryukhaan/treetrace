import numpy as np 
import os
import sys

import skimage.io as io
import skimage.transform as transform
import tensorflow as tf 

import model
import data

np.set_printoptions(threshold=sys.maxsize)

# Check if there are arguments
if len(sys.argv) < 3:
	print("Required 2 or 3 arguments ({} found)".format(len(sys.argv)))
	exit(2)

# Load network weights
net 	= model.unet()
name 	= "".join([model.save_dir, '/', sys.argv[1]])
try:
	net.load_weights(name)
except IOError:
	print("Not weights found ({} not exists)".format(name))
	exit(2)

# Create predict generator
input_dir 	= sys.argv[2]
target_size = (model.width, model.height)

if os.path.isdir(input_dir):
	for i in os.listdir(input_dir):
		if i == '.DS_Store': continue
		file_name = os.path.join(input_dir, i)
		img = io.imread(file_name)
		img = img / 255.0
		#img = cv2.resize(img, target_size)
		img = transform.resize(img, target_size)
		img = np.reshape(img,(1,)+img.shape)
		result = net.predict(img, verbose=1)
		data.save_image("/Users/remidecelle/Documents/output", result[0,:,:,:], i)
else:
	file_name = input_dir
	img = io.imread(file_name)
	img = img / 255.0
	img = cv2.resize(img, target_size)
	img = np.reshape(img,(1,)+img.shape)
	result = net.predict(img, verbose=1)
	data.save_image("/Users/remidecelle/Documents/output", result[0,:,:,:], "output.jpg")




