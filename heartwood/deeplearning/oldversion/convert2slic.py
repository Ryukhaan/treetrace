from glob import glob
from skimage.color import rgb2lab
from skimage.segmentation import slic, mark_boundaries
from skimage.exposure import rescale_intensity
from skimage.io import imread
import os
import re
import numpy as np
import matplotlib.pyplot as plt

data_dir =  os.path.join(os.getcwd(), 'data')
inputset='logyard'
setname = 'slic'

train_data = '/{}/val/images/'.format(inputset)
train_dataset = glob(data_dir + train_data + "*.jpeg")

for path in train_dataset:
	image = imread(path)
	labimg = rgb2lab(image)

	mask_path = re.sub(r'images','mask', path)
	mask_path = re.sub(r'jpeg', 'png', mask_path) 
	mask  = imread(mask_path)
	mask  = mask / 255.
	superpixel_labels = slic(labimg, 
		n_segments=512, 
		compactness=20, 
		start_label=1,
		convert2lab=False)

	# 	# dessiner les bordures des superpixels en noir et blanc
	# img = mark_boundaries(image, superpixel_labels, \
	#                       color=(1, 1, 1), outline_color=(0, 0, 0), \
	#                       mode='outer')

	# # afficher le résultat
	# plt.imshow(img)
	# plt.show()

	nb_superpixels = np.max(superpixel_labels) + 1
	height = image.shape[0]
	width  = image.shape[1]
	# pour calculer la position du barycentre dans la largeur de l'image
	x_idx = np.repeat(range(width), height)
	x_idx = np.reshape(x_idx, [width, height])
	x_idx = np.transpose(x_idx)
	# pour calculer la position du barycentre dans la hauteur de l'image
	y_idx = np.repeat(range(height), width)
	y_idx = np.reshape(y_idx, [height, width])
	# extraire les caractéristiques de chaque superpixel
	l = labimg[:,:,0]
	l = rescale_intensity(l)
	a = labimg[:,:,1]
	a = rescale_intensity(a)
	b = labimg[:,:,2]
	b = rescale_intensity(b)
	feature_superpixels = [] 
	y = []
	inpath = re.sub(r'logyard','slic', path)
	inpath = re.sub(r'images/', '', inpath)
	inpath = re.sub(r'jpeg', 'txt', inpath)
	with open(inpath, "w") as file:
		for label in range(nb_superpixels): 
			# pixels appartenant au superpixels
			idx = superpixel_labels == label 
			elt = len(idx)  
			sp = ""
			sp = sp + str(np.mean(a[idx]))
			sp = sp + "," + str(np.mean(b[idx]))
			sp = sp + "," + str(np.mean(l[idx]))
			sp = sp + "," + str(np.mean(x_idx[idx]) / (width - 1))
			sp = sp + "," + str(np.mean(y_idx[idx]) / (height - 1)) 		
			if np.sum(mask[idx]) > float(elt)*0.5:
				sp = sp + ",0 \n"
			else:
				sp = sp + ",1 \n"
			file.write(sp)

	#exit()
