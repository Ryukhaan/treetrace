import numpy as np 
import os
import sys

import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard

import model
import data

# Parameters Input
numChannels = model.channels
height      = model.height
width       = model.width
batch_size  = model.batch_size
epochs      = model.epochs

# Folders Train, Valid, Test and Save
if len(sys.argv) < 1:
  train_dir     = os.path.join(os.getcwd(), 'dataset', model.train_folder)
  valid_dir     = os.path.join(os.getcwd(), 'dataset', model.valid_folder)
else:
  train_dir     = sys.argv[1]
  valid_dir     = sys.argv[2]

input_folder  = 'images'
label_folder  = 'mask'

data_gen_args = dict(rescale = 1. / 255,
                     rotation_range=90,
                     width_shift_range=0.05,
                     height_shift_range=0.05,
                     shear_range=0.05,
                     zoom_range=0.05,
                     horizontal_flip=True,vertical_flip=True,
                     fill_mode='nearest')


# Create network
__generators    = data.trainset_generator(model.batch_size, train_dir, input_folder, label_folder,
                                 data_gen_args, save_to_dir=None)

__validator     = data.trainset_generator(1, valid_dir, input_folder, label_folder,
                                          data_gen_args, save_to_dir=None)


scalar_dir = ''.join([model.logs_dir, '/', model.name, '/'])
if not os.path.isdir(scalar_dir):
  os.mkdir(scalar_dir)

tensorboard_callback = TensorBoard(log_dir=scalar_dir)

model_checkpoint = ModelCheckpoint(model.fullname,
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=True)
net = model.unet()
net.summary()

try:
  net.load_weights(model.fullname)
except IOError:
  pass

net.fit(__generators,
        steps_per_epoch   = model.steps_per_epoch,
        validation_data   = __validator,
        validation_steps  = model.validation_steps,
        epochs=model.epochs,
        callbacks=[model_checkpoint, tensorboard_callback])

