import numpy as np 
import os
import tensorflow as tf 
import utils

l2 			= None #tf.keras.regularizers.L2(0.1)
weight_init = tf.initializers.glorot_uniform()
drate 		= 0.99

class SLICNet():
	"""
	"""
	def __init__(self, input_shape):
		self.input_size  	= input_shape

	def model(self):
		inputs 	= tf.keras.layers.Input(self.input_size)
		hl_1	= tf.keras.Sequential(
			[
				tf.keras.layers.Dense(1024,
					kernel_regularizer=l2,
					kernel_initializer=weight_init),
				tf.keras.layers.BatchNormalization(),
				tf.keras.layers.LeakyReLU(),
				tf.keras.layers.Dropout(drate)
			])(inputs)
		hl_2	= tf.keras.Sequential(
			[
				tf.keras.layers.Dense(1024,
					kernel_regularizer=l2,
					kernel_initializer=weight_init),
				tf.keras.layers.BatchNormalization(),
				tf.keras.layers.LeakyReLU(),
				tf.keras.layers.Dropout(drate)
			])(hl_1)
		outputs = tf.keras.layers.Dense(1,
						activation='sigmoid',
						kernel_regularizer=l2,
						kernel_initializer=weight_init)(hl_2)

		return tf.keras.Model(inputs, outputs)
