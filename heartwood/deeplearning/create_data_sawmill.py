import os
import numpy as np
from shutil import copy2
import copy

src_dir = "/Users/remidecelle/Documents/Treetrace/tmp_sawmill/{}/{}"
dst_dir = "/Users/remidecelle/Documents/Treetrace/code/treetrace/heartwood/deeplearning/data/sawmill_{}/{}/{}/"

if __name__ == '__main__':
	files = sorted(os.listdir(src_dir.format("input", "")))
	np.random.shuffle(files)
	j = 0
	for i in range(1,9):
		tmp_files = copy.copy(files)
		k = j
		if i == 7 or i == 8: 
			j += 18
		else:
			j += 19
		for img in files[k:j]:
			name, ext = img.split('.')
			# Image
			copy2(src_dir.format("input", img), dst_dir.format(i, "val", "images"))
			# Mask
			copy2(src_dir.format("output", name+".png"), dst_dir.format(i, "val", "mask"))

			tmp_files.remove(img)

		for img in tmp_files:
			name, ext = img.split('.')
			# Image
			copy2(src_dir.format("input", img), dst_dir.format(i, "train", "images"))
			# Mask
			copy2(src_dir.format("output", name+".png"), dst_dir.format(i, "train", "mask"))




