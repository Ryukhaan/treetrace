import numpy as np
import matplotlib.pyplot as plt
import skimage.io
from skimage.color import rgb2gray, rgb2lab, lab2rgb, rgb2hsv
import skimage.transform
#from sklearn.cluster import KMeans
import skimage.measure
from skimage.util import img_as_ubyte
import scipy.ndimage
import skimage.morphology as skmorph
import skimage.filters as skfilt
import cv2 as cv
from scipy import ndimage
full = skimage.io.imread("/Users/remidecelle/Documents/Treetrace/code/treetrace/heartwood/deeplearning/data/logyard/images/logyard_A13b.jpeg")
subimg = np.float32(full[1185:1585, 1185:1585, 0])
subimg = (subimg - subimg.min()) / (subimg.max() - subimg.min())
gradientsigma = 3.0
blocksigma=1.0
orientsmoothsigma=8.0

fx = ndimage.sobel(subimg, axis=0,mode='constant')
fy = ndimage.sobel(subimg, axis=1,mode='constant')            
Gx = skfilt.gaussian(fx, gradientsigma)
Gy = skfilt.gaussian(fy, gradientsigma)

Gxx = Gx ** 2
Gxy = np.multiply(Gx,Gy)
Gyy = Gy ** 2
    
Gxx = skfilt.gaussian(Gxx, blocksigma) 
Gxy = 2*skfilt.gaussian(Gxy, blocksigma) 
Gyy = skfilt.gaussian(Gyy, blocksigma)
    
denom = np.sqrt(Gxy**2 + (Gxx - Gyy)**2) + 10e-8
sin2theta = np.divide(Gxy,denom)
cos2theta = np.divide(Gxx-Gyy,denom)

cos2theta = skfilt.gaussian(cos2theta, orientsmoothsigma)
sin2theta = skfilt.gaussian(sin2theta, orientsmoothsigma)
    
orientim = np.pi/2 + np.arctan2(sin2theta,cos2theta)/2;
#plt.figure(figsize=(7,7))
#plt.imshow(orientim, cmap='gray'), plt.colorbar(), plt.axis('off')
#plt.savefig('orientation.png', bbox_inches='tight');

x, y = np.meshgrid(np.linspace(0,subimg.shape[0],subimg.shape[0]),np.linspace(0,subimg.shape[1],subimg.shape[1]))
u = np.cos(orientim)
v = np.sin(orientim)
length=50
xoff = length*u
yoff = length*v

X = x#x - xoff
Y = y#y - yoff
plt.figure(figsize=(7,7))
plt.quiver(X[::10],Y[::10],2*xoff[::10],2*yoff[::10])
plt.show()