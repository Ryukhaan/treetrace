import numpy as np 
import os
import tensorflow as tf 
from enum import Enum
import skimage.io
import matplotlib.pyplot as plt
import utils
import layers
import resnet


AUTO = tf.data.AUTOTUNE
CROP_TO = 64
SEED = 42
PROJECT_DIM = 2048
BATCH_SIZE = 512
EPOCHS = 100

class WarmUpCosine(tf.keras.optimizers.schedules.LearningRateSchedule):
    """
    Implements an LR scheduler that warms up the learning rate for some training steps
    (usually at the beginning of the training) and then decays it
    with CosineDecay (see https://arxiv.org/abs/1608.03983)
    """

    def __init__(
        self, learning_rate_base, total_steps, warmup_learning_rate, warmup_steps
    ):
        super(WarmUpCosine, self).__init__()

        self.learning_rate_base = learning_rate_base
        self.total_steps = total_steps
        self.warmup_learning_rate = warmup_learning_rate
        self.warmup_steps = warmup_steps
        self.pi = tf.constant(np.pi)

    def __call__(self, step):
        if self.total_steps < self.warmup_steps:
            raise ValueError("Total_steps must be larger or equal to warmup_steps.")
        learning_rate = (
            0.5
            * self.learning_rate_base
            * (
                1
                + tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
            )
        )

        if self.warmup_steps > 0:
            if self.learning_rate_base < self.warmup_learning_rate:
                raise ValueError(
                    "Learning_rate_base must be larger or equal to "
                    "warmup_learning_rate."
                )
            slope = (
                self.learning_rate_base - self.warmup_learning_rate
            ) / self.warmup_steps
            warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
            learning_rate = tf.where(
                step < self.warmup_steps, warmup_rate, learning_rate
            )
        return tf.where(
            step > self.total_steps, 0.0, learning_rate, name="learning_rate"
        )

class SimpleEncode():
	def __init__(self, num_filt=16, num_it=3, pretrained_weights=None):
		self.input_size  	= (64,64,3) #eval(args.shape)
		self.out_features 	= num_filt
		self.num_it = num_it

	def model(self):
		self.inputs  = tf.keras.layers.Input(self.input_size) # 304x304x3
		x = self.inputs
		block = x
		max_layer = x
		for i in range(self.num_it):
			block = layers.depth_conv2d_block(inputs=max_layer, num_filters=(2**i)*self.out_features)
			block = layers.depth_conv2d_block(inputs=block, num_filters=(2**i)*self.out_features)
			if i != self.num_it-1:
				max_layer = tf.keras.layers.MaxPooling2D()(block)

		x = layers.ShakeoutConv2D(filters=1, kernel_size=(1,1),
						padding = 'same', 
						kernel_initializer = layers.weight_init,
						kernel_regularizer = layers.l2,
						activation=tf.keras.activations.hard_sigmoid)(block)
		# x = tf.keras.layers.Dense(
		#     PROJECT_DIM//4,
		#     name="prediction_layer_0",
		#     kernel_regularizer=layers.l2)(tf.keras.layers.Flatten()(x))
		# x = tf.keras.layers.BatchNormalization()(x)
		# x = tf.keras.layers.LeakyReLU()(x)
		# self.outputs = tf.keras.layers.Dense(PROJECT_DIM,
		# 	name="prediction_output",
		# 	kernel_regularizer=layers.l2)(x)
		self.outputs = x
		return tf.keras.Model(self.inputs, self.outputs)


def load_image(filename):
	return skimage.io.imread(filename)

def slices_wood_image(image, output, ksize=3):
	height, width, _ = image.shape
	h, w = 128, 128
	for i in range(height//2-ksize*h, height//2+(ksize-1)*h,64):
		for j in range(width//2-ksize*w, width//2+(ksize-1)*w,64):
			output.append(np.array(image[i:i+h, j:j+w,:]))
	return output

x_train = []
dirpath = os.getcwd() + '/data/logyard_1/train/images/'
for file in os.listdir(dirpath):
	if file.split('.')[-1] != 'jpeg': continue
	x_train = slices_wood_image(load_image(dirpath + file), x_train)
	break
x_train = np.array(x_train)
print(x_train.shape)

def random_resize_crop(image, scale=[0.75, 1.0], crop_size=64):
    if crop_size == 64:
        image_shape = 96
        image = tf.image.resize(image, (image_shape, image_shape))
    else:
        image_shape = 48
        image = tf.image.resize(image, (image_shape, image_shape))
    size = tf.random.uniform(
        shape=(1,),
        minval=scale[0] * image_shape,
        maxval=scale[1] * image_shape,
        dtype=tf.float32,
    )
    size = tf.cast(size, tf.int32)[0]
    crop = tf.image.random_crop(image, (size, size, 3))
    crop_resize = tf.image.resize(crop, (crop_size, crop_size))
    return crop_resize

def flip_random_crop(image):
    image = tf.image.random_flip_left_right(image)
    image = tf.image.random_flip_up_down(image)
    image = random_resize_crop(image, crop_size=CROP_TO)
    return image


@tf.function
def float_parameter(level, maxval):
    return tf.cast(level * maxval / 10.0, tf.float32)


@tf.function
def sample_level(n):
    return tf.random.uniform(shape=[1], minval=0.1, maxval=n, dtype=tf.float32)


@tf.function
def solarize(image, level=6):
    #threshold = float_parameter(sample_level(level), 1)
    #return tf.where(image < threshold, image, 255 - image)
    return image

def color_jitter(x, strength=0.6):
    x = tf.image.random_brightness(x, max_delta=0.8 * strength)
    x = tf.image.random_contrast(
        x, lower=1 - 0.8 * strength, upper=1 + 0.8 * strength
    )
    x = tf.image.random_saturation(
        x, lower=1 - 0.8 * strength, upper=1 + 0.8 * strength
    )
    x = tf.image.random_hue(x, max_delta=0.05 * strength)
    x = tf.clip_by_value(x, 0, 255)
    return x


def color_drop(x):
    #x = tf.image.rgb_to_grayscale(x)
    #x = tf.tile(x, [1, 1, 3])
    return x


def random_apply(func, x, p):
    if tf.random.uniform([], minval=0, maxval=1) < p:
        return func(x)
    else:
        return x


def custom_augment(image):
    image = tf.cast(image, tf.float32)
    image = flip_random_crop(image)
    image = random_apply(color_jitter, image, p=0.9)
    image = random_apply(color_drop, image, p=0.3)
    image = random_apply(solarize, image, p=0.3)
    return image

ssl_ds_one = tf.data.Dataset.from_tensor_slices(x_train)
ssl_ds_one = (
    ssl_ds_one.shuffle(1024, seed=SEED)
    .map(custom_augment, num_parallel_calls=AUTO)
    .batch(BATCH_SIZE)
    .prefetch(AUTO)
)

ssl_ds_two = tf.data.Dataset.from_tensor_slices(x_train)
ssl_ds_two = (
    ssl_ds_two.shuffle(1024, seed=SEED)
    .map(custom_augment, num_parallel_calls=AUTO)
    .batch(BATCH_SIZE)
    .prefetch(AUTO)
)

ssl_ds_three = tf.data.Dataset.from_tensor_slices(x_train)
ssl_ds_three = (
    ssl_ds_three.shuffle(1024, seed=SEED)
    .map(custom_augment, num_parallel_calls=AUTO)
    .batch(BATCH_SIZE)
    .prefetch(AUTO)
)

# We then zip both of these datasets.
ssl_ds = tf.data.Dataset.zip((ssl_ds_one, ssl_ds_two, ssl_ds_three))

# sample_images_one = next(iter(ssl_ds_one))
# plt.figure(figsize=(10, 10))
# for n in range(25):
#     ax = plt.subplot(5, 5, n + 1)
#     plt.imshow(sample_images_one[n].numpy().astype("int"))
#     plt.axis("off")
# plt.show()
# # Ensure that the different versions of the dataset actually contain
# # identical images.
# sample_images_two = next(iter(ssl_ds_two))
# plt.figure(figsize=(10, 10))
# for n in range(25):
#     ax = plt.subplot(5, 5, n + 1)
#     plt.imshow(sample_images_two[n].numpy().astype("int"))
#     plt.axis("off")
# plt.show()
# exit()

def off_diagonal(x):
    n = tf.shape(x)[0]
    flattened = tf.reshape(x, [-1])[:-1]
    off_diagonals = tf.reshape(flattened, (n-1, n+1))[:, 1:]
    return tf.reshape(off_diagonals, [-1])


def normalize_repr(z):
    z_norm = (z - tf.reduce_mean(z, axis=0)) / tf.math.reduce_std(z, axis=0)
    return z_norm


def compute_loss(z_a, z_b, lambd):
    # Get batch size and representation dimension.
    batch_size = tf.cast(tf.shape(z_a)[0], z_a.dtype)
    repr_dim = tf.shape(z_a)[1]

    # Normalize the representations along the batch dimension.
    z_a_norm = normalize_repr(z_a)
    z_b_norm = normalize_repr(z_b)

    # Cross-correlation matrix.
    c = tf.matmul(z_a_norm, z_b_norm, transpose_a=True) / batch_size

    # Loss.
    on_diag = tf.linalg.diag_part(c) + (-1)
    on_diag = tf.reduce_sum(tf.pow(on_diag, 2))
    off_diag = off_diagonal(c)
    off_diag = tf.reduce_sum(tf.pow(off_diag, 2))
    loss = on_diag + (lambd * off_diag)
    return loss

def compute_xcorr2_loss(i_a, i_b, lambd):
    # Get batch size and representation dimension.
    batch_size = tf.cast(tf.shape(i_a)[0], tf.dtypes.int64)
    repr_dim = tf.cast(tf.shape(i_a)[1], tf.dtypes.int64)

    # Normalize the representations along the batch dimension.
    z_a_norm = normalize_repr(i_a)
    z_b_norm = normalize_repr(i_b)
    z_b_norm = tf.transpose(z_b_norm, perm=[1,2,3,0])
    # Cross-correlation matrix.
    loss = 0
    c = tf.nn.conv2d(z_a_norm, z_b_norm, 1, 'SAME')
    
    z_c_norm = (c - tf.math.reduce_min(c, axis=0)) / (tf.math.reduce_max(c, axis=0) - tf.math.reduce_min(c, axis=0))
    # None, H, W, None -> None, H, W, 1 !
    target = tf.SparseTensor(indices=[[repr_dim//2, repr_dim//2, 0]], 
            values=[1.0], dense_shape=[repr_dim, repr_dim, 1])
    target = tf.cast(tf.sparse.to_dense(target), i_a.dtype)
    target = tf.expand_dims(target, 0)
    target = tf.repeat(target, batch_size, axis=0) 
    #print(target.shape, z_c_norm.shape)
    loss = tf.reduce_mean(tf.pow(z_c_norm - target, 2))

    # Loss.
    #on_diag = tf.linalg.diag_part(c) + (-1)
    #on_diag = tf.reduce_sum(tf.pow(on_diag, 2))
    #off_diag = off_diagonal(c)
    #off_diag = tf.reduce_sum(tf.pow(off_diag, 2))
    #loss = on_diag + (lambd * off_diag)

    return loss


class BarlowTwins(tf.keras.Model):
    def __init__(self, encoder, lambd=5e-3):
        super(BarlowTwins, self).__init__()
        self.encoder = encoder
        self.lambd = lambd
        self.loss_tracker = tf.keras.metrics.Mean(name="loss")

    @property
    def metrics(self):
        return [self.loss_tracker]

    def train_step(self, data):
        # Unpack the data.
        ds_one, ds_two, ds_three = data

        # Forward pass through the encoder and predictor.
        with tf.GradientTape() as tape:
            z_a, z_b = self.encoder(ds_one, training=True), self.encoder(ds_two, training=True)
            #loss = compute_loss(z_a, z_b, self.lambd) 
            loss = compute_xcorr2_loss(z_a, z_b, self.lambd)

        # Compute gradients and update the parameters.
        gradients = tape.gradient(loss, self.encoder.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.encoder.trainable_variables))

        # Monitor loss.
        self.loss_tracker.update_state(loss)
        return {"loss": self.loss_tracker.result()}

STEPS_PER_EPOCH = len(x_train) // BATCH_SIZE
TOTAL_STEPS = STEPS_PER_EPOCH * EPOCHS
WARMUP_EPOCHS = int(EPOCHS * 0.1)
WARMUP_STEPS = int(WARMUP_EPOCHS * STEPS_PER_EPOCH)

lr_decayed_fn = WarmUpCosine(
    learning_rate_base=1e-3,
    total_steps=EPOCHS * STEPS_PER_EPOCH,
    warmup_learning_rate=0.0,
    warmup_steps=WARMUP_STEPS
)
#resnet_enc = resnet.get_network(hidden_dim=PROJECT_DIM, use_pred=False, 
#                                          return_before_head=False)
resnet_enc = SimpleEncode().model()
resnet_enc.summary()
try:
	resnet_enc.load_weights('barlow_brothers.hdf5')
except:
	pass
optimizer = tf.keras.optimizers.SGD(learning_rate=lr_decayed_fn, momentum=0.9)
barlow_twins = BarlowTwins(resnet_enc)
barlow_twins.compile(optimizer=optimizer)
history = barlow_twins.fit(ssl_ds, epochs=EPOCHS)
barlow_twins.encoder.save("barlow_brothers.hdf5")
