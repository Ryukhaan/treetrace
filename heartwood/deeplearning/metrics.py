import numpy as np 
import os
import tensorflow as tf 

def matthews_correlation(y_true, y_pred, clipped=True):
    if clipped:
        #y_pred_pos = tf.keras.backend.round(y_pred, 0, 1)
        y_pred_pos = tf.where(y_pred >= 0.5,
            np.dtype('float').type(1),
            np.dtype('float').type(0))
    else:
        y_pred_pos = y_pred
    y_pred_neg = 1 - y_pred_pos

    #y_pos = tf.keras.backend.round(y_true, 0, 1)
    y_pos = tf.where(y_true >= 0.5,
        np.dtype('float').type(1),
        np.dtype('float').type(0))
    y_neg = 1 - y_pos

    tp = tf.keras.backend.sum(y_pos * y_pred_pos)
    tn = tf.keras.backend.sum(y_neg * y_pred_neg)

    fp = tf.keras.backend.sum(y_neg * y_pred_pos)
    fn = tf.keras.backend.sum(y_pos * y_pred_neg)

    numerator = (tp * tn - fp * fn)
    denominator = tf.keras.backend.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

    return numerator / (denominator + tf.keras.backend.epsilon())


def numpy_mcc(y_true, y_pred):
    y_pred_pos = y_pred
    y_pred_neg = 1 - y_pred_pos

    y_pos = y_true
    y_neg = 1 - y_pos

    tp = np.sum(y_pos * y_pred_pos, dtype='object')
    tn = np.sum(y_neg * y_pred_neg, dtype='object')

    fp = np.sum(y_neg * y_pred_pos, dtype='object')
    fn = np.sum(y_pos * y_pred_neg, dtype='object')
    #print(tp,tn,fp,fn)
    numerator = (tp * tn - fp * fn)
    denominator = np.sqrt(float((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)))

    return numerator / (denominator + 1e-12)

def mcc_from_matrix(M):
    tp, tn, fp, fn = M
    numerator = (tp * tn - fp * fn)
    denominator = np.sqrt(float((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)))
    return numerator / (denominator + 1e-12)

def get_confusion_matrix(y_true, y_pred):
    y_pred_pos = y_pred
    y_pred_neg = 1 - y_pred_pos

    y_pos = y_true
    y_neg = 1 - y_pos

    tp = np.sum(y_pos * y_pred_pos, dtype='object')
    tn = np.sum(y_neg * y_pred_neg, dtype='object')

    fp = np.sum(y_neg * y_pred_pos, dtype='object')
    fn = np.sum(y_pos * y_pred_neg, dtype='object')
    return tp, tn, fp, fn