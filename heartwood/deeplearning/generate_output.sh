#!/bin/bash

# # Generate output
# python3 main.py --model=unet --name=Unet1 --set="logyard_1" --mode=test
# python3 main.py --model=unet --name=Unet2 --set="logyard_2" --mode=test
# python3 main.py --model=unet --name=Unet3 --set="logyard_3" --mode=test
# python3 main.py --model=unet --name=Unet4 --set="logyard_4" --mode=test
# python3 main.py --model=unet --name=Unet5 --set="logyard_5" --mode=test
# python3 main.py --model=unet --name=Unet6 --set="logyard_6" --mode=test
# python3 main.py --model=unet --name=Unet7 --set="logyard_7" --mode=test
# python3 main.py --model=unet --name=Unet8 --set="logyard_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/unet/$filename; 
# done

# python3 main.py --model=attunet --name=AttUnet1 --set="logyard_1" --mode=test
# python3 main.py --model=attunet --name=AttUnet2 --set="logyard_2" --mode=test
# python3 main.py --model=attunet --name=AttUnet3 --set="logyard_3" --mode=test
# python3 main.py --model=attunet --name=AttUnet4 --set="logyard_4" --mode=test
# python3 main.py --model=attunet --name=AttUnet5 --set="logyard_5" --mode=test
# python3 main.py --model=attunet --name=AttUnet6 --set="logyard_6" --mode=test
# python3 main.py --model=attunet --name=AttUnet7 --set="logyard_7" --mode=test
# python3 main.py --model=attunet --name=AttUnet8 --set="logyard_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/attunet/$filename; 
# done

# python3 main.py --model=simple --name=Sunet1 --set="logyard_1" --mode=test
# python3 main.py --model=simple --name=Sunet2 --set="logyard_2" --mode=test
# python3 main.py --model=simple --name=Sunet3 --set="logyard_3" --mode=test
# python3 main.py --model=simple --name=Sunet4 --set="logyard_4" --mode=test
# python3 main.py --model=simple --name=Sunet5 --set="logyard_5" --mode=test
# python3 main.py --model=simple --name=Sunet6 --set="logyard_6" --mode=test
# python3 main.py --model=simple --name=Sunet7 --set="logyard_7" --mode=test
# python3 main.py --model=simple --name=Sunet8 --set="logyard_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/simple/$filename; 
# done

# python3 main.py --model=cbam --name=CbamUnet1 --set="logyard_1" --mode=test
# python3 main.py --model=cbam --name=CbamUnet2 --set="logyard_2" --mode=test
# python3 main.py --model=cbam --name=CbamUnet3 --set="logyard_3" --mode=test
# python3 main.py --model=cbam --name=CbamUnet4 --set="logyard_4" --mode=test
# python3 main.py --model=cbam --name=CbamUnet5 --set="logyard_5" --mode=test
# python3 main.py --model=cbam --name=CbamUnet6 --set="logyard_6" --mode=test
# python3 main.py --model=cbam --name=CbamUnet7 --set="logyard_7" --mode=test
# python3 main.py --model=cbam --name=CbamUnet8 --set="logyard_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/cbam/$filename; 
# done

# python3 main.py --model=simple --name=SawSimple1 --set="sawmill_1" --mode=test
# python3 main.py --model=simple --name=SawSimple2 --set="sawmill_2" --mode=test
# python3 main.py --model=simple --name=SawSimple3 --set="sawmill_3" --mode=test
# python3 main.py --model=simple --name=SawSimple4 --set="sawmill_4" --mode=test
# python3 main.py --model=simple --name=SawSimple5 --set="sawmill_5" --mode=test
# python3 main.py --model=simple --name=SawSimple6 --set="sawmill_6" --mode=test
# python3 main.py --model=simple --name=SawSimple7 --set="sawmill_7" --mode=test
# python3 main.py --model=simple --name=SawSimple8 --set="sawmill_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/simple/$filename; 
# done
# 
# python3 main.py --model=unet --name=SawUnet1 --set="sawmill_1" --mode=test
# python3 main.py --model=unet --name=SawUnet2 --set="sawmill_2" --mode=test
# python3 main.py --model=unet --name=SawUnet3 --set="sawmill_3" --mode=test
# python3 main.py --model=unet --name=SawUnet4 --set="sawmill_4" --mode=test
# python3 main.py --model=unet --name=SawUnet5 --set="sawmill_5" --mode=test
# python3 main.py --model=unet --name=SawUnet6 --set="sawmill_6" --mode=test
# python3 main.py --model=unet --name=SawUnet7 --set="sawmill_7" --mode=test
# python3 main.py --model=unet --name=SawUnet8 --set="sawmill_8" --mode=test

# # Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/unet/$filename; 
# done

python3 main.py --model=mam --name=MAM2Unet1 --set="logyard_1" --mode=test
python3 main.py --model=mam --name=MAM2Unet2 --set="logyard_2" --mode=test
python3 main.py --model=mam --name=MAM2Unet3 --set="logyard_3" --mode=test
python3 main.py --model=mam --name=MAM2Unet4 --set="logyard_4" --mode=test
python3 main.py --model=mam --name=MAM2Unet5 --set="logyard_5" --mode=test
python3 main.py --model=mam --name=MAM2Unet6 --set="logyard_6" --mode=test
python3 main.py --model=mam --name=MAM2Unet7 --set="logyard_7" --mode=test
python3 main.py --model=mam --name=MAM2Unet8 --set="logyard_8" --mode=test

# Move output
# for file in `ls ./logs/predict/*.jpeg`; do 
# 	filename="$(basename -- $file)"; 
# 	mv $file ./logs/predict/attunet/$filename; 
# done