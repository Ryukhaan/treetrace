#from scipy.signal import correlate
import numpy as np
import matplotlib.pyplot as plt

data_1 = []
with open('/Users/remidecelle/Documents/Values.csv','r') as file:
    for idx, line in enumerate(file.readlines()):
        if idx == 0: continue
        _, value = line.rstrip().split(',')
        data_1.append(float(value))
data_2 = []
with open('/Users/remidecelle/Documents/Values_2.csv','r') as file:
    for idx, line in enumerate(file.readlines()):
        if idx == 0: continue
        _, value = line.rstrip().split(',')
        data_2.append(float(value))  
data_1 = np.array(data_1)
data_2 = np.array(data_2)
n1 = (data_1 - np.mean(data_1)) / (np.std(data_1))
n2 = (data_2 - np.mean(data_2)) / (np.std(data_2))
plt.plot(n1)
plt.plot(n2)
plt.show()