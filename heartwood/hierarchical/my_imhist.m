function H = my_imhist(X)
% Assert X is in gray scale
T = X;
[h, w] = size(T);
H = zeros(256, 1);

for i=1:h
    for j=1:w
        if (isnan(T(i,j)))
            continue
        end
        H(floor(255*T(i,j))+1) = H(floor(255*T(i,j))+1) + 1;
    end
end
end