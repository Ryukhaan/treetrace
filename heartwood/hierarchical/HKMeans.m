files = dir('/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/unet/f1/input/*.png');
file = files(1);
px = 736;
py = 768;

I = imread([file.folder, '/', file.name]);
%I = rgb2gray(I);
level   = 5;
nmeans  = 2;

J{1} = I;
for n=2:level
    J{n} = imresize(J{n-1}, 0.5);
end

[L, centers] = imsegkmeans(J{level}, nmeans);
for n=level-1:-1:1
    mx = zeros(1, nmeans);
    my = zeros(1, nmeans);
    imagesc(L), axis image, axis off;
    pause;
%     for i=1:nmeans
%         idxs = find(L==i);
%         [yy, xx] = ind2sub(size(J{n}), idxs);
%         mx(i) = mean(xx, 'all');
%         my(i) = mean(yy, 'all');
%         imagesc(L), axis image, axis off;
%         hold on;
%         scatter(mx, my, 64, 'r+');
%         hold off;
%         pause;
%     end
    [L, centers] = imsegkmeans(J{n}, nmeans);
end