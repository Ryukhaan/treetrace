function [O,C,G,D] = DirectionalMorphology(img, lambda, omega)
% img       : Input image in grayscale
% lambda    : Range of strucutral element length
% nbins     : Number of orientation
% width     : Range of line width
tic;
L = numel(lambda);
W = numel(omega);

% numbers = nbins;
% switch nbins
%     case 0
%         coef = [0, 1; 1, 1; 1, 0; -1, 1];
%     %case 2
%     %    coef = Farey(3);
%     otherwise
%         coef = Farey(numbers-1);
% end
arah = ridgorien(img, 2.0, 4.0);
arah = arah(:,:,1);

N = size(coef, 1);
closings    = zeros(size(img, 1), size(img, 2), L, W, 5);
openings    = zeros(size(img, 1), size(img, 2), L, W, 5);

k = 1;
for longueur=lambda
    [Y, X] = meshgrid(-longueur:longueur, -longueur:longueur);
    j = 1;
    tmp = zeros(size(X));
    for width=omega
        i = 1;
        old_se = zeros(size(X));
        for theta=-20:10:20
%             a = coef(theta, 1);
%             b = coef(theta, 2);
%             mu = 0;
%             d1 = -(max(abs(a),abs(b)) + width) * 0.5 <= a * X + b * Y;
%             d2 = a * X + b * Y < (max(abs(a),abs(b)) + width) * 0.5;
%             D = d1 & d2;
            se = strel('arbitrary', D);
               
            %tmp = tmp + D;
            %imagesc(D), pause();
            %imwrite(D, ['./fig/se/SE_' num2str(length) '-' num2str(width) '-' num2str(theta) '.png'], 'png');
            %figure(3), imagesc(se); pause(0.02);
            %if (sum(old_se == se, 'all') == (2*length+1)^2) 
            %    closings(:,:,k,j,i) = closings(:,:,k,j,i-1);
            %    openings(:,:,k,j,i) = openings(:,:,k,j,i-1);
            %else
                
            closings(:,:,k,j,i) = imclose(img, se);
            openings(:,:,k,j,i) = imopen(img, se);
            
%             subplot 221, imagesc(openings(:,:,k,j,i)), colormap gray, axis image;
%                 title(['Filtered Image Openings']);
%             subplot 222, imagesc(closings(:,:,k,j,i)), colormap gray, axis image;
%                 title(['Filtered Image Closings']);
%             se = single(se);
%             se(length+1,length+1) = se(length+1,length+1) + 1;
%             subplot 223, imagesc(se), colormap gray, axis image;
%             pause(0.5);

            old_se = se;
            i = i + 1;
        end
        j = j + 1;
    end
    k = k + 1;
end

O = openings;
C = closings;
nbins = size(coef,1);

%oneline      = reshape(max(max(openings, [], 3), [], 4), size(img,1), size(img,2), nbins);
oneline     = reshape(sum(sum(openings, 3), 4), size(img,1), size(img,2), nbins);
%[~, DirP]   = max(oneline, [], 3);

% [m, DirP]    = max(oneline, [], 3);
% x = oneline == m;
% y = sum(x, 3);
% for i=1:size(y,1)
%     for j=1:size(y,2)
%         if y(i,j) == nbins
%             DirP(i,j) = ceil(nbins/2);
%             continue;
%         end
%         if y(i,j) > 1
%             % Loop
% %             cl = 0;
% %             clmax = 0;
% %             S = [];
% %             checkcirc = 0;
% %             % For each value in array x
% %             for k=1:nbins
% %                 if x(i,j,k) == 1
% %                     % Case of circular components
% %                     %if k == 1
% %                     %    checkcirc = 1;
% %                     %end
% %                     cl = cl + 1;
% %                     S(k) = k;
% %                 else
% %                     if cl > clmax
% %                         clmax = cl;
% %                     else
% %                         S = [];
% %                     end
% %                     cl = 0;
% %                 end
% %             end
% %             DirP(i,j) = median(S);
% 
%             % Soille version
%             ll = x(i,j,:);
%             CC = bwconncomp(ll);
%             ii = 1;
%             if CC.NumObjects > 1
%                 for k=1:CC.NumObjects
%                     [nn, ~] = size(CC.PixelIdxList{k});
%                     if nn > ii
%                         ii = k;
%                     end
%                 end
%             end
%             DirP(i,j) = median(CC.PixelIdxList{ii});
%         end
%     end
% end

maxplus  = reshape(max(oneline, [], 3), size(img,1), size(img,2));
minplus  = reshape(min(oneline, [], 3), size(img,1), size(img,2));
Gdirplus = maxplus - minplus;

%% CLosings
oneline = reshape(sum(sum(closings, 3), 4), size(img,1), size(img,2), nbins);

% [m, DirM]  = min(oneline, [], 3);
% x = oneline == m;
% y = sum(x, 3);
% for i=1:size(y,1)
%     for j=1:size(y,2)
%         if y(i,j) == nbins
%             DirM(i,j) = ceil(nbins/2);
%         end
%         if y(i,j) > 1
% %             cl = 0;
% %             clmax = 0;
% %             S = [];
% %             for k=1:nbins
% %                 if x(i,j,k) == 1
% %                     cl = cl + 1;
% %                     S(k) = k;
% %                 else
% %                     if cl > clmax
% %                         clmax = cl;
% %                     else
% %                         S = [];
% %                     end
% %                     cl = 0;
% %                 end
% %             end
% %             DirM(i,j) = median(S);
%             
%             % Soille version
%             ll = x(i,j,:);
%             CC = bwconncomp(ll);
%             ii = 1;
%             if CC.NumObjects > 1
%                 for k=1:CC.NumObjects
%                     [nn, ~] = size(CC.PixelIdxList{k});
%                     if nn > ii
%                         ii = k;
%                     end
%                 end
%             end
%             DirM(i,j) = median(CC.PixelIdxList{ii});
%             
%         end
%     end
% end
maxminus  = reshape(max(oneline, [], 3), size(img,1), size(img,2));
minminus  = reshape(min(oneline, [], 3), size(img,1), size(img,2));
Gdirminus = maxminus - minminus;

G = max(Gdirminus, Gdirplus);

orientation = atan2(coef(:,2), coef(:,1)+1e-16);
if ~exist('opt','var') || isempty(opt)
    D = DirP .* (G == Gdirplus) + DirM .* (G ~= Gdirplus);
    %D = mod((D-1) * pi / size(coef,1) + pi/2,pi);
    D = orientation(ceil(D));
    %D = (thetas(D) * pi / 180.0);
    %D = (D-1) * pi / (2*nbins) + pi/2;
    %D = mod(mod(D,size(coef,1)/2) * pi / size(coef,1) + pi/2, pi);
else
    if strcmp(opt,'white')
        %D = DirP;
        %D = mod((D-1) * pi / nbins,pi);
        D = orientation(ceil(DirP));
    end
    if strcmp(opt,'black')
        %D = DirM;
        %D = mod((D-1) * pi / nbins,pi);
        D = orientation(ceil(DirM));
    end
end
%imagesc(D), colormap gray, axis image;
%Dir = x(Dir);
 
% figure(1), imagesc(DirM), title('Direction with closings');
% figure(2), imagesc(DirP), title('Direction with openings');
% figure(3), imagesc(Gdir), title('Global Direction (max open / close)');
% figure(4), imagesc(Dir), title('Final Direction');
% figure(5), imagesc(img), colormap gray, title('Original Image');
% D = 0 ==> pi/2
% D = N-1 ==> pi/2 + pi - pi/N
% B = pi/2
% A = pi / N
%figure(2), imagesc(D);
%toc;
end

function coef = Farey(n)
if n==0
    fprintf('%i %i\n', 0, 1);
    fprintf('%i %i\n', 1, 1);
    coef = [0,1;1,1;1,0;-1,1];
    return;
end

coef = zeros(n, 2);

nn = 1;

% for i=0:n
%     coef(nn,:) = [i, n];
%     nn = nn + 1;
% end
% %for i=n-1:-1:-n
% for i=n-1:-1:0
%     coef(nn,:) = [n, i];
%     nn = nn + 1;
% end
% % for i=n-1:-1:-n
% %     coef(nn,:) = [i, -n];
% %     nn = nn + 1;
% % end
% % for i=-n:n
% %     coef(nn,:) = [-n, i];
% %     nn = nn + 1;
% % end
% % for i=-n:-1
% %     coef(nn,:) = [i, n];
% %     nn = nn + 1;
% % end

% Ascending
a = 0;
b = 1;
c = 1;
d = n;
while (c <= n)
    p = floor((b+n)/d) * c - a;
    q = floor((b+n)/d) * d - b;
    coef(nn, :) = [b,a];
    %coef(nn+1, :) = [-b,a];
    %coef(nn+2, :) = [b,-a];
    %coef(nn+3, :) = [-b, -a];
    a = c;
    b = d;
    c = p;
    d = q;
    nn = nn + 1;
end
coef(nn, :) = [a, b];
nn = nn + 1;
% Descending
a = 1;
b = 1;
c = n-1;
d = n;
while (a > 0)
    p = floor((b+n)/d) * c - a;
    q = floor((b+n)/d) * d - b;
    a = c;
    b = d;
    c = p;
    d = q;
    coef(nn, :) = [a,b];
    nn = nn + 1;
end
tmp = coef;
tmp(:,1) = -tmp(:,1);
tmp(1,:) = [];
tmp(end,:) = [];
tmp = flipud(tmp);
coef = cat(1, coef, tmp);
%disp(size(coef,1));
end