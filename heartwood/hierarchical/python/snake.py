import os 
import sys
import numpy as np
import cv2
from skimage.segmentation import active_contour, chan_vese
import matplotlib.pyplot as plt

if __name__ == '__main__':
	basename   = '/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/'
	imagesname = 'unet/f1/input/'
	groundname = 'unet/f1/output/'
	images_directory = os.path.join(basename, imagesname)
	ground_directory = os.path.join(basename, groundname)

	n_iter = 0
	for file in os.listdir(images_directory):
		name, file_extension = os.path.splitext(file.split('/')[-1])

		if file == '.DS_Store': continue

		if 'sawmill' in name:
			continue

		img = cv2.imread("{}/{}".format(images_directory, file))
		img = np.float32(img)
		mask = cv2.imread("{}/{}".format(ground_directory, file))

		height, width, depth = img.shape
		J = img

		#
		# Resizing image by taking maximum
		#
		nn = 4
		height, width, depth = J.shape
		dx, dy = 0, 0
		dy = nn - height % nn
		dx = nn - width % nn
		J = cv2.copyMakeBorder(J, 0, dy, 0, dx, cv2.BORDER_REPLICATE)
		tmp = np.zeros((J.shape[0]//nn, J.shape[1]//nn, J.shape[2]))
		for i in range(0, height, nn):
			for j in range(0, width, nn):
				for k in range(0, depth):
					tmp[i//nn,j//nn,k] = np.amax(J[i:i+nn,j:j+nn,k])	

		J = tmp

		J 		= np.uint8(J)
		ilab 	= cv2.cvtColor(J, cv2.COLOR_BGR2LAB)
		a 		= ilab[:,:,1]
		na 		= cv2.normalize(a, None, 0, 255, cv2.NORM_MINMAX)
		ha 		= np.uint8(na)
		pam 	= ha
		for i in range(1,6):
			pam = cv2.bilateralFilter(pam, 19, 9.0, 4.5)
		pam = cv2.equalizeHist(pam)
		ret1, thresh = cv2.threshold(pam, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

		# kernel 		= np.ones((5,5),np.uint8)
		# thresh 		= cv2.morphologyEx(thresh, 
		# 	cv2.MORPH_CLOSE, 
		# 	kernel,
		# 	borderValue=0)
		# thresh 		= cv2.dilate(thresh, kernel, iterations=3)
		# contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		# init = np.array(contours[2][:,0,:])
		# init = np.reshape(np.append(init, init[0,:]), (-1,2))

		# tmp = np.zeros(init.shape)
		# tmp[:,0] = init[:,1]
		# tmp[:,1] = init[:,0]

		# snake = active_contour(pam, tmp, w_edge=9, w_line=1,
		# 	alpha=0.0, beta=100, gamma=1e-5, coordinates='rc', max_iterations=100)

		#cv = chan_vese(pam, mu=0.5, lambda1=1, lambda2=1, tol=1e-3, max_iter=300,
        #       dt=0.5, init_level_set="checkerboard", extended_output=True)
		# fig, ax = plt.subplots(2, 2, figsize=(8, 8))
		# ax = ax.flatten()

		# ax[0].imshow(pam, cmap=plt.cm.gray)
		# ax[1].imshow(cv[0], cmap=plt.cm.gray)

		# ax[2].imshow(cv[1], cmap="gray")
		# ax[2].set_axis_off()
		# ax[2].set_title("Final Level Set", fontsize=12)

		# ax[3].plot(cv[2])
		# ax[3].set_title("Evolution of energy over iterations", fontsize=12)
		
		fig, ax = plt.subplots(2, 2, figsize=(7, 7))
		ax = ax.flatten()

		ax[0].imshow(cv2.cvtColor(np.uint8(img), cv2.COLOR_BGR2RGB))
		ax[1].imshow(cv2.cvtColor(J, cv2.COLOR_BGR2RGB))
		ax[2].imshow(pam, cmap=plt.cm.gray)
		ax[3].imshow(thresh, cmap=plt.cm.gray)

		ax[0].axis([0, img.shape[1], img.shape[0], 0])
		ax[1].axis([0, pam.shape[1], J.shape[0], 0])
		ax[2].axis([0, pam.shape[1], pam.shape[0], 0])
		ax[3].axis([0, thresh.shape[1], thresh.shape[0], 0])

		#fig, ax = plt.subplots(igsize=(7, 7))
		#ax.imshow(pam, cmap=plt.cm.gray)
		#ax.plot(tmp[:, 1], tmp[:, 0], '--r', lw=3)
		#ax.plot(snake[:, 1], snake[:, 0], '-b', lw=3)
		#ax.set_xticks([]), ax.set_yticks([])
		#ax.axis([0, J.shape[1], J.shape[0], 0])

		plt.show()
		if n_iter == 5:
			break
		n_iter = n_iter + 1
