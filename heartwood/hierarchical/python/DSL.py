import numpy as np
import matplotlib.pyplot as plt
import os
import time
import sys
import argparse
import cv2 as cv

def construct(a, b, mu, omega, l):
	ll = 0
	dx = 0
	dy = 0
	pixel_idx_list = np.zeros((2*l+1,2))
	err 	= 0
	for i in range(0, l):
		pixel_idx_list[i, :] = [dx, dy]
		dx 		= dx + 1
		err  	= err + a
		if err > (mu + omega):
	    	err = err - b
	    	dy 	= dy + 1
	dx 	= 0
	dy 	= 0
	err = 0
	for i in range(l, 2*l):
        pixel_idx_list[i, :] = [dx, dy]
        dx 		= dx  - 1
        err  	= err - a
        if (err <= mu):
            err = err + b
            dy 	= dy - 1

    return T


if __name__ == '__main__':
	print(construct(2,5,0,5))