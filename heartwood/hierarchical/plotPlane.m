function Y = plotPlane(sa)
xyz = []; 
n = 1;
for i = 1:size(sa,1)
    for j = 1:size(sa,2)
        n = n+1;
        xyz(n,1) = i;
        xyz(n,2) = j;
        if (isnan(sa(i,j)))
            xyz(n,3) = sa(i,j);
        else
            xyz(n,3) = 255 * sa(i,j);
        end
    end
end
ptCloud = pointCloud(xyz);
normals = pcnormals(ptCloud);

maxDistance = 10.0;
referenceVector = [0 0 1];
maxAngularDistance = 5;

[model1,inlierIndices,outlierIndices] = pcfitplane(ptCloud,...
            maxDistance,referenceVector,maxAngularDistance);
plane1 = select(ptCloud,inlierIndices);
remainPtCloud = select(ptCloud,outlierIndices);

pcshow(plane1);
hold on;
pcshow(remainPtCloud);
%roi = [-inf,inf;0.4,inf;-inf,inf];
%sampleIndices = findPointsInROI(remainPtCloud,roi);

end