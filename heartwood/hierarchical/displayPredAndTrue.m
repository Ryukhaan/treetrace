base = '/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/';
subset = 'woborder1/';
method = 'fill/';

filename = 'logyard_C07b.png';
filepath = [base, 'unet/', subset, 'input/', filename];
groundd  = [base, 'unet/', subset, 'output/'];
preddir  = [base, 'snake/', method];

I       = imread(filepath);
y_true  = imread([groundd, filename]);
try
    y_pred  = imread([preddir, filename]);
catch
    disp('File not found');
    return;
end
y_pred  = imresize(y_pred, [size(I,1), size(I,2)]);
y_true  = imresize(y_true, [size(I,1), size(I,2)]);

bw_true = imdilate( boundarymask(y_true), ones(3,3));
bw_pred = imdilate( boundarymask(y_pred), ones(3,3));

delines = bw_true + 2 * bw_pred;
figure;
b_true = labeloverlay(I, bw_true, ...
    'Colormap', [0,1,0], 'Transparency', 0.2); 
b_pred = labeloverlay(b_true, bw_pred, ...
    'Colormap', [1,0,0], 'Transparency', 0.2);
imagesc(b_pred);
axis image, axis off;
