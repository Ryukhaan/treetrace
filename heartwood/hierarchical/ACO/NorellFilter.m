function J = NorellFilter(img, sigma, band, opt)
%
% Norell et al. 2011

if ~exist('opt','var') || isempty(opt)
    debug = 0;
else
    debug = 1;
end

if length(size(img)) == 3
    I = rgb2gray(img);
else
    I = img;
end
% Resize image
%I = imresize(I, [1080, 1440]);

% Parameters
%sigma = 5;
%R = 10;
%sigmal = 10;

% Get middle and define sigma for gaussian
[h, w] = size(I);
xc = ceil(w / 2 + 1);
yc = ceil(h / 2 + 1);

% Create circle to occult boundaries
[X, Y] = meshgrid(1:w, 1:h);
% Gaussian filter

%myFilt = imgaussfilt(double(myCircle), 10);
% Pixelwise with sub-image
%ti = myFilt .* double(I);
%imagesc(ti), pause();

% Compute FFT
%F = fftshift(fft2(ti));
%Fa = log(abs(F));
%Fa = log(abs(real(F)));
%Fa = normalize(normaliser(Fa));
% Gaussian Filter
%Fb = imgaussfilt(Fa, sigmaf);
%Fc = Fb;
% Remove 15 percent maximum value (almost DC e.g.)
%Fc(Fc > 0.90*max(Fc(:))) = 0.0;
Fc = fftshift(fft2(I));
Fc = log(abs(Fc)+1);
Fs = Fc;
%Fc = Fc .* (1-im2double(myCircle));

% Remove horizontal and vertical value
hor = 1 - ((abs(X-xc)-1) < 1);
ver = 1 - ((abs(Y-yc)-1) < 1);
Fc = Fc .* im2double(hor);
Fc = Fc .* im2double(ver);
% Low-pass filter
myCircle = ((X-xc).^2 + (Y-yc).^2 > (h/3).^2) | ((X-xc).^2 + (Y-yc).^2 <= (h/band).^2);
Fc = Fc .* im2double(1-myCircle);

% Look at variance
%var = std(Fc(:));

T = 0.875;
M = max(Fc(:));
%lineFilter = 1-im2double(Fc<=T*M);
[rows, cols] = find(Fc>=T*M);
XY = [cols, rows];
[W,~,latent,~,explained,~] = pca(XY);
%certainty = (explained(1) - explained(2)) / explained(1);
certainty = (latent(1) - latent(2)) / latent(1);

%W = cov(XY);
%[~,lambda,W] = eig(W);
%certainty = 0.5;
% l1 = lambda(1,1);
% l2 = lambda(2,2);
% if l1 < l2
%     tmp = l2;
%     l2 = l1;
%     l1 = tmp;
% end
% certainty = (l1-l2) / l1;

slope = W(2,1) / W(1,1);
[X, Y] = meshgrid(-floor(w/2):ceil(w/2)-1, -floor(h/2):ceil(h/2)-1);
a = slope;
k = 1;
b = 0;
w = max(abs(a), 1);
lineFilter = (-k*w < -a * X + Y) & (-a * X + Y <= k*w);

% if certainty < 0.5
%    slope = W(2,2) / W(1,2);
%    a = slope;
%    b = 0;
%    w = max(abs(a), 1);
%    lineFilter = lineFilter | ((-k*w < -a * X + Y) & (-a * X + Y <= k*w));
% end

% % Calculations, orientation peaks
% [r, c] = size(Fc);
% angles = linspace(0, pi, 180);
% angles(end) = [];
%
% total = zeros(1, length(angles));
% old_total = 0;
% argi = 1;
% [X, Y] = meshgrid(-floor(c/2):ceil(c/2)-1, -floor(r/2):ceil(r/2)-1);
% lineFilter = zeros(size(X));
%
% for i=1:length(angles)
%     % line equation
%     % y = tan(theta)(x-a) + b
%     a = tan(angles(i));
%     w = max(abs(a), 1);
%     D = (-w < a * X + Y) & (a * X + Y <= w);
%
%     vals = 0;
%     k = 0;
%     [rows, cols] = find(D);
%     for j=1:length(rows)
%         vals = vals + Fc(rows(j), cols(j));
%         k = k + 1;
%     end
%
%     total(i) = vals ./ k;
%     if total(i) > old_total
%         old_total = total(i);
%         lineFilter = D;
%     end
% end

%smoothtotal = medfilt1(total, 7);
%figure(2), subplot 121, plot(total);
%figure(2), subplot 122, plot(smoothtotal);
%lineFilter = zeros(r, c);
% for i=1:length(lmx)
%     if lmx(i)<1 || lmx(i)>c || lmy(i)<1 || lmy(i)>r
%         continue
%     end
%     xi = round(c/2);
%     yi = round(r/2);
%     lineFilter(lmy(i), lmx(i)) = 1;
%     lineFilter(2 * yi - lmy(i)+1, 2 * xi - lmx(i)+1) = 1;
% end

if certainty > 0.5
    lineFilter = 1 - lineFilter;
    lineFilter = imgaussfilt(im2double(lineFilter), sigma);
    lineFilter = (lineFilter - min(lineFilter(:))) / (max(lineFilter(:)) - min(lineFilter(:)));
else
    lineFilter = ones(size(X));
end

Ft = fftshift(fft2(I));
%Ft = normalize(Ft);
% GI = 0.2989 * I(:,:,1) + 0.5870 * I(:,:,2);
% Ft = fftshift(fft2(GI));
% lineFilter = imresize(lineFilter, [1620, 2160]);


res = Ft .* lineFilter;
J = real(ifft2(ifftshift(res)));
J = normaliser(J);

if debug
    f1 = figure(1); f1.Name = 'Img originale';
    f2 = figure(2); f2.Name = 'FFT originale';
    f3 = figure(3); f3.Name = 'Passe-bande';
    f4 = figure(4); f4.Name = 'Seuill�e';
    f5 = figure(5); f5.Name = 'Droites';
    f6 = figure(6); f6.Name = 'Resultat';
    
    figure(f1), imagesc(img), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f2), imagesc(Fs), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f3), imagesc(Fc), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f4), imagesc(Fc>=T*M), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f5), imagesc(lineFilter),  colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    figure(f6), imagesc(J), colormap gray, axis image;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    
    %for i=1:6
    %    saveas(i, ['../png/aco/preprocess/' num2str(i) '.png']);
    %end
    pause();
end
end

function Y = normaliser(X)
%X = im2double(X);
M0 = 0;
V0 = 1;
M = mean(X(:));
V = std(X(:));
f1 = X > M;
Y1 = M0 + sqrt(V0 .* (X-M).^2 / V);
Y2 = M0 - sqrt(V0 .* (X-M).^2 / V);
Y = Y1 .* f1 + Y2 .* (1-f1);
end



