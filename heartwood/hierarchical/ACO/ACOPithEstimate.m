function [px, py, t] = ACOPithEstimate(img)
% Clock the timer
tic;
%% First Stage
% Resize
scale = 0.5; %1.0; %0.4;

% Convert into grayscale
if size(img, 3) == 3
    I = rgb2gray(img);
else
    I = img;
end

% Downsample image
I = imresize(I, scale, 'AntiAliasing', false);
J = I;
[H, W] = size(I);

% Compute sawing marks removal
I = blockproc(I, [ceil(H/4), ceil(W/4)], @(x) (NorellFilter(x.data, 6, 64)));

% Estimate local orientation of tree rings
Y = blockproc(I, [ceil(H/4), ceil(W/4)], @(x) (ridgeorient(x.data, 2.0, 4.0)));
O = Y(:,:,1);
R = Y(:,:,2);

% ACO main algorithm
[px, py] = ACO(I, O, R, ...
    'ants', 4, 'alpha', 2, ...
    'cluster', 11, ...
    'scale', scale, ...
    'animation', false, ...
    'uiimage', imresize(img, scale, 'AntiAliasing', false), ...
    'speed', 0.01);
old_px = px;
old_py = py;

%% Second Stage
% Select a subwindow centered on first pith estimation
scale = 0.4;
ww = 512;%128; % 512
sy = uint16((py-ww)*scale);
sx = uint16((px-ww)*scale);
ey = uint16((py+ww)*scale);
ex = uint16((px+ww)*scale);
if sy < 1
    sy = 1;
end
if sx < 1
    sx = 1;
end
if ey > size(img,1)
    ey = size(img,1);
end
if ex > size(img,2)
    ex = size(img,2);
end

% Convert into grayscale and downsample image
if size(img, 3) == 3
    I = imresize((rgb2gray(img)), scale, 'AntiAliasing', false);
else
    I = imresize(img, scale, 'AntiAliasing', false);
end
subimage = imresize(img, scale, 'AntiAliasing', false);
% Select subimage
I = I(sy:ey, sx:ex);
subimage = subimage(sy:ey, sx:ex, :);

% Sawing marks removal
I = NorellFilter(I, 6, 64);

% Compute local orientation of tree rings
Y = ridgeorient(I, 2.0, 4.0);
O = Y(:,:,1);
R = Y(:,:,2);

% ACO algorithm
[px, py] = ACO(I, O, R, ...
    'scale', scale, ...
    'quantizer', 2, ... %2
    'thresh', 0.5, ...
    'animation', false, ...
    'uiimage', subimage);

px = px + old_px-ww;
py = py + old_py-ww;
t = toc;
%fprintf('%4.f,%4.f,%4.f,%4.f,%4.4f\n', old_px, old_py, px, py, t);
%pause(1.5);
end