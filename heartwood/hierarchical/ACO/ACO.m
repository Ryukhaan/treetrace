% Decelle R�mi 
% Phd Student at INRAE & LORIA
% The University of Lorraine, France
%
% First version: January 2020

function [px, py, taus] = ACO(image, orientim, reliability, varargin)
%% Parse inputs
p = inputParser;

% Resize
defaultScale        = 0.4;

% Ant pheromone deposit parameters
defaultNAnts        = 4; 
defaultClusterSize  = 3; 
defaultBlocSize     = 8;

% Update
defaultGamma        = 0.07;
defaultQuantizer    = 4; %5
defaultAlpha        = 2.0;
defaultBeta         = 1.0;

% Misc
defaultAnimation    = false;
defaultMaxNumIter   = 50;
defaultThresh       = 2;
defaultIThresh      = 5;
defaultUIImage      = image;

defaultSpeed        = 0.01;

addParameter(p,'scale',defaultScale,@isnumeric);
addParameter(p,'ants',defaultNAnts,@isnumeric);
addParameter(p,'block',defaultBlocSize,@isnumeric);
addParameter(p,'cluster',defaultClusterSize,@isnumeric);

addParameter(p,'alpha',defaultAlpha,@isnumeric);
addParameter(p,'beta',defaultBeta,@isnumeric);
addParameter(p,'gamma',defaultGamma,@isnumeric);
addParameter(p,'quantizer',defaultQuantizer,@isnumeric);

addParameter(p,'iter',defaultMaxNumIter,@isnumeric);
addParameter(p,'thresh',defaultThresh,@isnumeric);
addParameter(p,'ithresh', defaultIThresh, @isnumeric);
addParameter(p,'animation',defaultAnimation,@islogical);
addParameter(p,'uiimage', defaultUIImage, @isnumeric);
addParameter(p,'speed', defaultSpeed, @isnumeric);

parse(p,varargin{:});
scale       = p.Results.scale;

alpha       = p.Results.alpha;
beta        = p.Results.beta;
gamma       = p.Results.gamma;
quantizer   = p.Results.quantizer;

bloc_size   = p.Results.block;
num_blocs   = p.Results.cluster;
num_ants    = p.Results.ants;

animation   = p.Results.animation;
num_max_it  = p.Results.iter;
thresh      = p.Results.thresh;
ithresh     = p.Results.ithresh;
uiimage     = p.Results.uiimage;
speed       = p.Results.speed;


%% Initialize
old_px          = 0;
old_py          = 0;
dis             = zeros(1,num_max_it);

I               = image;
[H, W]          = size(I);
ants_position   = zeros(num_ants*num_ants, 3);
% Half size of an ant neighborhood
delta           = ceil(num_blocs * bloc_size / 2);
% Size of pheromone matrix
Hq              = ceil(H/quantizer);
Wq              = ceil(W/quantizer);
% Space between each ant
hg              = H / num_ants;
wg              = W / num_ants;
% Initialize ants positions like a uniform grid
for i=1:num_ants
    for j=1:num_ants
        k = (i-1) * num_ants + j;
        ants_position(k,1) = ceil((i-1)*hg + ceil(hg/2));
        ants_position(k,2) = ceil((j-1)*wg + ceil(wg/2));
        ants_position(k,3) = 1;
    end
end
% Initialize pheromones matrix (could be randomized or set to a default value)
%pheromones = 30 * ones(Hq, Wq);
%pheromones = randi(20, Hq, Wq);
pheromones = rand(Hq, Wq);

%% tmp
taus = zeros(Hq, Wq, num_max_it);

%% UI
% Refresh figure
if animation
    figure(1), clf;
    figure(2), clf;
    % Resize image if requiscale

    % Intersect image
    figure(1);
    gm      = imagesc(log(abs(pheromones+1))); colormap gray, axis image, hold on;
    title('Pheromones Coordinates');
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    %green   = cat(3, zeros(Hq, Wq), ones(Hq, Wq), zeros(Hq,Wq)); 
    %h = imagesc(green);
    
    % Display ants and remove axis
    figure(2), imagesc(uiimage), colormap gray, axis image, hold on;
    title('Ants and Image Coordinates');
    %subplot 122, imagesc(rgbimage), colormap gray, axis image, hold on;
    set(gca,'xtick',[]);
    set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
    set(gca,'yticklabel',[]);
    color = linspace(0,1,num_ants*num_ants);
    yplot = ants_position(:,1);
    xplot = ants_position(:,2);
    ants = scatter(xplot, yplot, 64, color, 'x', 'LineWidth', 2);
    
    % Draw pith estimation
    M = max(pheromones(:));
    [rows,cols] = find(pheromones>=0.8*M);
    pith_y = mean(rows(:));
    pith_x = mean(cols(:));
    pith = scatter(quantizer * pith_x, quantizer * pith_y, 64, 'ro', 'filled');
    
    % Blocks limits
    pl = [];
    j = 1;
    for ix=0:num_blocs
        %subplot 122;
        pl(j) = line([1,1],[1,1], 'Color', 'r');
        j = j + 1;
    end
    for ix=0:num_blocs
        %subplot 122;
        pl(j) = line([1,1],[1,1], 'Color', 'r');
        j = j + 1;
    end
    
    drawnow;
end

%taus = zeros(Hq, Wq, num_max_it);

%% Get starting !
for it=1:num_max_it
    
    % UI
    if animation
        % Update ants position
        set(gm, 'CData', pheromones);
        set(gca,'xtick',[]);
        set(gca,'xticklabel',[]);
        set(gca,'ytick',[]);
        set(gca,'yticklabel',[]);
        yplot = ants_position(:,1);
        xplot = ants_position(:,2);
        set(ants, 'XData', xplot, 'YData', yplot, 'CData', [0 1 0]);
        %saveas(1, ['../png/aco/tau_' num2str(it) '_' num2str(sdf) '.png']); 
        
        % Update pith potision
        M = max(pheromones(:));
        [rows,cols] = find(pheromones>=0.8*M);
        pith_y = mean(rows(:));
        pith_x = mean(cols(:));
        set(pith, 'XData', quantizer * pith_x, 'YData', quantizer * pith_y);
        %saveas(2, ['../png/aco/ants_' num2str(it) '_' num2str(sdf) '.png']);
        hold off;
        
        drawnow;
        pause(speed);
    end

    % Rho matrix
    rho = zeros(Hq, Wq);
    % Move each ant
    for i=1:size(ants_position,1)   
        ay = ants_position(i,1);
        ax = ants_position(i,2);
        anto = zeros(Hq, Wq);
        antg = zeros(Hq, Wq);
        for iy=0:num_blocs-1
            for ix=0:num_blocs-1
                % Retrieve local orientation of a bloc
%                 mR = reliability( ...
%                     uint16(ay-delta+iy*bloc_size):uint16(ay-delta+(iy+1)*bloc_size-1), ...
%                     uint16(ax-delta+ix*bloc_size):uint16(ax-delta+(ix+1)*bloc_size-1));
                mO = orientim( ...
                    uint16(ay-delta+iy*bloc_size):uint16(ay-delta+(iy+1)*bloc_size-1), ...
                    uint16(ax-delta+ix*bloc_size):uint16(ax-delta+(ix+1)*bloc_size-1));
                %[~, idx] = max(mR(:));
                % Take the median value
                %theta = mO(idx);
                theta = median(mO(:));
                
                % Compute the coordinate of the block centre
                cx = ax - delta + ix * bloc_size + bloc_size/2;
                cy = ay - delta + iy * bloc_size + bloc_size/2;
                cx = ceil(cx/quantizer);
                cy = ceil(cy/quantizer);
                anto(cy,cx) = theta;
                antg(cy,cx) = 1;
            end
        end
        % Draw lines for each block according to their orientation
        A = deposit(antg, anto);
        
        % Accumulate pheromones
        rho = rho + A;
        
        % Compute desirability
        desirability = zeros(Hq, Wq);
        desirability(ceil(ay / quantizer), ceil(ax / quantizer)) = 1;
        desirability = bwdist(desirability);
        desirability = 1 ./ (desirability+1);
        
        % Compute probability to moves to a pixel
        P = (pheromones.^alpha) .* (desirability.^beta);
        %P = pheromones .* desirability;
        P = P ./ sum(P, 'all');
        [px, py] = pinky(linspace(1, Wq, Wq), linspace(1, Hq, Hq), P);
        
        
        % Update ant position
        py = quantizer * py;
        px = quantizer * px;
        py = max(min(py, H - delta), delta+1);
        px = max(min(px, W - delta), delta+1);
        
        ants_position(i,1) = py;
        ants_position(i,2) = px;
        
    end
    taus(:,:,it) = pheromones;
    
    % Update pheromone matrix
    pheromones = (1-gamma) * pheromones + rho;
    
    M           = max(pheromones(:));
    [rows,cols] = find(pheromones>=0.8*M);
    qq = numel(rows);
    py          = mean(rows(:));
    px          = mean(cols(:));
    di          = (old_px-px)^2+(old_py-py)^2;
    old_px      = px;
    old_py      = py;
    
    dis(it)   = di;
    
    
    %if (it > ithresh)
    %    fprintf('%4.1f;', mean(dis(it-ithresh:it)));
    %end
    
    %if (it > ithresh && mean(dis(it-ithresh:it))< thresh)
    %    break;
    %end
end
%fprintf('\n');
% Take the barycenter as the pith
M           = max(pheromones(:));
[rows,cols] = find(pheromones>=0.8*M);
py          = mean(rows(:));
px          = mean(cols(:));
px          = quantizer * px / scale;
py          = quantizer * py / scale;
end