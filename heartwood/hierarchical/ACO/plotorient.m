% PLOTRIDGEORIENT - plot of ridge orientation data
%
% Usage:   plotridgeorient(orient, spacing, im, figno)
%
%        orientim - Ridge orientation image (obtained from RIDGEORIENT)
%        spacing  - Sub-sampling interval to be used in ploting the
%                   orientation data the (Plotting every point is
%                   typically not feasible) 
%        im       - Optional fingerprint image in which to overlay the
%                   orientation plot.
%        figno    - Optional figure number for plot
%
% A spacing of about 20 is recommended for a 500dpi fingerprint image
%
% See also: RIDGEORIENT, RIDGEFREQ, FREQEST, RIDGESEGMENT

% Peter Kovesi  
% School of Computer Science & Software Engineering
% The University of Western Australia
% pk at csse uwa edu au
% http://www.csse.uwa.edu.au/~pk
%
% January 2005

function plotorient(orient, mask, spacing, color, figno)

    if fix(spacing) ~= spacing
        error('spacing must be an integer');
    end
    
    [rows, cols] = size(orient);
    
    lw = 0.5;               % linewidth
    len = lw*0.8*spacing;   % length of orientation lines
    
    % Subsample the orientation data according to the specified spacing
    s_orient = orient(spacing:spacing:rows-spacing, ...
		      spacing:spacing:cols-spacing);

    xoff = len * cos(s_orient);
    yoff = len * sin(s_orient);     
    
%   if nargin >= 3     % Display fingerprint image
% 	if nargin == 4
% 	    show(im, figno); hold on
% 	else
% 	    show(im); hold on
% 	end
%     end
    
    % Determine placement of orientation vectors
    [x,y] = meshgrid(spacing:spacing:cols-spacing, ...
		     spacing:spacing:rows-spacing);
    x = x-xoff;
    y = y-yoff;
    %x = x + spacing * 0.5;
    %y = y + spacing * 0.5;
   
    
    % Orientation vectors
    u = xoff;
    v = yoff;
    
    figure(figno); 
    hold on;
    submask = mask(spacing:spacing:rows-spacing, ...
		     spacing:spacing:cols-spacing);
    idx = find(submask);
    for i=idx
        quiver(x(i),y(i),u(i),v(i),'.', 'linewidth', 2, 'color', color);
    end
    axis equal, axis ij,  hold off
    
    %show(orient, 6);
    %orient = imgaussfilt(orient, 1.0);
end