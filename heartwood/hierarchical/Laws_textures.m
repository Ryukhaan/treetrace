ew = 5;

filters{1}  =  [ +1  +4  6  +4  +1 ];  %(Niveau)   L5
filters{2}  =  [ -1  -2  0  +2  +1 ];  %(Contour)  E5
filters{3}  =  [ -1   0  2   0  -1 ];  %(Point)    S5
filters{4}  =  [ -1  +2  0  -2  +1 ];  %(Vague)    W5
filters{5}  =  [ +1  -4  6  -4  +1 ];  %(Ride)     R5
% Original
textures_filters = [];
u = 1;
for t=1:numel(filters)
    for s=1:t
        if (s == t)
            textures_filters{u} = filters{t}' * filters{s};
        else
            textures_filters{u} = 0.5 * (filters{t}' * filters{s} + filters{s}' * filters{t});
        end
        u = u + 1;
    end
end

textures = zeros(size(gX,1), size(gX,2), numel(textures_filters));
h = single(rgb2gray(gX));
%hsv = rgb2hsv(gX);
%h = im2double(hsv(:,:,2));
%h = (h - min(h(:))) / (max(h(:)) - min(h(:)));
%hsv = rgb2cmyk(gX);
%h = im2double(hsv(:,:,4));
h = (h - min(h(:))) / (max(h(:)) - min(h(:)));

res = zeros(size(gX,1), size(gX,2));
for t=1:numel(textures_filters)
    th = imfilter(h, textures_filters{t});
    textures(:,:,t) = imfilter( ...
        abs(th), ...
        ones(ew, ew)./(ew^2));
    th = textures(:,:,t);
    th = 255 * (th - min(th(:))) / (max(th(:)) - min(th(:)));
    textures(:,:,t) = th;
    %[BW, ~] = canny_code(h-textures(:,:,t), ...
    %    single(uint16(px*alpha)), ...
    %    single(uint16(py*alpha)), ...
    %    0.08, 0.1);
    %res = res + BW;
end
%montage(textures, 'DisplayRange', [0, 1]);
%res = (res - min(res(:))) / (max(res(:)) - min(res(:)));
%imwrite(labeloverlay(X, imresize(imbinarize(res), [size(X,1), size(X,2)], 'nearest')), ...
%    ['/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/rings/', file.name]);
%k = k + 1;
%pause(1.0);