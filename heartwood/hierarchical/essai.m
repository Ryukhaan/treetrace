files = dir('/Volumes/LaCie/LaCie/Documents/TreeTrace/data/Echantillonnage/BBF-campagne-2020/images/logyard/*.jpeg');
truth = dir('/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/truth/full/*.png');

my_var = [];
for k=1:20
    file=files(k);
    gtru=truth(k);

    I = imread([file.folder, '/', file.name]);
    T = imread([gtru.folder, '/', gtru.name]);

    gt_idxs = find(T(:,:,1));
    bbox = regionprops(T(:,:,1), 'BoundingBox');
    xywh = bbox(255).BoundingBox;
    X = I(xywh(2):xywh(2)+xywh(4), xywh(1):xywh(1)+xywh(3), :);
    Y = T(xywh(2):xywh(2)+xywh(4), xywh(1):xywh(1)+xywh(3), :);

    gX = X .* uint8(Y(:,:,1)>1);
    lab = rgb2lab(gX);
    for i=1:3
        x = lab(:,:,i);
        x_min = min(x(:)); x_max = max(x(:));
        x = (x - x_min) / (x_max - x_min);
        x = (x - mean(x(:))) / std(x(:));
        lab(:,:,i) = x;
    end
    [Ll, Nl] = superpixels(lab, 512);

    outputImage = zeros(size(lab), 'like', lab);
    idx = label2idx(Ll);
    numRows = size(lab,1);
    numCols = size(lab,2);
    xx = NaN(Nl, 4);
    for labelVal = 1:Nl
        %redIdx      = idx{labelVal};
        greenIdx    = idx{labelVal}+numRows*numCols;
        %blueIdx     = idx{labelVal}+2*numRows*numCols;

        %outputImage(redIdx)     = median(rgb2(redIdx));
        %outputImage(greenIdx)   = median(rgb2(greenIdx));
        %outputImage(blueIdx)    = median(rgb2(blueIdx));
        if median(gX(greenIdx)) == 0
            continue;
        end
        xx(labelVal, 1)   = median(lab(greenIdx));
        xx(labelVal, 2)   = std(lab(greenIdx));
        xx(labelVal, 3)   = min(lab(greenIdx));
        xx(labelVal, 4)   = max(lab(greenIdx));
        
    end    
    xx(isnan(xx)) = [];
    xx = reshape(xx, size(xx,2)/4, 4);
    
    my_var = cat(1, my_var, xx);
end
