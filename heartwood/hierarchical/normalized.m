function Y = normalized(X)
Y = X;
Y_min = min(X(:)); Y_max = max(X(:));
Y = (Y - Y_min) / (Y_max - Y_min);
end