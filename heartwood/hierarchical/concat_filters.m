files = dir('/Users/remidecelle/Documents/TreeTrace/code/treetrace/heartwood/deeplearning/logs/predict/conv2d_*_filter*.png');

square = 4;
full_filters = zeros(square * 512, square * 512, 3, 'uint8');
k = 0;
for file=files'
    myfilt = imread([file.folder, '/', file.name]);
    %f_max = max(myfilt(:)); f_min = min(myfilt(:));
    %myfilt = (myfilt - f_min) / (f_max - f_min);
    %imagesc(myfilt); axis image, axis off;
    %pause();
    %myfilt = 255 * myfilt;
    i = mod(k, square);
    j = floor(k / square);
    full_filters(j*512+1:(j+1)*512,i*512+1:(i+1)*512, :) = myfilt;
    k = k + 1;
end

imwrite(full_filters, './cond2d_filters.png');
    