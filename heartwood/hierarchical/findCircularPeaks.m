function Y = findCircularPeaks(img, px, py, rayon)

[H,W] = size(img);
Y = zeros(size(img));

thetas = linspace(0, 2*pi, rayon);
rp = sqrt(px.^2 + py.^2);
tp = atan2(py, (px+1e-16));

% d1 = sqrt(py^2+px^2);
% d2 = sqrt((H-py)^2+px^2);
% d3 = sqrt(py^2+(W-px)^2);
% d4 = sqrt((H-py)^2+(W-py)^2);
%rrange = min([d1,d2,d3,d4]);
rrange  = max([H-py,W-px]);

img = normalized(img);
positions = zeros(2, numel(thetas));
old_idx = 0;
old_locs = [0,0];
ii = 1;
figure(1), imagesc(img), axis image, colormap gray;
hold on;
line__ = plot([0,0],[0,0], 'LineWidth', 2, 'Color', 'green');
for theta=thetas
    %t = tan(theta); %tan(theta) + tp;
    r = sqrt(rp.^2 + rrange.^2 + 2*rp*rrange*cos(tp-theta)); 
    t = acos( (rp*cos(tp) + rrange*cos(theta)) / (r+1e-16));
    starter = [px, r*cos(t)];
    ender = [py, r*sin(t)];
    [cx, cy, c_img] = improfile(img, starter, ender);
   
    set(line__, 'XData', cx, 'YData', cy);
    drawnow;
    pause(0.1);
    
    figure(2);
    plot(cumsum(c_img));
    pause(0.1)
    
    [pks, locs] = findpeaks(c_img, 'MinPeakDistance', 10);
    idx = old_idx;
    %% Suivi de contour
    if idx == 0
        [~, idx] = max(pks(:));
        old_idx = idx;
    else
        xx = cx(locs(:));
        yy = cy(locs(:));
        xx = xx - old_locs(1);
        yy = yy - old_locs(2);
        points = cat(2, xx, yy);
        dist = points(:,1).^2 + points(:,2).^2;
        [value, idx] = min(dist);  
        if (value > 100 || value < 20)
            old_idx = idx;
        end
    end
    
    positions(:, ii) = [cx(locs(idx)), cy(locs(idx))];
    old_locs = positions(:,ii);
    ii = ii + 1;
    
end
end