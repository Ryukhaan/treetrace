
% k0 = 2;
% 
% srgb = s{4};
% srgb = imgaussfilt(srgb, 1.0);
% 
% slab = rgb2lab(srgb);
% shsv = rgb2hsv(srgb);
% 
% % HSV
% shue = shsv(:,:,1);
% ssat = shsv(:,:,2);
% sval = shsv(:,:,3);
% shue(sred==0) = NaN;
% ssat(sred==0) = NaN;
% sval(sred==0) = NaN;
% 
% %shue = normalized(shue);
% %ssat = normalized(ssat);
% %sval = normalized(sval);
% 
% % RGB
% sred = srgb(:,:,1);
% sgreen = srgb(:,:,2);
% sblue = srgb(:,:,3);
% sred(sred==0) = NaN;
% sgreen(sgreen==0) = NaN;
% sblue(sblue==0) = NaN;
% sred = normalized(sred);
% sgreen = normalized(sgreen);
% sblue = normalized(sblue);
% 
% % LAB
% sl = slab(:,:,1);
% sa = slab(:,:,2);
% sb = slab(:,:,3);
% sl(sl==0) = NaN;
% sa(sa==0) = NaN;
% sb(sb==0) = NaN;
% %sl = normalized(sl);
% %sa = normalized(sa);
% %sb = normalized(sb);
% 
% varx = [];
% n = 1;
% for i = 1:size(sa,1)
%     for j = 1:size(sa,2)
%         if (isnan(sa(i,j)))
%             continue;
%         end
%         varx(n, 1) = shue(i,j);
%         varx(n, 2) = sval(i,j);
%         varx(n, 3) = sa(i,j);
%         n = n + 1;
%     end
% end
% [idx,C] = kmeans(varx, k0);
% 
% segI = NaN(size(sa));
% n = 1;
% for i = 1:size(sa,1)
%     for j = 1:size(sa,2)
%         if (isnan(sa(i,j)))
%             continue;
%         end
%         segI(i, j) = idx(n);
%         n = n + 1;
%     end
% end
% segI(isnan(segI)) = 0;
% imagesc(segI);
% 
% % outputImage = zeros(size(slab),'like', slab);
% % nlabel = max(segI(:));
% % idxs = label2idx(segI);
% % numRows = size(slab,1);
% % numCols = size(slab,2);
% % for labelVal= 1:nlabel
% %     redIdx = idxs{labelVal};
% %     greenIdx = idxs{labelVal}+numRows*numCols;
% %     blueIdx = idxs{labelVal}+2*numRows*numCols;
% %     outputImage(redIdx) = mean(sred(redIdx), 'omitnan');
% %     outputImage(greenIdx) = mean(sgreen(greenIdx), 'omitnan');
% %     outputImage(blueIdx) = mean(sblue(blueIdx), 'omitnan');    
% % %     outputImage(redIdx) = mean(slab(redIdx), 'omitnan');
% % %     outputImage(greenIdx) = mean(slab(greenIdx), 'omitnan');
% % %     outputImage(blueIdx) = mean(slab(blueIdx), 'omitnan');
% % end
% % srgb = lab2rgb(outputImage);
% % imshow((srgb)), axis image, axis off;

files = dir('/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/unet/woborder1/input/*.png');
groundd = '/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/unet/woborder1/output';
savedir = '/Volumes/LaCie/LaCie/Documents/segmentation_heartwood/data/snake/';

for file=files'
    I   = imread([file.folder, '/', file.name]);
    gt  = imread([groundd, '/', file.name]);
    
    if (strcmp(file.name(1:7), 'sawmill'))
        continue;
    end
    if (size(I,1) ~= size(gt, 1) || size(I,2) ~= size(gt,2))
        continue;
    end
    
%     %% Snake
    s   = imresize(I, 0.125);
    hsv = rgb2hsv(s);
    lab = rgb2lab(s);
    hsv = imbilatfilt(hsv, 1000, 2.0);
    lab = imbilatfilt(lab, 1000, 2.0);
    
    gh   = 0.75*imgradient(normalized(hsv(:,:,1))) ...
        + 0.00*imgradient(normalized(hsv(:,:,2))) ...
        + 0.25*imgradient(normalized(lab(:,:,2)));
    [h, w, c]   = size(hsv);
    [xx, yy]    = meshgrid(1:w, 1:h);
    mask = (xx - w/2).^2 + (yy - h/2).^2 <= (max([w/4, h/4]))^2;
    
    bw = activecontour(gh, mask, 300);
    bw = imresize(bw, [size(I,1), size(I,2)]);
    bw = imfill(bw, 'holes');
%     figure(1), imagesc(labeloverlay(I, bw)), axis image, axis off;
%     figure(2), imagesc(labeloverlay(gh, mask)), axis image, axis off;
%     pause()
%    imwrite(bw, [savedir, file.name]);
    
    %% Threshold
%     alpha = 0.125;
%     %[px, py] = ACOPithEstimate(I);
%     
%     s   = imresize(I, alpha);
%     s   = im2double(s);
%     cmy = rgb2cmyk(s);
%     hsv = rgb2hsv(s);
%     lab = rgb2lab(s);
%     hsv = imbilatfilt(hsv, 1000, 2.0);
%     lab = imbilatfilt(lab, 1000, 2.0);
%     cmy = imbilatfilt(cmy(:,:,1:3), 1000, 2.0);
%     
%     a = 0.7;
%     b = 0.0;
%     c = 0.3;
%     gh   = a*imgradient(normalized(hsv(:,:,1))) ...
%         + b*imgradient(normalized(cmy(:,:,2))) ...
%         + c*imgradient(normalized(lab(:,:,2)));
%     gh = normalized(gh);
%     imwrite(gh, [savedir, file.name]);
    
    %Y = findCircularPeaks(gh, dpx, dpy, 64);
    %Y = CircularLABDuramn(s, gh, dpx, dpy, 64, 'debug', 0.5);
    %% Compute confusion matrix
%     y_true_pos = im2double(gt == 1);
%     y_true_neg = 1. - y_true_pos;
%     y_pred_pos = im2double(bw == 1);
%     y_pred_neg = 1. - y_pred_pos;
% 
%     tp = sum(y_pred_pos .* y_true_pos, 'all');
%     tn = sum(y_pred_neg .* y_true_neg, 'all');
%     fp = sum(y_pred_pos .* y_true_neg, 'all');
%     fn = sum(y_pred_neg .* y_true_pos, 'all');
%     
%     X = sprintf('%s, %i, %i, %i, %i', file.name, tp, tn, fp, fn);
%     disp(X);
end

