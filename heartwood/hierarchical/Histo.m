function H = Histo(I)
%
%    

[N,M]= size(I);
H=zeros(256,1);
for i = 1:N
   for j = 1:M
      if (isnan(I(i,j)))
          continue
      end
      NgR=double(I(i,j));
      H(NgR+1)=H(NgR+1)+1; % +1 car les tableaux commencent � 1 ss Matlab
   end
end
figure;
bar(H,'b');


