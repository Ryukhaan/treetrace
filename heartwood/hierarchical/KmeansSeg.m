imagdir = '/Volumes/LaCie/LaCie/Documents/TreeTrace/data/Segmentation/full/lumix/';
subset  = 'f1/';

files = dir([imagdir, subset, 'input/*.jpg']);
maskdir = [imagdir, subset, 'output/'];
for file=files'
    basename = split(file.name, '.');
    ext = basename{2};
    image = imread([file.folder, '/', file.name]);
    image = imresize(image, 0.25);
    for i = 1:3
        image(:,:,i) = medfilt2(image(:,:,i), [5,5]);
    end
    mask  = imread([maskdir, basename{1}, '.tif']);
    %lab = rgb2lab(image);
    hsv = im2single(rgb2hsv(image));
    [ll, nlabel] = superpixels(hsv, 512);
    
    outputImage = zeros(size(image),'like', image);
    idxs = label2idx(ll);
    numRows = size(image,1);
    numCols = size(image,2);
    for labelVal= 1:nlabel
        redIdx = idxs{labelVal};
        greenIdx = idxs{labelVal}+numRows*numCols;
        blueIdx = idxs{labelVal}+2*numRows*numCols;
        outputImage(redIdx) = mean(image(redIdx), 'omitnan');
        outputImage(greenIdx) = mean(image(greenIdx), 'omitnan');
        outputImage(blueIdx) = mean(image(blueIdx), 'omitnan');
    end
    imagesc(outputImage);
end
