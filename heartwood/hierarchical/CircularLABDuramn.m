function [P, Y] = CircularLABDuramn(img, edges, px, py, rayon, debug, timer)
%% Initialize
[H,W] = size(img);
Y = zeros(size(img));

thetas = linspace(0, 2*pi, rayon);
thetas = circshift(thetas, randi(numel(thetas)));

delta_r = 70;
rp = sqrt(px.^2 + py.^2);
tp = atan2(py, (px+1e-16));
r0 = max([H-py,W-px])-10;

r2 = sqrt(rp.^2 + r0.^2 + 2*rp*r0*cos(tp-thetas(1)));
t2 = acos( (rp*cos(tp) + r0*cos(thetas(1))) / (r2+1e-16));
r1 = sqrt(rp.^2 + 100.^2 + 2*rp*100*cos(tp-thetas(1)));
t1 = acos( (rp*cos(tp) + 100*cos(thetas(1))) / (r1+1e-16));

%% UI
if exist('debug','var')
    figure(1), clf(1);
    tiledlayout(2,2);
    
    nexttile(1);
    imagesc(img); 
    axis image, axis off;
    hold on;
    line__  = plot([0,0],[0,0], 'LineWidth', 2, 'Color', 'green');
    
    nexttile(2);
    imagesc(edges);
    axis image, axis off, colormap gray;
    hold on;
    line2__ = plot([0,0],[0,0], 'LineWidth', 2, 'Color', 'green');
    
    nexttile(3);
    hold on;
    old__  = plot([0,0], 'LineWidth', 2, 'Color', 'green');
    new__  = plot([0,0], 'LineWidth', 2, 'Color', 'black');
    cernes = stem([0,0], ':+r');
    hold off;
end

old_locs = [0,0];
old_idx = 0;
positions = [];

ii = 1;
for theta=thetas(2:end)
    %starter = [px, r*cos(t)];
    %ender = [py, r*sin(t)];
    starter = [r1*cos(t1), r2*cos(t2)];
    ender   = [r1*sin(t1), r2*sin(t2)];
    
    [cx, cy, c_img] = improfile(img, starter, ender);
    [~, ~, c_edg]   = improfile(edges, starter, ender);
    
    old_c_img = c_img;   
    [pks,locs,w,p] = findpeaks(c_edg, 'Annotate', 'extents');
    idx = old_idx;
    %% Suivi de contour
    if idx == 0
        [~, idx] = max(pks(:));
        old_idx = idx;
    else
        xx = cx(locs(:));
        yy = cy(locs(:));
        xx = xx - old_locs(1);
        yy = yy - old_locs(2);
        points = cat(2, xx, yy);
        dist = points(:,1).^2 + points(:,2).^2;
        [value, idx] = min(dist);  
        if (pks(idx) > 0.1)
            old_idx = idx;
        end
    end
    
    positions(:, ii) = [cx(locs(idx)), cy(locs(idx))];
    old_locs = positions(:,ii);
    
    % Get next line 
    r0 = sqrt( (positions(1,ii)-px).^2 + (positions(2,ii)-py).^2);
    r2 = sqrt(rp.^2 + (r0+delta_r).^2 + 2*rp*(r0+delta_r)*cos(tp-theta));
    t2 = acos( (rp*cos(tp) + (r0+delta_r)*cos(theta)) / (r2+1e-16));
    r1 = sqrt(rp.^2 + (r0-delta_r).^2 + 2*rp*(r0-delta_r)*cos(tp-theta));
    t1 = acos( (rp*cos(tp) + (r0-delta_r)*cos(theta)) / (r1+1e-16));

    ii = ii + 1;
    %% UI
    if exist('debug','var')
        nexttile(4);
        findpeaks(c_edg);
        nexttile(3);
        tmpx = [1:numel(old_c_img)];
        set(old__, 'YData', old_c_img, 'XData', tmpx);
        set(new__, 'YData', c_img, 'XData', tmpx);
        set(cernes, 'YData', c_edg, 'XData', tmpx);
        
        nexttile(1);
        set(line__, 'XData', cx, 'YData', cy);
        plot(positions(1,:), positions(2,:), 'LineWidth', 2, 'Color', 'yellow');
        
        nexttile(2);
        set(line2__, 'XData', cx, 'YData', cy);
        scatter(old_locs(1), old_locs(2), 64, 'ys', 'filled');
        drawnow;
        if exist('timer', 'var')
            pause(timer);
        else
            pause();
        end
    end
end



figure(1), clf(1);
imagesc(img), axis image, colormap gray;
hold on;
plot(positions(1,:), positions(2,:), 'LineWidth', 2, 'Color', 'red');
hold off;
% hold on;
% plot(p2(1,:), p2(2,:), 'LineWidth', 2, 'Color', 'yellow');
% hold off;
P = positions;
% res = zeros(size(edges));
% L = bwlabel(edges);
% [row, col] = find(edges);
% for i=1:numel(thetas)
%     dist = (row - positions(2,i)).^2 + (col - positions(1,i)).^2;
%     [~, idx] = min(dist);
%     res(L==L(row(idx), col(idx))) = res(L==L(row(idx), col(idx))) + 1;
% end
Y = img;
end