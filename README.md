# TreeTrace

TreeTrace repository for algorithms which are developped for the project.

Algorithms     |  Matlab | Python | C++ | 
---------------|---------|--------|-----|
Pith Detection | **Available**      | x | **Available**   | 
Heartwood      | x    | x | x | 
Tree Rings     | x    | x | x | 

# Matlab

## Ant Colony Optimization for Pith Estimation 

Ant colony optimization can be found in the folder *pith* then *matlab*.
Code source has been written under Matlab 2019b.

It has been tested under:

*  MacOS Mojave 10.14.5 with Matlab 2019b
*  Ubuntu 18.04 with Matlab 2019a

### Get started
##### 1.   Play with Application

Launch the file *ACOapp.mlapp* if you want to try on predefined. 
You can play with the 4 predefined images or import your own images.

First parameter to check is the scale as predefined images 
(or your images) are not of the same size.

##### 2.  Get results for an imageset

If you want to get pith estimation over an imageset, see file *antfiles.m*.

In *antfiles.m* you have to write the path to the imageset.
Check others files to change parameters. See *ACO.m* for default parameters values.

# C++

## Ant Colony Optimization for Pith Estimation 

Ant colony optimization can be found in the folder *pith* then *c++*.

### Get started

##### 1.    Dependencies

Required: 

-  `Opencv 4.+`

##### 2.    Compile Sources

``` 
cd pith/c++/;
mkdir build
cd build
cmake ..
make
```

##### 3.    Execute

```
./AntColonyPith --input="path_to_image" --parameters="path_to_parameters.json"
```

