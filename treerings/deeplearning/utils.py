import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as KL


def crop_and_resize(images, boxes, box_inds, crop_size):
    boxes = tf.stop_gradient(boxes)

    def transform_fpcoor_for_tf(boxes, image_shape, crop_shape):
        x0, y0, x1, y1 = tf.split(boxes, 4, axis=1)

        spacing_w = (x1 - x0) / tf.cast(crop_shape[1], dtype=tf.float32)
        spacing_h = (y1 - y0) / tf.cast(crop_shape[0], dtype=tf.float32)

        nx0 = (x0 + spacing_w / 2 - 0.5) / tf.cast(image_shape[1] - 1, dtype=tf.float32)
        ny0 = (y0 + spacing_h / 2 - 0.5) / tf.cast(image_shape[0] - 1, dtype=tf.float32)

        nw = spacing_w * tf.cast(crop_shape[1] - 1, dtype=tf.float32) / tf.cast(image_shape[1] - 1,
                                                                                dtype=tf.float32)
        nh = spacing_h * tf.cast(crop_shape[0] - 1, dtype=tf.float32) / tf.cast(image_shape[0] - 1,
                                                                                dtype=tf.float32)

        return tf.concat([ny0, nx0, ny0 + nh, nx0 + nw], axis=1)

    image_shape = tf.shape(images)[1:]
    boxes = transform_fpcoor_for_tf(boxes, image_shape, crop_size)
    ret = tf.image.crop_and_resize(images, boxes, box_inds, crop_size=crop_size)
    return ret


def roi_align(images, boxes, pool_shape):
    # boxes = tf.concat(boxes, axis=1)
    # TODO
    # box_inds = np.arange(self.cfg.BATCH_SIZE)
    # box_inds = np.repeat(box_inds, self.cfg.MAX_GT_INSTANCES)
    # box_inds = tf.convert_to_tensor(box_inds, dtype=tf.int32)
    num_images = images.shape[0]
    batch_size = boxes.shape[0]
    boxes = tf.reshape(boxes, [-1, 4])
    num_box = boxes.shape[0]

    if num_images != batch_size:
        num_box = num_box // num_images
        batch_size = num_images
    else:
        num_box = num_box // batch_size
    
    box_inds = tf.repeat(tf.range(0, batch_size, delta=1, dtype=tf.int32, name='range'), num_box)
    
    ret = crop_and_resize(images, boxes, box_inds, pool_shape)
    return ret


class ROIAlign(KL.Layer):
    def __init__(self, cfg, **kwargs):
        super(ROIAlign, self).__init__(**kwargs)
        self.cfg = cfg
        self.pool_shape = (cfg.ROI_RESOLUTION, cfg.ROI_RESOLUTION)

    def crop_and_resize(self, image, boxes, box_inds, crop_size):
        """
        Aligned version of tf.image.crop_and_resize, following our definition of floating point boxes.
        Args:
            image: NCHW
            boxes: nx4, x1y1x2y2
            box_inds: (n,)
            crop_size (tuple):
        Returns:
            n,C,size,size
        """
        boxes = tf.stop_gradient(boxes)

        def transform_fpcoor_for_tf(boxes, image_shape, crop_shape):
            x0, y0, x1, y1 = tf.split(boxes, 4, axis=1)

            spacing_w = (x1 - x0) / tf.cast(crop_shape[1], dtype=tf.float32)
            spacing_h = (y1 - y0) / tf.cast(crop_shape[0], dtype=tf.float32)

            nx0 = (x0 + spacing_w / 2 - 0.5) / tf.cast(image_shape[1] - 1, dtype=tf.float32)
            ny0 = (y0 + spacing_h / 2 - 0.5) / tf.cast(image_shape[0] - 1, dtype=tf.float32)

            nw = spacing_w * tf.cast(crop_shape[1] - 1, dtype=tf.float32) / tf.cast(image_shape[1] - 1,
                                                                                    dtype=tf.float32)
            nh = spacing_h * tf.cast(crop_shape[0] - 1, dtype=tf.float32) / tf.cast(image_shape[0] - 1,
                                                                                    dtype=tf.float32)

            return tf.concat([ny0, nx0, ny0 + nh, nx0 + nw], axis=1)

        image_shape = tf.shape(image)[1:]
        boxes = transform_fpcoor_for_tf(boxes, image_shape, crop_size)
        ret = tf.image.crop_and_resize(image, boxes, box_inds, crop_size=crop_size)
        return ret

    def call(self, inputs):
        images = inputs[0]
        boxes = inputs[1]
        boxes = tf.concat(boxes, axis=1)
        boxes = tf.reshape(boxes, [-1, 4])

        # box_inds = np.arange(self.cfg.BATCH_SIZE)
        # box_inds = np.repeat(box_inds, self.cfg.MAX_GT_INSTANCES)
        # box_inds = tf.convert_to_tensor(box_inds, dtype=tf.int32)
        box_inds = tf.zeros([tf.shape(boxes)[0]], dtype=tf.int32)

        ret = self.crop_and_resize(images, boxes, box_inds, self.pool_shape)
        # ret = tf.nn.avg_pool(ret, [1, 1, 2, 2], [1, 1, 2, 2], padding='SAME', data_format='NHWC')
        return ret

    def compute_output_shape(self, input_shape):
        return (input_shape[1][0] * input_shape[1][1]) + self.pool_shape + (4)


def conv_block(input_tensor, filters=128, kernel_size=3, strides=1, padding="same", use_bias=True, **kwargs):
    x = KL.Conv2D(filters=filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  padding=padding,
                  use_bias=use_bias,
                  **kwargs)(input_tensor)
    x = KL.BatchNormalization()(x)  # TODO: use group norm
    x = KL.Activation('relu')(x)
    return x


# This function is from https://github.com/matterport/Mask_RCNN/blob/master/mrcnn/utils.py
def extract_bboxes(mask):
    """Compute bounding boxes from masks.
    mask: [num_instances, height, width]. Mask pixels are either 1 or 0.

    Returns: bbox array [num_instances, (y1, x1, y2, x2)].
    """
    boxes = np.zeros([mask.shape[0], 4], dtype=np.float32)
    for i in range(mask.shape[0]):
        m = mask[i, :, :]
        # Bounding box.
        horizontal_indicies = np.where(np.any(m, axis=0))[0]
        vertical_indicies = np.where(np.any(m, axis=1))[0]
        if horizontal_indicies.shape[0]:
            x1, x2 = horizontal_indicies[[0, -1]]
            y1, y2 = vertical_indicies[[0, -1]]
            # x2 and y2 should not be part of the box. Increment by 1.
            x2 += 1
            y2 += 1
        else:
            # No mask for this instance. Might happen due to
            # resizing or cropping. Set bbox to zeros
            x1, x2, y1, y2 = 0, 0, 0, 0
        boxes[i] = np.array([y1, x1, y2, x2])
    return boxes.astype(np.float32)


def mold_image(images, config):
    """Expects an RGB image (or array of images) and subtracts
    the mean pixel and converts it to float. Expects image
    colors in RGB order.
    """
    return images.astype(np.float32) - config.DATA.MEAN_PIXEL


def get_backbone(backbone, **kwargs):
    """return backbone as keras model

    Arguments:
        backbone: str or keras model
    """
    if isinstance(backbone, str):
        assert backbone in ['ResNet50V2', 'ResNet101V2', 'ResNet152V2', 'ResNet50', 'ResNet101', 'ResNet152']
        backbone = getattr(tf.keras.applications, backbone)(**kwargs)

    return backbone


import os
import shutil
import time
import importlib.util
from datetime import datetime
from math import log10

import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Logger(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush() # If you want the output to be visible immediately
    def flush(self) :
        for f in self.files:
            f.flush()


def import_module(name, path):
    spec = importlib.util.spec_from_file_location(name, path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def get_num_params(model):
    num_params = 0
    for param in model.parameters():
        num_params += param.nelement()
    return num_params


def get_expired_time(start_time):
    curr_time = time.perf_counter()
    delta = curr_time - start_time
    hour = int(delta / 3600)
    delta -= hour * 3600
    minute = int(delta / 60)
    delta -= minute * 60
    seconds = delta
    return '%02d' % hour + ':%02d' % minute + ':%02d' % seconds


def get_eta_time(start_time, curr_iter, num_iters):
    curr_time = time.perf_counter()
    delta = curr_time - start_time
    eta = ((num_iters - curr_iter) / curr_iter) * delta

    hour = int(eta / 3600)
    eta -= hour * 3600
    minute = int(eta / 60)
    eta -= minute * 60
    seconds = eta
    return '%02d' % hour + ':%02d' % minute + ':%02d' % seconds


def get_time_string():
    time = datetime.now()
    name = str(time.year) + f"_{time.month:02d}" + f"_{time.day:02d}" \
        + f"_{time.hour:02d}" + f"{time.minute:02d}" + f"{time.second:02d}"
    return name


def get_time():
    time = datetime.now()
    return '%02d' % time.hour + ':%02d' % time.minute + ':%02d' % time.second


def freeze_batch_norm(model):
    for module in model.modules():
        if isinstance(module, nn.BatchNorm2d):
            module.eval()
            # finetuning is unstable if we don't also freeze params
            module.requires_grad = False
            for param in module.parameters():
                param.requires_grad = False


def unfreeze_batch_norm(model):
    for module in model.modules():
        if isinstance(module, nn.BatchNorm2d):
            module.train()


def set_train_mode(model, freeze_bachnorm):
    model.train()

    if not freeze_bachnorm:
        unfreeze_batch_norm(model)
    else:
        freeze_batch_norm(model)


def set_learning_rate_linear(optimizer, step, num_steps, lr_min, lr_max):
    loglr = (log10(lr_max) - log10(lr_min)) * (1 - step / num_steps) + log10(lr_min)
    lr = 10**loglr
    for param_group in optimizer.param_groups:
        if 'lr_factor' in param_group:
            param_group['lr'] = lr / param_group['lr_factor']
        else:
            param_group['lr'] = lr
    return lr


# from bokeh.io import output_file, show, save
# from bokeh.layouts import column
# from bokeh.plotting import figure
# def lr_find(model, data_loader, optimizer, lr_min=1e-6, lr_max=1, num_iters=256):
def lr_find(model, data_loader, optimizer, lr_min=1e-6, lr_max=1, num_iters=512):
    step = 0
    done = False
    final_step = num_iters - 1
    lr_data = []
    loss_data = []
    while not done:
        for batch in data_loader:
            if step == num_iters:
                done = True
                break
            optimizer.zero_grad()
            model.send_to_gpu(batch)
            loss = model.forward_loss(batch)
            loss.backward()
            optimizer.step()
            lr = set_learning_rate_linear(optimizer, final_step-step, final_step, lr_min, lr_max)
            lr_data.append(lr)
            loss_data.append(loss.item())
            print(step, lr, '->', loss.item())
            step += 1
    # plot data
    # fig = figure(plot_width=1024, plot_height=512, title='loss/lr')
    fig = figure(plot_width=1024, plot_height=512, x_axis_type='log', title='loss/lr')
    fig.line(x=lr_data, y=loss_data, line_width=4, color='navy')
    output_file('/tmp/plot.html')
    save(fig)


def release_memory(model):
    model.eval()
    with torch.no_grad():
        _ = model(torch.zeros(1,3,32,32).cuda())
    model.train()
    # torch.cuda.empty_cache()
    # gc.collect()


def print_memory_stats(msg=''):
    alloc_size = torch.cuda.memory_allocated() / 1024**2
    max_alloc_size = torch.cuda.max_memory_allocated() / 1024**2
    cache_size = torch.cuda.memory_cached() / 1024**2
    max_cache_size = torch.cuda.max_memory_cached() / 1024**2
    print(msg)
    print('ALLOCATED =', alloc_size, max_alloc_size)
    print('CACHED =', cache_size, max_cache_size)
    print()


def mkdir(path, clean=False):
    if clean and os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path, exist_ok=True)


def rmfile(path):
    if os.path.exists(path):
        os.remove(path)


def map_ids_to_classes(class_ids, class_names):
    names = []
    for cid in class_ids:
        if cid < len(class_names):
            names.append(class_names[cid])
    return 