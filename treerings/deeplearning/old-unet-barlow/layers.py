import tensorflow as tf
#import tensorflow_addons as tfa

l2 			= tf.keras.regularizers.L2(0.01)
weight_init = tf.initializers.glorot_uniform()

class Erosion2D(tf.keras.layers.Layer):
	'''
	Erosion 2D Layer
	for now assuming channel last
	'''

	def __init__(self, num_filters, kernel_size, strides=(1, 1),
	             padding='SAME', kernel_initializer='glorot_uniform',
	             kernel_constraint=None,
	             **kwargs):
		super(Erosion2D, self).__init__(**kwargs)
		self.num_filters = num_filters
		self.kernel_size = kernel_size
		self.strides = strides
		self.padding = padding

		self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)
		self.kernel_constraint = tf.keras.constraints.get(kernel_constraint)
		# for we are assuming channel last
		self.channel_axis = -1

		# self.output_dim = output_dim

	def build(self, input_shape):
		if input_shape[self.channel_axis] is None:
			raise ValueError('The channel dimension of the inputs '
		                 'should be defined. Found `None`.')

		input_dim = input_shape[self.channel_axis]
		kernel_shape = self.kernel_size + (input_dim, self.num_filters)

		self.kernel = self.add_weight(shape=kernel_shape,
		                          initializer=self.kernel_initializer,
		                          name='kernel',
		                          constraint=self.kernel_constraint)

		# Be sure to call this at the end
		super(Erosion2D, self).build(input_shape)

	def call(self, x):
		#outputs = K.placeholder()
		for i in range(self.num_filters):
			# erosion2d returns image of same size as x
			# so taking min over channel_axis
			out = tf.math.reduce_min(
			    self._erosion2d(x, self.kernel[..., i],
			                     self.strides, self.padding),
			    axis=self.channel_axis, keepdims=True)

			if i == 0:
				outputs = out
			else:
				outputs = tf.keras.layers.Concatenate()([outputs, out])

		return outputs

	def compute_output_shape(self, input_shape):
		# if self.data_format == 'channels_last':
		space = input_shape[1:-1]
		new_space = []
		for i in range(len(space)):
			new_dim = conv_utils.conv_output_length(
			    space[i],
			    self.kernel_size[i],
			    padding=self.padding,
			    stride=self.strides[i],
			    dilation=1)  # self.erosion_rate[i])
			new_space.append(new_dim)

		return (input_shape[0],) + tuple(new_space) + (self.num_filters,)

	def _erosion2d(self, x, st_element, strides, padding,
		            rates=(1, 1, 1, 1)):
		# tf.nn.erosion2d(input, filter, strides, rates, padding, name=None)
		x = tf.nn.erosion2d(x, st_element, (1, ) + strides + (1, ), padding, "NHWC",
		                    rates)
		return x

	def get_config(self):
		config = super().get_config()
		config.update({
			'filters':
			    self.num_filters,
			'kernel_size':
			    self.kernel_size,
			'strides':
			    self.strides,
			'padding':
			    self.padding
			})
		return config

class Dilation2D(tf.keras.layers.Layer):
	'''
	Dilation 2D Layer
	for now assuming channel last
	'''

	def __init__(self, num_filters, kernel_size, strides=(1, 1),
		         padding='SAME', kernel_initializer='glorot_uniform',
		         kernel_constraint=None,
		         **kwargs):
		super(Dilation2D, self).__init__(**kwargs)
		self.num_filters = num_filters
		self.kernel_size = kernel_size
		self.strides = strides
		self.padding = padding

		self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)
		self.kernel_constraint = tf.keras.constraints.get(kernel_constraint)

		# for we are assuming channel last
		self.channel_axis = -1

		# self.output_dim = output_dim

	def build(self, input_shape):
		if input_shape[self.channel_axis] is None:
			raise ValueError('The channel dimension of the inputs '
				'should be defined. Found `None`.')

		input_dim = input_shape[self.channel_axis]
		kernel_shape = self.kernel_size + (input_dim, self.num_filters)

		self.kernel = self.add_weight(shape=kernel_shape,
		                              initializer=self.kernel_initializer,
		                              name='kernel',
		                              constraint=self.kernel_constraint)

		# Be sure to call this at the end
		super(Dilation2D, self).build(input_shape)

	def call(self, x):
		#outputs = K.placeholder()
		for i in range(self.num_filters):
			# dilation2d returns image of same size as x
			# so taking max over channel_axis
			out = tf.math.reduce_max(self._dilation2d(x, self.kernel[..., i],
						self.strides, self.padding),
					axis=self.channel_axis, keepdims=True)

			if i == 0:
				outputs = out
			else:
				outputs = tf.keras.layers.Concatenate()([outputs, out])

		return outputs

	def compute_output_shape(self, input_shape):
		# if self.data_format == 'channels_last':
		space = input_shape[1:-1]
		new_space = []
		for i in range(len(space)):
			new_dim = conv_utils.conv_output_length(
				space[i],
				self.kernel_size[i],
				padding=self.padding,
				stride=self.strides[i],
				dilation=1)  # self.dilation_rate[i])
			new_space.append(new_dim)

		return (input_shape[0],) + tuple(new_space) + (self.num_filters,)

	def _dilation2d(self, x, st_element, strides, padding, rates=(1, 1, 1, 1)):
		# tf.nn.dilation2d(input, filter, strides, rates, padding, name=None)
		x = tf.nn.dilation2d(x, st_element, (1, ) + strides + (1, ), padding, "NHWC",
							rates)
		return x

	def get_config(self):
		config = super().get_config()
		config.update({
			'filters':
			    self.num_filters,
			'kernel_size':
			    self.kernel_size,
			'strides':
			    self.strides,
			'padding':
			    self.padding
			})
		return config

class ShakeoutConv2D(tf.keras.layers.Conv2D):

    def __init__(self, tau=0.1, c=0.1, **kwargs):
        super(ShakeoutConv2D, self).__init__(**kwargs)
        assert 0 < tau < 1
        assert 0 < c
        self.tau    = tf.constant([tau])
        self.itau   = tf.constant([1 / (1 - tau)])
        self.c      = c

    def generate_bernoulli_matrix_imatrix(self, shape):
        r_matrix    = tf.random.uniform(shape=shape, maxval=1)
        b           = tf.math.greater(self.tau, r_matrix)
        ib          = tf.math.greater(r_matrix, self.tau)
        f           = tf.cast(b, dtype=tf.float32)
        fi          = tf.cast(ib, dtype=tf.float32)
        return f, fi

    def call(self, inputs, **kwargs):
        input_dim       = inputs.get_shape().as_list()[-1]
        kernel_shape    = self.kernel_size + (input_dim, self.filters)
        mask, imask     = self.generate_bernoulli_matrix_imatrix(kernel_shape)
        mask_sign       = tf.keras.backend.softsign(self.kernel * mask)
        imask_sign      = tf.keras.backend.softsign(self.kernel * imask)
        weight          = self.c * imask_sign + \
                            self.itau * (self.kernel + self.c * self.tau * mask_sign)
        outputs = tf.keras.backend.conv2d(
            inputs,
            weight,
            strides=self.strides,
            padding=self.padding)
        if self.use_bias:
            outputs = tf.keras.backend.bias_add(outputs, self.bias)
        if self.activation is not None:
            return self.activation(outputs)
        return outputs

class ShakeoutDepthConv2D(tf.keras.layers.SeparableConv2D):

    def __init__(self, tau=0.1, c=0.1, **kwargs):
        super(ShakeoutDepthConv2D, self).__init__(**kwargs)
        assert 0 < tau < 1
        assert 0 < c
        self.tau    = tf.constant([tau])
        self.itau   = tf.constant([1 / (1 - tau)])
        self.c      = c

    def generate_bernoulli_matrix_imatrix(self, shape):
        r_matrix    = tf.random.uniform(shape=shape, maxval=1)
        b           = tf.math.greater(self.tau, r_matrix)
        ib          = tf.math.greater(r_matrix, self.tau)
        f           = tf.cast(b, dtype=tf.float32)
        fi          = tf.cast(ib, dtype=tf.float32)
        return f, fi

    def call(self, inputs, **kwargs):
        input_dim       = inputs.get_shape().as_list()[-1]

        # Depthwise weights
        kernel_shape    = self.kernel_size + (input_dim, self.depth_multiplier)
        mask, imask     = self.generate_bernoulli_matrix_imatrix(kernel_shape)
        mask_sign       = tf.keras.backend.softsign(self.depthwise_kernel * mask)
        imask_sign      = tf.keras.backend.softsign(self.depthwise_kernel * imask)
        depth_weight    = self.c * imask_sign + \
                            self.itau * (self.depthwise_kernel + self.c * self.tau * mask_sign)

        # Pointwise weights
        kernel_shape    = (1,1) + (input_dim * self.depth_multiplier, self.filters)
        mask, imask     = self.generate_bernoulli_matrix_imatrix(kernel_shape)
        mask_sign       = tf.keras.backend.softsign(self.pointwise_kernel * mask)
        imask_sign      = tf.keras.backend.softsign(self.pointwise_kernel * imask)
        point_weight    = self.c * imask_sign + \
                            self.itau * (self.pointwise_kernel + self.c * self.tau * mask_sign)
        
        # Conv2d
        outputs = tf.keras.backend.separable_conv2d(
            inputs,
            depth_weight,
            point_weight,
            strides=self.strides,
            padding=self.padding)
        if self.use_bias:
            outputs = tf.keras.backend.bias_add(outputs, self.bias)
        if self.activation is not None:
            return self.activation(outputs)
        return outputs

def depth_conv2d_block(inputs, 
				num_filters, 
				kernel_size=3,
				batchnorm=True,
				recurrent=1):

	conv = ShakeoutDepthConv2D(filters = num_filters, 
				kernel_size = kernel_size, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(inputs)
	if batchnorm:
		conv = tf.keras.layers.BatchNormalization()(conv)
	output = tf.keras.layers.LeakyReLU()(conv)

	for _ in range(recurrent - 1):
		conv = ShakeoutDepthConv2D(filters = num_filters, 
				kernel_size = kernel_size, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(output)
		if batchnorm:
			conv = tf.keras.layers.BatchNormalization()(conv)
		output = tf.keras.layers.LeakyReLU()(conv)

	return output

def conv2d_block(inputs, 
				num_filters, 
				kernel_size=3,
				batchnorm=True,
				recurrent=1):

	conv = ShakeoutConv2D(filters = num_filters, 
				kernel_size = kernel_size, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(inputs)
	if batchnorm:
		conv = tf.keras.layers.BatchNormalization()(conv)
	output = tf.keras.layers.LeakyReLU()(conv)

	for _ in range(recurrent - 1):
		conv = ShakeoutConv2D(filters = num_filters, 
				kernel_size = kernel_size, 
				padding = 'same', 
				kernel_initializer = weight_init,
				kernel_regularizer = l2)(output)
		if batchnorm:
			conv = tf.keras.layers.BatchNormalization()(conv)
		output = tf.keras.layers.LeakyReLU()(conv)

	return output

def attention_gating_block(G, X, out_features):
	shape_g = tf.keras.backend.int_shape(G)
	shape_x = tf.keras.backend.int_shape(X)

	# First convolutions
	W_g = tf.keras.layers.Conv2D(filters=out_features, 
		kernel_size=1,
		strides=1,
		padding='same',
		kernel_initializer = weight_init,
		kernel_regularizer = l2,
		use_bias=True)(G)

	W_x = tf.keras.layers.Conv2D(filters=out_features, 
			kernel_size=(2,2),
			strides=2,
			padding='valid',
			kernel_initializer = weight_init,
			kernel_regularizer = l2,
			use_bias=False)(X)

	# Addition pixelwise and relu activation
	relu = tf.keras.layers.ReLU()(tf.keras.layers.Add()([W_g,W_x]))

	# Follow by one convolution 1x1x1 with sigmoid activation
	psi  = tf.keras.layers.Conv2D(filters=1, 
			kernel_size=(1,1),
			padding='same',
			kernel_initializer = weight_init,
			kernel_regularizer = l2,
			activation='sigmoid')(relu)
	#psi = tf.keras.layers.Activation('sigmoid')(psi)

	# Trilinear interpolation
	#shape_psi = tf.keras.backend.int_shape(psi)
	#up_shape  = (shape_x[1] // shape_psi[1], 
	#			 shape_x[2] // shape_psi[2])
	trilinear = tf.keras.layers.UpSampling2D(interpolation='bilinear')(psi)

	# Expand to get same shape as input X
	#trilinear = tf.keras.backend.repeat_elements(trilinear, shape_x[3], axis=3)
	alpha = tf.keras.layers.Multiply()([trilinear, X])

	# Final 1x1x1 convolution to consolidate attention signal to original X dimensions
	output = tf.keras.layers.Conv2D(filters=shape_x[3], 
		kernel_size=1, 
		strides=1, 
		kernel_initializer = weight_init,
		kernel_regularizer = l2,
		padding='same')(alpha)
	output = tf.keras.layers.BatchNormalization()(output)

	return output

def channel_attention_block(inputs, ratio=8):
	channel = inputs.get_shape()[-1]

	shared_layer_one = tf.keras.layers.Dense(channel//ratio,
						 activation='relu',
						 kernel_initializer=weight_init,
						 kernel_regularizer=l2,
						 use_bias=True,
						 bias_initializer='zeros')
	shared_layer_two = tf.keras.layers.Dense(channel,
						 kernel_initializer=weight_init,
						 kernel_regularizer=l2,
						 use_bias=True,
						 bias_initializer='zeros')

	avg_pool = tf.keras.layers.GlobalAveragePooling2D(keepdims=True)(inputs)
	avg_pool = shared_layer_one(avg_pool)
	avg_pool = shared_layer_two(avg_pool)

	max_pool = tf.keras.layers.GlobalMaxPool2D(keepdims=True)(inputs)   
	max_pool = shared_layer_one(max_pool)
	max_pool = shared_layer_two(max_pool)

	add_layer = tf.keras.layers.Add()([avg_pool, max_pool])
	scale = tf.keras.layers.Activation('sigmoid')(add_layer)

	return tf.keras.layers.Multiply()([inputs,scale])

def spatial_attention_block(inputs, kernel_size = 7):
	avg_pool = tf.math.reduce_mean(inputs, axis=[3], keepdims=True)
	max_pool = tf.math.reduce_max(inputs, axis=[3], keepdims=True)
	concat = tf.keras.layers.Concatenate(axis=3)([avg_pool,max_pool])

	concat = tf.keras.layers.Conv2D(
		filters=1, kernel_size=kernel_size,
		padding="same",
		activation='sigmoid',
		kernel_initializer = weight_init,
		use_bias = False,
		kernel_regularizer = l2)(concat)

	return tf.keras.layers.Multiply()([inputs,concat])

def cbam_attention_block(inputs, ratio=1):
	attention_feature = channel_attention_block(inputs, ratio)
	attention_feature = spatial_attention_block(attention_feature)
	return attention_feature

def morpho_attention_block(inputs, name, iterations=5):

	pool = tf.keras.layers.Conv2D(filters=1, 
				kernel_size=(1,1),
				padding="same",
				kernel_initializer = weight_init,
				kernel_regularizer = l2,
				use_bias = False)(inputs)

	erode_layer = Erosion2D(num_filters=1, 
		kernel_size=(7,7),
		padding='SAME',
		kernel_initializer = weight_init)
	dilat_layer = Dilation2D(num_filters=1, 
		kernel_size=(7,7),
		padding='SAME',
		kernel_initializer = weight_init)

	erode_features = erode_layer(pool)
	dilat_features = dilat_layer(pool)
	opens_features = pool
	close_features = pool
	#erode_features = tf.keras.layers.Add()([erode_features, pool])
	#dilat_features = tf.keras.layers.Add()([dilat_features, pool])

	for _ in range(iterations-1):
		erode_features = erode_layer(erode_features)
		#Erosion2D(num_filters=1, 
		#		kernel_size=(7,7),
		#		padding='SAME',
		#		kernel_initializer = weight_init)(erode_features)
	for _ in range(iterations-1):
		dilat_features = dilat_layer(dilat_features)
		#Dilation2D(num_filters=1, 
		#		kernel_size=(7,7),
		#		padding='SAME',
		#		kernel_initializer = weight_init)(dilat_features)

	for _ in range(iterations):
		opens_features = dilat_layer(erode_layer(opens_features))

	for _ in range(iterations):
		close_features = erode_layer(dilat_layer(close_features))

		#erode_features = tf.keras.layers.Add()([erode_features, pool])
		#dilat_features = tf.keras.layers.Add()([dilat_features, pool])

	#alpha = tf.keras.layers.Add()([erode_features, dilat_features])
	alpha = tf.keras.layers.Conv2D(filters=1, 
				kernel_size=(1,1),
				padding="same",
				kernel_initializer = weight_init,
				kernel_regularizer = l2,
				use_bias = False)(tf.keras.layers.Concatenate(axis=3)([erode_features, dilat_features, opens_features, close_features]))
	alpha = tf.keras.layers.Activation('sigmoid', name=name)(alpha)

	return tf.keras.layers.Multiply()([alpha, inputs])

# def morpho_attention_block(inputs, name, iterations=3):

# 	pool = tf.keras.layers.Conv2D(filters=1, 
# 				kernel_size=(1,1),
# 				padding="same",
# 				kernel_initializer = weight_init,
# 				kernel_regularizer = l2,
# 				use_bias = False)(inputs)

# 	erode_layer = Erosion2D(num_filters=1, 
# 		kernel_size=(7,7),
# 		padding='SAME',
# 		kernel_initializer = weight_init)
# 	dilat_layer = Dilation2D(num_filters=1, 
# 		kernel_size=(7,7),
# 		padding='SAME',
# 		kernel_initializer = weight_init)

# 	erode_features = erode_layer(pool)
# 	dilat_features = dilat_layer(pool)

# 	erode_features = tf.keras.layers.Add()([erode_features, pool])
# 	dilat_features = tf.keras.layers.Add()([dilat_features, pool])

# 	for _ in range(iterations-1):
# 		erode_features = Erosion2D(num_filters=1, 
# 				kernel_size=(7,7),
# 				padding='SAME',
# 				kernel_initializer = weight_init)(erode_features)
# 		dilat_features = Dilation2D(num_filters=1, 
# 				kernel_size=(7,7),
# 				padding='SAME',
# 				kernel_initializer = weight_init)(erode_features)

# 		erode_features = tf.keras.layers.Add()([erode_features, pool])
# 		dilat_features = tf.keras.layers.Add()([dilat_features, pool])

# 	alpha = tf.keras.layers.Add()([erode_features, dilat_features])
# 	alpha = tf.keras.layers.Activation('sigmoid', name=name)(alpha)

# 	return tf.keras.layers.Multiply()([alpha, inputs])

def triplet_attention(inputs, name, kernel_size=7, r=16, batchnorm=True):
	def zpool(inputs, ratio=16):
		avg_pool = tf.math.reduce_mean(inputs, axis=[3], keepdims=True)
		max_pool = tf.math.reduce_max(inputs, axis=[3], keepdims=True)
		return tf.keras.layers.Concatenate(axis=3)([max_pool,avg_pool])


	rot_1 = tf.transpose(tf.reverse(tf.transpose(inputs, perm=[0,1,3,2]), axis=[1]), perm=[0,2,3,1])
	xhi_1 = zpool(rot_1, ratio=r)
	conv_1 = tf.keras.layers.Conv2D(
		filters=1, kernel_size=kernel_size,
		padding="same",
		kernel_initializer = weight_init,
		use_bias = False,
		kernel_regularizer = l2)(xhi_1)
	if batchnorm:
		conv_1 = tf.keras.layers.BatchNormalization()(conv_1)
	out_1 = tf.keras.layers.Activation('sigmoid')(conv_1)
	out_1 = tf.keras.layers.Multiply()([rot_1, out_1])
	out_1 = tf.transpose(tf.reverse(tf.transpose(out_1, perm=[0,3,1,2]), axis=[1]), perm=[0,1,3,2])

	rot_2 = tf.transpose(tf.reverse(tf.transpose(inputs, perm=[0,2,3,1]), axis=[0]), perm=[0,3,2,1])
	xhi_2 = zpool(rot_2, ratio=r)
	conv_2 = tf.keras.layers.Conv2D(
		filters=1, kernel_size=kernel_size,
		padding="same",
		kernel_initializer = weight_init,
		use_bias = False,
		kernel_regularizer = l2)(xhi_2)
	if batchnorm:
		conv_2 = tf.keras.layers.BatchNormalization()(conv_2)
	out_2 = tf.keras.layers.Activation('sigmoid')(conv_2)
	out_2 = tf.keras.layers.Multiply()([rot_2,out_2])
	out_2 = tf.transpose(tf.reverse(tf.transpose(out_2, perm=[0,3,2,1]), axis=[0]), perm=[0,3,1,2])

	rot_3 = inputs
	xhi_3 = zpool(rot_3, ratio=r)
	conv_3 = tf.keras.layers.Conv2D(
		filters=1, kernel_size=kernel_size,
		padding="same",
		kernel_initializer = weight_init,
		use_bias = False,
		kernel_regularizer = l2)(xhi_3)
	if batchnorm:
		conv_3 = tf.keras.layers.BatchNormalization()(conv_3)
	out_3 = tf.keras.layers.Activation('sigmoid')(conv_3)
	out_3 = tf.keras.layers.Multiply()([rot_3, out_3])

	return 1. / 3 * tf.keras.layers.Add(name=name)([out_1, out_2, out_3])


class BilateralFilter(tf.keras.layers.Layer):
	'''
	Bilateral Filtering 2D Layer
	Assuming channel last
	'''

	def __init__(self, num_filters, kernel_size, strides=(1, 1),
	             padding='SAME', kernel_initializer='glorot_uniform',
	             kernel_constraint=None,
	             **kwargs):
		super(BilateralFilter, self).__init__(**kwargs)
		self.num_filters = num_filters
		self.kernel_size = kernel_size
		self.strides = strides
		self.padding = padding
		self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)
		self.kernel_constraint = tf.keras.constraints.get(kernel_constraint)
		# Assuming channel last
		self.channel_axis = -1

	def build(self, input_shape):
		if input_shape[self.channel_axis] is None:
			raise ValueError('The channel dimension of the inputs '
		                 'should be defined. Found `None`.')

		input_dim = input_shape[self.channel_axis]
		kernel_shape = self.kernel_size + (input_dim, self.num_filters)

		self.kernel = self.add_weight(shape=kernel_shape,
		                          initializer=self.kernel_initializer,
		                          name='kernel',
		                          constraint=self.kernel_constraint)

		# Be sure to call this at the end
		super(BilateralFilter, self).build(input_shape)

	def call(self, x):
		for i in range(self.num_filters):
			# BilateralFilter returns image of same size as x
			# so applying filter over channel_axis
			out[..., i] = self.__bilateral(x[..., i], self.kernel[..., i], 
			    	self.strides, self.padding)
			if i == 0:
				outputs = out
			else:
				outputs = tf.keras.layers.Concatenate()([outputs, out])

		return outputs

	def compute_output_shape(self, input_shape):
		# if self.data_format == 'channels_last':
		space = input_shape[1:-1]
		new_space = []
		for i in range(len(space)):
			new_dim = conv_utils.conv_output_length(
			    space[i],
			    self.kernel_size[i],
			    padding=self.padding,
			    stride=self.strides[i],
			    dilation=1)  # self.erosion_rate[i])
			new_space.append(new_dim)

		return (input_shape[0],) + tuple(new_space) + (self.num_filters,)

	def __bilateral(self, x, st_element, strides, padding,
		            rates=(1, 1, 1, 1)):
		# tf.nn.erosion2d(input, filter, strides, rates, padding, name=None)
		x = tf.nn.erosion2d(x, st_element, (1, ) + strides + (1, ), padding, "NHWC",
		                    rates)
		return x

	def get_config(self):
		config = super().get_config()
		config.update({
			'filters':
			    self.num_filters,
			'kernel_size':
			    self.kernel_size,
			'strides':
			    self.strides,
			'padding':
			    self.padding
			})
		return config
