from enum import Enum
import os


class Flags(Enum):
	TRAINING_MODE 	= "train"
	PREDICT_MODE 	= "test"
	ANALYZE_MODE 	= "analyze"
	SHOW_MODE 		= "show"
	EVAL_MODE 		= "eval"

	def __str__(self):
		return self.value

DefaultDirectories = dict(
	datadir 	= os.path.join(os.getcwd(), 'data'),
	logdir 		= os.path.join(os.getcwd(), 'logs')
	)

DefaultValues = dict(
	input_shape			= '(304,304,3)',
	num_filters 		= 8,
	batch_size			= 13,
	epochs				= 100,
	learning_rate 		= 1e-3,
	subset				= 'logyard',
	seed 				= 42
	)
