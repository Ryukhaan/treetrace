import os
import numpy as np
import tensorflow as tf 
import logging
import time
import cv2 as cv
import re

import skimage.io as io
from skimage.color import label2rgb
import skimage.transform as transform
import matplotlib.pyplot as plt
import pathlib
from glob import glob

import networks 
import losses
import metrics
import dataloader
import utils

class Trainer():
	"""docstring for Trainer"""

	def __init__(self, args, 
				train_path,#data, 
				val_path,
				trainsize, 
				valsize):
		super(Trainer, self).__init__()
		self.args = args
		self.input_size  	= eval(args.shape)[0:2]
		#self.data = data
		self.train_path = train_path
		self.val_path  	= val_path
		if self.args.name == None:
			self.args.name = ''.join([
				str(self.args.model), 
				str(self.args.num_filt),
				str(self.args.set)])
		self.trainsize = trainsize
		self.valsize   = valsize
		self.init_log()
		self.init_scalars()
		self.parse_network()
		self.build_model()
		self.load_weights()

	def init_log(self):
		logfile = '{}/logfile/{}.log'.format(self.args.logdir, self.args.name)
		logging.basicConfig(filename=logfile, 
    		level=logging.INFO,
    		filemode='w')

	def init_scalars(self):
		self.scalardir = "{}/scalars/{}/".format(self.args.logdir, self.args.name)
		if not os.path.isdir(self.scalardir):
			logging.info("Create scalars directory {}".format(self.scalardir))
			os.mkdir(self.scalardir)

	def parse_network(self):
		logging.info("Selected Net: {}".format(self.args.model))
		if self.args.model == networks.NetworkType._unet_t:
			self.net = networks.Unet(self.args)
		if self.args.model == networks.NetworkType._simple_t:
			self.net = networks.SimpleUnet(self.args)
		if self.args.model == networks.NetworkType._gating_t:
			self.net = networks.AttUnet(self.args)
		if self.args.model == networks.NetworkType._cbam_t:
			self.net = networks.CbamUnet(self.args)
		if self.args.model == networks.NetworkType._mam_t:
			self.net = networks.MAMUnet(self.args)
		if self.args.model == networks.NetworkType._triplet_t:
		 	self.net = networks.TripletUnet(self.args)
		#if self.args.model == NetworkType._CLASSIC_t:
		#	self.net = ClassicUnet(self.args)
		if self.args.model == None or self.args.model == '':
			logging.error("No network type found ! (Please select one)")
			exit()
	
	def build_model(self):
		self.model = self.net.model()
		self.model.compile(optimizer = tf.keras.optimizers.Adam(
			learning_rate = self.args.lr), 
			loss    = losses.mcc_loss, #'binary_crossentropy', 
			metrics = metrics.matthews_correlation)
		if self.args.verbose:
			self.model.summary()

	def load_weights(self):
		self.checkpoint_path = '{}/hdf5/{}/'.format(self.args.logdir, self.args.name)
		self.checkpoint_path = self.checkpoint_path + "model.hdf5"
		self.checkpoint_dir  = os.path.dirname(self.checkpoint_path)
		if not os.path.isdir(self.checkpoint_dir):
			os.mkdir(self.checkpoint_dir)

		logging.info("Last checkpoint: {}".format(self.checkpoint_path))
		try:
			#self.model.load_weights(self.checkpoint_path.format(self.args.it))
			self.model.load_weights(self.checkpoint_path)
			logging.info("Weights loaded")
		except:
			logging.warning("Skipping: No weights found (missing ckpt file) at {}".format(self.checkpoint_path))

	def train(self):
		STEPS_PER_EPOCH   = self.trainsize // self.args.batch_size
		VALIDATION_STEPS  = self.valsize // self.args.batch_size
		
		data_gen_args = dict(rescale = 1. / 255,
                     rotation_range=90,
                     width_shift_range=0.05,
                     height_shift_range=0.05,
                     shear_range=0.05,
                     zoom_range=0.05,
                     horizontal_flip=True,vertical_flip=True,
                     fill_mode='nearest')

		__train_gen = dataloader.create_generator(
			self.args.batch_size,
			self.train_path, 'images', 'mask',
			data_gen_args, self.input_size
			)
		__valid_gen = dataloader.create_generator(
			1,
			self.val_path, 'images', 'mask',
			data_gen_args, self.input_size
			)

		tensorboard_callback  = tf.keras.callbacks.TensorBoard(
			log_dir=self.scalardir,
			#histogram_freq=10,
			write_images=False)
			#embeddings_freq=10)
		model_checkpoint      = tf.keras.callbacks.ModelCheckpoint(
			self.checkpoint_path,
			monitor='val_loss',
			verbose=self.args.verbose,
			save_best_only=True)

		logging.info("Fitting model...")
		# for epoch in range(self.args.epochs):
		# 	print("\nStart of epoch {}".format(epoch))
		# 	start_time = time.time()

		# 	for step, (x_train_batch, y_train_batch) in enumerate(self.data['train']):

		# 		loss_value = self.model.train_step(x_train_batch, y_train_batch)

		# 		if step % 200 == 0:
		# 			print("Training loss (for one batch) at step {}: {}".format(step, float(loss_value)))
		# 			print("Seen so far: %d samples" % ((step+1)*batch_size))

		# 	for x_val_batch, y_val_batch in self.data['val']:
		# 		val_logits = self.model.test_step(x_val_batch, y_val_batch)
		# 	print("Validation %.4f" % float(val_logits))                  
		model_history = self.model.fit(
			__train_gen, 
			epochs=self.args.epochs,
			steps_per_epoch=STEPS_PER_EPOCH,
			validation_steps=VALIDATION_STEPS,
			validation_data=__valid_gen,
			callbacks=[model_checkpoint, tensorboard_callback])
		logging.info("Fitting done !")

	def test(self, path, valpath=None):
		if os.path.isdir(path):
			for file in sorted(os.listdir(path)):
				if file == '.DS_Store': continue
				file_name = os.path.join(path, file)
				if valpath:
					realmask = io.imread(os.path.join(path, "..", "mask", file)[:-4]+"png")
					realmask = (realmask>0).astype(np.uint8)
				image = io.imread(file_name)
				img = image / 255.0
				#img = cv2.resize(img, target_size)
				img = transform.resize(img, self.input_size)
				img = np.reshape(img,(1,)+img.shape)
				result = self.model.predict(img, verbose=1)
				mask = np.array(result[0,:,:,0]) #> 0, dtype=np.uint8)

				filename = os.path.join(self.args.logdir + "/predict/", file)
				io.imsave(filename, mask)
				#plt.imshow(mask, cmap="gray")
				#plt.colorbar()
				#plt.show()
				#utils.save_image(self.args.logdir + "/predict/", mask, i)

	def analyze(self):
		tf.keras.utils.plot_model(self.model, to_file=self.args.logdir + '/model.png')
		if self.args.verbose:
			self.model.summary()
		#exit()
		self.model.trainable = False
		layer_outputs = [layer.output for layer in self.model.layers[:-1] if re.match('.*alpha.*', layer.name) ]
		layer_name = [layer.name.split('/')[0] for layer in self.model.layers[:-1] if re.match('.*alpha.*', layer.name) ]
		
		layer_name = ['alpha_1','multiply', 'alpha_2','multiply_1',
					'alpha_3', 'multiply_2', 'alpha_4', 'multiply_3']
		layer_outputs = [layer.output for layer in self.model.layers[:-1] if layer.name in layer_name]
		for layer in self.model.layers[:-1]:
			if re.match('.*ion2d.*', layer.name):
				weights = layer.get_weights()
				weights = np.squeeze(weights, axis=(0,3,4))
				plt.imshow(weights)
				plt.colorbar()
				plt.title(layer.name)
				plt.show()
		# # Extracts the outputs of the top 12 layers
		#bloc = self.net.up_3
		activation_model = tf.keras.Model(self.net.inputs, layer_outputs)
		#activation_model.summary()
		# Creates a model that will return these outputs, given the model input
		img_tensor = io.imread(os.getcwd() + '/data/logyard/images/logyard_C12d.jpeg')
		img_tensor = img_tensor / 255.0
		img_tensor = transform.resize(img_tensor, self.input_size)
		img_tensor = np.reshape(img_tensor, (1,)+img_tensor.shape)
		activations = activation_model.predict(img_tensor)
		for idx, fmap in enumerate(activations):
			#print(fmap.shape)
			for i in range(fmap.shape[-1]):
				if len(fmap.shape) == 3:
					out = fmap[:,:,i]
				else:
					out = fmap[0,:,:,i]
				f_min, f_max = out.min(), out.max()
				out_img = (out - f_min ) / (f_max - f_min)
				utils.save_image(self.args.logdir + "/filters/", 
			 					out_img, 
			 					"{}_map{}.png".format(layer_name[idx], i))

	@staticmethod
	def eval(args):
		utils.eval(args)