import tensorflow as tf

def balanced_bce(inputs, label, name='cross_entrony_loss'):
    """
    Ref.: Holistically-Nested Edge Detection (CVPR 15)'
    Implements Equation [2] in https://arxiv.org/pdf/1504.06375.pdf
    """
    y = tf.cast(label, tf.float32)

    negatives   = tf.math.reduce_sum(1.-y)
    positives   = tf.math.reduce_sum(y)
    # Equation (2) (refer to the article)
    beta = positives / (negatives + positives)
    # Equation (2) divide by 1 - beta
    pos_weight = beta / (1. - beta)

    cost = tf.nn.weighted_cross_entropy_with_logits(
        logits=inputs, labels=label, pos_weight=pos_weight)
    # Multiply by 1 - beta 
    cost = tf.math.reduce_mean(cost * (1. - beta))

    # Check if image has no segmented pixels return 0 else return complete error function
    return tf.where(tf.equal(positives, 0.0), 0.0, cost, name=name)


def mcc_loss(y_pred, y_true, name='mcc_loss'):
    y_true = tf.cast(y_true, tf.float32)
    y_pred_pos = y_pred
    y_pred_neg = 1 - y_pred_pos

    y_pos = y_true
    y_neg = 1 - y_pos

    tp = tf.keras.backend.sum(y_pos * y_pred_pos)
    tn = tf.keras.backend.sum(y_neg * y_pred_neg)

    fp = tf.keras.backend.sum(y_neg * y_pred_pos)
    fn = tf.keras.backend.sum(y_pos * y_pred_neg)

    numerator = (tp * tn - fp * fn)
    denominator = tf.keras.backend.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))

    return 1. - numerator / (denominator + tf.keras.backend.epsilon())