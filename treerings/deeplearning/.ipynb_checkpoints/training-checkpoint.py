import torch
from torchvision import transforms, datasets
from torchsummary import summary
import torch.utils.data as data
import os
import glob
import cv2
from skimage import io
import numpy as np
import pathlib

from transformations import ComposeDouble, AlbuSeg2d, FunctionWrapperDouble, normalize_01, create_dense_target
from dataset import SegmentationDataSet
from trainer import Trainer
from skimage.transform import resize
from sklearn.model_selection import train_test_split
import morphological as mph

import losses
import math
import network

# training transformations and augmentations
transforms_training = ComposeDouble([
    FunctionWrapperDouble(resize,
                          input=True,
                          target=False,
                          output_shape=(128, 128, 3)),
    FunctionWrapperDouble(resize,
                          input=False,
                          target=True,
                          output_shape=(128, 128),
                          order=0,
                          anti_aliasing=False,
                          preserve_range=True),
    FunctionWrapperDouble(create_dense_target, input=False, target=True),
    FunctionWrapperDouble(np.moveaxis, input=True, target=False, source=-1, destination=0),
    FunctionWrapperDouble(normalize_01)
])

# validation transformations
transforms_validation = ComposeDouble([
    FunctionWrapperDouble(resize,
                          input=True,
                          target=False,
                          output_shape=(128, 128, 3)),
    FunctionWrapperDouble(resize,
                          input=False,
                          target=True,
                          output_shape=(128, 128),
                          order=0,
                          anti_aliasing=False,
                          preserve_range=True),
    FunctionWrapperDouble(create_dense_target, input=False, target=True),
    FunctionWrapperDouble(np.moveaxis, input=True, target=False, source=-1, destination=0),
    FunctionWrapperDouble(normalize_01)
])

root = pathlib.Path.cwd() / '../../../treerings/data/train/'

def get_filenames_of_path(path: pathlib.Path, ext: str = '*'):
    """Returns a list of files in a directory/path. Uses pathlib."""
    filenames = [file for file in sorted(path.glob(ext)) if file.is_file()]
    return filenames

inputs = get_filenames_of_path(root / 'images/', ext='*.tiff')
targets = get_filenames_of_path(root / 'mask/', ext='*.png')

# random seed
random_seed = 42

# split dataset into training set and validation set
train_size = 0.8  # 80:20 split

inputs_train, inputs_valid = train_test_split(
    inputs,
    random_state=random_seed,
    train_size=train_size,
    shuffle=True)

targets_train, targets_valid = train_test_split(
    targets,
    random_state=random_seed,
    train_size=train_size,
    shuffle=True)

batch_size = 32
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# dataset training
dataset_train = SegmentationDataSet(inputs=inputs_train,
                                    targets=targets_train,
                                    transform=transforms_training)

# dataset validation
dataset_valid = SegmentationDataSet(inputs=inputs_valid,
                                    targets=targets_valid,
                                    transform=transforms_validation)

# dataloader training
dataloader_training = torch.utils.data.DataLoader(dataset=dataset_train,
                                 batch_size=batch_size,
                                 shuffle=True)

# dataloader validation
dataloader_validation = torch.utils.data.DataLoader(dataset=dataset_valid,
                                   batch_size=batch_size,
                                   shuffle=True)

max_dist = math.sqrt(128**2 + 128**2)
n_pixels = 128 * 128
all_img_locations = torch.from_numpy(losses.cartesian([np.arange(128),
                                                       np.arange(128)]))
all_img_locations = all_img_locations.to(device)

cross = torch.tensor([[[0, 1, 0], [1, 1, 1], [0, 1, 0]]])
kernel = cross * 0.2
kernel = kernel[None]
    
def mcc_loss(output, target):
    mcc = 0.0
    for b in range(target.shape[0]):
        y_pred_pos = output[b]
        y_pred_neg = 1. - output[b]
        y_true_pos = target[b]
        y_true_neg = 1. - target[b]
    
        tp = torch.sum(y_pred_pos * y_true_pos)
        tn = torch.sum(y_pred_neg * y_true_neg)
        fp = torch.sum(y_pred_pos * y_true_neg)
        fn = torch.sum(y_pred_neg * y_true_pos)
        numerator = tn * tp - fn * fp
        denominator = torch.sqrt((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn))
        mcc += 1. - numerator / (denominator + 1e-6)
    return mcc / target.shape[0]
    
def hd_loss(output, target):
    return _weightedHausdorffDistance(output, target, 
                                            max_dist=max_dist, 
                                            all_img_locations=all_img_locations,
                                            n_pixels=n_pixels,
                                            device=device)


def perform_erosion(pred, target, kernel, alpha = 2.0, k = 10):
    dilation = mph.Dilation2d(1, 1, 3, inference=True)
    dilation.kernel = torch.as_tensor(kernel, device=pred.device, dtype=pred.dtype)
    eps = 1e-6
    y_true = torch.as_tensor(target > 0, device=pred.device, dtype=pred.dtype)
    omega_true = [y_true] 
    omega_pred = [pred]
    for i in range(k):
        omega_true.append(dilation(omega_true[i])-0.2)
        omega_pred.append(dilation(omega_pred[i])-0.2)
        
    acc_true = (k+1) - torch.sum(torch.stack(omega_true), axis=0)
    acc_pred = (k+1) - torch.sum(torch.stack(omega_pred), axis=0)
    gap_pred2true = torch.mean(pred * acc_true, axis=[1,2,3])
    gap_true2pred = torch.mean(y_true * acc_pred, axis=[1,2,3])
    gap_pred = torch.mean(y_true, axis=[1,2,3])
    gap_true = torch.mean(pred, axis=[1,2,3])
    loss = torch.mean( 0.5 * (gap_pred2true + eps) / (gap_pred + eps) \
                        + 0.5 * (gap_true2pred + eps) / (gap_true +eps ))
    return loss

#def own_loss(output, target):
#    return perform_erosion(output, target, kernel)

def l1_loss(y_pred, y_true):
    n = torch.count_nonzero(y_true)
    beta = n / torch.numel(y_true)
    pos = y_true * y_pred
    neg = (1. - y_true) * y_pred
    l_pos = torch.nn.L1Loss()(pos, y_true)
    l_neg = torch.nn.L1Loss()(neg, y_true)
    return (1. - beta) * l_pos + beta * l_neg
    
def new_loss(pred, target, alpha = 2.0, k = 10):
    cross = torch.tensor([[[0, 1, 0], [1, 1, 1], [0, 1, 0]]])
    kernel = cross * 0.2
    kernel = kernel[None]
    
    dilation = mph.Dilation2d(1, 1, 3, inference=True)
    dilation.kernel = torch.as_tensor(kernel, device=pred.device, dtype=pred.dtype)
    eps = 1e-6
    y_true = torch.as_tensor(target > 0, device=pred.device, dtype=pred.dtype)
    omega_true = [y_true] 
    for i in range(k):
        omega_true.append(dilation(omega_true[i])-0.2)
        
    acc_true = (k+1) - torch.sum(torch.stack(omega_true), axis=0)
    gap_pred2true = torch.mean(pred * acc_true, axis=[1,2,3])
    gap_pred = torch.mean(y_true, axis=[1,2,3])
    
    loss_d = torch.mean(gap_pred2true) #torch.mean( (gap_pred2true) / (gap_pred + eps) )
    loss_e = mcc_loss(pred, target)
    return loss_d + loss_e

#class MyModel(torch.nn.Module):
#    def __init__(self):
#        super(MyModel, self).__init__()
#        MODEL_NAME = "deeplabv3plus_resnet101"
#        PATH_TO_PTH = "best_deeplabv3plus_resnet101_voc_os16.pth"
#        self.backbone = network.modeling.__dict__[MODEL_NAME](num_classes=21, output_stride=8)
#        self.backbone.load_state_dict( torch.load( PATH_TO_PTH )['model_state']  )
#        self.backbone.classifier = torch.nn.Sequential(
#            torch.nn.Conv2d(304, 256, 3, padding=1, bias=False),
#            torch.nn.BatchNorm2d(256),
#            torch.nn.ReLU(inplace=True),
#            torch.nn.Conv2d(256, 1, 1)
#        )
#        #self.reduce = torch.nn.Conv2d(21, 1, 1)
#        self.out = torch.nn.Hardsigmoid()
#        
#    def forward(self, x):
#        x = self.backbone(x)
#        #x = self.reduce(x)
#        return self.out(x)

import network
from network import densenet
import argparse

dict_args = {
    "depth": 121,
    "weights": "",
    "pretrained": 1,
    "subsets": "val",
    "downsample": 0,
    "checkpointing": 0,
    "mutiscale-test": 0,
    "save-outputs": 0,
    "batch-size": 32,
    "aux_loss": 0,
    "save-submit": 0
}

class AllMyFields:
    def __init__(self, dictionary):
        for k, v in dictionary.items():
            setattr(self, k, v)
args = AllMyFields(dict_args)

class MyModel(torch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        MODEL_NAME = "deeplabv3plus_resnet101"
        PATH_TO_PTH = "best_deeplabv3plus_resnet101_voc_os16.pth"
        self.backbone = densenet.DenseNet(None, args, num_init_features=64, growth_rate=64, bn_size=2, block_config=(2, 4, 6, 4))
        self.out = torch.nn.Hardsigmoid()
        
    def forward(self, x):
        x, aux_logits = self.backbone(x)
        return self.out(x)

model = MyModel()
model = model.to(device)
summary(model)
learning_rate = 1e-3

n_epochs = 100

optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# trainer
_trainer = Trainer(model=model,
                  device=device,
                  criterion=new_loss,
                  optimizer=optimizer,
                  training_DataLoader=dataloader_training,
                  validation_DataLoader=dataloader_validation,
                  lr_scheduler=None,
                  epochs=n_epochs,
                  epoch=0,
                  notebook=False)

training_losses, validation_losses, lr_rates = _trainer.run_trainer()
