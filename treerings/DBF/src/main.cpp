//
//  main.cpp
//  Directionnal Bilateral Filtering
//
//  Created by Remi DECELLE on 24/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <map>

#include "boost/filesystem.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/ximgproc.hpp"

//#include "utils.hpp"
//#include "ACannyDetector.hpp"
#include "filter/gaussian-filter.hpp"
#include "ray-tracing.hpp"
//#include "ridgeorient.hpp"

using namespace cv;
using namespace std;

const cv::String keys =
"{help h usage ?    |       | print this message   }"
"{@image            |       | input image          }"
"{px x              | -1    | X coordinate of the pith }"
"{py y              | -1    | Y coordinate of the pith }"
"{sigma_r sr        | 15.0  | Range sigma }"
"{sigma_s ss        | 2.0   | Spatial sigma }"
"{ksize k           | 10    | Window of spatial / range (k x k)}"
"{color c           | false | Proceed RGB image}";

std::map<string, Point> read_csv(const string filename)
{
    std::ifstream file(filename.c_str());
    string line;
    string cell;
    std::map<string, Point> piths;
    int index = 0;
    int x_index, y_index;
    int label_index;
    if ( file.is_open() )
    {
        while ( getline(file,line) )
        {
            stringstream lineStream(line);
            int j = 0;
            Point pith;
            string name;
            while ( getline(lineStream, cell, ';' ) )
            {
                if ( j == 0 ) name = cell;
                if ( j == 1 ) pith.x = stoi(cell);
                if ( j == 2 ) pith.y = stoi(cell);
                j++;
            }
            piths[name] = pith;
            index++;
        }
    }
    return piths;
}

void proceed_image(const Mat image, const Point pith, const cv::CommandLineParser parser, const string filename)
{
    // Vector to split RGB -> {R,G,B}
    vector<Mat> channels(3);
    split(image, channels);

    // Get current working directory
    string cwd = boost::filesystem::current_path().parent_path().string();
    
    // Convert into graylevel keep only Blue component ?
    Mat graylevel;
    if ( !parser.get<bool>("color") )
    {
        graylevel = channels[2];
        cv::normalize(graylevel, graylevel, 0, 255, cv::NORM_MINMAX, CV_8U);
        imwrite(cwd + "/output/graylevel/" + filename + ".jpeg", graylevel);
    }
    else
    {
        image.copyTo(graylevel);
    }
    // Due to JPEG format some pixels appear at the log border
    /*
    Mat superior = (channels[0]>0 & channels[1]>0 & channels[2]>0);
    superior.convertTo(superior, CV_8U);
    // Some morphology to fill holes
    morphologyEx(superior, superior, MORPH_OPEN, Mat::ones(5, 5, CV_8U));
    morphologyEx(superior, superior, MORPH_CLOSE, Mat::ones(7, 7, CV_8U));
    */
    
    // Compute theorical orientation - Circle orientation
    int height = graylevel.rows;
    int width = graylevel.cols;
    Mat angle = Mat(height, width, CV_32F);
    Mat xx, yy;
    basic::meshgrid(cv::Range(0,width-1), cv::Range(0,height-1), xx, yy);
    xx = xx - pith.x;
    yy = (yy - pith.y);
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    for (int i = 0; i < height; i++)
    {
        const float *y_i = yy.ptr<float>(i);
        const float *x_i = xx.ptr<float>(i);
        float* ptrAngle  = angle.ptr<float>(i);
        for (int j = 0; j < width; j++)
        {
            /* Other method */
            //ptrAngle[j] = static_cast<float>(atan(x_i[j]/(y_i[j]+1e-10)));
            //ptrAngle[j] = atan2(-y_i[j], x_i[j])+ CV_PI/2;
            /* Seconde derivative */
            ptrAngle[j] = atan2(x_i[j], -y_i[j]);
        }
    }
    
    /*
     * Estimate sigma according to the distance to the border
     */
    /*
    cv::Mat sigma;
    cv::distanceTransform(superior, sigma, DIST_L2, 3);
    cv::normalize(sigma, sigma, 0, 1, cv::NORM_MINMAX);
    sigma = 2.5 * sigma + 2.5;
    Mat filterImage = Mat::zeros(graylevel.rows, graylevel.cols, CV_64F);
    approx_filter(graylevel, filterImage, angle, sigma);
    */
    

    float sigma_s = parser.get<float>("sigma_s");
    float sigma_r = parser.get<float>("sigma_r");
    int ksize = parser.get<int>("ksize");
    Mat filterImage;
    filterImage = parser.get<bool>("color") ? Mat::zeros(image.rows, image.cols, CV_32FC3) : Mat::zeros(image.rows, image.cols, CV_32F);
    
    // Proceed DBF
    directional_bilateral_filter(channels[1], filterImage, angle, sigma_s, sigma_r, ksize);

    split(filterImage, channels);
    Mat normalizeImage;
    normalize(filterImage, normalizeImage, 0, 255, NORM_MINMAX, CV_8U);

    // Convert into Polar Coordinates ?
    /*
    Mat test;
    test = cart2pol(filterImage, pith.x, pith.y);
    */
    cv::Mat threshImage = cv::Mat::zeros(filterImage.rows, filterImage.cols, CV_32F);
    /*
     * Enhance Constrast
     */
    auto clahe = cv::createCLAHE();
    clahe->setTilesGridSize(Size(11,11));
    clahe->setClipLimit(1.0);
    clahe->apply(normalizeImage, normalizeImage);
    imwrite(cwd + "/output/filtered/" + filename + ".png", normalizeImage);
    
    /*
     * Threshhold image with an adaptive thresh
     * Then remove small connected components
     */
    adaptiveThreshold(255-normalizeImage, threshImage,
                      255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY , 11, -4);
    /*
    auto canny_detector = new ACannyDetector(0.05, 0.1);
    canny_detector->setPith(pith);
    threshImage = canny_detector->applyTo(normalizeImage);
    */
    
    /*
     * Thin the threshold
     */
    threshImage = suppress_rings(normalizeImage, normalizeImage, pith.x, pith.y);
    imwrite(cwd + "/output/unpruned/" + filename + ".png", threshImage);
    
    /*
     * Post-treatment again to have smooth edges
     */
    Mat prunedImage = Mat::zeros(height, width, CV_8U);
    threshImage.copyTo(prunedImage);
    cv::morphologyEx(threshImage, prunedImage, cv::MORPH_CLOSE, cv::Mat::ones(3, 3, CV_8U));
    
    // Remove small connected components
    /*
    Mat img_labels, stats, centroids;
    int nLabels = cv::connectedComponentsWithStats(prunedImage, img_labels, stats, centroids, 8, CV_32S);
    prunedImage = cv::Mat::zeros(prunedImage.rows, prunedImage.cols, CV_8U);
    //int nLabels = connectedComponents(output, labelImage, 4, CV_32S);
    int minLabelSize = 30;
    for(int i=1; i<nLabels; i++)
    {
        int area = stats.at<int>(i, CC_STAT_AREA);
        if ( area > minLabelSize )
        {
            prunedImage += img_labels == i;
        }
    }
    */
    imwrite(cwd + "/output/threshold/" + filename + ".png", prunedImage);

    /*
     * Get only high - low transition
     * And reconnected with morphology
     */
    Mat output;
    output = suppress_rings(prunedImage, pith.x, pith.y);

    /*
     * Remove small connected components
     */
    Mat img_labels, stats, centroids;
    int nLabels = cv::connectedComponentsWithStats(output, img_labels, stats, centroids, 8, CV_32S);
    output = cv::Mat::zeros(prunedImage.rows, prunedImage.cols, CV_8U);
    int minLabelSize = 5;
    for(int i=1; i<nLabels; i++)
    {
        int area = stats.at<int>(i, CC_STAT_AREA);
        if ( area > minLabelSize )
        {
            output += img_labels == i;
        }
    }
    imwrite(cwd + "/output/thin/" + filename + ".png", output);
    
    /*
     * Write Overlay (Edges over Input Image)
     */
    vector<Mat> ringsChannels = {80*output, 120*output, output};
    Mat coloredRings;
    merge(ringsChannels, coloredRings);
    Mat overlay;
    cv::resize(coloredRings, coloredRings, cv::Size(image.cols, image.rows));
    addWeighted(image, 0.6, coloredRings, 0.4, 0.0, overlay);
    imwrite(cwd + "/output/overlay/" + filename + ".png", overlay);
}

int main(int argc, char** argv)
{
    // Parser of arguments
    cv::CommandLineParser parser(argc, argv, keys);
    parser.about("Directionnal Bilateral filtering");
    if ( parser.has("help") )
    {
        parser.printMessage();
        return 0;
    }
    
    // Read image
    string cwd = boost::filesystem::current_path().string();
    string parent = boost::filesystem::current_path().parent_path().string();
    if ( !parser.has("@image") )
    {
        parser.printErrors();
        return 0;
    }
    string filename = parser.get<cv::String>("@image");
    std::string base_filename = filename.substr(filename.find_last_of("/") + 1);
    std::string::size_type const p(base_filename.find_last_of('.'));
    std::string file_without_extension = base_filename.substr(0, p);
    cv::Mat image = imread(cwd + "/" + filename, IMREAD_COLOR);
    if ( image.empty() )
    {
        cerr << "Image not found/loaded !" << endl;
        return 0;
    }

    // Check pith
    cv::Point pith(parser.get<int>("px"), parser.get<int>("py"));
    if ( pith.x == -1 || pith.y == -1)
    {
        string pith_filename = "/pith_estimation.csv";
        std::map<string, Point> piths = read_csv(parent + pith_filename);
        pith = piths[file_without_extension];
    }
    
    // Proceed the directionnal bilateral filtering
    TickMeter tm;
    tm.start();
    cout << "Start processsing image. Please wait..." << endl;
    proceed_image(image, pith, parser, file_without_extension);
    tm.stop();
    cout << tm.getTimeMilli() << endl;
    return 1;
    
}
