//
//  basic.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 25/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "basic.hpp"

namespace basic {
    void repeatGrid(const cv::Mat &xgv, const cv::Mat &ygv, cv::Mat &X, cv::Mat &Y)
    {
        repeat(xgv.reshape(1,1), ygv.total(), 1, X);
        repeat(ygv.reshape(1,1).t(), 1, xgv.total(), Y);
    }
    
    // helper function (maybe that goes somehow easier)
    void meshgrid(const cv::Range &xgv, const cv::Range &ygv, cv::Mat &X, cv::Mat &Y)
    {
        std::vector<int> t_x, t_y;
        for (int i = xgv.start; i <= xgv.end; i++) t_x.push_back(i);
        for (int i = ygv.start; i <= ygv.end; i++) t_y.push_back(i);
        repeatGrid(cv::Mat(t_x), cv::Mat(t_y), X, Y);
    }
    
    void imagesc(cv::Mat InputArray, std::string windowName, int wait, int map)
    {
        cv::Mat display;
        InputArray.copyTo(display);
        std::string type = type2str(InputArray.type());
        if (type.compare("8UC1") == 0)
        {
            cv::normalize(display, display, 0, 255, cv::NORM_MINMAX);
        }
        else
        {
            cv::normalize(display, display, 0, 1, cv::NORM_MINMAX);
        }
        if (map != -1)
        {
            applyColorMap(display, display, map);
        }
        cv::imshow(windowName, display);
        cv::waitKey(wait);
    }
    
    std::string type2str(int type) {
        std::string r;
        
        uchar depth = type & CV_MAT_DEPTH_MASK;
        uchar chans = 1 + (type >> CV_CN_SHIFT);
        
        switch ( depth ) {
            case CV_8U:  r = "8U"; break;
            case CV_8S:  r = "8S"; break;
            case CV_16U: r = "16U"; break;
            case CV_16S: r = "16S"; break;
            case CV_32S: r = "32S"; break;
            case CV_32F: r = "32F"; break;
            case CV_64F: r = "64F"; break;
            default:     r = "User"; break;
        }
        
        r += "C";
        r += (chans+'0');
        
        return r;
    }
}
