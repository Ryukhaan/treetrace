//
//  ACannyDetector.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 24/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "ACannyDetector.hpp"

ACannyDetector::ACannyDetector()
{
    t_low  = 0.05;
    t_high = 0.1;
}

ACannyDetector::~ACannyDetector()
{
    
};

ACannyDetector::ACannyDetector(const double &low, const double &high)
{
    t_low  = low;
    t_high = high;
};

cv::Mat ACannyDetector::applyTo(cv::Mat InputArray)
{
    cv::Mat OutputArray = cv::Mat::zeros(InputArray.size(), CV_8U);
    
    //cv::Mat filteredArray = cv::Mat::zeros(InputArray.size(), CV_32F);
    //smooth(InputArray, filteredArray);
    //InputArray.copyTo(filteredArray);
    cv::normalize(InputArray, InputArray, 0, 1, cv::NORM_MINMAX, CV_64F);
    
    //InputArray.convertTo(InputArray, CV_32F);
    //cv::bilateralFilter(InputArray, filteredArray, 20, 2.0, 2.0);
    //basic::imagesc(InputArray, "Filtered", 0);
    
    cv::Mat magnitude   = cv::Mat::zeros(InputArray.size(), CV_64F);
    cv::Mat orientation = cv::Mat::zeros(InputArray.size(), CV_8U);
    gradient(InputArray, magnitude, orientation);
    //basic::imagesc(orientation, "Orientation", 0);
    
    cv::Mat nms = cv::Mat::zeros(InputArray.size(), CV_64F);
    nonMaximaSuppression(InputArray, nms, magnitude, orientation);
    //basic::imagesc(nms, "NMS", 0);
    
    doubleThreshold(nms, OutputArray);
    //basic::imagesc(OutputArray, "Output", 0);
    return OutputArray;
}

void ACannyDetector::smooth(cv::Mat InputArray, cv::Mat OutputArray)
{
    int dw = (window_size+1)*0.5;
    
    bool has_color = false;
    cv::Mat planes[3];
    int H = InputArray.rows;
    int W = InputArray.cols;
    int channels = InputArray.channels();
    if (channels == 3)
    {
        has_color = true;
        split(InputArray, planes);  // planes[2] is the red channel
    }
    
    // CIRLCES
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(0,window_size-1), cv::Range(0,window_size-1), xx, yy);
    xx = xx - pith.x;
    yy = yy - pith.y;
    for (int y=dw; y<H-dw-1; y++)
    {
        double *out_row = OutputArray.ptr<double>(y);
        
        for (int x=dw; x<W-dw-1; x++)
        {
            int r = static_cast<int>(sqrt( pow(y-pith.y, 2) + pow(x-pith.x, 2)));
            if (r==0) continue;
            
            double r1 = pow(r + 0.5, 2);
            double r2 = pow(r - 0.5, 2);
            cv::Mat tmpx, tmpy;
            pow(xx+x-dw, 2, tmpx);
            pow(yy+y-dw, 2, tmpy);
            cv::Mat c1 = (tmpx + tmpy) <  r1;
            cv::Mat c2 = (tmpx + tmpy) >= r2;
            cv::Mat circle = c1 & c2;
            
            vector<cv::Point> locations;  // output, locations of non-zero pixels
            cv::findNonZero(circle, locations);
            double _mean = 0;
            //double _max = -1;
            //double _min = 1000;
            for (cv::Point loc: locations)
            {
                double local = InputArray.at<double>(loc.y + y - dw, loc.x + x - dw);
                //double local = in_row[loc.x + x - dw];
                _mean = _mean + local;
            }
            _mean = _mean / locations.size();
            
            //OutputArray.at<double>(y,x) = _mean;
            out_row[x] = _mean;
        }
    }
};

void ACannyDetector::gradient(cv::Mat InputArray, cv::Mat MagnitudeArray, cv::Mat OrientationArray)
{
    int H = InputArray.rows;
    int W = InputArray.cols;
    
    // Directionnal gradient mask
    // Sobel
    //double tx[] = {-1, 0, 1, -2, 0, 2, -1, 0, 1};
    //double ty[] = {1, 2, 1, 0, 0, 0, -1, -2, -1};
    // Scharr
    //double tx[] = {-3, 0, 3, -10, 0, 10, -3, 0, 3};
    //double ty[] = {3, 10, 3, 0, 0, 0, -3, -10, -3};
    // Gravitation
    double val = sqrt(2) * 0.25;
    double tx[] = {-val, 0, val, -1, 0, 1, -val, 0, val};
    double ty[] = {val, 1, val, 0, 0, 0, -val, -1, -val};
    
    cv::Mat KGx = cv::Mat(3, 3, CV_64F, tx);
    cv::Mat KGy = cv::Mat(3, 3, CV_64F, ty);
    
    // Smoothing
    cv::GaussianBlur(KGx, KGx, cv::Size(3,3), 1.0);
    cv::GaussianBlur(KGy, KGy, cv::Size(3,3), 1.0);
    
    cv::Mat grad_x, grad_y;
    cv::filter2D(InputArray, grad_x, -1, KGx);
    cv::filter2D(InputArray, grad_y, -1, KGy);
    cv::Mat mag;
    
    // Derive norm and orientation
    cv::Mat tmpx, tmpy;
    cv::pow(grad_x, 2.0, tmpx);
    cv::pow(grad_y, 2.0, tmpy);
    cv::sqrt(tmpx+tmpy, MagnitudeArray);
    
    double grad, h1, h2;
    cv::Mat rothwell = cv::Mat::zeros(MagnitudeArray.size(), MagnitudeArray.type());
    // Can we do better for angle ?
    for (int i = 1; i<H-1; i++) // i=0 -> H-1 in Canny
    {
        /* ROTHWELL */
        double* g0  = MagnitudeArray.ptr<double>(i-1);
        double* g1  = MagnitudeArray.ptr<double>(i);
        double* g2  = MagnitudeArray.ptr<double>(i+1);
        double* grad_x_r = grad_x.ptr<double>(i);
        double* grad_y_r = grad_y.ptr<double>(i);
        /* End Rothwell */
        
        uint8_t* out_row = OrientationArray.ptr<uint8_t>(i);
        for (int j = 1; j<W-1; j++) // j=0 -> W-1 in Canny
        {
            double yy = i - pith.y;
            double xx = j - pith.x;
            double angle = -atan2(yy, xx) * 180 / CV_PI;
            if (angle < 0) angle += 360;
            
            if (((angle >= 0 ) && (angle < 22.5)) || ((angle >= 337.5) && (angle <= 360)))  out_row[j] = basic::ANGULAR::RIGHT;
            else if ((angle >= 22.5) && (angle < 67.5))       out_row[j] = basic::ANGULAR::TOP_RIGHT;
            else if ((angle >= 67.5 && angle < 112.5))        out_row[j] = basic::ANGULAR::TOP;
            else if ((angle >= 112.5 && angle < 157.5))       out_row[j] = basic::ANGULAR::TOP_LEFT;
            else if ((angle >= 157.5) && (angle < 202.5))     out_row[j] = basic::ANGULAR::LEFT;
            else if ((angle >= 202.5) && (angle < 247.5))     out_row[j] = basic::ANGULAR::BOTTOM_LEFT;
            else if ((angle >= 247.5) && (angle < 292.5))     out_row[j] = basic::ANGULAR::BOTTOM;
            else if ((angle >= 292.5) && (angle < 337.5))     out_row[j] = basic::ANGULAR::BOTTOM_RIGHT;
            
            /* Rothwell */
            /*
            switch (out_row[j]) {
                case basic::ANGULAR::RIGHT:
                    grad = grad_y_r[j] / grad_x_r[j];
                    h1   = grad * g0[j-1] + (1-grad)*g1[j-1];
                    h2   = grad * g2[j+1] + (1-grad)*g1[j+1];
                    break;
                case basic::ANGULAR::BOTTOM_RIGHT:
                    grad = grad_x_r[j] / grad_y_r[j];
                    h1   = grad * g0[j-1] + (1-grad)*g0[j];
                    h2   = grad * g2[j+1] + (1-grad)*g2[j];
                    break;
                case basic::ANGULAR::BOTTOM:
                    grad = - grad_x_r[j] / grad_y_r[j];
                    h1   = grad * g0[j+1] + (1-grad)*g0[j];
                    h2   = grad * g2[j-1] + (1-grad)*g2[j];
                    break;
                case basic::ANGULAR::BOTTOM_LEFT:
                    grad = - grad_y_r[j] / grad_x_r[j];
                    h1   = grad * g0[j+1] + (1-grad)*g1[j+1];
                    h2   = grad * g2[j-1] + (1-grad)*g1[j-1];
                    break;
                case basic::ANGULAR::LEFT:
                    grad = grad_y_r[j] / grad_x_r[j];
                    h1   = grad * g2[j+1] + (1-grad)*g1[j+1];
                    h2   = grad * g0[j-1] + (1-grad)*g1[j-1];
                    break;
                case basic::ANGULAR::TOP_LEFT:
                    grad = grad_x_r[j] / grad_y_r[j];
                    h1   = grad * g2[j+1] + (1-grad)*g2[j];
                    h2   = grad * g0[j-1] + (1-grad)*g0[j];
                    break;
                case basic::ANGULAR::TOP:
                    grad = - grad_x_r[j] / grad_y_r[j];
                    h1   = grad * g2[j-1] + (1-grad)*g2[j];
                    h2   = grad * g0[j+1] + (1-grad)*g0[j];
                    break;
                case basic::ANGULAR::TOP_RIGHT:
                    grad = - grad_y_r[j] / grad_x_r[j];
                    h1   = grad * g2[j-1] + (1-grad)*g1[j-1];
                    h2   = grad * g0[j+1] + (1-grad)*g1[j+1];
                    break;
                default:
                    h1 = h2 = 0.0;
                    grad = 0.0;
                    break;
            }
            double dnewx = 0.0, dnewy = 0.0;
            double fraction = (h1-h2)/(2.0*(h1-2.0*g1[j]+h2));
            switch (out_row[j]) {
                case basic::ANGULAR::RIGHT:
                    dnewx = fraction;
                    dnewy = grad * fraction;
                    break;
                case basic::ANGULAR::BOTTOM_RIGHT:
                    dnewx = grad * fraction;
                    dnewy = fraction;
                    break;
                case basic::ANGULAR::BOTTOM:
                    dnewx = - grad * fraction;
                    dnewy = fraction;
                    break;
                case basic::ANGULAR::BOTTOM_LEFT:
                    dnewx = - fraction;
                    dnewy = - grad * fraction;
                    break;
                case basic::ANGULAR::LEFT:
                    dnewx = fraction;
                    dnewy = grad * fraction;
                    break;
                case basic::ANGULAR::TOP_LEFT:
                    dnewx = grad * fraction;
                    dnewy = fraction;
                    break;
                case basic::ANGULAR::TOP:
                    dnewx = - grad * fraction;
                    dnewy = fraction;
                    break;
                case basic::ANGULAR::TOP_RIGHT:
                    dnewx = - fraction;
                    dnewy = - grad * fraction;
                    break;
                default:
                    break;
            }
            if ( (g1[j]>=h1) && (g1[j]>=h2) && (fabs(dnewx)<=0.5) && (fabs(dnewy)<=0.5) )
            {
                //if (g1[j]>=t_low) rothwell.at<double>(i,j) = g1[j];
                rothwell.at<double>(i,j) = g1[j];
            }
            if ( (fabs(dnewx)<=0.5) && (fabs(dnewy)<=0.5) )
            {
                grad_x_r[j] = j + dnewx + 0.5;
                grad_y_r[j] = i + dnewy + 0.5;
            }
            else
            {
                grad_x_r[j] = j + 0.5;
                grad_y_r[j] = i + 0.5;
            }
            */
            /* END ROTHWELL */
        }
    }
    //rothwell.copyTo(MagnitudeArray);
    //basic::imagesc(rothwell>0, "Roth", 1);
    //basic::imagesc(MagnitudeArray>t_low, "Mag", 0);
    /*
    basic::imagesc(grad_x);
    basic::imagesc(grad_y);
    */
};

void ACannyDetector::nonMaximaSuppression(cv::Mat InputArray, cv::Mat OutputArray, cv::Mat magnitude, cv::Mat orientation)
{
    int H = InputArray.rows;
    int W = InputArray.cols;
    for (int i=1; i<H-1; i++)
    {
        double* out_row = OutputArray.ptr<double>(i);
        uint8_t* ori_row = orientation.ptr<uint8_t>(i);
        for (int j=1; j<W-1; j++)
        {
            double pixelmag = magnitude.at<double>(i,j);
            switch (ori_row[j])
            {
                case basic::ANGULAR::RIGHT:
                    if (InputArray.at<double>(i,j-1) < InputArray.at<double>(i,j+1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i,j+1), magnitude.at<double>(i,j-1)));
                    }
                    break;
                case basic::ANGULAR::TOP_RIGHT:
                    if (InputArray.at<double>(i+1, j-1) < InputArray.at<double>(i-1, j+1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i+1,j-1), magnitude.at<double>(i-1,j+1)));
                    }
                    break;
                case basic::ANGULAR::TOP:
                    if (InputArray.at<double>(i+1, j) < InputArray.at<double>(i-1, j))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i+1,j), magnitude.at<double>(i-1,j)));
                    }
                    break;
                case basic::ANGULAR::TOP_LEFT:
                    if (InputArray.at<double>(i+1, j+1) < InputArray.at<double>(i-1, j-1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i+1,j+1), magnitude.at<double>(i-1,j-1)));
                    }
                    break;
                case basic::ANGULAR::LEFT:
                    if (InputArray.at<double>(i, j+1) < InputArray.at<double>(i, j-1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i,j+1), magnitude.at<double>(i,j-1)));
                    }
                    break;
                case basic::ANGULAR::BOTTOM_LEFT:
                    if (InputArray.at<double>(i-1, j+1) < InputArray.at<double>(i+1, j-1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i-1,j+1), magnitude.at<double>(i+1,j-1)));
                    }
                    break;
                case basic::ANGULAR::BOTTOM:
                    if (InputArray.at<double>(i-1, j) < InputArray.at<double>(i+1, j))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i-1,j), magnitude.at<double>(i+1,j)));
                    }
                    break;
                case basic::ANGULAR::BOTTOM_RIGHT:
                    if (InputArray.at<double>(i-1, j-1) < InputArray.at<double>(i+1, j+1))
                    {
                        out_row[j] = pixelmag * (pixelmag >= max(magnitude.at<double>(i-1,j-1), magnitude.at<double>(i+1,j+1)));
                    }
                    break;
                default:
                    break;
            }
        }
    }
    //basic::imagesc(OutputArray, "NMS", 0);
};


void ACannyDetector::doubleThreshold(cv::Mat InputArray, cv::Mat OutputArray)
{
    double min_val, max_val;
    cv::Point min_loc, max_loc;
    cv::minMaxLoc(InputArray, &min_val, &max_val, &min_loc, &max_loc);
    double low  = t_low  * max_val;
    double high = t_high * max_val;
    
    cv::Mat T1      = (InputArray > low);
    cv::Mat T2      = (InputArray > high);
    cv::Mat super   = (T1 & 0x1) + (T2 & 0x1);
    
    vector<vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;
    findContours(T1, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);
    
    cv::Mat edges = cv::Mat::zeros(InputArray.size(), CV_8U);
    for (int icmp = 0; icmp < contours.size(); icmp++)
    {
        vector<cv::Point> list = contours.at(icmp);
        bool empty = true;
        for (int elt = 0; elt < list.size(); elt++)
        {
            if (super.at<uint8_t>(list.at(elt)) == 2)
            {
                empty = false;
                break;
            }
        }
        if ( !empty )
        {
            for (int elt = 0; elt < list.size(); elt++)
            {
                // OutputArray.at<uint8_t>(list.at(elt)) = 1;
                edges.at<uint8_t>(list.at(elt)) = 255;
            }
        }
    }
    edges.copyTo(OutputArray);
    //basic::imagesc(edges, "Hyst", 1);
};


