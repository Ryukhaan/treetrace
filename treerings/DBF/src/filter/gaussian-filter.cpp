//
//  gaussian-filter.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 22/04/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//

#include "gaussian-filter.hpp"


void gaussian_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, float sigma)
{
    //int ksize = 6 * sigma;
    int ksize = 24;
    // Meshgrid (x-axis and y-axis coordinates)
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(-ksize,ksize), cv::Range(-ksize,ksize), xx, yy);
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    
    // Gaussian kernel
    cv::Mat X, Y;
    // X=x^2
    cv::pow(xx, 2.0, X);
    // Y=y^2
    cv::pow(yy, 2.0, Y);

    cv::Mat G;
    // exp(-(x^2+y^2)/(2sigma^2))
    float s2 = sigma * sigma;
    cv::exp(-(X+Y)/(2*s2), G);
    // Gaussian
    G = G / sigma*sqrt(2*CV_PI);
    G.convertTo(G, CV_32F);
    
    // Basis function G2a, G2b and G2c
    float s4 = pow(sigma, 4.0);
    cv::Mat G2a, G2b, G2c;
    cv::pow(xx, 2.0, G2a);
    cv::pow(yy, 2.0, G2c);
    cv::multiply(xx, yy, G2b);
    cv::multiply(G2a, G, G2a);
    G2a = G2a / s4 - (G / s2);
    
    cv::multiply(G2b, G, G2b);
    G2b = G2b / s4;
    
    cv::multiply(G2c, G, G2c);
    G2c = G2c / s4 - (G / s2);

    
    // Filter input image with basis function kernel
    cv::Mat J;
    image.convertTo(J, CV_32F);
    cv::normalize(J, J, 0, 1, cv::NORM_MINMAX);
    cv::Mat If, k;
    cv::Size contSize = angle.size();
    for (int n = 0; n<=2; n++)
    {
        if (n==0) { cv::filter2D(J, If, CV_32F, G2a); };
        if (n==1) { cv::filter2D(J, If, CV_32F, G2b); };
        if (n==2) { cv::filter2D(J, If, CV_32F, G2c); };

        for (int i = 0; i < contSize.height; i++)
        {
            float* ptr    = output.ptr<float>(i);
            float* ptrI   = If.ptr<float>(i);
            const float* ang_i   = angle.ptr<float>(i);
            for (int j = 0; j < contSize.width; j++)
            {
                const float cij = cos(ang_i[j]);
                const float sij = sin(ang_i[j]);
                switch (n) {
                    case 0:
                        ptr[j] += cij * cij * ptrI[j];
                        break;
                    case 1:
                        ptr[j] += - 2.0 * cij * sij * ptrI[j];
                        break;
                    case 2:
                        ptr[j] += sij * sij * ptrI[j];
                        break;
                    default:
                        break;
                }
                if ( isnan(ptr[j]) ) ptr[j] = 0.0;
            }
        }
    }
};

void derivate_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, const cv::Mat sigma)
{
    cv::Mat J;
    image.convertTo(J, CV_64F);
    cv::normalize(J, J, 0, 1, cv::NORM_MINMAX);
    
    int ksize = 24;
    // Meshgrid (x-axis and y-axis coordinates)
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(-ksize,ksize), cv::Range(-ksize,ksize), xx, yy);
    xx.convertTo(xx, CV_64F);
    yy.convertTo(yy, CV_64F);
    // Gaussian kernel
    cv::Mat X, Y;
    // X=x^2
    cv::pow(xx, 2.0, X);
    // Y=y^2
    cv::pow(yy, 2.0, Y);

    
    cv::Mat isigma;
    sigma.convertTo(isigma, CV_64F);
    cv::divide(1.0, isigma, isigma);
    cv::Mat Z = X + Y;

    cv::Mat Ga, Gb;
    Ga = cv::Mat::zeros(J.rows, J.cols, CV_64F);
    Gb = cv::Mat::zeros(J.rows, J.cols, CV_64F);
    for (int k = 0; k<52; k++)
    {
        cv::Mat kernel_x, kernel_y, tZ;
        double beta_s = pow(-1.0, k+1) / tgamma(k);
        cv::pow(Z, k, tZ);

        cv::multiply(xx, tZ, kernel_x);
        cv::multiply(yy, tZ, kernel_y);
        kernel_x *= beta_s;
        kernel_y *= beta_s;

        cv::Mat Ja, Jb;
        cv::filter2D(J, Ja, CV_64F, kernel_x);
        cv::filter2D(J, Jb, CV_64F, kernel_y);
        
        cv::Mat tS;
        cv::pow(isigma, 2*k+2, tS);
        cv::multiply(Ja, tS, Ja);
        cv::multiply(Jb, tS, Jb);
        Ga += Ja;
        Gb += Jb;
    }
    
    // Filter input image with basis function kernel
    cv::Size contSize = angle.size();
    for (int i = 0; i < contSize.height; i++)
    {
        double* ptr      = output.ptr<double>(i);
        double* ptr_Ga   = Ga.ptr<double>(i);
        double* ptr_Gb   = Gb.ptr<double>(i);
        const float* ang_i   = angle.ptr<float>(i);
        for (int j = 0; j < contSize.width; j++)
        {
            const double cij = static_cast<double>(cos(ang_i[j]));
            const double sij = static_cast<double>(sin(ang_i[j]));
            ptr[j] += cij * ptr_Gb[j] + sij * ptr_Ga[j];
            if ( isnan(ptr[j]) ) ptr[j] = 0.0;
        }
    }
}

void approx_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, const cv::Mat sigma)
{
    cv::Mat J;
    image.convertTo(J, CV_64F);
    cv::normalize(J, J, 0, 1, cv::NORM_MINMAX);
    
    int ksize = 5;
    // Meshgrid (x-axis and y-axis coordinates)
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(-ksize,ksize), cv::Range(-ksize,ksize), xx, yy);
    xx.convertTo(xx, CV_64F);
    yy.convertTo(yy, CV_64F);
    //xx = 5.0 * xx / 24.0;
    //yy = 5.0 * yy / 24.0;
    // Gaussian kernel
    cv::Mat X, Y;
    // X=x^2
    cv::pow(xx, 2.0, X);
    // Y=y^2
    cv::pow(yy, 2.0, Y);

    
    cv::Mat isigma, isigma2;
    sigma.convertTo(isigma, CV_64F);
    cv::divide(1.0, isigma, isigma);
    cv::pow(isigma, 2.0, isigma2);
    cv::Mat Z = X + Y;

    cv::Mat G2a, G2b, G2c;
    G2a = cv::Mat::zeros(J.rows, J.cols, CV_64F);
    G2b = cv::Mat::zeros(J.rows, J.cols, CV_64F);
    G2c = cv::Mat::zeros(J.rows, J.cols, CV_64F);
    
    for (int k = 0; k<41; k++)
    {
        cv::Mat kernel_a, kernel_b, kernel_c, kernel_G, Zk;
        
        // (-1)^k / k!
        double coef = pow(-1.0, k+1) / tgamma(k);
        // Z^k
        cv::pow(Z, k, Zk);
        
        // 1 / sigma^2k
        cv::Mat Sk;
        cv::pow(isigma2, k, Sk);

        // Zk
        kernel_G = coef * Zk;
        // x^2 * Zk
        cv::multiply(X, Zk, kernel_a);
        // x * y * Zk
        cv::multiply(xx, yy, kernel_b);
        cv::multiply(kernel_b, Zk, kernel_b);
        // y^2 * Zk
        cv::multiply(Y, Zk, kernel_c);
        kernel_a *= coef;
        kernel_c *= coef;
        kernel_b *= coef;

        cv::Mat Ja, Jb, Jc, Jnormal;
        cv::filter2D(J, Jnormal, CV_64F, kernel_G);
        cv::filter2D(J, Ja, CV_64F, kernel_a);
        cv::filter2D(J, Jb, CV_64F, kernel_b);
        cv::filter2D(J, Jc, CV_64F, kernel_c);
        
        cv::multiply(Jnormal, Sk, Jnormal);
        cv::multiply(Ja, Sk, Ja);
        cv::multiply(Jb, Sk, Jb);
        cv::multiply(Jc, Sk, Jc);
        
        cv::multiply(Ja, isigma2, Ja);
        cv::multiply(Jb, isigma2, Jb);
        cv::multiply(Jc, isigma2, Jc);
        G2a += Ja - Jnormal;
        G2b += Jb;
        G2c += Jc - Jnormal;
    }
    cv::multiply(G2a, isigma2, G2a);
    cv::multiply(G2b, isigma2, G2b);
    cv::multiply(G2c, isigma2, G2c);
    // Filter input image with basis function kernel
    cv::Size contSize = angle.size();
    for (int i = 0; i < contSize.height; i++)
    {
        double* ptr      = output.ptr<double>(i);
        double* ptr_Ga   = G2a.ptr<double>(i);
        double* ptr_Gb   = G2b.ptr<double>(i);
        double* ptr_Gc   = G2c.ptr<double>(i);
        const float* ang_i   = angle.ptr<float>(i);
        for (int j = 0; j < contSize.width; j++)
        {
            const double cij = static_cast<double>(cos(ang_i[j]));
            const double sij = static_cast<double>(sin(ang_i[j]));
            ptr[j] += cij * cij * ptr_Ga[j];
            ptr[j] += - 2.0 * cij * sij * ptr_Gb[j];
            ptr[j] += sij * sij * ptr_Gc[j];
            if ( isnan(ptr[j]) ) ptr[j] = 0.0;
        }
    }
}

void directional_bilateral_filter(const cv::Mat InputArray, cv::Mat OutputArray, const cv::Mat angle, float spatial, float range, int size)
{
    int ksize = size;
    if ( ksize == -1 ) ksize = static_cast<int>(3 * spatial);
    
    // Meshgrid (x-axis and y-axis coordinates)
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(-ksize,ksize), cv::Range(-ksize,ksize), xx, yy);
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    
    // Gaussian kernel
    cv::Mat X, Y;
    cv::pow(xx, 2.0, X);
    cv::pow(yy, 2.0, Y);

    cv::Mat G;
    // exp(-(x^2+y^2)/(2sigma^2))
    float s2 = spatial * spatial;
    cv::exp(-(X+Y)/(2*s2), G);
    // Gaussian
    G = G / spatial*sqrt(2*CV_PI);
    //G.convertTo(G, CV_32F);
    
    // Basis function G2a, G2b and G2c
    /*
    float s4 = pow(spatial, 4.0);
    cv::Mat G2a, G2b, G2c;
    cv::pow(xx, 2.0, G2a);
    cv::pow(yy, 2.0, G2c);
    cv::multiply(xx, yy, G2b);
    cv::multiply(G2a, G, G2a);
    G2a = G2a / s4 - (G / s2);
    
    cv::multiply(G2b, G, G2b);
    G2b = G2b / s4;
    
    cv::multiply(G2c, G, G2c);
    G2c = G2c / s4 - (G / s2);
    */
    cv::Mat G2a, G2b, G2c;
    cv::pow(5.0*xx, 2.0, G2a);
    cv::multiply(xx, yy, G2b);
    cv::pow(5.0*yy, 2.0, G2c);
    
    cv::multiply(G2b, G, G2b);
    cv::multiply(G2a, G, G2a);
    cv::multiply(G2c, G, G2c);
    // Basis G1a, G1b;
    cv::Mat G1a, G1b;
    cv::multiply(-2*xx, G, G1a);
    cv::multiply(-2*yy, G, G1b);
    
    // initialize color-related bilateral filter coefficients
    //float gauss_color_coeff = -0.5f / (range*range);
    float gauss_color_coeff = 2 * pow(range, 2);
    std::vector<float> _color_weight(256);
    float* color_weight = &_color_weight[0];
    for( int i = 0; i < 256; i++ )
    {
        color_weight[i] = exp(-(pow(i, 2))/gauss_color_coeff) / (CV_PI * gauss_color_coeff);
    }
    /*
    for (int i = 0; i < 256; i++)
        std::cout << color_weight[i] << std::endl;
    */
    // Filter input image with basis function kernel
    
    cv::Mat temp;
    cv::normalize(G1a, G1a, 0, 1, cv::NORM_MINMAX);
    cv::normalize(G1b, G1b, 0, 1, cv::NORM_MINMAX);
    
    /*
    cv::normalize(G2a, G2a, 0, 1, cv::NORM_MINMAX);
    cv::normalize(G2b, G2b, 0, 1, cv::NORM_MINMAX);
    cv::normalize(G2c, G2c, 0, 1, cv::NORM_MINMAX);
    */
    //cv::Mat temp;
    copyMakeBorder( InputArray, temp, ksize, ksize, ksize, ksize, cv::BORDER_CONSTANT, 0 );

    std::vector<cv::Mat> channels(3);
    cv::split(InputArray, channels);
    
    std::vector<cv::Mat> temp_channels(3);
    cv::split(temp, temp_channels);
    
    std::vector<cv::Mat> out_channels(3);
    cv::split(OutputArray, out_channels);
    
    float sx = 3.0;
    float sy = 1.0;
    float psi = 0;
    float lambda = 10;
    float gamma = 0.5;
    float sigma_x = 2.0;
    float sigma_y = sigma_x / gamma;
    for (int channel = 0; channel < InputArray.channels(); channel++)
    {
        for (int i = 0; i < InputArray.rows; i++)
        {
            float* out_ptr = out_channels[channel].ptr<float>(i);
            const uint8_t* ptrI = channels[channel].ptr<uint8_t>(i);
            const float* angI = angle.ptr<float>(i);
            
            for (int j = 0; j < InputArray.cols; j++)
            {
                const float cij = cos(angI[j]);
                const float sij = sin(angI[j]);
                const float ka = cij * cij;
                const float kc = sij * sij;
                const float kb = - 2.0 * cij * sij;
                
                const int Ip = static_cast<int>(ptrI[j]);
                
                // Convolution
                float sum = 0.0;
                float wsum = 0.0;
                for ( int k = 0; k<G2a.rows; k++)
                {
                    const uint8_t* Irow = temp_channels[channel].ptr<uint8_t>(i+k);
                    
                    const float* G2a_ptr = G2a.ptr<float>(k);
                    const float* G2b_ptr = G2b.ptr<float>(k);
                    const float* G2c_ptr = G2c.ptr<float>(k);
                    
                    /*
                     const float* G1a_ptr = G1a.ptr<float>(k);
                     const float* G1b_ptr = G1b.ptr<float>(k);
                     const float* G_ptr = G.ptr<float>(k);
                     */
                    for (int l = 0; l<G2a.cols; l++)
                    {
                        const int Iq = static_cast<int>(Irow[j+l]);
                        //int ttt = static_cast<int>(Iq);
                        
                        // Directionnal 2nd Derivative of Gaussian
                        float wspatial = ka * G2a_ptr[l] + kb * G2b_ptr[l] + kc * G2c_ptr[l];
                        //float wspatial = G_ptr[l];
                        
                        // Gabor
                        /*
                        float x_theta = cij * (k-G2a.rows/2) + sij * (l-G2a.cols/2);
                        float y_theta = - sij * (k-G2a.rows/2) + cij * (l-G2a.cols/2);
                        float wspatial = exp(-.5 * ( pow(x_theta / sigma_x, 2.0) + pow(y_theta / sigma_y, 2.0))) * cos(2 * CV_PI / lambda * x_theta + psi);
                        */
                        
                        // Directionnal Gaussian
                        /*
                         float xtheta = cij * (k-G2a.rows/2) + sij * (l-G2a.cols/2);
                         float ytheta = sij * (k-G2a.rows/2) + cij * (l-G2a.cols/2);
                         float wspatial = exp(-0.5*(pow(xtheta/sx, 2.0) + pow(ytheta/sy, 2.0)));
                        */
                        
                        // Lorentz Function
                        /*
                         float xtheta = cij * (k-G2a.rows/2) + sij * (l-G2a.cols/2);
                         float ytheta = sij * (k-G2a.rows/2) + cij * (l-G2a.cols/2);
                         float wspatial = 1.0 / (1.0 + pow(xtheta/sx,2.0) + pow(ytheta/sy, 2.0));
                        */
                        
                        
                        //float wspatial = cij * G1a_ptr[l] + sij * G1b_ptr[l] + 1;
                        float wrange = color_weight[std::abs(Ip-Iq)];
                        
                        float w = wrange * wspatial;
                        sum += w * Iq;
                        wsum += w;
                    }
                }
                out_ptr[j] = sum/wsum;
            }
        }
    }
    cv::merge(out_channels, OutputArray);
};


typedef struct {
    int v[256];
    int n;
} histo_t;

int median(const int *x, int n)
{
    int i;
    for (n /= 2, i = 0; i < 256 && (n -= x[i]) > 0; i++);
    return i;
}

void emf2(const cv::Mat InputArray, cv::Mat OutputArray, int ksize)
{
    auto size = (2 * ksize + 1);
    auto z = size * size / 2;
    auto alpha = size * size;
    // Init histo
    histo_t histo;
    
    for (int i = 0; i<InputArray.rows; i++)
    {
        const auto* Irow = InputArray.ptr(i);
        auto* Jrow = OutputArray.ptr<uint8_t>(i);
        for (int j = 0; j<InputArray.cols; j++)
        {
            if ( !j )
            {
                //init_histo(InputArray, i, ksize, &histo);
                memset(&histo, 0, sizeof(histo_t));
                for (int m = 0; m < ksize && j+m < InputArray.cols; m++)
                {
                    for (int n = i - ksize; n <= i + ksize && n < InputArray.cols; n++)
                    {
                        if (n < 0) continue;
                        int pix = static_cast<int>(InputArray.at<uint8_t>(n, j+m));
                        histo.v[pix]++;
                        histo.n++;
                    }
                }
            }
            else
            {
                if ( j-ksize >= 0 )
                {
                    // Del pixels
                    for (int m = i - ksize; m <= i + ksize && m < InputArray.cols; m++)
                    {
                        if (m < 0) continue;
                        int pix = static_cast<int>(InputArray.at<uint8_t>(m, j-ksize));
                        histo.v[pix]--;
                        histo.n--;
                    }
                }
                if ( j+ksize < InputArray.cols )
                {
                    // Add pixels
                    for (int m = i - ksize; m <= i + ksize && m < InputArray.cols; m++)
                    {
                        if (m < 0) continue;
                        int pix = static_cast<int>(InputArray.at<uint8_t>(m, j+ksize));
                        histo.v[pix]++;
                        histo.n++;
                    }
                }
            }
            int n = median(histo.v, histo.n);
            if ( Irow[j] >= n + alpha || Irow[j] <= n - alpha )
            {
                Jrow[j] = static_cast<uint8_t>(n);
            }
        }
    }
}


std::vector<int> as_fraction(const double n, const int cycles, const double precision)
{
    int sign  = n > 0 ? 1 : -1;
    double number = n * sign; //abs(number);
    double new_number,whole_part;
    double decimal_part =  number - (int)number;
    int counter = 0;
    
    std::valarray<double> vec_1{double((int) number), 1}, vec_2{1,0}, temporary;
    
    while(decimal_part > precision & counter < cycles)
    {
        new_number = 1 / decimal_part;
        whole_part = (int) new_number;
        
        temporary = vec_1;
        vec_1 = whole_part * vec_1 + vec_2;
        vec_2 = temporary;
        
        decimal_part = new_number - whole_part;
        counter += 1;
    }
    return std::vector<int>{static_cast<int>(sign * vec_1[0]), static_cast<int>(vec_1[1])};
}

void morph_directionnal(const cv::Mat InputArray, cv::Mat OutputArray, const cv::Mat angle, const int length)
{
    
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(-length,length), cv::Range(-length,length), xx, yy);
    // Dilatation
    for (int i = length; i<InputArray.rows-length; i++)
    {
        for (int j = length; j<InputArray.cols-length; j++)
        {
            bool found = false;
            std::vector<int> ab = as_fraction(static_cast<double>(angle.at<float>(i,j))+3*CV_PI/2);
            //std::cout << ab[0] << "/" << ab[1] << std::endl;
            cv::Mat D = (0 < ab[0] * xx + ab[1] * yy) & (ab[0] * xx + ab[1] * yy <= cv::max(abs(ab[0]),abs(ab[1])));
            //cv::imshow("D", D);
            //cv::waitKey(1);
            for (int m = -length; m<=length; m++)
            {
                for (int n = -length; n<=length; n++)
                {
                    if ( InputArray.at<uint8_t>(i+m, j+n) && D.at<uint8_t>(m+length, n+length) )
                    {
                        OutputArray.at<uint8_t>(i,j) = 255;
                        found = true;
                        break;
                    }
                }
                if ( found ) break;
            }
        }
    }
    // Erosion
    cv::Mat tmp;
    OutputArray.copyTo(tmp);
    for (int i = 0; i<InputArray.rows; i++)
    {
        for (int j = 0; j<InputArray.cols; j++)
        {
            bool found = false;
            std::vector<int> ab = as_fraction(static_cast<double>(angle.at<float>(i,j)+3*CV_PI/2));
            cv::Mat D = (0 < ab[0] * xx + ab[1] * yy) & (ab[0] * xx + ab[1] * yy <= cv::max(abs(ab[0]),abs(ab[1])));
            for (int m = -length; m<=length; m++)
            {
                for (int n = -length; n<=length; n++)
                {
                    if ( tmp.at<uint8_t>(i+m, j+n) == 0 && D.at<uint8_t>(m+length, n+length) )
                    {
                        OutputArray.at<uint8_t>(i,j) = 0;
                        found = true;
                        break;
                    }
                }
                if ( found ) break;
            }
        }
    }
}
