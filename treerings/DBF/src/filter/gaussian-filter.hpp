//
//  gaussian-filter.hpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 22/04/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//

#ifndef gaussian_filter_hpp
#define gaussian_filter_hpp

#include <stdio.h>

#include <valarray>
#include <math.h>
#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include "../basic.hpp"

void gaussian_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, float sigma);
void derivate_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, const cv::Mat sigma);
void approx_filter(const cv::Mat image, cv::Mat output, const cv::Mat angle, const cv::Mat sigma);
void directional_bilateral_filter(const cv::Mat InputArray, cv::Mat OutputArray, const cv::Mat angle, float spatial, float range, int ksize = -1);

void emf2(const cv::Mat InputArray, cv::Mat OutputArray, int ksize);

std::vector<int> as_fraction(const double number, const int cycles = 1, const double precision = 5e-4);
void morph_directionnal(const cv::Mat InputArray, cv::Mat OutputArray, const cv::Mat angle, const int length);

#endif /* gaussian_filter_hpp */
