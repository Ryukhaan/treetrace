//
//  temp.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 22/06/2022.
//  Copyright © 2022 Remi DECELLE. All rights reserved.
//

/*
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <map>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/ximgproc.hpp"

const cv::String keys =
"{help h usage ?    |       | print this message   }"
"{@input            |       | input image          }"
"{px x              | 0     | X coordinate of the pith }"
"{py y              | 0     | Y coordinate of the pith }"
"{spatial s         | 4.0   | Std spatial }"
"{range r           | 15.0  | Std range domain }"
"{ksize k           | 10    | Window size (k x k)}";

using namespace std;
using namespace cv;
// Create a meshgrid
void repeatGrid(const cv::Mat &xgv, const cv::Mat &ygv, cv::Mat &X, cv::Mat &Y)
{
    repeat(xgv.reshape(1,1), ygv.total(), 1, X);
    repeat(ygv.reshape(1,1).t(), 1, xgv.total(), Y);
}

void meshgrid(const cv::Range &xgv, const cv::Range &ygv, cv::Mat &X, cv::Mat &Y)
{
    std::vector<int> t_x, t_y;
    for (int i = xgv.start; i <= xgv.end; i++) t_x.push_back(i);
    for (int i = ygv.start; i <= ygv.end; i++) t_y.push_back(i);
    repeatGrid(cv::Mat(t_x), cv::Mat(t_y), X, Y);
}

// Bilateral function
void directional_bilateral_filter(const cv::Mat InputArray,
                                  cv::Mat OutputArray,
                                  const cv::Mat angle,
                                  float spatial,
                                  float range,
                                  int size)
{
    int ksize = size;
    if ( ksize == -1 ) ksize = static_cast<int>(3 * spatial);
    
    // Meshgrid (x-axis and y-axis coordinates)
    cv::Mat xx, yy;
    meshgrid(cv::Range(-ksize,ksize), cv::Range(-ksize,ksize), xx, yy);
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    
    // Gaussian kernel
    cv::Mat X, Y;
    cv::pow(xx, 2.0, X);
    cv::pow(yy, 2.0, Y);

    cv::Mat G;
    // exp(-(x^2+y^2)/(2sigma^2))
    float s2 = spatial * spatial;
    cv::exp(-(X+Y)/(2*s2), G);
    // Gaussian
    G = G / spatial*sqrt(2*CV_PI);
    G.convertTo(G, CV_32F);
    
    // Basis function G2a, G2b and G2c
    float s4 = pow(spatial, 4.0);
    cv::Mat G2a, G2b, G2c;
    cv::pow(xx, 2.0, G2a);
    cv::pow(yy, 2.0, G2c);
    cv::multiply(xx, yy, G2b);
    cv::multiply(G2a, G, G2a);
    G2a = G2a / s4 - (G / s2);
    
    cv::multiply(G2b, G, G2b);
    G2b = G2b / s4;
    
    cv::multiply(G2c, G, G2c);
    G2c = G2c / s4 - (G / s2);

    
    // initialize color-related bilateral filter coefficients
    float gauss_color_coeff = -0.5/(range*range);
    std::vector<float> _color_weight(256);
    float* color_weight = &_color_weight[0];
    for( int i = 0; i < 256; i++ )
        color_weight[i] = std::exp(i*i*gauss_color_coeff);
    
    // Filter input image with basis function kernel
    cv::Mat temp;
    copyMakeBorder( InputArray, temp, ksize, ksize, ksize, ksize, cv::BORDER_CONSTANT, 0 );
    
    for (int i = 0; i < InputArray.rows; i++)
    {
        float* out_ptr = OutputArray.ptr<float>(i);
        const uint8_t* ptrI = InputArray.ptr<uint8_t>(i);
        const float* angI = angle.ptr<float>(i);

        for (int j = 0; j < InputArray.cols; j++)
        {
            const float cij = cos(angI[j]);
            const float sij = sin(angI[j]);
            const float c2 = cij * cij;
            const float s2 = sij * sij;
            const float cs = - 2.0 * cij * sij;
            const uint8_t Ip = ptrI[j];
            
            // Convolution
            float sum = 0.0;
            float wsum = 0.0;
            for ( int k = 0; k<G2a.rows; k++)
            {
                const uint8_t* Irow = temp.ptr<uint8_t>(i+k);
                const float* G2a_ptr = G2a.ptr<float>(k);
                const float* G2b_ptr = G2b.ptr<float>(k);
                const float* G2c_ptr = G2c.ptr<float>(k);
                
                const float* G_ptr = G.ptr<float>(k);
                for (int l = 0; l<G2a.cols; l++)
                {
                    const uint8_t Iq = Irow[j+l];
                    int ttt = static_cast<int>(Iq);
                    float wspatial = c2 * G2a_ptr[l] + cs * G2b_ptr[l] + s2 * G2c_ptr[l];
                    //float wspatial = G_ptr[l];
                    float range = color_weight[std::abs(Ip-Iq)];
                    float w = range * wspatial;
                    sum += w * Iq;
                    wsum += w;
                }
            }
            out_ptr[j] = sum/wsum;
        }
    }
};


// Get pith position
std::map<string, Point> read_csv(const string filename)
{
    std::ifstream file(filename.c_str());
    string line;
    string cell;
    std::map<string, Point> piths;
    int index = 0;
    int x_index, y_index;
    int label_index;
    if ( file.is_open() )
    {
        while ( getline(file,line) )
        {
            stringstream lineStream(line);
            int j = 0;
            Point pith;
            string name;
            while ( getline(lineStream, cell, ';' ) )
            {
                if ( j == 0 ) name = cell;
                if ( j == 1 ) pith.x = stoi(cell);
                if ( j == 2 ) pith.y = stoi(cell);
                j++;
            }
            piths[name] = pith;
            index++;
        }
    }
    return piths;
}

// Proceed pre-treatment
void proceed_image(const Mat image,
                   const Point pith,
                   const string filename,
                   const float sigma_s,
                   const float sigma_r,
                   const int ksize)
{
    vector<Mat> channels(3);
    split(image, channels);

    // Convert into graylevel keep only Blue component
    Mat graylevel;
    graylevel = channels[2];
    cv::normalize(graylevel, graylevel, 0, 255, cv::NORM_MINMAX, CV_8U);
    imwrite("/Users/remidecelle/Documents/output/rings/graylevel/" + filename + ".jpeg", graylevel);
    
    // Get border
    Mat superior = (channels[0]>0 & channels[1]>0 & channels[2]>0);
    superior.convertTo(superior, CV_8U);
    // Some morphology to fill holes
    morphologyEx(superior, superior, MORPH_OPEN, Mat::ones(5, 5, CV_8U));
    morphologyEx(superior, superior, MORPH_CLOSE, Mat::ones(7, 7, CV_8U));
    
    // Compute theorical orientation
    int height = graylevel.rows;
    int width = graylevel.cols;
    Mat angle = Mat(height, width, CV_32F);
    Mat xx, yy;
    meshgrid(cv::Range(0,width-1), cv::Range(0,height-1), xx, yy);
    xx = xx - pith.x;
    yy = yy - pith.y;
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    for (int i = 0; i < height; i++) {
        const float *y_i = yy.ptr<float>(i);
        const float *x_i = xx.ptr<float>(i);
        float* ptrAngle  = angle.ptr<float>(i);
        for (int j = 0; j < width; j++) {
            ptrAngle[j] = atan2(-y_i[j], x_i[j]);
        }
    }
    
    Mat filterImage = Mat::zeros(graylevel.rows, graylevel.cols, CV_32F);
    directional_bilateral_filter(graylevel, filterImage, angle, sigma_s, sigma_r, ksize);
    Mat normalizeImage;
    normalize(filterImage, normalizeImage, 0, 255, NORM_MINMAX, CV_8U);
    // CHANGE THE PATH
    imwrite("/Users/remidecelle/Documents/output/rings/filtered/" + filename + ".png", filterImage);
}

int main(int argc, const char* argv[])
{
    cv::CommandLineParser parser(argc, argv, keys);
    parser.about("Directionnal Bilateral filtering");
    if (parser.has("help"))
    {
        parser.printMessage();
        return 0;
    }
    cv::Mat image;
    string filename = parser.get<cv::String>(0); // Image input
    image = imread(filename, IMREAD_COLOR);
    
    // CHANGE THE PATH
    std::map<string, Point> piths = read_csv("/Users/remidecelle/Documents/segmentation_fred_finale/pith_sawmill_2.csv");
    std::string base_filename = filename.substr(filename.find_last_of("/") + 1);
    std::string::size_type const p(base_filename.find_last_of('.'));
    std::string file_without_extension = base_filename.substr(0, p);
    Point pith;
    //pith = piths[file_without_extension+".jpeg"];
    pith = piths[file_without_extension];
    TickMeter tm;
    tm.start();
    proceed_image(image,
                  pith,
                  file_without_extension,
                  parser.get<float>("spatial"),
                  parser.get<float>("range"),
                  parser.get<int>("ksize"));
    tm.stop();
    return 0;
}
*/
