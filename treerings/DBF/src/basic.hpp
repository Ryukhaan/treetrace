//
//  basic.hpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 25/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef basic_hpp
#define basic_hpp

#include <stdio.h>
#include <iostream>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

namespace basic
{
    typedef enum ANGULAR
    {
        RIGHT           = 0,
        TOP_RIGHT       = 1,
        TOP             = 2,
        TOP_LEFT        = 3,
        LEFT            = 4,
        BOTTOM_LEFT     = 5,
        BOTTOM          = 6,
        BOTTOM_RIGHT    = 7
    } ANGULAR;
    
    void repeatGrid(const cv::Mat &xgv, const cv::Mat &ygv, cv::Mat &X, cv::Mat &Y);
    
    // helper function (maybe that goes somehow easier)
    void meshgrid(const cv::Range &xgv, const cv::Range &ygv, cv::Mat &X, cv::Mat &Y);
    
    void imagesc(cv::Mat InputArray,
                 std::string windowName = "Output",
                 int wait = 0,
                 int map = -1);
    
    std::string type2str(int type);
}

#endif /* basic_hpp */
