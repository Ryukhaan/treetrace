//
//  ray-tracing.hpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 22/04/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//

#ifndef ray_tracing_hpp
#define ray_tracing_hpp

#include <stdio.h>
#include <vector>
#include <limits>
#include <algorithm>
#include <iostream>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include "basic.hpp"

using namespace std;

typedef struct PointDeCerne
{
    cv::Point pos;
    int num_ray;
    float radius;
} PdC;

typedef struct Courbe
{
    std::vector<PdC> points;
    float radius;
} Courbe;


std::vector<Courbe> ray_tracing(const cv::Mat InputArray,
                                const cv::Mat angle,
                                const int px,
                                const int py,
                                vector<int> *num_rings,
                                vector<vector<float>> *widths);

cv::Mat suppress_rings(const cv::Mat EdgesArray,
                       const int px,
                       const int py);

cv::Mat suppress_rings(const cv::Mat EdgesArray,
                       const cv::Mat GrayArray,
                       const int px,
                       const int py);

cv::Mat cart2pol(const cv::Mat EdgesArray, const cv::Mat GrayArray, const int px, const int py);


#endif /* ray_tracing_hpp */
