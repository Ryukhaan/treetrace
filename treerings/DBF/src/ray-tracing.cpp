//
//  ray-tracing.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 22/04/2021.
//  Copyright © 2021 Remi DECELLE. All rights reserved.
//

#include "ray-tracing.hpp"

using namespace std;
/*
typedef struct Courbe
{
    vector<cv::Point> points;
    float radius;
} Courbe;
*/
vector<Courbe> ray_tracing(const cv::Mat InputArray,
                           const cv::Mat angle,
                           const int px,
                           const int py,
                           vector<int> *num_rings,
                           vector<vector<float>> *widths)
{
    int min_peaks   = 0;
    int delta       = 3;
    float gamma     = 1;
    float T_low     = 2;
    float thresh_d  = 100;
    
    int height = InputArray.rows;
    int width  = InputArray.cols;
    
    vector<Courbe> Ck;
    //int num_curves = 0;
    cv::Point start_point(px,py);
    
    // Radii
    
    cv::Mat xx, yy;
    basic::meshgrid(cv::Range(0,width-1), cv::Range(0,height-1), xx, yy);
    xx.convertTo(xx, CV_32F);
    yy.convertTo(yy, CV_32F);
    xx = xx - px;
    yy = yy - py;
    cv::Mat radii;
    cv::pow(xx, 2.0, xx);
    cv::pow(yy, 2.0, yy);
    cv::sqrt(xx+yy, radii);
    
    
    int lend = 0;
    int i = 0;
    int j = 0;
    int num_ray =-1;
    
    for (int n = 0; n<4; n++)
    {
        if (n%2==0) { lend = width; };
        if (n%2==1) { lend = height; };
        for (int m = 0; m<lend; m+=delta)
        {
            if (n==0) { j = 0; i=m; };
            if (n==1) { i = width-1; j=m; };
            if (n==2) { j = height-1; i=width-m-1; };
            if (n==3) { i = 0; j=height-m-1; };
            
            num_ray++;
            
            // Improfile equivalent + findpeaks
            cv::Point end_point(i,j);
            cv::LineIterator it(InputArray, start_point, end_point);
            float prev_value = numeric_limits<float>::max();
            float curr_value = 0.0;
            float next_value = numeric_limits<float>::max();
            cv::Point curr_point(0,0);
            cv::Point next_point(0,0);
            std::vector<cv::Point> peaks;
            int delay = min_peaks;
            
            float prev_min_value = numeric_limits<float>::min();
            vector<float> current_width;
            for (int k = 0; k<it.count-1; k++, ++it)
            {
                delay++;
                prev_value = curr_value;
                curr_value = next_value;
                curr_point = next_point;
                next_value = InputArray.at<float>(it.pos());

                next_point = it.pos();
                if ( curr_value <= T_low )
                    continue;
                if ( curr_value > prev_value && curr_value > next_value && delay >= min_peaks)
                {
                    if ( (curr_value - prev_min_value) > gamma)
                    {
                        delay = 0;
                        peaks.push_back(curr_point);
                    }
                    
                }
                if ( curr_value < prev_value && curr_value < next_value)
                {
                    prev_min_value = curr_value;
                }
            }
            
            num_rings->push_back(peaks.size());
            // Iterate through each peaks
            // and find the nearest curve (polar coordinate)
            for (cv::Point p : peaks)
            {
                //float radius    = sqrt(pow(px-p.x, 2.0)+pow(py-p.y, 2.0));
                float radius    = radii.at<float>(p);
                float theta     = angle.at<float>(p);
                
                PdC _p;
                _p.pos = p;
                _p.radius = radius;
                _p.num_ray = num_ray;
                
                // Append distance to vector
                current_width.push_back(radius);
                // If no curve have been created
                // then create the first one
                if (Ck.size() == 0)
                {
                    Courbe C;
                    C.points.push_back(_p);
                    C.radius = radius;
                    Ck.push_back(C);
                }
                else
                {
                    // Find the nearest curve
                    float shortest_distance = numeric_limits<float>::max();
                    int nearest_k = 0;
                    for (int k=0; k < Ck.size(); k++)
                    {
                        cv::Point q = Ck[k].points.back().pos;
                        float r = radii.at<float>(q);
                        //float r = sqrt(pow(px-q.x, 2.0)+pow(py-q.y, 2.0));
                        float t = angle.at<float>(q);
                        float delta_r = abs(r-radius);
                        float delta_T = min<float>( abs(theta-t), 2.0*CV_PI-abs(theta-t) );
                        float distance = pow(delta_r, 2.0) + radius*pow(delta_T, 2.0);
                        if ( distance < shortest_distance )
                        {
                            shortest_distance = distance;
                            nearest_k = k;
                        }
                    }
                    if (shortest_distance < thresh_d)
                    {
                        //n = Ck[nearest_k].points.size();
                        Ck[nearest_k].points.push_back(_p);
                        //Ck[nearest_k].radius =  Ck{imin, 2}*n/(n+1) + d/(n+1);
                    }
                    else
                    {
                        Courbe C;
                        C.points.push_back(_p);
                        C.radius = radius;
                        Ck.push_back(C);
                    }
                }
            }
            widths->push_back(current_width);
        }
    }
    
    /*
    cv::Mat ColoredCernes = cv::Mat::zeros(height, width, CV_8U);
    int k = 1;
    for (Courbe C : Ck)
    {
        for (cv::Point p : C.points)
        {
            ColoredCernes.at<uint8_t>(p) = k;
        }
        k++;
    }
    basic::imagesc(ColoredCernes, "Cernes", 0, cv::COLORMAP_RAINBOW);
    */
    return Ck;
}

cv::Mat suppress_rings(const cv::Mat EdgesArray,
                       const cv::Mat GrayArray,
                       const int px,
                       const int py)
{
    int height = EdgesArray.rows;
    int width  = EdgesArray.cols;
    cv::Mat OutputArray = cv::Mat(height, width, CV_8U);
    
    //int num_curves = 0;
    cv::Point start_point(px,py);
    
    int lend = 0;
    int i = 0;
    int j = 0;
    for (int n = 0; n<4; n++)
    {
        if (n%2==0) { lend = width; };
        if (n%2==1) { lend = height; };
        for (int m = 0; m < lend; m++)
        {
            if (n==0) { j = 0; i=m; };
            if (n==1) { i = width-1; j=m; };
            if (n==2) { j = height-1; i=width-m-1; };
            if (n==3) { i = 0; j=height-m-1; };
            
            // Improfile equivalent + findpeaks
            cv::Point end_point(i,j);
            cv::LineIterator it(EdgesArray, start_point, end_point);
            uint8_t curr_edge_value = EdgesArray.at<uint8_t>(py,px);
            uint8_t next_edge_value = EdgesArray.at<uint8_t>(py,px);
            uint8_t curr_gray_value = GrayArray.at<uint8_t>(py,px);
            uint8_t next_gray_value = GrayArray.at<uint8_t>(py,px);
            for (int k = 0; k<it.count-1; k++, ++it)
            {
                curr_gray_value = next_gray_value;
                curr_edge_value = next_edge_value;
                next_edge_value = EdgesArray.at<uint8_t>(it.pos());
                next_gray_value = GrayArray.at<uint8_t>(it.pos());
                //if ( curr_edge_value > next_edge_value && curr_gray_value > next_edge_value )
                if (curr_gray_value > next_edge_value)
                {
                    OutputArray.at<uint8_t>(it.pos()) = 255;
                }
                else
                    OutputArray.at<uint8_t>(it.pos()) = 0;
            }
        }
    }
    return OutputArray;
};

cv::Mat suppress_rings(const cv::Mat EdgesArray,
                       const int px,
                       const int py)
{
    int height = EdgesArray.rows;
    int width  = EdgesArray.cols;
    cv::Mat OutputArray = cv::Mat(height, width, CV_8U);
    
    //int num_curves = 0;
    cv::Point start_point(px,py);
    
    int lend = 0;
    int i = 0;
    int j = 0;
    for (int n = 0; n<4; n++)
    {
        if (n%2==0) { lend = width; };
        if (n%2==1) { lend = height; };
        for (int m = 0; m < lend; m++)
        {
            if (n==0) { j = 0; i=m; };
            if (n==1) { i = width-1; j=m; };
            if (n==2) { j = height-1; i=width-m-1; };
            if (n==3) { i = 0; j=height-m-1; };
            
            // Improfile equivalent + findpeaks
            cv::Point end_point(i,j);
            cv::LineIterator it(EdgesArray, start_point, end_point);
            uint8_t curr_edge_value = EdgesArray.at<uint8_t>(py,px);
            uint8_t next_edge_value = EdgesArray.at<uint8_t>(py,px);
            //uint8_t curr_gray_value = GrayArray.at<uint8_t>(py,px);
            //uint8_t next_gray_value = GrayArray.at<uint8_t>(py,px);
            for (int k = 0; k<it.count-1; k++, ++it)
            {
                //curr_gray_value = next_gray_value;
                curr_edge_value = next_edge_value;
                next_edge_value = EdgesArray.at<uint8_t>(it.pos());
                //next_gray_value = GrayArray.at<uint8_t>(it.pos());
                if ( curr_edge_value > next_edge_value )
                {
                    OutputArray.at<uint8_t>(it.pos()) = 255;
                }
                else
                    OutputArray.at<uint8_t>(it.pos()) = 0;
            }
        }
    }
    return OutputArray;
};


cv::Mat cart2pol(const cv::Mat InputArray, const int px, const int py)
{
    int height = InputArray.rows;
    int width  = InputArray.cols;
    int W = max(max(height, width)-py, max(height, width)-px);
    cv::Mat OutputArray = cv::Mat(W, 2*(width+height), InputArray.type());

    cv::Point start_point(px,py);
    
    int lend = 0;
    int i = 0;
    int j = 0;
    int w = 0;
    for (int n = 0; n<4; n++)
    {
        if (n%2==0) { lend = width; };
        if (n%2==1) { lend = height; };
        for (int m = 0; m < lend; m++)
        {
            if (n==0) { j = 0; i=m; };
            if (n==1) { i = width-1; j=m; };
            if (n==2) { j = height-1; i=width-m-1; };
            if (n==3) { i = 0; j=height-m-1; };
            
            // Improfile equivalent + findpeaks
            //cv::Mat temp = cv::Mat(InputArray.rows, InputArray.cols, InputArray.type());
            //InputArray.copyTo(temp);
            cv::Point end_point(i,j);
            cv::LineIterator it(InputArray, start_point, end_point);
            for (int k = 0; k<it.count; k++, ++it)
            {
                //temp.at<float>(it.pos()) = 0;
                OutputArray.at<float>(k,w) = InputArray.at<float>(it.pos());
            }
            /*
            cv::imshow("T", temp);
            cv::waitKeyEx(1);
            */
            ++w;
        }
    }
    return OutputArray;
}
