//
//  ridgeorient.cpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 25/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#include "ridgeorient.hpp"

int ddepth = CV_64F;

void gradient(cv::Mat image, cv::Mat xGradient, cv::Mat yGradient) {
    xGradient = cv::Mat::zeros(image.rows, image.cols, ddepth);
    yGradient = cv::Mat::zeros(image.rows, image.cols, ddepth);
    
    // Pointer access more effective than Mat.at<T>()
    for (int i = 1; i < image.rows - 1; i++) {
        const double *image_i = image.ptr<double>(i);
        double *xGradient_i = xGradient.ptr<double>(i);
        double *yGradient_i = yGradient.ptr<double>(i);
        for (int j = 1; j < image.cols - 1; j++) {
            double xPixel1 = image_i[j - 1];
            double xPixel2 = image_i[j + 1];
            
            double yPixel1 = image.at<double>(i - 1, j);
            double yPixel2 = image.at<double>(i + 1, j);
            
            double xGrad;
            double yGrad;
            
            if (j == 0) {
                xPixel1 = image_i[j];
                xGrad = xPixel2 - xPixel1;
            } else if (j == image.cols - 1) {
                xPixel2 = image_i[j];
                xGrad = xPixel2 - xPixel1;
            } else {
                xGrad = 0.5 * (xPixel2 - xPixel1);
            }
            
            if (i == 0) {
                yPixel1 = image_i[j];
                yGrad = yPixel2 - yPixel1;
            } else if (i == image.rows - 1) {
                yPixel2 = image_i[j];
                yGrad = yPixel2 - yPixel1;
            } else {
                yGrad = 0.5 * (yPixel2 - yPixel1);
            }
            
            xGradient_i[j] = xGrad;
            yGradient_i[j] = yGrad;
        }
    }
}

cv::Mat estimate_orientation(const cv::Mat im,
                             const double gradientsigma,
                             const double blocksigma,
                             const double orientsmoothsigma) {
    cv::Mat grad_x, grad_y;
    int sze = 6 * round(gradientsigma) + 1;
    
    // Define Gaussian kernel
    cv::Mat gaussKernelX = cv::getGaussianKernel(sze, gradientsigma, ddepth);
    cv::Mat gaussKernelY = cv::getGaussianKernel(sze, gradientsigma, ddepth);
    cv::Mat gaussKernel = gaussKernelX * gaussKernelY.t();
    
    // Peform Gaussian filtering
    cv::Mat fx, fy;
    cv::Mat kernelx = (cv::Mat_<double>(1, 3) << -0.5, 0, 0.5);
    cv::Mat kernely = (cv::Mat_<double>(3, 1) << -0.5, 0, 0.5);
    cv::filter2D(gaussKernel, fx, -1, kernelx);
    cv::filter2D(gaussKernel, fy, -1, kernely);
    
    gradient(gaussKernel, fx, fy); // Gradient of Gaussian

    grad_x.convertTo(grad_x, ddepth);
    grad_y.convertTo(grad_y, ddepth);
    
    cv::filter2D(im, grad_x, -1, fx, cv::Point(-1, -1), 0,
                 cv::BORDER_DEFAULT); // Gradient of the image in x
    cv::filter2D(im, grad_y, -1, fy, cv::Point(-1, -1), 0,
                 cv::BORDER_DEFAULT); // Gradient of the image in y
    
    cv::Mat grad_xx, grad_xy, grad_yy;
    cv::multiply(grad_x, grad_x, grad_xx);
    cv::multiply(grad_x, grad_y, grad_xy);
    cv::multiply(grad_y, grad_y, grad_yy);
    
    // Now smooth the covariance data to perform a weighted summation of the data
    int sze2 = 6 * round(blocksigma) + 1;
    
    cv::Mat gaussKernelX2 = cv::getGaussianKernel(sze2, blocksigma, ddepth);
    cv::Mat gaussKernelY2 = cv::getGaussianKernel(sze2, blocksigma, ddepth);
    cv::Mat gaussKernel2 = gaussKernelX2 * gaussKernelY2.t();
    
    cv::filter2D(grad_xx, grad_xx, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    cv::filter2D(grad_xy, grad_xy, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    cv::filter2D(grad_yy, grad_yy, -1, gaussKernel2, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    
    grad_xy *= 2;
    
    // Analytic solution of principal direction
    cv::Mat denom, sin2theta, cos2theta;
    
    cv::Mat G1, G2, G3;
    cv::multiply(grad_xy, grad_xy, G1);
    G2 = grad_xx - grad_yy;
    cv::multiply(G2, G2, G2);
    
    G3 = G1 + G2;
    cv::sqrt(G3, denom);
    
    cv::divide(grad_xy, denom, sin2theta);
    cv::divide(grad_xx - grad_yy, denom, cos2theta);
    
    int sze3 = 6 * round(orientsmoothsigma);
    
    if (sze3 % 2 == 0) {
        sze3 += 1;
    }
    
    cv::Mat gaussKernelX3 = cv::getGaussianKernel(sze3, orientsmoothsigma, ddepth);
    cv::Mat gaussKernelY3 = cv::getGaussianKernel(sze3, orientsmoothsigma, ddepth);
    cv::Mat gaussKernel3 = gaussKernelX3 * gaussKernelY3.t();
    
    cv::filter2D(cos2theta, cos2theta, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    cv::filter2D(sin2theta, sin2theta, -1, gaussKernel3, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    
    sin2theta.convertTo(sin2theta, ddepth);
    cos2theta.convertTo(cos2theta, ddepth);
    cv::Mat orientim = cv::Mat::zeros(sin2theta.rows, sin2theta.cols, ddepth);

    // Pointer access more effective than Mat.at<T>()
    
    for (int i = 0; i < sin2theta.rows; i++) {
        const double *sin2theta_i = sin2theta.ptr<double>(i);
        const double *cos2theta_i = cos2theta.ptr<double>(i);
        double *orientim_i = orientim.ptr<double>(i);
        for (int j = 0; j < sin2theta.cols; j++) {
            orientim_i[j] = (std::atan2(sin2theta_i[j], cos2theta_i[j])) / 2; // + pi ?
        }
    }
    
    cv::Mat T1 = (grad_xx-grad_yy);
    cv::multiply(T1, cos2theta*0.5, T1);
    cv::Mat T2;
    cv::multiply(grad_xy, sin2theta*0.5, T2);
    cv::Mat Imin = (grad_yy+grad_xx)*0.5 - T1 - T2;
    cv::Mat Imax = grad_yy+grad_xx - Imin;
    
    cv::Mat coherence;
    cv::divide(Imax-Imin, Imax+Imin, coherence);
    cv::pow(coherence, 2.0, coherence);
    
    return coherence;
}
