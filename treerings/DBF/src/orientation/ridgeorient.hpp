//
//  ridgeorient.hpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 25/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef ridgeorient_hpp
#define ridgeorient_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>

void gradient(cv::Mat image, cv::Mat xGradient, cv::Mat yGradient);
cv::Mat estimate_orientation(const cv::Mat im,
                             const double gradientsigma,
                             const double blocksigma,
                             const double orientsmoothsigma);

#endif /* ridgeorient_hpp */
