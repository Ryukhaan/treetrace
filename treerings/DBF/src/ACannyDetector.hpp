//
//  ACannyDetector.hpp
//  ModifiedCanny
//
//  Created by Remi DECELLE on 24/06/2020.
//  Copyright © 2020 Remi DECELLE. All rights reserved.
//

#ifndef ACannyDetector_hpp
#define ACannyDetector_hpp

#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include "basic.hpp"

using namespace std;

class ACannyDetector {
    
public:
    /**
     Create an abstract Canny Detector
     */
    ACannyDetector ();

    /**
     Create an abstract Canny Detector
     
     @param low Lowest threshold value
     @param high Highest threshold value
     */
    ACannyDetector (const double &low, const double &high);
    
    /**
    Delete the Canny Detector
     */
    virtual ~ACannyDetector ();
    
    /**
     Apply Canny Detector to an image

     @param InputArray  Input image
     @return            Detected edges
     */
    cv::Mat applyTo(cv::Mat InputArray);
    
    /**
     * Smooth input image. A circular filtering accord to pith position.
     
     @param InputArray      Input image
     @param OutputArray     Filtered image
     */
    virtual void smooth(cv::Mat InputArray, cv::Mat OutputArray);
    
    /**
     Compute gradient of image and orientation which is quantized in 8 directions according to the pith position.
     
     @param InputArray          Input image
     @param MagnitudeArray      Magnitude image (Mag = sqrt(Gx^2 + Gy^2) ).
     @param OrientationArray    Orientation image
     */
    virtual void gradient(cv::Mat InputArray, cv::Mat MagnitudeArray, cv::Mat OrientationArray);
    
    /**
     Compute non maxima suppression. Look at neighborhood according to orientation and check if current pixel has the highest magnitude (still according to pixel's neighborhood).

     @param InputArray      Input image
     @param OutputArray     Image with non maxima pixel suppressed
     @param magnitude       Magnitude of gradient
     @param orientation     Orientation of image
     */
    virtual void nonMaximaSuppression(cv::Mat InputArray, cv::Mat OutputArray, cv::Mat magnitude, cv::Mat orientation);
    
    /**
     Apply double thresholding to Non-Maxima Suppression image.

     @param InputArray      Non-Maxima image
     @param OutputArray     Detected edges
     */
    virtual void doubleThreshold(cv::Mat InputArray, cv::Mat OutputArray);
    
    const double getLowThreshold() const
    {
        return t_low;
    };
    
    const double getHighThreshold() const
    {
        return t_high;
    };
    
    const cv::Point getPith() const
    {
        return pith;
    };
    
    const int getWindowSize() const
    {
        return window_size;
    };
    
    void setLowThreshold(double low)
    {
        t_low = low;
    };
    
    void setHighThreshold(double high)
    {
        t_high = high;
    };
    
    void setPith(cv::Point new_pith)
    {
        pith = new_pith;
    };

    void setWindowSize(int ww)
    {
        window_size = ww;
    };
    
    
protected:
    /** Lowest threshold for the double threshold method. */
    double t_low;
    /** Highest threshold for the double threshold method. */
    double t_high;

private:
    /** Pith position. */
    cv::Point pith;
    /** Window size for segment discrete. */
    int window_size;
};

#endif /* ACannyDetector_hpp */
