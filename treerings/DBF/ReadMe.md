# Directionnal Bilateral Filtering to Enhance Tree Rings

Version 1.0
Last update: 12/07/2022
Authors: 
-	Rémi Decelle, remi.decelle@loria.fr
-	Phuc Ngo, hoai-diem-phuc.ngo@loria.fr
-	Isabelle Debled-Rennesson, isabelle.debled-rennesson@loria.fr
-	Frédéric Mothe, frederic.mothe@inrae.fr
-	Fleur Longuetaud, fleur.longuetaud@inrae.fr

## Get started

#### 1. Folders
All files are in src/ folder.

pith_estimation.csv contains the pith position to the dataset if no pith position are passed.


### 2.    Dependencies

Required: 

-  `Opencv 4.+`
-  `Boost`

#### 2.    Compilation Instructions

The following steps allow to compile the code:
``` 
mkdir build/
cd build/
cmake ..
make
```

#### 3.    Examples of usage

Here some examples of usage:
```
./DBF ../../pith/samples/harvest.jpeg
./DBF --input ../../pith/samples/logyard.jpg --px=1065 --py=1270
```

To have information about arguments:
```
./DBF --help
```
or
```
./DBF -h
```
